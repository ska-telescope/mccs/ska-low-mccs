# -*- coding: utf-8 -*-
"""This module provides a flexible test harness for testing Tango devices."""

from __future__ import annotations

import os
import unittest.mock
from types import TracebackType
from typing import Any

import numpy as np
import tango
from ska_control_model import LoggingLevel
from ska_tango_testing.harness import TangoTestHarness, TangoTestHarnessContext
from tango.server import Device


def get_subarray_trl(subarray_id: int) -> str:
    """
    Construct the subarray Tango device TRL from its ID number.

    :param subarray_id: ID number of the subarray.

    :returns: subarray device TRL
    """
    return f"low-mccs/subarray/{subarray_id:02}"


def get_subarray_beam_trl(subarray_beam_id: int) -> str:
    """
    Construct the subarray beam Tango device TRL from its ID number.

    :param subarray_beam_id: ID number of the beam.

    :returns: subarray beam device TRL
    """
    return f"low-mccs/subarraybeam/{subarray_beam_id:02}"


def get_pasdbus_trl(station_name: str) -> str:
    """
    Construct the pasdbus Tango device TRL from its station name.

    :param station_name: name of the station

    :returns: pasdbus device TRL
    """
    return f"low-mccs/pasdbus/{station_name}"


def get_fndh_trl(station_name: str) -> str:
    """
    Construct the FNDH Tango device TRL from its station name.

    :param station_name: name of the station

    :returns: FNDH device TRL
    """
    return f"low-mccs/fndh/{station_name}"


def get_smartbox_trl(station_name: str, smartbox_id: int) -> str:
    """
    Construct a smartbox Tango device TRL from its station name.

    :param station_name: name of the station
    :param smartbox_id: the id of the smartbox.

    :returns: station device TRL
    """
    return f"low-mccs/smartbox/{station_name}-sb{smartbox_id:02}"


def get_calibration_store_trl(station_name: str) -> str:
    """
    Construct a calibration store Tango device TRL from its station name.

    :param station_name: name of the station

    :returns: calibration store device TRL
    """
    return f"low-mccs/calibrationstore/{station_name}"


def get_station_trl(station_name: str) -> str:
    """
    Construct a station Tango device TRL from its station name.

    :param station_name: name of the station

    :returns: station device TRL
    """
    return f"low-mccs/station/{station_name}"


def get_station_beam_trl(station_name: str, station_beam_id: int) -> str:
    """
    Construct a station beam Tango device TRL from its station name and beam ID.

    :param station_name: name of the station
    :param station_beam_id: ID of the station beam

    :returns: station beam device TRL
    """
    return f"low-mccs/beam/{station_name}-{station_beam_id:02}"


def get_station_calibrator_trl(station_name: str) -> str:
    """
    Construct a station calibrator Tango device TRL from its station name.

    :param station_name: name of the station

    :returns: station calibrator device TRL
    """
    return f"low-mccs/stationcalibrator/{station_name}"


def get_field_station_trl(station_name: str) -> str:
    """
    Construct a field station Tango device TRL from its station name.

    :param station_name: name of the station

    :returns: field station device TRL
    """
    return f"low-mccs/fieldstation/{station_name}"


def get_solver_trl(station_name: str) -> str:
    """
    Construct a Solver Tango device TRL from station name.

    :param station_name: name of the station

    :returns: Calibration solver device TRL.
    """
    return f"low-mccs/solver/solver-{station_name}"


def get_sps_station_trl(station_name: str) -> str:
    """
    Construct an SPS station Tango device TRL from its station name.

    :param station_name: name of the station

    :returns: SPS station device TRL
    """
    return f"low-mccs/spsstation/{station_name}"


def get_tile_trl(station_name: str, tile_id: int) -> str:
    """
    Construct a tile Tango device TRL from its station name and tile id.

    :param station_name: name of the station
    :param tile_id: ID of the tile within the station

    :returns: tile device TRL
    """
    return f"low-mccs/tile/{station_name}-tpm{tile_id:02}"


def get_subrack_trl(station_name: str, subrack_id: int) -> str:
    """
    Construct a subrack Tango device TRL from its station name and subrack id.

    :param station_name: name of the station
    :param subrack_id: ID of the subrack within the station

    :returns: subrack device TRL
    """
    return f"low-mccs/subrack/{station_name}-sr{subrack_id:01}"


def get_controller_trl() -> str:
    """
    Return the controller Tango device TRL.

    :returns: the controller device TRL
    """
    return "low-mccs/control/control"


def get_antenna_trl(station_name: str, antenna_name: str) -> str:
    """
    Construct an antenna Tango device TRL from its station and antenna names.

    :param station_name: name of the station
    :param antenna_name: name of the antenna within the station

    :returns: antenna device TRL
    """
    return f"low-mccs/antenna/{station_name}-{antenna_name}"


def get_transient_buffer_trl() -> str:
    """
    Return the transient buffer Tango device TRL.

    :returns: the transient buffer device TRL
    """
    return "low-mccs/transientbuffer/transientbuffer"


class MccsTangoTestHarnessContext:
    """Handle for the Mccs test harness context."""

    def __init__(
        self: MccsTangoTestHarnessContext, tango_context: TangoTestHarnessContext
    ):
        """
        Initialise a new instance.

        :param tango_context: handle for the underlying test harness
            context.
        """
        self._tango_context = tango_context

    def get_controller_device(self: MccsTangoTestHarnessContext) -> tango.DeviceProxy:
        """
        Get a proxy to the controller device.

        :returns: a proxy to the controller Tango device.
        """
        return self._tango_context.get_device(get_controller_trl())

    def get_subarray_device(
        self: MccsTangoTestHarnessContext,
        subarray_id: int,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the subarray device.

        :param subarray_id: ID of the subarray

        :returns: a proxy to the subarray Tango device.
        """
        return self._tango_context.get_device(get_subarray_trl(subarray_id))

    def get_subarray_beam_device(
        self: MccsTangoTestHarnessContext,
        subarray_beam_id: int,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the subarray beam device.

        :param subarray_beam_id: ID of the subarray beam

        :returns: a proxy to the subarray beam Tango device.
        """
        return self._tango_context.get_device(get_subarray_beam_trl(subarray_beam_id))

    def get_station_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the station device.

        :param station_name: name of the station

        :returns: a proxy to the station Tango device.
        """
        return self._tango_context.get_device(get_station_trl(station_name))

    def get_station_beam_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
        station_beam_id: int,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the station beam device.

        :param station_name: name of the station
        :param station_beam_id: ID of the station beam within the station

        :returns: a proxy to the station beam Tango device.
        """
        return self._tango_context.get_device(
            get_station_beam_trl(station_name, station_beam_id)
        )

    def get_antenna_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
        antenna_name: str,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the antenna device.

        :param station_name: name of the station
        :param antenna_name: name of the antenna within the station

        :returns: a proxy to the antenna Tango device.
        """
        return self._tango_context.get_device(
            get_antenna_trl(station_name, antenna_name)
        )

    def get_sps_station_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the SPS station Tango device.

        :param station_name: name of the station

        :returns: a proxy to the SPS station Tango device.
        """
        return self._tango_context.get_device(get_sps_station_trl(station_name))

    def get_solver_device(
        self: MccsTangoTestHarnessContext, station_name: str
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the solver device.

        :param station_name: name of the station

        :returns: a proxy to the solver Tango device.
        """
        return self._tango_context.get_device(get_solver_trl(station_name))

    def get_subrack_device(
        self: MccsTangoTestHarnessContext, station_name: str, subrack_id: int
    ) -> tango.DeviceProxy:
        """
        Get a subrack Tango device by its ID number.

        :param station_name: name of the station
        :param subrack_id: the ID number of the subrack.

        :returns: a proxy to the subrack Tango device.
        """
        return self._tango_context.get_device(get_subrack_trl(station_name, subrack_id))

    def get_tile_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
        tile_id: int,
    ) -> tango.DeviceProxy:
        """
        Get a tile Tango device by its ID number.

        :param station_name: name of the station
        :param tile_id: the ID number of the tile.

        :returns: a proxy to the tile Tango device.
        """
        return self._tango_context.get_device(get_tile_trl(station_name, tile_id))

    def get_calibration_store_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the CalibrationStore Tango device.

        :param station_name: name of the station

        :returns: a proxy to the CalibrationStore Tango device.
        """
        return self._tango_context.get_device(get_calibration_store_trl(station_name))

    def get_station_calibrator_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the StationCalibrator Tango device.

        :param station_name: name of the station

        :returns: a proxy to the StationCalibrator Tango device.
        """
        return self._tango_context.get_device(get_station_calibrator_trl(station_name))

    def get_field_station_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the Field station Tango device.

        :param station_name: name of the station

        :returns: a proxy to the Field station Tango device.
        """
        return self._tango_context.get_device(get_field_station_trl(station_name))

    def get_smartbox_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
        smartbox_id: int,
    ) -> tango.DeviceProxy:
        """
        Get a smartbox Tango device by its ID number.

        :param station_name: name of the station
        :param smartbox_id: the ID number of the smartbox.

        :returns: a proxy to the smartbox Tango device.
        """
        return self._tango_context.get_device(
            get_smartbox_trl(station_name, smartbox_id)
        )

    def get_pasdbus_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the PasdBus Tango device.

        :param station_name: name of the station

        :returns: a proxy to the PasdBus Tango device.
        """
        return self._tango_context.get_device(get_pasdbus_trl(station_name))

    def get_fndh_device(
        self: MccsTangoTestHarnessContext,
        station_name: str,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the FNDH Tango device.

        :param station_name: name of the station

        :returns: a proxy to the FNDH Tango device.
        """
        return self._tango_context.get_device(get_fndh_trl(station_name))

    def get_transient_buffer_device(
        self: MccsTangoTestHarnessContext,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the transient buffer device.

        :returns: a proxy to the transient buffer Tango device.
        """
        return self._tango_context.get_device(get_transient_buffer_trl())


# pylint: disable=too-many-public-methods
class MccsTangoTestHarness:
    """A test harness for testing monitoring and control of Mccs hardware."""

    def __init__(self: MccsTangoTestHarness) -> None:
        """Initialise a new test harness instance."""
        self._tango_test_harness = TangoTestHarness()

    def set_controller_device(  # pylint: disable=too-many-arguments
        self: MccsTangoTestHarness,
        subarrays: list[int],
        subarray_beams: list[int],
        stations: list[str],
        station_beams: list[tuple[str, int]],
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_low_mccs.MccsController",
    ) -> None:
        """
        Set the test harness controller device.

        :param subarrays: list of subarray ids
        :param subarray_beams: list of subarray beam ids
        :param stations: list of station labels
        :param station_beams: list of station label, beam id pairs
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """

        kwargs: dict[str, Any] = {"LoggingLevelDefault": logging_level}

        # workaround because it is impossible to pass empty lists to Tango
        if subarrays:
            kwargs["MccsSubarrays"] = [get_subarray_trl(i) for i in subarrays]
        if stations:
            kwargs["MccsStations"] = [
                get_station_trl(station_name) for station_name in stations
            ]
        if subarray_beams:
            kwargs["MccsSubarrayBeams"] = [
                get_subarray_beam_trl(i) for i in subarray_beams
            ]
        if station_beams:
            kwargs["MccsStationBeams"] = [
                get_station_beam_trl(station_name, i)
                for (station_name, i) in station_beams
            ]
        self._tango_test_harness.add_device(
            get_controller_trl(),
            device_class,
            **kwargs,
        )

    def set_solver_device(
        self: MccsTangoTestHarness,
        station_name: str,
        root_path: str,
        eep_path: str,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: (
            type[Device] | str
        ) = "ska_low_mccs.calibration_solver.StationCalibrationSolverDevice",
    ) -> None:
        """
        Set the test harness controller device.

        :param station_name: the name of the station under test.
        :param root_path: the root path for loading data from
        :param eep_path: the root path for the embedded
            element pattern files.
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """
        self._tango_test_harness.add_device(
            get_solver_trl(station_name),
            device_class,
            RootPath=root_path,
            EEPRootPath=eep_path,
            LoggingLevelDefault=logging_level,
        )

    def add_subarray_device(
        self: MccsTangoTestHarness,
        subarray_id: int,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_low_mccs.MccsSubarray",
    ) -> None:
        """
        Add subarray to test harness.

        :param subarray_id: id of the subarray.
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """
        self._tango_test_harness.add_device(
            get_subarray_trl(subarray_id),
            device_class,
            SubarrayId=subarray_id,
            LoggingLevelDefault=logging_level,
        )

    def add_mock_subarray_device(
        self: MccsTangoTestHarness,
        subarray_id: int,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock subarray Tango device to this test harness.

        :param subarray_id: An ID number for the mock subarray.
        :param mock: the mock to be used as a mock tile device.
        """
        self._tango_test_harness.add_mock_device(get_subarray_trl(subarray_id), mock)

    def add_subarray_beam_device(
        self: MccsTangoTestHarness,
        subarray_beam_id: int,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_low_mccs.MccsSubarrayBeam",
    ) -> None:
        """
        Add subarray beam to test harness.

        :param subarray_beam_id: id of the subarray beam.
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """
        self._tango_test_harness.add_device(
            get_subarray_beam_trl(subarray_beam_id),
            device_class,
            LoggingLevelDefault=logging_level,
            ParentTRL=get_controller_trl(),
        )

    def add_mock_subarray_beam_device(
        self: MccsTangoTestHarness,
        subarray_beam_id: int,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock subarray beam Tango device to this test harness.

        :param subarray_beam_id: An ID number for the mock subarray.
        :param mock: the mock to be used as a mock tile device.
        """
        self._tango_test_harness.add_mock_device(
            get_subarray_beam_trl(subarray_beam_id), mock
        )

    def add_station_device(  # pylint: disable=too-many-arguments
        self: MccsTangoTestHarness,
        station_name: str,
        station_id: int,
        antenna_names: list[str],
        field_station_trl: str | None = None,
        station_calibrator_trl: str | None = None,
        sps_station_trl: str | None = None,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_low_mccs.MccsStation",
    ) -> None:
        """
        Add station to test harness.

        :param station_name: name of the station
        :param station_id: ID of the station
        :param antenna_names: list of antenna names for this station
        :param field_station_trl: TRL of the field station device
        :param sps_station_trl: TRL of the SPS station device
        :param station_calibrator_trl: TRL of the station calibrator device
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """
        if field_station_trl is None:
            field_station_trl = get_field_station_trl(station_name)
        if station_calibrator_trl is None:
            station_calibrator_trl = get_station_calibrator_trl(station_name)
        if sps_station_trl is None:
            sps_station_trl = get_sps_station_trl(station_name)

        kwargs: dict[str, Any] = {
            "StationId": station_id,
            "LoggingLevelDefault": logging_level,
        }

        # workaround because passing string property ""
        # seems to end up in Tango as " "
        if field_station_trl:
            kwargs["FieldStationName"] = field_station_trl
        if sps_station_trl:
            kwargs["SpsStationTrl"] = sps_station_trl
        if station_calibrator_trl:
            kwargs["StationCalibratorTrl"] = station_calibrator_trl

        # workaround because it is impossible to pass empty lists to Tango
        if antenna_names:
            kwargs["AntennaTrls"] = [
                get_antenna_trl(station_name, antenna_name)
                for antenna_name in antenna_names
            ]

        # Add antenna properties
        aavs3_antenna_positions_file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "data/aavs3_pos_eep_order.npy",
        )
        antenna_positions_eep_order = np.load(aavs3_antenna_positions_file)
        kwargs["AntennaXs"] = list(antenna_positions_eep_order[:, 0])
        kwargs["AntennaYs"] = list(antenna_positions_eep_order[:, 1])
        kwargs["AntennaZs"] = list(antenna_positions_eep_order[:, 2])
        kwargs["AntennaIDs"] = list(antenna_positions_eep_order[:, 3].astype(int))
        kwargs["ParentTRL"] = get_controller_trl()

        self._tango_test_harness.add_device(
            get_station_trl(station_name),
            device_class,
            **kwargs,
        )

    def add_mock_station_device(
        self: MccsTangoTestHarness,
        station_name: str,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock station Tango device to this test harness.

        :param station_name: name of the station
        :param mock: the mock to be used as a mock station beam device.
        """
        self._tango_test_harness.add_mock_device(get_station_trl(station_name), mock)

    def add_station_beam_device(
        self: MccsTangoTestHarness,
        station_name: str,
        beam_id: int,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_low_mccs.MccsStationBeam",
    ) -> None:
        """
        Add station beam to test harness.

        :param station_name: name of the station
        :param beam_id: id of the beam.
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """
        self._tango_test_harness.add_device(
            get_station_beam_trl(station_name, beam_id),
            device_class,
            BeamId=beam_id,
            StationTRL=get_station_trl(station_name),
            LoggingLevelDefault=logging_level,
            ParentTRL=get_controller_trl(),
        )

    def add_mock_station_beam_device(
        self: MccsTangoTestHarness,
        station_name: str,
        station_beam_id: int,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock station beam Tango device to this test harness.

        :param station_name: name of the station
        :param station_beam_id: An ID number for the mock station beam.
        :param mock: the mock to be used as a mock station beam device.
        """
        self._tango_test_harness.add_mock_device(
            get_station_beam_trl(station_name, station_beam_id), mock
        )

    def add_station_calibrator_device(
        self: MccsTangoTestHarness,
        station_label: str,
        station_id: int,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_low_mccs.MccsStationCalibrator",
    ) -> None:
        """
        Add a Station Calibrator Tango device in the test harness.

        :param station_label: name of the station
        :param station_id: the id of the station.
        :param logging_level: the Tango device's default logging level.
        :param device_class: The device class to use.
            This may be used to override the usual device class,
            for example with a patched subclass.
        """
        self._tango_test_harness.add_device(
            get_station_calibrator_trl(station_label),
            device_class,
            StationId=station_id,
            StationName=get_station_trl(station_label),
            CalibrationStoreName=get_calibration_store_trl(station_label),
            CalibrationSolverTrl=get_solver_trl(station_label),
            LoggingLevelDefault=logging_level,
            ParentTRL=get_station_trl(station_label),
        )

    # pylint: disable=too-many-arguments
    def set_calibration_store_device(
        self: MccsTangoTestHarness,
        station_label: str,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_low_mccs.MccsCalibrationStore",
        database_host: str = "station-calibration-postgresql",
        database_port: int = 5432,
        database_name: str = "postgres",
        database_admin_user: str = "postgres",
        database_admin_password: str = "",
    ) -> None:
        """
        Add a Calibration Store Tango device in the test harness.

        :param station_label: name of the station
        :param logging_level: the Tango device's default logging level.
        :param device_class: The device class to use.
            This may be used to override the usual device class,
            for example with a patched subclass.
        :param database_host: the database host
        :param database_port: the database port
        :param database_name: the database name
        :param database_admin_user: the database admin user
        :param database_admin_password: the database admin password
        """
        self._tango_test_harness.add_device(
            get_calibration_store_trl(station_label),
            device_class,
            DatabaseHost=database_host,
            DatabasePort=database_port,
            DatabaseName=database_name,
            DatabaseAdminUser=database_admin_user,
            DatabaseAdminPassword=database_admin_password,
            LoggingLevelDefault=logging_level,
            policy_name="preferred",
            policy_frequency_tolerance=0,
            policy_temperature_tolerance=200.0,
            ParentTRL=get_station_trl(station_label),
        )

    def add_mock_station_calibrator_device(
        self: MccsTangoTestHarness,
        station_name: str,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock station calibrator Tango device to this test harness.

        :param station_name: name of the station
        :param mock: the mock to be used as a mock station calibrator device.
        """
        self._tango_test_harness.add_mock_device(
            get_station_calibrator_trl(station_name), mock
        )

    def add_mock_calibration_store_device(
        self: MccsTangoTestHarness,
        station_label: str,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock calibration store Tango device to this test harness.

        :param station_label: name of the station
        :param mock: the mock to be used as a mock calibration store device.
        """
        self._tango_test_harness.add_mock_device(
            get_calibration_store_trl(station_label), mock
        )

    def add_antenna_device(  # pylint: disable=too-many-arguments
        self: MccsTangoTestHarness,
        station_name: str,
        antenna_name: str,
        antenna_id: int,
        tile_id: int,
        tile_y_channel: int,
        tile_x_channel: int,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_low_mccs.MccsAntenna",
    ) -> None:
        """
        Add an antenna device to the test harness.

        :param station_name: name of the station
        :param antenna_name: name of the antenna within the station.
        :param antenna_id: id of the antenna.
        :param tile_id: id of the tile within the station
        :param tile_y_channel: the ADC channel for the antenna's Y pol signal
        :param tile_x_channel: the ADC channel for the antenna's X pol signal
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """
        self._tango_test_harness.add_device(
            get_antenna_trl(station_name, antenna_name),
            device_class,
            FieldStationName=get_field_station_trl(station_name),
            AntennaId=antenna_id,
            TileName=get_tile_trl(station_name, tile_id),
            TileYChannel=tile_y_channel,
            TileXChannel=tile_x_channel,
            LoggingLevelDefault=logging_level,
            ParentTRL=get_station_trl(station_name),
        )

    def add_mock_antenna_device(
        self: MccsTangoTestHarness,
        station_name: str,
        antenna_name: str,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock antenna Tango device to this test harness.

        :param station_name: name of the station
        :param antenna_name: name of the mock antenna.
        :param mock: the mock to be used as a mock antenna device.
        """
        self._tango_test_harness.add_mock_device(
            get_antenna_trl(station_name, antenna_name), mock
        )

    def add_mock_field_station_device(
        self: MccsTangoTestHarness,
        station_name: str,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock Field Station Tango device to this test harness.

        :param station_name: name of the station
        :param mock: the mock to be used as a mock Field Station device.
        """
        self._tango_test_harness.add_mock_device(
            get_field_station_trl(station_name), mock
        )

    def add_mock_solver_device(
        self: MccsTangoTestHarness,
        station_name: str,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock Solver Tango device to this test harness.

        :param station_name: name of the station
        :param mock: the mock to be used as a mock Solver device.
        """
        self._tango_test_harness.add_mock_device(get_solver_trl(station_name), mock)

    def add_mock_sps_station_device(
        self: MccsTangoTestHarness,
        station_name: str,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock SPS station Tango device to this test harness.

        :param station_name: name of the station
        :param mock: the mock to be used as a mock SPS Station device.
        """
        self._tango_test_harness.add_mock_device(
            get_sps_station_trl(station_name), mock
        )

    # TODO: No device in ska-low-mccs should be talking directly to tile devices.
    # They should be going through SpsStation.
    # This method should be removed as soon as this is achieved.
    def add_mock_tile_device(
        self: MccsTangoTestHarness,
        station_name: str,
        tile_id: int,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock tile Tango device to this test harness.

        :param station_name: name of the station
        :param tile_id: An ID number for the mock tile.
        :param mock: the mock to be used as a mock tile device.
        """
        self._tango_test_harness.add_mock_device(
            get_tile_trl(station_name, tile_id), mock
        )

    def add_transient_buffer_device(
        self: MccsTangoTestHarness,
        device_class: type[Device] | str = "ska_low_mccs.MccsTransientBuffer",
    ) -> None:
        """
        Add an transient buffer device to the test harness.

        :param device_class: class of the device
        """
        self._tango_test_harness.add_device(
            get_transient_buffer_trl(),
            device_class,
        )

    def __enter__(
        self: MccsTangoTestHarness,
    ) -> MccsTangoTestHarnessContext:
        """
        Enter the context.

        :return: the entered context.
        """
        return MccsTangoTestHarnessContext(self._tango_test_harness.__enter__())

    def __exit__(
        self: MccsTangoTestHarness,
        exc_type: type[BaseException] | None,
        exception: BaseException | None,
        trace: TracebackType | None,
    ) -> bool | None:
        """
        Exit the context.

        :param exc_type: the type of exception thrown in the with block,
            if any.
        :param exception: the exception thrown in the with block, if
            any.
        :param trace: the exception traceback, if any,

        :return: whether the exception (if any) has been fully handled
            by this method and should be swallowed i.e. not re-
            raised
        """
        return self._tango_test_harness.__exit__(exc_type, exception, trace)
