# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains BDD tests for the On and Off commands."""
from __future__ import annotations

import time
from typing import Any, Callable

import pytest
import tango
from pytest_bdd import given, parsers, scenarios, then, when
from ska_control_model import AdminMode, ResultCode
from ska_tango_testing.mock.placeholders import Anything
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from tests.harness import MccsTangoTestHarnessContext, get_controller_trl

scenarios("features/on_off.feature")


@pytest.fixture(name="set_up_subscriptions")
def set_up_subscriptions_fixture(
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> tango.DeviceProxy:
    """
    Set up subscriptions on a device proxy.

    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.

    :return: A cached device proxy with subscriptions set up.
    """

    def _set_up_subscriptions(proxy: tango.DeviceProxy, attributes: list[str]) -> None:
        device_name = proxy.dev_name()
        for attribute_name in attributes:
            print(f"Subscribing proxy to {device_name}/{attribute_name}...")
            proxy.subscribe_event(
                attribute_name,
                tango.EventType.CHANGE_EVENT,
                change_event_callbacks[f"{device_name}/{attribute_name}"],
            )
            change_event_callbacks[
                f"{device_name}/{attribute_name}"
            ].assert_change_event(Anything)

    return _set_up_subscriptions


@pytest.fixture(name="set_device_state")
def set_device_state_fixture(
    set_up_subscriptions: Callable,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> Callable:
    """
    Set device state.

    :param set_up_subscriptions: convenience method for setting up device subscriptions.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.

    :return: A function to set device state.
    """

    def _set_device_state(
        device_proxy: tango.DeviceProxy,
        attributes_to_subscribe: list[str],
        state: tango.DevState,
        mode: AdminMode,
        expect_known_state_discovery: bool = True,
        max_time_for_establishing_communications: int = 1,
    ) -> None:
        set_up_subscriptions(device_proxy, attributes_to_subscribe)
        device_name = device_proxy.dev_name()
        admin_mode_callback = change_event_callbacks[f"{device_name}/adminMode"]
        state_callback = change_event_callbacks[f"{device_name}/state"]
        device_adminmode = device_proxy.adminMode
        if device_adminmode != mode:
            device_proxy.adminMode = mode
            admin_mode_callback.assert_change_event(mode)
            # A transition to ONLINE from OFFLINE should transition to UNKNOWN before
            # working out the state of the device under test.
            if mode == AdminMode.ONLINE and device_adminmode == AdminMode.OFFLINE:
                state_callback.assert_change_event(tango.DevState.UNKNOWN)
            # A transition between ONLINE and ENGINEERING does not change state.
            if (
                AdminMode.ENGINEERING not in [device_adminmode, mode]
                and expect_known_state_discovery
            ):
                state_callback.assert_change_event(Anything)
            else:
                # AdminMode changes ONLINE before communication changes to ESTABLISHED
                # This sleep is to give sufficient time to ESTABLISHED communication.
                time.sleep(max_time_for_establishing_communications)
        if device_proxy.read_attribute("state").value != state:
            print(f"Turning {device_proxy.dev_name()} {state}")
            set_tango_device_state(
                change_event_callbacks,
                device_proxy,
                state,
            )

    return _set_device_state


def set_tango_device_state(
    change_event_callbacks: MockTangoEventCallbackGroup,
    device_proxy: tango.DeviceProxy,
    desired_state: tango.DevState,
) -> None:
    """
    Turn a Tango device on or off using its On() and Off() commands.

    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support.
    :param device_proxy: Proxy to device to set state of.
    :param desired_state: the desired power state, either
        "on" or "off" or "standby".

    :raises ValueError: if input desired_state is not valid.
    """
    # If UNKNOWN state, expect device to transition out of UNKNOWN
    if device_proxy.state() == tango.DevState.UNKNOWN:
        print(f"Have device {device_proxy} in state unknown {device_proxy.state()}")
        change_event_callbacks[f"{device_proxy.dev_name()}/state"].assert_change_event(
            Anything, lookahead=2
        )
        print(f"Now have device {device_proxy} in state {device_proxy.state()}")
    else:
        print(f"Have device {device_proxy} in state {device_proxy.state()}")

    # Issue the command if necessary
    if desired_state != device_proxy.state():
        if desired_state == tango.DevState.ON:
            [result_code], [command_id] = device_proxy.On()
        elif desired_state == tango.DevState.OFF:
            [result_code], [command_id] = device_proxy.Off()
        elif desired_state == tango.DevState.STANDBY:
            [result_code], [command_id] = device_proxy.Standby()
        else:
            raise ValueError(f"State {desired_state} is not a valid state.")
    else:
        return

    assert result_code == ResultCode.QUEUED
    print(f"Command queued on {device_proxy.dev_name()}: {command_id}")
    timeout = 300
    start_time = time.time()
    while time.time() < start_time + timeout:
        try:
            assert device_proxy.state() == desired_state
            break
        except AssertionError:
            time.sleep(1)
    assert device_proxy.state() == desired_state


@pytest.fixture(name="controller")
def controller_fixture(
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> tango.DeviceProxy:
    """
    Return the controller device.

    :param test_context: the test context in which this test will be run.
    :param set_up_subscriptions: convenience method for setting up device subscriptions.

    :return: the controller device.
    """
    controller = test_context.get_controller_device()
    set_up_subscriptions(controller, ["state", "healthState", "adminMode"])
    return controller


@pytest.fixture(name="device_proxy_names")
def device_proxy_names_fixture() -> dict[str, str]:
    """
    Aggregate the proxy fixtures into a dictionary.

    The keys of the dictionary are the device names.

    :return: dictionary of proxies
    """
    return {
        "MccsController": "low-mccs/control/*",
        "MccsSubarray": "low-mccs/subarray/*",
        "MccsSubarrayBeam": "low-mccs/subarraybeam/*",
        "MccsStationBeam": "low-mccs/beam/*",
        "MccsStation": "low-mccs/station/*",
        "SpsStation": "low-mccs/spsstation/*",
        "MccsSubrack": "low-mccs/subrack/*",
        "MccsTile": "low-mccs/tile/*",
        "FieldStation": "low-mccs/fieldstation/*",
        "MccsFndh": "low-mccs/fndh/*",
        "MccsSmartbox": "low-mccs/smartbox/*",
        "MccsPasdBus": "low-mccs/pasdbus/*",
        "MccsAntenna": "low-mccs/antenna/*",
    }


@given("This test is running in a true context")
def check_enviroment(true_context: bool) -> None:
    """
    Check test is being run in a true context.

    :param true_context: true if we are running against a real
        deployment.
    """
    if not true_context:
        pytest.skip("This test is designed to run in a true context.")


@given(parsers.parse("All {device_name} are {admin_mode} and {state}"))
def given_device_state(
    test_context: Any,
    set_device_state: Callable,
    device_name: str,
    admin_mode: str,
    state: str,
    device_proxy_names: dict[str, str],
) -> None:
    """
    Set all devices of a given type to the desired admin mode and state.

    :param test_context: the test context in which this test will be run.
    :param set_device_state: fixture function which sets device state.
    :param device_name: device class to set the state of.
    :param admin_mode: the desired admin mode.
    :param state: the desired state.
    :param device_proxy_names: dictionary of device proxy names.
    """
    desired_admin_mode = AdminMode[admin_mode]
    desired_state = {
        "ON": tango.DevState.ON,
        "OFF": tango.DevState.OFF,
        "STANDBY": tango.DevState.STANDBY,
    }[state]

    db = tango.Database()
    devices_exported = db.get_device_exported(device_proxy_names[device_name])
    proxies = [tango.DeviceProxy(device) for device in devices_exported]

    for proxy in proxies:
        set_device_state(
            proxy,
            ["state", "healthState", "adminMode"],
            state=desired_state,
            mode=desired_admin_mode,
        )


@when("the MccsController is commanded to turn On")
def controller_on(controller: tango.DeviceProxy) -> None:
    """
    Tell the controller to turn everything on.

    :param controller: MccsController device proxy for use in the test.
    """
    controller.On()


@when("the MccsController is commanded to turn Off")
def controller_off(controller: tango.DeviceProxy) -> None:
    """
    Tell the controller to turn everything off.

    :param controller: MccsController device proxy for use in the test.
    """
    controller.Off()


@when(parsers.parse("Most {device_name} are commanded to turn Off"))
def turn_most_devices_off(
    device_name: str,
    device_proxy_names: dict[str, str],
) -> None:
    """
    Tell all devices of a given type to turn off.

    :param device_name: the device class to check.
    :param device_proxy_names: dictionary of device proxy names.
    """
    db = tango.Database()
    devices_exported = db.get_device_exported(device_proxy_names[device_name])
    proxies = [tango.DeviceProxy(device) for device in devices_exported]
    for i, proxy in enumerate(proxies):
        if i != 0:  # skip the first device
            proxy.Off()


@then(parsers.parse("the MccsController is in state {state}"))
def device_is_in_state_parse(
    state: str,
    device_proxy_names: dict[str, str],
) -> None:
    """
    Verify that the controller is in the correct state.

    Note: device_report_state() checks the long running command
    response of the controller before it asserts it's state.

    In some cases, there are no LRCs sent to the controller, so
    the function fails even when in the correct state.

    This function simply runs an assert statement to avoid this

    :param state: the desired state.
    :param device_proxy_names: dictionary of device proxy names.
    """
    db = tango.Database()
    device_name = db.get_device_exported(device_proxy_names["MccsController"])
    controler = tango.DeviceProxy(device_name[0])
    device_state = {
        "STANDBY": tango.DevState.STANDBY,
        "ON": tango.DevState.ON,
        "OFF": tango.DevState.OFF,
    }[state]

    timeout = 60  # seconds

    start_time = time.time()
    while time.time() < start_time + timeout:
        try:
            assert controler.state() == device_state
            break
        except AssertionError:
            time.sleep(1)
    assert controler.state() == device_state


@then(parsers.parse("All {device_name} report they are {state}"))
def device_report_state(
    device_name: str,
    state: str,
    device_proxy_names: dict[str, str],
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Verify that all devices of a given type are in the correct state.

    :param device_name: the device class to check.
    :param state: the desired state.
    :param device_proxy_names: dictionary of device proxy names.
    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support.
    """
    db = tango.Database()
    devices_exported = db.get_device_exported(device_proxy_names[device_name])
    proxies = [tango.DeviceProxy(device) for device in devices_exported]
    device_state = {
        "STANDBY": tango.DevState.STANDBY,
        "ON": tango.DevState.ON,
        "OFF": tango.DevState.OFF,
    }[state]

    if device_name == "MccsController":
        change_event_callbacks[f"{get_controller_trl()}/state"].assert_change_event(
            device_state, lookahead=20
        )

    # Controller got a change event, lets then check all the other devices got the
    # correct state
    timeout = 60  # seconds
    for proxy in proxies:
        start_time = time.time()
        while time.time() < start_time + timeout:
            try:
                assert proxy.state() == device_state
                break
            except AssertionError:
                time.sleep(1)
        assert proxy.state() == device_state


@then(
    parsers.parse(
        "All MccsTile report that they are in programming state {programming_state}"
    )
)
def tile_programming_state_correct(
    programming_state: str,
    device_proxy_names: dict[str, str],
) -> None:
    """
    Verify the tile programming state of all tiles.

    :param programming_state: the desired programming state.
    :param device_proxy_names: dictionary of device proxy names.
    """
    db = tango.Database()
    devices_exported = db.get_device_exported(device_proxy_names["MccsTile"])
    proxies = [tango.DeviceProxy(device) for device in devices_exported]

    timeout = 60  # seconds
    for proxy in proxies:
        start_time = time.time()
        while time.time() < start_time + timeout:
            try:
                assert proxy.tileProgrammingState.lower() == programming_state.lower()
                break
            except AssertionError:
                time.sleep(1)
        assert proxy.tileProgrammingState.lower() == programming_state.lower()
