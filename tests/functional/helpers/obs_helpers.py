# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains helper methods functional tests of the ska-low-mccs project."""
from __future__ import annotations

import json
import time
from datetime import datetime
from random import uniform
from typing import Callable, Iterable

import tango
from ska_control_model import ObsState, ResultCode

RFC_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"


def _flatten(two_dimension_list: Iterable[Iterable]) -> list:
    return [
        value
        for one_dimension_list in two_dimension_list
        for value in one_dimension_list
    ]


def allocate_resources(
    controller: tango.DeviceProxy,
    subarray: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    schema_compliant_allocate: dict,
) -> dict:
    """
    Allocate resources according to values given by resources_to_allocate.

    :param controller: MccsController device proxy for use in the test.
    :param subarray: MccsSubarray device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param schema_compliant_allocate: fixture for schema compliant allocate argument.

    :returns: resources allocated by MccsController.
    """
    wait_for_lrcs_to_finish()

    assert (
        controller.Allocate(json.dumps(schema_compliant_allocate))[0]
        == ResultCode.QUEUED
    )
    wait_for_lrcs_to_finish()
    wait_for_obsstate(ObsState.IDLE, subarray)
    return schema_compliant_allocate


def release_resources(
    controller: tango.DeviceProxy,
    subarray: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
) -> None:
    """
    Release all resources from MccsController.

    :param controller: MccsController device proxy for use in the test.
    :param subarray: MccsSubarray device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    controller.ReleaseAll()
    wait_for_lrcs_to_finish()
    wait_for_obsstate(ObsState.EMPTY, subarray)


def configure_resources(
    subarray: tango.DeviceProxy,
    subarray_beam_count: int,
    resources_allocated: dict,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
) -> list:
    """
    Configure resources according to data within bounds specified by Configure() schema.

    :param subarray: MccsSubarray device proxy for use in the test.
    :param subarray_beam_count: amonut of MccsSubarrayBeam used in this test.
    :param resources_allocated: schema compliant fixture holding resources
        allocated earlier.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.

    :returns: list of configuration parameters
    """
    subarray_beam_config_list = []
    for subarray_beam_no in range(subarray_beam_count):
        # Fetching subarray beams from resources_allocated dictionary
        # see MccsController.Allocate() schema.
        subarray_beams = resources_allocated.get("subarray_beams", [])
        for subarray_beam in subarray_beams:
            # Fetching apertures relevent to this subbaray beam.
            if subarray_beam.get("subarray_beam_id") == subarray_beam_no + 1:
                desired_apertures = subarray_beam.get("apertures", [])
                break
        subarray_beam_config = {
            "subarray_beam_id": subarray_beam_no + 1,
            "update_rate": 5 * (subarray_beam_no + 1),
            "logical_bands": [
                {
                    "start_channel": 2,
                    "number_of_channels": 8,
                },
                {
                    "start_channel": 2,
                    "number_of_channels": 8,
                },
            ],
            "field": {
                "reference_frame": "ICRS",
                "target_name": "some star",
                "timestamp": "2023-04-05T12:34:56.000Z",
                "attrs": {
                    "c1": uniform(20, 340),
                    "c1_rate": uniform(-0.010, 0.010),
                    "c2": uniform(-60, 60),
                    "c2_rate": uniform(-0.010, 0.010),
                },
            },
            "apertures": desired_apertures,
        }
        subarray_beam_config_list.append(subarray_beam_config)

    configure_interface = r"https://schema.skao.int/ska-low-mccs-subarray-configure/3.0"
    assert (
        subarray.Configure(
            json.dumps(
                {
                    "interface": configure_interface,
                    "subarray_beams": subarray_beam_config_list,
                }
            )
        )[0]
        == ResultCode.QUEUED
    )

    wait_for_lrcs_to_finish()
    wait_for_obsstate(ObsState.READY, subarray)
    return subarray_beam_config_list


def deconfigure_resources(
    subarray: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
) -> None:
    """
    Deconfigure resources from the MccsSubarray.

    :param subarray: MccsSubarray device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    subarray.End()
    wait_for_lrcs_to_finish()
    wait_for_obsstate(ObsState.IDLE, subarray)


def scan(
    subarray: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
) -> None:
    """
    Perform a Scan using the MccsSubarray.

    :param subarray: MccsSubarray device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """

    scan_id = 1234
    start_time = datetime.strftime(
        datetime.fromtimestamp(int(time.time()) + 2), RFC_FORMAT
    )

    subarray.Scan(
        json.dumps(
            {
                "interface": r"https://schema.skao.int/ska-low-mccs-subarray-scan/3.0",
                "scan_id": scan_id,
                "start_time": start_time,
            }
        )
    )
    wait_for_lrcs_to_finish()
    wait_for_obsstate(ObsState.SCANNING, subarray)


def end_scan(
    subarray: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
) -> None:
    """
    End a Scan performed by the MccsSubarray.

    :param subarray: MccsSubarray device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    assert (
        subarray.obsState == ObsState.SCANNING
    ), "Subarray needs to be scanning to end scan"
    subarray.EndScan()
    wait_for_lrcs_to_finish()
    wait_for_obsstate(ObsState.READY, subarray)


def abort(
    subarray: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
) -> None:
    """
    Abort a MccsSubarray.

    :param subarray: MccsSubarray device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    subarray.Abort()
    wait_for_lrcs_to_finish()
    wait_for_obsstate(ObsState.ABORTED, subarray)
