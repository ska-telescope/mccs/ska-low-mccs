# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains functional test fixtures of the ska-low-mccs project."""
from __future__ import annotations

import time
from typing import Callable, Iterable, Optional

import pytest
import tango
from ska_control_model import ObsState
from ska_tango_testing.mock.placeholders import Anything
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup


def _flatten(two_dimension_list: Iterable[Iterable]) -> list:
    return [
        value
        for one_dimension_list in two_dimension_list
        for value in one_dimension_list
    ]


@pytest.fixture(name="station_names")
def station_names_fixture() -> list[str]:
    """
    Return base fixture for holding present station labels.

    :returns: empty list for population by BDD Given steps.
    """

    return []


#
#
#
# ----------------- MAIN REPO -----------------
#
#
#


@pytest.fixture(name="controller")
def controller_fixture() -> Optional[tango.DeviceProxy]:
    """
    Return base fixture for holding MccsController proxy.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: None
    """

    return None


@pytest.fixture(name="mccs_stations")
def mccs_stations_fixture() -> dict[str, tango.DeviceProxy]:
    """
    Return base fixture for holding MccsStation proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="subarrays")
def subarrays_fixture() -> list[tango.DeviceProxy]:
    """
    Return base fixture for holding MccsSubarray proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty list for population by BDD Given steps.
    """

    return []


@pytest.fixture(name="subarray_beams")
def subarray_beams_fixture() -> list[tango.DeviceProxy]:
    """
    Return base fixture for holding MccsSubarrayBeam proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty list for population by BDD Given steps.
    """

    return []


@pytest.fixture(name="station_beams")
def station_beams_fixture() -> dict[str, list[tango.DeviceProxy]]:
    """
    Return base fixture for holding MccsStationBeam proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.
    The dict is of the form {station_name : list[MccsStationBeam on that station]}

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="antennas")
def antennas_fixture() -> dict[str, list[tango.DeviceProxy]]:
    """
    Return base fixture for holding MccsAntenna proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.
    The dict is of the form {station_name : list[MccsAntenna on that station]}

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


#
#
#
# ----------------- SPS REPO -----------------
#
#
#


@pytest.fixture(name="sps_stations")
def sps_stations_fixture() -> dict[str, tango.DeviceProxy]:
    """
    Return base fixture for holding SpsStation proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="tiles")
def tiles_fixture() -> dict[str, list[tango.DeviceProxy]]:
    """
    Return base fixture for holding MccsTile proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="subracks")
def subracks_fixture() -> dict[str, list[tango.DeviceProxy]]:
    """
    Return base fixture for holding MccsSubrack proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="daqs")
def daqs_fixture() -> dict[str, tango.DeviceProxy]:
    """
    Return base fixture for holding MccsDaqReceiver proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="station_calibrators")
def station_calibrators_fixture() -> dict[str, tango.DeviceProxy]:
    """
    Return base fixture for holding MccsStationCalibrators proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="calibration_stores")
def calibration_stores_fixture() -> dict[str, tango.DeviceProxy]:
    """
    Return base fixture for holding MccsCalibrationStore proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


#
#
#
# ----------------- PASD REPO -----------------
#
#
#


@pytest.fixture(name="field_stations")
def field_stations_fixture() -> dict[str, tango.DeviceProxy]:
    """
    Return base fixture for holding FieldStation proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="smartboxes")
def smartboxes_fixture() -> dict[str, list[tango.DeviceProxy]]:
    """
    Return base fixture for holding MccsSmartbox proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="fndhs")
def fndhs_fixture() -> dict[str, tango.DeviceProxy]:
    """
    Return base fixture for holding MccsFndh proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


@pytest.fixture(name="pasdbuses")
def pasdbuses_fixture() -> dict[str, tango.DeviceProxy]:
    """
    Return base fixture for holding MccsPasdbus proxies.

    This will be populated by a BDD Given step, this serves only as a placeholder.

    :returns: empty dict for population by BDD Given steps.
    """

    return {}


# pylint: disable=too-many-arguments
@pytest.fixture(name="all_devices")
def all_devices_fixture(
    controller: Optional[tango.DeviceProxy],
    mccs_stations: dict[str, tango.DeviceProxy],
    sps_stations: dict[str, tango.DeviceProxy],
    field_stations: dict[str, tango.DeviceProxy],
    subarrays: list[tango.DeviceProxy],
    subarray_beams: list[tango.DeviceProxy],
    daqs: dict[str, tango.DeviceProxy],
    fndhs: dict[str, tango.DeviceProxy],
    pasdbuses: dict[str, tango.DeviceProxy],
    station_beams: dict[str, list[tango.DeviceProxy]],
    antennas: dict[str, list[tango.DeviceProxy]],
    tiles: dict[str, list[tango.DeviceProxy]],
    subracks: dict[str, list[tango.DeviceProxy]],
    smartboxes: dict[str, list[tango.DeviceProxy]],
) -> dict[str, list[tango.DeviceProxy]]:
    """
    Return fixture containing all devices defined in the test.

    :param controller: the MccsController in the test.
    :param mccs_stations: the MccsStations in the test.
    :param sps_stations: the SpsStations in the test.
    :param field_stations: the FieldStations in the test.
    :param subarrays: the MccsSubarrays in the test.
    :param subarray_beams: the MccsSubarrayBeams in the test.
    :param daqs: the MccsDAQs in the test.
    :param fndhs: the MccsFndhs in the test.
    :param pasdbuses: the MccsPasdBuses in the test.
    :param station_beams: the MccsStationBeams in the test.
    :param antennas: the MccsAntennas in the test.
    :param tiles: the MccsTiles in the test.
    :param subracks: the MccsSubracks in the test.
    :param smartboxes: the MccsSmartboxes in the test.

    :returns: dict of all devices in the test.
    """
    devices = {}
    if controller is not None:
        devices["MccsController"] = [controller]
    devices["MccsSubarray"] = subarrays
    devices["MccsSubarrayBeam"] = subarray_beams
    devices["MccsStationBeam"] = _flatten(list(station_beams.values()))
    devices["MccsAntenna"] = _flatten(list(antennas.values()))
    devices["MccsTile"] = _flatten(list(tiles.values()))
    devices["MccsSubrack"] = _flatten(list(subracks.values()))
    devices["MccsSmartbox"] = _flatten(list(smartboxes.values()))
    devices["MccsStation"] = list(mccs_stations.values())
    devices["FieldStation"] = list(field_stations.values())
    devices["SpsStation"] = list(sps_stations.values())
    devices["MccsDAQ"] = list(daqs.values())
    devices["MccsFndh"] = list(fndhs.values())
    devices["MccsPasdBus"] = list(pasdbuses.values())

    return devices


@pytest.fixture(name="set_up_subscriptions")
def set_up_subscriptions_fixture(
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> Callable:
    """
    Set up subscriptions on a device proxy.

    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.

    :return: A callable to set up device proxy subscriptions.
    """

    def _set_up_subscriptions(proxy: tango.DeviceProxy, attributes: list[str]) -> None:
        device_name = proxy.dev_name()
        for attribute_name in attributes:
            print(f"Subscribing proxy to {device_name}/{attribute_name}...")
            proxy.subscribe_event(
                attribute_name,
                tango.EventType.CHANGE_EVENT,
                change_event_callbacks[f"{device_name}/{attribute_name}"],
            )
            change_event_callbacks[
                f"{device_name}/{attribute_name}"
            ].assert_change_event(Anything)

    return _set_up_subscriptions


@pytest.fixture(name="wait_for_lrcs_to_finish")
def wait_for_lrcs_to_finish_fixture(
    all_devices: dict[str, list[tango.DeviceProxy]]
) -> Callable:
    """
    Wait for Long Running Commands on devices under test to finish.

    We are sending quite a few LRCs in some of these tests, we must ensure
    they are complete before moving onto the next step, or before trying to
    read attributes.

    :param all_devices: dict of all devices in the test.

    :returns: a callable checking status of LRCs on all devices.
    """

    def _wait_for_lrcs_to_finish(timeout: int = 30) -> None:
        count = 0

        for device in _flatten(list(all_devices.values())):
            count = 0
            while device.longRunningCommandsInQueue != ():
                time.sleep(1)
                count += 1
                if count == timeout:
                    pytest.fail(
                        f"LRCs still running after {timeout} seconds: "
                        f"{device.dev_name()} : {device.longRunningCommandsInQueue}"
                    )

    return _wait_for_lrcs_to_finish


@pytest.fixture(name="wait_for_obsstate")
def wait_for_obsstate_fixture(
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> Callable:
    """
    Check we will recieve change event for obsState of our obsDevices.

    Before moving on from running any of our obs commands, we need to make sure
    that our obsState has reached the correct obsState. For a given obsState
    all obsDevices should be in that obsState.

    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.

    :returns: a callable which can be called with desired obsState.
    """

    def _wait_for_obsstate(
        desired_obsstate: ObsState, device: tango.DeviceProxy, timeout: int = 20
    ) -> None:
        dev_name = device.dev_name()
        print(f"Waiting for {dev_name} obsState: {desired_obsstate.name}")
        print(f"Current {dev_name} obsState: {device.obsState.name}")
        # These callbacks are getting called a lot, so we need to look far ahead
        # for change events, then once we find one, we cant assume we'll quickly
        # reach that obstate.
        if device.obsState != desired_obsstate:
            change_event_callbacks[f"{dev_name}/obsState"].assert_change_event(
                desired_obsstate, lookahead=10
            )
        current_time = time.time()
        # We know there is a change event for obsstate on the way, but we need to
        # wait until it arrives.
        while time.time() < current_time + timeout:
            try:
                assert device.obsState == desired_obsstate
                break
            except AssertionError:
                time.sleep(0.5)
        assert device.obsState == desired_obsstate
        print(f"Got {dev_name} obsState: {device.obsState.name}")

    return _wait_for_obsstate
