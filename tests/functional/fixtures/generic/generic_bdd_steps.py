# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains functional test BDD steps of the ska-low-mccs project."""
from __future__ import annotations

import os
import time
import traceback
import warnings
from typing import Any, Callable, Optional, Type

import decorator
import pytest
import tango
from pytest_bdd import given, parsers
from ska_control_model import AdminMode

from tests.harness import MccsTangoTestHarnessContext


# pylint: disable=broad-exception-caught
def flaky(
    reruns: int = 2,
    reruns_delay: int = 0,
    expected_exception: Type[Exception] = Exception,
) -> Callable:
    """
    Return a barebones re-implementation of pytest-rerunfailures.

    This is needed as we can't use @given steps in conjunction with @pytest.mark.flaky.

    :param reruns: how many times to attempt this step.
    :param reruns_delay: how long to wait between attempts.
    :param expected_exception: which exception to retry for, other failures will not
        retry and fail immediately.

    :returns: flaky decorator.
    """

    def decorator_method(step: Callable) -> Callable:
        def wrapper(
            func: Callable,
            *args: Any,
            **kwargs: Any,
        ) -> None:
            for _ in range(reruns):
                try:
                    step(*args, **kwargs)
                    return
                except Exception as e:
                    error = e
                    error_traceback = traceback.format_exc()
                    if isinstance(e, expected_exception):
                        time.sleep(reruns_delay)
                    else:
                        pytest.fail(
                            f"Step {step.__name__} failed with error "
                            f"type {type(e).__name__}, not rerunning "
                            f"as expected {expected_exception.__name__}\n"
                            f"Error: {repr(error)}, {error_traceback}"
                        )

            pytest.fail(
                f"Step {step.__name__} failed after {reruns} attempt(s). \n"
                f"Error: {repr(error)}, {error_traceback}"
            )

        return decorator.decorator(wrapper, step)

    return decorator_method


@given(parsers.parse("we have raised a warning {warning_message}"))
def raise_warning(warning_message: str) -> None:
    """
    Raise a warning.

    :param warning_message: The meaage to warn user about.
    """
    print(warning_message)
    warnings.warn(warning_message)


@given(
    parsers.parse("{station_no} stations"),
    target_fixture="station_names",
    converters={"station_no": int},
)
def get_station_names(station_names: list[str], station_no: int) -> list[str]:
    """
    Get list of stations present.

    :param station_names: fixture to store present stations.
    :param station_no: how many stations are required.

    :returns: list of stations present.
    """
    station_env = os.getenv("K8S_TEST_STATIONS")
    if station_env:
        # Colon separated list in env variable
        station_id_list = station_env.split(":")
    else:
        station_id_list = ["ci-1", "ci-2"]
    assert len(station_id_list) >= station_no
    station_id_list = station_id_list[:station_no]
    for station_id in station_id_list:
        station_names.append(station_id.strip())
    return station_names


def retry_communication(device_proxy: tango.Deviceproxy, timeout: int = 30) -> None:
    """
    Retry communication with the backend.

    NOTE: This is to be used for devices that do not know if the backend is avaliable
    at the time of the call. For example the daq_handler backend gRPC server
    may not be ready when we try to start communicating.
    In this case we will retry connection.

    :param device_proxy: A 'tango.DeviceProxy' to the backend device.
    :param timeout: A max time in seconds before we give up trying
    """
    tick = 2
    if device_proxy.adminMode != AdminMode.ONLINE:
        terminate_time = time.time() + timeout
        while time.time() < terminate_time:
            try:
                device_proxy.adminMode = AdminMode.ONLINE
                break
            except tango.DevFailed:
                print(f"{device_proxy.dev_name()} failed to communicate with backend.")
                time.sleep(tick)
        assert device_proxy.adminMode == AdminMode.ONLINE
    else:
        print(f"Device {device_proxy.dev_name()} is already ONLINE nothing to do.")


# Marking this as flaky due to pipeline issues with device connection.
# About 1/10 times we'll get a CORBA exception, or a ZMQ exception,
# this causes the test to fail despite the functionality the test is testing
# being acceptable.
# By default this step will be run once, then once again if it fails.
# If both fail this step will fail.
# pylint: disable=too-many-locals
# pylint: disable=too-many-nested-blocks
# pylint: disable=too-many-branches
# pylint: disable=broad-exception-caught
@flaky(reruns_delay=60)
@given("an MCCS deployment which is ONLINE and ON")
def get_online_on_mccs(
    true_context: bool,
    test_context: MccsTangoTestHarnessContext,
) -> None:
    """
    Put all MCCS devices ONLINE, then ON from MccsController.

    :param true_context: True if running against a K8s cluster, needed as this test is
        cross-repo.
    :param test_context: the context in which the test is being run.
    """
    if not true_context:
        pytest.skip("This test is designed to run in a true context.")

    controller = test_context.get_controller_device()
    if controller.state() != tango.DevState.ON:
        # k8s-wait sometimes returns too quickly and our devices aren't actually ready
        time.sleep(20)

        db = tango.Database()

        # Turn off inheritmodes until the tests are refactored to support it.
        for device in [
            tango.DeviceProxy(trl)
            for trl in tango.Database().get_device_exported("low-mccs/*")
        ]:
            try:
                device.inheritmodes = False
            except Exception:  # pylint: disable=broad-exception-caught
                print(f"Skipping inheritmodes on {device.dev_name()}")

        pasd_bus_trls = db.get_device_exported("low-mccs/pasdbus/*")
        for pasd_bus_trl in pasd_bus_trls:
            pasdbus = tango.DeviceProxy(pasd_bus_trl)
            if pasdbus.adminmode != AdminMode.ONLINE:
                pasdbus.adminmode = AdminMode.ONLINE
                time.sleep(0.1)

        device_trls = db.get_device_exported("low-mccs/*")
        devices = []

        for device_trl in device_trls:
            device = tango.DeviceProxy(device_trl)
            devices.append(device)
            if "daq" in device_trl or "calibrationstore" in device_trl:
                # In these devices we can have a corba timeout due to
                # start_communicating being a fast command and the server not
                # being ready during initial deployment. For example DAQ is
                # configured with a `wait_for_ready` flag, but due to the limitation
                # Of 3 seconds from corba it gives up after 3 seconds.
                retry_communication(device, 30)

            if device.adminmode != AdminMode.ONLINE:
                device.adminmode = AdminMode.ONLINE
                time.sleep(0.1)

        for device in devices:
            assert device.adminmode == AdminMode.ONLINE

        [[_], [command_id]] = controller.On()

        timeout = 600  # seconds
        start_time = time.time()
        # Wait for the command to COMPLETE, FAIL otherwise.
        lrc_endpoints = ["COMPLETED", "FAILED", "REJECTED", "ABORTED"]
        while time.time() < start_time + timeout:
            command_status = controller.CheckLongrunningcommandstatus(command_id)
            if command_status in lrc_endpoints:
                if command_status != "COMPLETED":
                    pytest.fail(f"Controller ON command {command_id} {command_status}")
                break
            time.sleep(1)
        assert controller.state() == tango.DevState.ON

        for device in devices:
            assert (
                device.state() == tango.DevState.ON
            ), f"{device.dev_name()} is not ON, it is in state {device.state()}, \n"
            print(f"{device.dev_name()} is ON")


#
#
#
# ----------------- MAIN REPO -----------------
#
#
#


@given("1 MccsController", target_fixture="controller")
def get_controller(
    controller: Optional[tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> tango.DeviceProxy:
    """
    Get a MccsController.

    :param controller: fixture for proxy to controller device, None if not populated
        by Given step yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: proxy to MccsController device.
    """
    if controller is None:
        controller = test_context.get_controller_device()

    assert controller.state() == tango.DevState.ON
    assert controller.adminmode == AdminMode.ONLINE
    set_up_subscriptions(controller, ["state", "healthState", "adminMode"])
    return controller


@given(
    parsers.parse("{count} MccsSubarray"),
    target_fixture="subarrays",
    converters={"count": int},
)
def get_subarrays(
    count: int,
    subarrays: list[tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> list[tango.DeviceProxy]:
    """
    Check we have a "count" no of MccsSubarrays.

    :param count: how many MccsSubarrays are required for the test.
    :param subarrays: fixture for list of proxies to MccsSubarray devices, empty if not
        populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: list of MccsSubarray proxies.
    """
    if len(subarrays) < count:
        subarrays = [
            test_context.get_subarray_device(subarray_no)
            for subarray_no in range(1, count + 1)
        ]

    for subarray in subarrays:
        set_up_subscriptions(
            subarray, ["state", "healthState", "adminMode", "obsState"]
        )
        assert subarray.state() == tango.DevState.ON
        assert subarray.adminmode == AdminMode.ONLINE

    return subarrays


@given(
    parsers.parse("{count} MccsSubarrayBeam"),
    target_fixture="subarray_beams",
    converters={"count": int},
)
def get_subarray_beams(
    count: int,
    subarray_beams: list[tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> list[tango.DeviceProxy]:
    """
    Check we have a "count" no of MccsSubarrayBeam.

    :param count: how many MccsSubarrayBeams are required for the test.
    :param subarray_beams: fixture for list of proxies to MccsSubarray devices,
        empty if not populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: list of MccsSubarrayBeam proxies.
    """
    if len(subarray_beams) < count:
        subarray_beams = [
            test_context.get_subarray_beam_device(subarray_beam_no)
            for subarray_beam_no in range(1, count + 1)
        ]

    for subarray_beam in subarray_beams:
        set_up_subscriptions(
            subarray_beam, ["state", "healthState", "adminMode", "obsState"]
        )
        assert subarray_beam.state() == tango.DevState.ON
        assert subarray_beam.adminmode == AdminMode.ONLINE

    return subarray_beams


@given(
    parsers.parse("{count} MccsStationBeam in every station"),
    target_fixture="station_beams",
    converters={"count": int},
)
def get_station_beams(
    count: int,
    station_names: list[str],
    station_beams: dict[str, list[tango.DeviceProxy]],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, list[tango.DeviceProxy]]:
    """
    Check we have a "count" no of MccsStationBeams on each station.

    :param count: how many MccsStationBeams are required for the test.
    :param station_names: station labels present for the test.
    :param station_beams: fixture for list of proxies to MccsStationBeam devices,
        empty if not populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: dict of MccsStationBeam proxies.
    """
    for station_name in station_names:
        if station_name in station_beams.keys():
            station_beam_list = station_beams[station_name]
        else:
            station_beam_list = []
        if len(station_beam_list) < count:
            station_beam_list = [
                test_context.get_station_beam_device(station_name, station_beam_no)
                for station_beam_no in range(1, count + 1)
            ]
        station_beams[station_name] = station_beam_list

        for station_beam_list in station_beams.values():
            for station_beam in station_beam_list:
                set_up_subscriptions(
                    station_beam, ["state", "healthState", "adminMode", "obsState"]
                )
                assert station_beam.state() == tango.DevState.ON
                assert station_beam.adminmode == AdminMode.ONLINE

    return station_beams


@given(
    parsers.parse("{count} MccsAntenna in every station"),
    target_fixture="antennas",
    converters={"count": int},
)
def get_antennas(
    count: int,
    station_names: list[str],
    antennas: dict[str, list[tango.DeviceProxy]],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, list[tango.DeviceProxy]]:
    """
    Check we have a "count" no of MccsAntennas on each station.

    :param count: how many MccsAntennas are required for the test.
    :param station_names: station labels present for the test.
    :param antennas: fixture for list of proxies to MccsAntenna devices, empty if not
        populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: dict of MccsAntenna proxies.
    """
    for station_name in station_names:
        if station_name in antennas.keys():
            antenna_list = antennas[station_name]
        else:
            antenna_list = []
        if len(antenna_list) < count:
            antenna_list = [
                test_context.get_antenna_device(station_name, f"sb1-{antenna_no}")
                for antenna_no in range(1, count + 1)
            ]
        antennas[station_name] = antenna_list

        for antenna_list in antennas.values():
            for antenna in antenna_list:
                set_up_subscriptions(antenna, ["state", "healthState", "adminMode"])
                assert antenna.state() == tango.DevState.ON
                assert antenna.adminmode == AdminMode.ONLINE

    return antennas


@given(
    parsers.parse("MccsStation {station_no}"),
    target_fixture="mccs_stations",
    converters={"station_no": int},
)
def get_mccs_station(
    station_names: list[str],
    station_no: int,
    mccs_stations: dict[str, tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, tango.DeviceProxy]:
    """
    Get specified present MccsStation.

    :param station_names: station labels present for the test.
    :param station_no: station no of the station.
    :param mccs_stations: fixture for list of proxies to MccsStation devices,
        empty if not populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :returns: list of MccsStation device proxies.
    """
    station_name = station_names[station_no - 1]
    mccs_station = test_context.get_station_device(station_name)
    set_up_subscriptions(mccs_station, ["state", "healthState", "adminMode"])
    assert mccs_station.state() == tango.DevState.ON
    assert mccs_station.adminmode == AdminMode.ONLINE
    mccs_stations[station_name] = mccs_station
    return mccs_stations


#
#
#
# ----------------- SPS REPO -----------------
#
#
#


@given(
    parsers.parse("SpsStation {station_no}"),
    target_fixture="sps_stations",
    converters={"station_no": int},
)
def get_sps_station(
    station_names: list[str],
    station_no: int,
    sps_stations: dict[str, tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, tango.DeviceProxy]:
    """
    Get specified present SpsStation.

    :param station_names: station labels present for the test.
    :param station_no: station no of the station.
    :param sps_stations: fixture for list of proxies to SpsStation devices,
        empty if not populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :returns: list of SpsStation device proxies.
    """
    station_name = station_names[station_no - 1]
    sps_station = test_context.get_sps_station_device(station_name)
    set_up_subscriptions(sps_station, ["state", "healthState", "adminMode"])
    assert sps_station.state() == tango.DevState.ON
    assert sps_station.adminmode == AdminMode.ONLINE
    sps_stations[station_name] = sps_station
    return sps_stations


@given(
    parsers.parse("{count} MccsTile in every station"),
    target_fixture="tiles",
    converters={"count": int},
)
def get_tiles(
    count: int,
    station_names: list[str],
    tiles: dict[str, list[tango.DeviceProxy]],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, list[tango.DeviceProxy]]:
    """
    Check we have a "count" no of MccsTiles on each station.

    :param count: how many MccsTiles are required for the test.
    :param station_names: station labels present for the test.
    :param tiles: fixture for list of proxies to MccsTile devices, empty if not
        populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: dict of MccsTile proxies.
    """
    for station_name in station_names:
        if station_name in tiles.keys():
            tile_list = tiles[station_name]
        else:
            tile_list = []
        if len(tile_list) < count:
            tile_list = [
                test_context.get_tile_device(station_name, tile_no)
                for tile_no in range(1, count + 1)
            ]
        tiles[station_name] = tile_list

        for tile_list in tiles.values():
            for tile in tile_list:
                set_up_subscriptions(tile, ["state", "healthState", "adminMode"])
                assert tile.state() == tango.DevState.ON
                assert tile.adminmode == AdminMode.ONLINE

    return tiles


@given(
    parsers.parse("{count} MccsSubrack in every station"),
    target_fixture="subracks",
    converters={"count": int},
)
def get_subracks(
    count: int,
    station_names: list[str],
    subracks: dict[str, list[tango.DeviceProxy]],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, list[tango.DeviceProxy]]:
    """
    Check we have a "count" no of MccsSubracks on each station.

    :param count: how many MccsSubracks are required for the test.
    :param station_names: station labels present for the test.
    :param subracks: fixture for list of proxies to MccsSubrack devices, empty if not
        populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: dict of MccsSubrack proxies.
    """
    for station_name in station_names:
        if station_name in subracks.keys():
            subrack_list = subracks[station_name]
        else:
            subrack_list = []
        if len(subrack_list) < count:
            subrack_list = [
                test_context.get_subrack_device(station_name, subrack_no)
                for subrack_no in range(1, count + 1)
            ]
        subracks[station_name] = subrack_list

        for subrack_list in subracks.values():
            for subrack in subrack_list:
                set_up_subscriptions(subrack, ["state", "healthState", "adminMode"])
                assert subrack.state() == tango.DevState.ON
                assert subrack.adminmode == AdminMode.ONLINE

    return subracks


@given("1 MccsStationCalibrator in every station", target_fixture="station_calibrators")
def get_station_calibrators(
    station_names: list[str],
    station_calibrators: dict[str, tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, tango.DeviceProxy]:
    """
    Check we have a MccsStationCalibrator on each station.

    :param station_names: station labels present for the test.
    :param station_calibrators: fixture for list of proxies to MccsStationCalibrator
        devices, empty if not populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: list of MccsStationCalibrator proxies.
    """
    for station_name in station_names:
        station_calibrators[station_name] = test_context.get_station_calibrator_device(
            station_name
        )

    for station_calibrator in station_calibrators.values():
        set_up_subscriptions(station_calibrator, ["state", "healthState", "adminMode"])
        assert station_calibrator.state() == tango.DevState.ON
        assert station_calibrator.adminmode == AdminMode.ONLINE

    return station_calibrators


@given("1 MccsCalibrationStore in every station", target_fixture="calibration_stores")
def get_calibration_stores(
    station_names: list[str],
    calibration_stores: dict[str, tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, tango.DeviceProxy]:
    """
    Check we have a MccsCalibrationStore on each station.

    :param station_names: station labels present for the test.
    :param calibration_stores: fixture for list of proxies to MccsCalibrationStore
        devices, empty if not populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: list of MccsCalibrationStore proxies.
    """
    for station_name in station_names:
        calibration_stores[station_name] = test_context.get_calibration_store_device(
            station_name
        )

    for calibration_store in calibration_stores.values():
        set_up_subscriptions(calibration_store, ["state", "healthState", "adminMode"])
        if calibration_store.adminmode != AdminMode.ONLINE:
            calibration_store.adminmode = AdminMode.ONLINE
        assert calibration_store.state() == tango.DevState.ON
        assert calibration_store.adminmode == AdminMode.ONLINE

    return calibration_stores


#
#
#
# ----------------- PASD REPO -----------------
#
#
#


@given(
    parsers.parse("FieldStation {station_no}"),
    target_fixture="field_stations",
    converters={"station_no": int},
)
def get_field_station(
    station_names: list[str],
    station_no: int,
    field_stations: dict[str, tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, tango.DeviceProxy]:
    """
    Get specified present FieldStation.

    :param station_names: station labels present for the test.
    :param station_no: station no of this station.
    :param field_stations: fixture for list of proxies to FieldStation devices,
        empty if not populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :returns: list of FieldStation device proxies.
    """
    station_name = station_names[station_no - 1]
    field_station = test_context.get_field_station_device(station_name)
    set_up_subscriptions(field_station, ["state", "healthState", "adminMode"])
    assert field_station.state() == tango.DevState.ON
    assert field_station.adminmode == AdminMode.ONLINE
    field_stations[station_name] = field_station
    return field_stations


@given(
    parsers.parse("{count} MccsSmartbox in every station"),
    target_fixture="smartboxes",
    converters={"count": int},
)
def get_smartboxes(
    count: int,
    station_names: list[str],
    smartboxes: dict[str, list[tango.DeviceProxy]],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, list[tango.DeviceProxy]]:
    """
    Check we have a "count" no of MccsSubracks on each station.

    :param count: how many MccsSubracks are required for the test.
    :param station_names: station labels present for the test.
    :param smartboxes: fixture for list of proxies to MccsSubrack devices, empty if not
        populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: dict of MccsSubrack proxies.
    """
    for station_name in station_names:
        if station_name in smartboxes.keys():
            smartbox_list = smartboxes[station_name]
        else:
            smartbox_list = []
        if len(smartbox_list) < count:
            smartbox_list = [
                test_context.get_smartbox_device(station_name, smartbox_no)
                for smartbox_no in range(1, count + 1)
            ]
        smartboxes[station_name] = smartbox_list

        for smartbox_list in smartboxes.values():
            for smartbox in smartbox_list:
                set_up_subscriptions(smartbox, ["state", "healthState", "adminMode"])
                assert smartbox.state() == tango.DevState.ON
                assert smartbox.adminmode == AdminMode.ONLINE

    return smartboxes


@given("1 MccsFndh in every station", target_fixture="fndhs")
def get_fndhs(
    station_names: list[str],
    fndhs: dict[str, tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, tango.DeviceProxy]:
    """
    Check we have a "count" no of MccsSubracks on each station.

    :param station_names: station labels present for the test.
    :param fndhs: fixture for list of proxies to MccsSubrack devices, empty if not
        populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: dict of MccsSubrack proxies.
    """
    for station_name in station_names:
        fndhs[station_name] = test_context.get_fndh_device(station_name)

    for fndh in fndhs.values():
        set_up_subscriptions(fndh, ["state", "healthState", "adminMode"])
        assert fndh.state() == tango.DevState.ON
        assert fndh.adminmode == AdminMode.ONLINE

    return fndhs


@given("1 MccsPasdBus in every station", target_fixture="pasdbuses")
def get_pasdbuses(
    station_names: list[str],
    pasdbuses: dict[str, tango.DeviceProxy],
    test_context: MccsTangoTestHarnessContext,
    set_up_subscriptions: Callable,
) -> dict[str, tango.DeviceProxy]:
    """
    Check we have a "count" no of MccsSubracks on each station.

    :param station_names: station labels present for the test.
    :param pasdbuses: fixture for list of proxies to MccsSubrack devices, empty if not
        populated by previous Given steps yet.
    :param test_context: the context in which the test is being run.
    :param set_up_subscriptions: helper function for setting up subscriptions.

    :return: dict of MccsSubrack proxies.
    """
    for station_name in station_names:
        pasdbuses[station_name] = test_context.get_pasdbus_device(station_name)

    for pasdbus in pasdbuses.values():
        set_up_subscriptions(pasdbus, ["state", "healthState", "adminMode"])
        assert pasdbus.state() == tango.DevState.ON
        assert pasdbus.adminmode == AdminMode.ONLINE

    return pasdbuses
