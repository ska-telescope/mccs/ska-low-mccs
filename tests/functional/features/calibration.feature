Feature: Test calibration
    Test that calibration solutions can be stored and loaded correctly

    @XTP-25768
    Scenario: Store a calibration solution
        Given an MCCS deployment which is ONLINE and ON
        And 1 stations
        And 1 MccsCalibrationStore in every station
        And the database table is initially empty

        When MccsCalibrationStore 1 is given a solution to store

        Then the solution is stored in the database

    @XTP-25944
    Scenario: Add a calibration solution to existing table
        Given an MCCS deployment which is ONLINE and ON
        And 1 stations
        And 1 MccsCalibrationStore in every station
        And 1 MccsStationCalibrator in every station
        And the calibration store database contains calibration solutions

        When MccsCalibrationStore 1 is given a solution to store

        Then the solution is stored in the database
        And existing data is not overwritten

    @XTP-25989
    Scenario: Load a non-existent calibration solution
        Given an MCCS deployment which is ONLINE and ON
        And 1 stations
        And 1 MccsCalibrationStore in every station
        And the calibration store database contains calibration solutions

        When MccsCalibrationStore 1 tries to get a calibration solution not in the database

        Then the calibration store returns an empty array

    @XTP-25769
    Scenario: Load a calibration solution
        Given an MCCS deployment which is ONLINE and ON
        And 1 stations
        And 1 MccsPasdBus in every station
        And MccsPasdBus 1 is initialised
        And 1 MccsFndh in every station
        And FieldStation 1
        And MccsStation 1
        And 1 MccsCalibrationStore in every station
        And 1 MccsStationCalibrator in every station
        And Station 1 has read the outside temperature
        And the calibration store database contains calibration solutions

        When MccsStationCalibrator 1 tries to get a calibration solution

        Then the correct calibration solution is retrieved

    @XTP-25946
    Scenario: Load a calibration solution with multiple available
        Given an MCCS deployment which is ONLINE and ON
        And 1 stations
        And 1 MccsPasdBus in every station
        And MccsPasdBus 1 is initialised
        And 1 MccsFndh in every station
        And FieldStation 1
        And MccsStation 1
        And 1 MccsCalibrationStore in every station
        And 1 MccsStationCalibrator in every station
        And Station 1 has read the outside temperature
        And the calibration store is set to closest_in_range
        And the calibration store database contains multiple calibration solutions for the same inputs

        When MccsStationCalibrator 1 tries to get a calibration solution

        Then the most recently stored calibration solution is retrieved


    Scenario: Test update database.
        Given we have a the old production database table
        And the old database schema has solutions.

        When we update to the new schema

        Then the database ends up in desired state.