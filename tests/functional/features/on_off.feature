# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

Feature: Test on and off commands
    As a developer,
    I want to test the on and off commands
    So that we can turn the telescope on and off

    @XTP-77142
    Scenario: Turn telescope on
        Given This test is running in a true context
        And we have raised a warning "MCCS has no control over the subrack PDUs. This test is will arrange the devices in the OFF state where possible. However subrack cannot be commanded `OFF`, meaning the lowest drivable state for SpsStation is `STANDBY`."
        And All MccsPasdBus are ONLINE and ON
        And All SpsStation are ONLINE and STANDBY
        And All MccsFndh are ONLINE and ON
        And All FieldStation are ONLINE and OFF
        And All MccsTile are ONLINE and OFF
        And All MccsSubrack are ONLINE and ON
        And All MccsAntenna are ONLINE and OFF
        And All MccsSmartbox are ONLINE and OFF
        And All MccsStation are ONLINE and OFF
        And All MccsController are ONLINE and OFF
        When the MccsController is commanded to turn On
        Then All MccsController report they are ON
        And All MccsSubrack report they are ON
        And All MccsTile report they are ON
        And All SpsStation report they are ON
        And All MccsAntenna report they are ON
        And All MccsSmartbox report they are ON
        And All FieldStation report they are ON
        And All MccsStation report they are ON
        And All MccsTile report that they are in programming state SYNCHRONISED

    @XTP-77143
    Scenario: Turn stations off
        Given This test is running in a true context
        And we have raised a warning "MCCS has no control over the subrack PDUs. This test is will check for the OFF state where possible. However subrack is always `ON`, meaning the lowest state for SpsStation is `STANDBY`."
        And All MccsFndh are ONLINE and ON
        And All MccsPasdBus are ONLINE and ON
        And All MccsTile are ONLINE and ON
        And All MccsSubrack are ONLINE and ON
        And All SpsStation are ONLINE and ON
        And All MccsAntenna are ONLINE and ON
        And All MccsSmartbox are ONLINE and ON
        And All FieldStation are ONLINE and ON
        And All MccsStation are ONLINE and ON
        And All MccsController are ONLINE and ON
        When Most MccsStation are commanded to turn Off
        Then the MccsController is in state ON

    @XTP-77145
    Scenario: Turn telescope off
        Given This test is running in a true context
        And we have raised a warning "MCCS has no control over the subrack PDUs. This test is will check for the OFF state where possible. However subrack is always `ON`, meaning the lowest state for SpsStation is `STANDBY`."
        And All MccsFndh are ONLINE and ON
        And All MccsPasdBus are ONLINE and ON
        And All MccsTile are ONLINE and ON
        And All MccsSubrack are ONLINE and ON
        And All SpsStation are ONLINE and ON
        And All MccsAntenna are ONLINE and ON
        And All MccsSmartbox are ONLINE and ON
        And All FieldStation are ONLINE and ON
        And All MccsStation are ONLINE and ON
        And All MccsController are ONLINE and ON
        When the MccsController is commanded to turn Off
        Then All MccsController report they are OFF
        And All MccsAntenna report they are OFF
        And All MccsSubrack report they are ON
        And All MccsTile report they are OFF
        And All SpsStation report they are STANDBY
        And All MccsSmartbox report they are OFF
        And All FieldStation report they are OFF
        And All MccsStation report they are OFF