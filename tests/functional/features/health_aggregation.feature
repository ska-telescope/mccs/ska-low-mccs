# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

Feature: Test health aggregation
    As a developer,
    I want to test health aggregation
    So that we can ensure proper fault reporting

    Background: MCCS is ONLINE and ON with proxies set up
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 1 MccsStationBeam in every station
        And MccsStation 1
        And MccsStation 2
        And SpsStation 1

    @XTP-77136
    Scenario: Healthy MccsController reports healthy
        Given MccsController is OFFLINE
        When MccsController is turned ONLINE
        Then MccsController reports OK
        And MccsStation 1 reports OK
        And MccsStation 2 reports OK

    @XTP-77137
    Scenario: Failed MccsController reports failed
        Given MccsController reports OK
        When SpsStation 1 reports FAILED
        Then MccsController reports FAILED
        And MccsStation 1 reports FAILED
        And MccsStation 2 reports OK

    @XTP-77138
    Scenario: Healthy MccsSubarray reports healthy
        Given MccsSubarray 1 is OFFLINE
        When MccsSubarray 1 is turned ONLINE
        Then MccsSubarray 1 reports OK
        And MccsSubarrayBeam 1 reports OK
        And MccsSubarrayBeam 2 reports OK
        And MccsStationBeam 1 on station 1 reports OK
        And MccsStationBeam 1 on station 2 reports OK

    @XTP-77139
    Scenario: Failed MccsSubarray reports failed
        Given MccsController has allocated resources to MccsSubarray 1
        And MccsSubarray 1 reports OK
        When SpsStation 1 reports FAILED
        Then MccsSubarray 1 reports FAILED
        And MccsSubarrayBeam 1 reports FAILED
        And MccsSubarrayBeam 2 reports FAILED
        And MccsStationBeam 1 on station 1 reports FAILED
        And MccsStationBeam 1 on station 2 reports OK

    @XTP-77140
    Scenario: Allocate failed resources to Subarray
        Given MccsController has not allocated resources to MccsSubarray 1
        And SpsStation 1 reports FAILED
        And MccsSubarray 1 reports OK
        And MccsSubarrayBeam 1 reports OK
        And MccsSubarrayBeam 2 reports OK
        And MccsStation 1 reports FAILED
        And MccsStation 2 reports OK
        And MccsStationBeam 1 on station 1 reports FAILED
        And MccsStationBeam 1 on station 2 reports OK
        When MccsController allocates resources to MccsSubarray 1
        Then MccsSubarray 1 reports FAILED
        And MccsSubarrayBeam 1 reports FAILED
        And MccsSubarrayBeam 2 reports FAILED
        And MccsStationBeam 1 on station 1 reports FAILED
        And MccsStationBeam 1 on station 2 reports OK
        And MccsStation 1 reports FAILED
        And MccsStation 2 reports OK

    @XTP-77141
    Scenario: Failed MccsSubarray Becomes OK when resources released
        Given MccsController has allocated resources to MccsSubarray 1
        And SpsStation 1 reports FAILED
        And MccsSubarray 1 reports FAILED
        When MccsController releases resources from MccsSubarray 1
        Then MccsSubarray 1 reports OK
        And MccsSubarrayBeam 1 reports OK
        And MccsSubarrayBeam 2 reports OK
        And MccsStationBeam 1 on station 1 reports FAILED
        And MccsStationBeam 1 on station 2 reports OK
        And MccsStation 1 reports FAILED
        And MccsStation 2 reports OK
