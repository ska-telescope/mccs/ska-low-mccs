# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains BDD tests for the health aggregation."""
from __future__ import annotations

import json
from typing import Callable

import tango
from pytest_bdd import given, parsers, scenario, then, when
from ska_control_model import AdminMode, HealthState, ObsState
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

DEFAULT_SPS_PARAMS = {
    "subrack_degraded": 0.05,
    "subrack_failed": 0.2,
    "tile_degraded": 0.05,
    "tile_failed": 0.2,
}


@scenario(
    "features/health_aggregation.feature", "Healthy MccsController reports healthy"
)
def test_healthy_controller_health() -> None:
    """
    Test a healthy MCCS deployment results in a healthy controller.

    Any code in this scenario method is run at the *end* of the
    scenario.
    """


@scenario("features/health_aggregation.feature", "Healthy MccsSubarray reports healthy")
def test_healthy_subarray_health() -> None:
    """
    Test a healthy MCCS deployment results in a healthy subarray.

    Any code in this scenario method is run at the *end* of the
    scenario.
    """


@scenario("features/health_aggregation.feature", "Failed MccsController reports failed")
def test_failed_controller_health(sps_stations: dict[str, tango.DeviceProxy]) -> None:
    """
    Test a FAILED SPS station results in a failed controller.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param sps_stations: dict containing SPS stations.
    """
    for sps_station in sps_stations.values():
        sps_station.healthmodelparams = json.dumps(DEFAULT_SPS_PARAMS)


@scenario(
    "features/health_aggregation.feature",
    "Failed MccsSubarray reports failed",
)
def test_failed_subarray_health(sps_stations: dict[str, tango.DeviceProxy]) -> None:
    """
    Test a subarray fails when an allocated station fails.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param sps_stations: dict containing SPS stations.
    """
    for sps_station in sps_stations.values():
        sps_station.healthmodelparams = json.dumps(DEFAULT_SPS_PARAMS)


@scenario(
    "features/health_aggregation.feature",
    "Allocate failed resources to Subarray",
)
def test_allocate_failed_resources_to_subarray(
    sps_stations: dict[str, tango.DeviceProxy]
) -> None:
    """
    Test a FAILED subarray becomes OK when released.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param sps_stations: dict containing SPS stations.
    """
    for sps_station in sps_stations.values():
        sps_station.healthmodelparams = json.dumps(DEFAULT_SPS_PARAMS)


@scenario(
    "features/health_aggregation.feature",
    "Failed MccsSubarray Becomes OK when resources released",
)
def test_failed_subarray_health_then_release(
    sps_stations: dict[str, tango.DeviceProxy]
) -> None:
    """
    Test a FAILED subarray becomes OK when released.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param sps_stations: dict containing SPS stations.
    """
    for sps_station in sps_stations.values():
        sps_station.healthmodelparams = json.dumps(DEFAULT_SPS_PARAMS)


@given("MccsController is OFFLINE")
def controller_offline(
    controller: tango.DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Turn the MccsController OFFLINE.

    :param controller: controller under test.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.
    """
    if controller.adminmode != AdminMode.OFFLINE:
        controller.adminmode = AdminMode.OFFLINE
        change_event_callbacks[
            f"{controller.dev_name()}/adminMode"
        ].assert_change_event(AdminMode.OFFLINE, consume_nonmatches=True, lookahead=5)
    assert controller.adminmode == AdminMode.OFFLINE


@given(
    parsers.parse("MccsSubarray {subarray_id} is OFFLINE"),
    converters={
        "subarray_id": int,
    },
)
def subarray_offline(
    subarrays: list[tango.DeviceProxy],
    subarray_id: int,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Turn the MccsSubarray OFFLINE.

    :param subarrays: list of subarrays under test.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.
    """
    subarray = subarrays[subarray_id - 1]
    if subarray.adminmode != AdminMode.OFFLINE:
        subarray.adminmode = AdminMode.OFFLINE
        change_event_callbacks[f"{subarray.dev_name()}/adminMode"].assert_change_event(
            AdminMode.OFFLINE, consume_nonmatches=True, lookahead=5
        )
    assert subarray.adminmode == AdminMode.OFFLINE


@when("MccsController is turned ONLINE")
def controller_online(
    controller: tango.DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Turn the MccsController ONLINE.

    :param controller: controller under test.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.
    """
    assert controller.adminmode == AdminMode.OFFLINE
    controller.adminmode = AdminMode.ONLINE
    change_event_callbacks[f"{controller.dev_name()}/adminMode"].assert_change_event(
        AdminMode.ONLINE, consume_nonmatches=True, lookahead=5
    )
    assert controller.adminmode == AdminMode.ONLINE
    change_event_callbacks[f"{controller.dev_name()}/state"].assert_change_event(
        tango.DevState.ON, consume_nonmatches=True, lookahead=10
    )


@when(
    parsers.parse("MccsSubarray {subarray_id} is turned ONLINE"),
    converters={
        "subarray_id": int,
    },
)
def subarray_online(
    subarrays: list[tango.DeviceProxy],
    subarray_id: int,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Turn the MccsSubarray ONLINE.

    :param subarrays: list of subarrays under test.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.
    """
    subarray = subarrays[subarray_id - 1]
    assert subarray.adminmode == AdminMode.OFFLINE
    subarray.adminmode = AdminMode.ONLINE
    change_event_callbacks[f"{subarray.dev_name()}/adminMode"].assert_change_event(
        AdminMode.ONLINE, consume_nonmatches=True, lookahead=5
    )
    assert subarray.adminmode == AdminMode.ONLINE
    change_event_callbacks[f"{subarray.dev_name()}/state"].assert_change_event(
        tango.DevState.ON, consume_nonmatches=True, lookahead=10
    )


@given(
    parsers.parse("SpsStation {station_id} reports FAILED"),
    converters={"station_id": int},
)
@when(
    parsers.parse("SpsStation {station_id} reports FAILED"),
    converters={"station_id": int},
)
def fail_sps_station(
    sps_stations: dict[str, tango.DeviceProxy],
    station_id: int,
    station_names: list[str],
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Fail an SpsStation by requiring 0 tiles to be failed to fail.

    :param sps_stations: dict containing SPS stations to fail.
    :param station_id: id of the station to fail.
    :param station_names: names of the stations in the test.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.
    """
    station_name = station_names[station_id - 1]
    sps_station = sps_stations[station_name]
    sps_station_params = json.loads(sps_station.healthModelParams)
    sps_station_params["tile_failed"] = 0.0
    sps_station.healthModelParams = json.dumps(sps_station_params)
    change_event_callbacks[f"{sps_station.dev_name()}/healthState"].assert_change_event(
        HealthState.FAILED, consume_nonmatches=True, lookahead=10
    )
    assert sps_station.healthState == HealthState.FAILED, sps_station.healthReport


@when(
    parsers.parse("MccsController releases resources from MccsSubarray {subarray_id}"),
    converters={
        "subarray_id": int,
    },
)
def release_resources_step(
    controller: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> None:
    """
    Release resources from MccsController.

    :param controller: MccsController device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    """
    subarray = subarrays[int(subarray_id) - 1]
    release_args = json.dumps({"subarray_id": subarray_id})
    controller.Release(release_args)
    wait_for_lrcs_to_finish()
    wait_for_obsstate(ObsState.EMPTY, subarray)


@then(parsers.parse("MccsController reports {health_name}"))
def check_controller_health_then(
    controller: tango.DeviceProxy,
    health_name: str,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Check MccsController has the required HealthState.

    :param controller: controller under test.
    :param health_name: string name of desired HealthState.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.
    """
    health_state = HealthState[health_name]
    change_event_callbacks[f"{controller.dev_name()}/healthState"].assert_change_event(
        health_state, consume_nonmatches=True, lookahead=10
    )
    assert controller.healthState == health_state, controller.healthReport


@given(parsers.parse("MccsController reports {health_name}"))
def check_controller_health_given(
    controller: tango.DeviceProxy,
    health_name: str,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Check MccsController has the required HealthState.

    :param controller: controller under test.
    :param health_name: string name of desired HealthState.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.
    """
    health_state = HealthState[health_name]
    if controller.healthState != health_state:
        change_event_callbacks[
            f"{controller.dev_name()}/healthState"
        ].assert_change_event(health_state, consume_nonmatches=True, lookahead=10)
    assert controller.healthState == health_state, controller.healthReport


@given(
    parsers.parse("MccsStation {station_id} reports {health_name}"),
    converters={"station_id": int},
)
@then(
    parsers.parse("MccsStation {station_id} reports {health_name}"),
    converters={"station_id": int},
)
def check_mccs_station_health(
    mccs_stations: dict[str, tango.DeviceProxy],
    station_id: int,
    station_names: list[str],
    health_name: str,
) -> None:
    """
    Check MccsStations have the required HealthState.

    :param mccs_stations: dict containing stations under test.
    :param station_id: id of the station.
    :param station_names: names of the stations in the test.
    :param health_name: string name of desired HealthState.
    """
    station_name = station_names[station_id - 1]
    health_state = HealthState[health_name]
    mccs_station = mccs_stations[station_name]
    assert mccs_station.healthState == health_state, mccs_station.healthReport


@given(
    parsers.parse("MccsSubarray {subarray_id} reports {health_name}"),
    converters={"subarray_id": int},
)
@then(
    parsers.parse("MccsSubarray {subarray_id} reports {health_name}"),
    converters={"subarray_id": int},
)
def check_subarray_health(
    subarrays: list[tango.DeviceProxy],
    subarray_id: int,
    health_name: str,
) -> None:
    """
    Check MccsSubarrays have the required HealthState.

    :param subarrays: list containing subarray under test.
    :param subarray_id: ID of the subarray.
    :param health_name: string name of desired HealthState.
    """
    health_state = HealthState[health_name]
    subarray = subarrays[subarray_id - 1]
    assert subarray.healthState == health_state, subarray.healthReport


@given(
    parsers.parse("MccsSubarrayBeam {subarray_beam_id} reports {health_name}"),
    converters={"subarray_beam_id": int},
)
@then(
    parsers.parse("MccsSubarrayBeam {subarray_beam_id} reports {health_name}"),
    converters={"subarray_beam_id": int},
)
def check_subarray_beam_health(
    subarray_beams: list[tango.DeviceProxy],
    subarray_beam_id: int,
    health_name: str,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Check MccsSubarrayBeams have the required HealthState.

    :param subarray_beams: list containing subarray beams under test.
    :param subarray_beam_id: ID of the subarray beam.
    :param health_name: string name of desired HealthState.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.
    """
    health_state = HealthState[health_name]
    subarray_beam = subarray_beams[subarray_beam_id - 1]
    if subarray_beam.healthState != health_state:
        change_event_callbacks[
            f"{subarray_beam.dev_name()}/healthState"
        ].assert_change_event(health_state, consume_nonmatches=True, lookahead=10)
    assert subarray_beam.healthState == health_state, subarray_beam.healthReport


@given(
    parsers.parse(
        "MccsStationBeam {station_beam_id} on "
        "station {station_id} reports {health_name}"
    ),
    converters={"station_beam_id": int, "station_id": int},
)
@then(
    parsers.parse(
        "MccsStationBeam {station_beam_id} on "
        "station {station_id} reports {health_name}"
    ),
    converters={"station_beam_id": int, "station_id": int},
)
def check_station_beam_health(
    station_beams: dict[str, list[tango.DeviceProxy]],
    station_beam_id: int,
    station_id: int,
    station_names: list[str],
    health_name: str,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Check MccsStationBeams have the required HealthState.

    :param station_beams: dictionary containing station beams under test.
    :param station_beam_id: ID of the station beam.
    :param station_id: id of the station containing the station beam.
    :param station_names: names of the stations in the test.
    :param health_name: string name of desired HealthState.
    :param change_event_callbacks: a dictionary of callables to be used as
        tango change event callbacks.
    """
    station_name = station_names[station_id - 1]
    health_state = HealthState[health_name]
    station_beam = station_beams[station_name][station_beam_id - 1]
    if station_beam.healthState != health_state:
        change_event_callbacks[
            f"{station_beam.dev_name()}/healthState"
        ].assert_change_event(health_state, consume_nonmatches=True, lookahead=10)
    assert station_beam.healthState == health_state, station_beam.healthReport
