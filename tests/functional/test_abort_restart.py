# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains BDD tests for a "Controller Only" deployment of MCCS."""
from __future__ import annotations

from typing import Callable

import tango
from pytest_bdd import parsers, scenarios, then, when
from ska_control_model import ObsState

from tests.functional.helpers.obs_helpers import _flatten

scenarios("features/abort_and_restart.feature")


@when(
    parsers.parse("MccsSubarray {subarray_id} aborts its observation"),
    converters={"subarray_id": int},
)
def abort_subarray(
    subarrays: list[tango.DeviceProxy],
    subarray_id: int,
    wait_for_lrcs_to_finish: Callable,
) -> None:
    """
    Abort a MccsSubarray.

    :param subarrays: A 0-indexed list of Subarrays present.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    """
    subarray = subarrays[subarray_id - 1]
    subarray.abort()
    wait_for_lrcs_to_finish()


@then(
    parsers.parse("MccsSubarray {subarray_id} report ABORTED"),
    converters={"subarray_id": int},
)
def check_subarray_aborted(
    subarrays: list[tango.DeviceProxy],
    subarray_id: int,
    wait_for_obsstate: Callable,
) -> None:
    """
    Check a MccsSubarray reaches ABORTED.

    :param subarrays: A 0-indexed list of Subarrays present.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    subarray = subarrays[subarray_id - 1]
    wait_for_obsstate(ObsState.ABORTED, subarray)


@then(
    parsers.parse("The {subarray_beam_count} MccsSubarrayBeam report ABORTED"),
    converters={"subarray_beam_count": int},
)
def check_subarray_beam_aborted(
    subarray_beams: list[tango.DeviceProxy],
    subarray_beam_count: int,
    wait_for_obsstate: Callable,
) -> None:
    """
    Check MccsSubarrayBeams reach ABORTED.

    :param subarray_beams: A 0-indexed list of SubarraysBeams present.
    :param subarray_beam_count: number of SubarrayBeams to check.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    aborted_subarray_beams = subarray_beams[0:subarray_beam_count]
    for aborted_subarray_beam in aborted_subarray_beams:
        wait_for_obsstate(ObsState.ABORTED, aborted_subarray_beam)


@then(parsers.parse("The {station_beam_count} MccsStationBeam report ABORTED"))
def check_station_beam_aborted(
    station_beams: dict[str, list[tango.DeviceProxy]],
    subarray_beams: list[tango.DeviceProxy],
    resources_to_allocate: dict,
    wait_for_obsstate: Callable,
) -> None:
    """
    Check allocated MccsStationBeams reach ABORTED.

    :param station_beams: Dictionary of StationBeams use in the test.
    :param subarray_beams: A 0-indexed list of SubarraysBeams present.
    :param resources_to_allocate: non-compliant dictionary of resources to allocate.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    for subarray_beam_id in resources_to_allocate:
        subarray_beam_proxy = subarray_beams[subarray_beam_id - 1]
        for station_beam in _flatten(station_beams.values()):
            if (
                station_beam.dev_name().split("/")[-1]
                in subarray_beam_proxy.stationbeamids
            ):
                wait_for_obsstate(ObsState.ABORTED, station_beam)


@when(
    parsers.parse("MccsController restarts MccsSubarray {subarray_id}"),
    converters={"subarray_id": int},
)
def restart_subarray(
    controller: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    subarray_id: int,
) -> None:
    """
    Restart a MccsSubarray.

    :param controller: MccsController to use in this test.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    """
    controller.RestartSubarray(subarray_id)
    wait_for_lrcs_to_finish()


@then(
    parsers.parse("MccsSubarray {subarray_id} report EMPTY"),
    converters={"subarray_id": int},
)
def check_subarray_empty(
    subarrays: list[tango.DeviceProxy],
    subarray_id: int,
    wait_for_obsstate: Callable,
) -> None:
    """
    Check a MccsSubarray reaches EMPTY.

    :param subarrays: A 0-indexed list of Subarrays present.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    subarray = subarrays[subarray_id - 1]
    wait_for_obsstate(ObsState.EMPTY, subarray)


@then(
    parsers.parse("The {subarray_beam_count} MccsSubarrayBeam report EMPTY"),
    converters={"subarray_beam_count": int},
)
def check_subarray_beam_empty(
    subarray_beams: list[tango.DeviceProxy],
    subarray_beam_count: int,
    wait_for_obsstate: Callable,
) -> None:
    """
    Check MccsSubarrayBeams reach EMPTY.

    :param subarray_beams: A 0-indexed list of SubarraysBeams present.
    :param subarray_beam_count: number of SubarrayBeams to check.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    aborted_subarray_beams = subarray_beams[0:subarray_beam_count]
    for aborted_subarray_beam in aborted_subarray_beams:
        wait_for_obsstate(ObsState.EMPTY, aborted_subarray_beam)


@then(parsers.parse("The {station_beam_count} MccsStationBeam report EMPTY"))
def check_station_beam_empty(
    station_beams: dict[str, list[tango.DeviceProxy]],
    wait_for_obsstate: Callable,
) -> None:
    """
    Check all MccsStationBeams reach EMPTY.

    :param station_beams: Dictionary of StationBeams use in the test.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    for station_beam in _flatten(station_beams.values()):
        wait_for_obsstate(ObsState.EMPTY, station_beam)


@then("MccsController reports no resources allocated")
def check_controller_no_resources(
    controller: tango.DeviceProxy, wait_for_lrcs_to_finish: Callable
) -> None:
    """
    Check MccsController reports no resources allocated to the MccsSubarray.

    :param controller: MccsController device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    """
    wait_for_lrcs_to_finish()
    assert controller.GetAssignedResources(1) == "{}"


@then(
    "The 4 MccsStationBeam report unconfigured",
    converters={"station_beam_count": int},
)
def check_resources_unallocated(
    station_beams: dict[str, list[tango.DeviceProxy]],
    wait_for_lrcs_to_finish: Callable,
) -> None:
    """
    Check each MccsStationBeam not configurated after deconfiguration by MccsSubarray.

    :param station_beams: list of MccsStationBeam device proxies for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    """
    wait_for_lrcs_to_finish()
    for station_beam in _flatten(station_beams.values()):
        wait_for_lrcs_to_finish()
        assert station_beam.apertureId == "AP000.00"
        assert station_beam.subarrayId == 0


@then("The MccsTiles report that the beamformer is not running")
def beamformer_not_running(tiles: dict[str, list[tango.DeviceProxy]]) -> None:
    """
    Verify the beamformer is not running on the tile.

    :param tiles: the MccsTiles device proxies for use in the test.
    """
    for tile in _flatten(tiles.values()):
        assert not tile.isBeamformerRunning


@when(
    parsers.parse("a MccsStationBeam on MccsSubarrayBeam {subarray_beam_id} FAULTS"),
    converters={"subarray_beam_id": int},
)
def fault_station_beam(
    station_beams: dict[str, list[tango.DeviceProxy]],
    subarray_beam_id: int,
    subarray_beams: list[tango.DeviceProxy],
    wait_for_obsstate: Callable,
) -> None:
    """
    Put one StationBeam into FAULT state.

    :param station_beams: dictionary of StationBeams use in the test.
    :param subarray_beams: A 0-indexed list of SubarraysBeams present.
    :param subarray_beam_id: 1-indexed id of the SubarrayBeam to FAULT.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    subarray_beam = subarray_beams[subarray_beam_id - 1]
    for station_beam in _flatten(station_beams.values()):
        if station_beam.dev_name().split("/")[-1] in subarray_beam.stationbeamids:
            station_beam.ToFault()
            wait_for_obsstate(ObsState.FAULT, station_beam)
            return


@then(
    parsers.parse("MccsSubarrayBeam {subarray_beam_id} reports FAULT"),
    converters={"subarray_beam_id": int},
)
def check_subarray_beam_fault(
    subarray_beams: list[tango.DeviceProxy],
    subarray_beam_id: int,
    wait_for_obsstate: Callable,
) -> None:
    """
    Check the SubarrayBeam with the StationBeam which FAULTED also FAULTS.

    :param subarray_beams: A 0-indexed list of SubarraysBeams present.
    :param subarray_beam_id: 1-indexed id of the SubarrayBeam to FAULT.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    wait_for_obsstate(ObsState.FAULT, subarray_beams[subarray_beam_id - 1])


@then(
    parsers.parse("MccsSubarrayBeam {subarray_beam_id} reports SCANNING"),
    converters={"subarray_beam_id": int},
)
def check_subarray_beam_scanning(
    subarray_beams: list[tango.DeviceProxy],
    subarray_beam_id: int,
    wait_for_obsstate: Callable,
) -> None:
    """
    Check the SubarrayBeam with the StationBeam which didn't FAULTED didn't FAULTS.

    :param subarray_beams: A 0-indexed list of SubarraysBeams present.
    :param subarray_beam_id: 1-indexed id of the SubarrayBeam to FAULT.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    wait_for_obsstate(ObsState.SCANNING, subarray_beams[subarray_beam_id - 1])


@then(
    parsers.parse("MccsSubarray {subarray_id} reports FAULT"),
    converters={"subarray_id": int},
)
def check_subarray_fault(
    subarrays: list[tango.DeviceProxy],
    subarray_id: int,
    wait_for_obsstate: Callable,
) -> None:
    """
    Check the Subarray FAULTS.

    :param subarrays: A 0-indexed list of Subarrays present.
    :param subarray_id: 1-indexed id of the Subarray which FAULTED.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    wait_for_obsstate(ObsState.FAULT, subarrays[subarray_id - 1])
