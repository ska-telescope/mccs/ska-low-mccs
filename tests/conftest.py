# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
This module contains pytest fixtures other test setups.

These are common to all ska-low-mccs tests: unit, integration and
functional (BDD).
"""
from __future__ import annotations

import json
import logging
import time
from typing import Set, cast

import _pytest
import pytest
import tango
import yaml
from ska_control_model import TaskStatus
from ska_tango_testing.mock import MockCallableGroup
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

pytest_plugins = [
    "tests.functional.fixtures.generic.generic_bdd_steps",
    "tests.functional.fixtures.generic.generic_fixtures",
    "tests.functional.fixtures.obs_related.obs_bdd_steps",
    "tests.functional.fixtures.obs_related.obs_fixtures",
]


def pytest_sessionstart(session: pytest.Session) -> None:
    """
    Pytest hook; prints info about tango version.

    :param session: a pytest Session object
    """
    print(tango.utils.info())


with open("tests/testbeds.yaml", "r", encoding="utf-8") as stream:
    _testbeds: dict[str, set[str]] = yaml.safe_load(stream)


# TODO: pytest is partially typehinted but does not yet export Config
def pytest_configure(
    config: _pytest.config.Config,  # type: ignore[name-defined]
) -> None:
    """
    Register custom markers to avoid pytest warnings.

    :param config: the pytest config object
    """
    all_tags: Set[str] = cast(Set[str], set()).union(*_testbeds.values())
    for tag in all_tags:
        config.addinivalue_line("markers", f"needs_{tag}")


def pytest_addoption(
    parser: _pytest.config.argparsing.Parser,
) -> None:
    """
    Implement the add the `--testbed` option.

    Used to specify the context in which the test is running. This could
    be used, for example, to skip tests that have requirements not met
    by the context.

    :param parser: the command line options parser
    """
    parser.addoption(
        "--testbed",
        choices=_testbeds.keys(),
        default="test",
        help="Specify the testbed on which the tests are running.",
    )

    # This is a pytest hook, here implemented to add the `--true-context`
    # option, used to indicate that a true Tango subsystem is available,
    # so there is no need for the test harness to spin up a Tango test
    # context.

    parser.addoption(
        "--true-context",
        action="store_true",
        default=False,
        help=(
            "Tell pytest that you have a true Tango context and don't "
            "need to spin up a Tango test context"
        ),
    )


def pytest_collection_modifyitems(
    config: _pytest.config.Config,
    items: list[pytest.Item],
) -> None:
    """
    Modify the list of tests to be run, after pytest has collected them.

    This hook implementation skips tests that are marked as needing some
    tag that is not provided by the current test context, as specified
    by the "--testbed" option.

    For example, if we have a hardware test that requires the presence
    of a real TPM, we can tag it with "@needs_tpm". When we run in a
    "test" context (that is, with "--testbed test" option), the test
    will be skipped because the "test" context does not provide a TPM.
    But when we run in "pss" context, the test will be run because the
    "pss" context provides a TPM.

    :param config: the pytest config object
    :param items: list of tests collected by pytest
    """
    testbed = config.getoption("--testbed")
    available_tags = _testbeds.get(testbed, set())

    prefix = "needs_"
    for item in items:
        needs_tags = set(
            tag[len(prefix) :] for tag in item.keywords if tag.startswith(prefix)
        )
        unmet_tags = list(needs_tags - available_tags)
        if unmet_tags:
            item.add_marker(
                pytest.mark.skip(
                    reason=(
                        f"Testbed '{testbed}' does not meet test needs: "
                        f"{unmet_tags}."
                    )
                )
            )


@pytest.fixture(scope="session", name="logger")
def logger_fixture() -> logging.Logger:
    """
    Fixture that returns a default logger.

    :return: a logger
    """
    debug_logger = logging.getLogger()
    debug_logger.setLevel(logging.DEBUG)
    return debug_logger


@pytest.fixture(name="obs_command_timeout")
def obs_command_timeout_fixture() -> int:
    """
    Return the obs command timeout in seconds.

    :return: the obs command timeout in seconds.
    """
    return 60


@pytest.fixture(name="callbacks")
def callbacks_fixture() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "communication_state",
        "component_state",
        "task",
        timeout=15.0,
    )


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :return: a callback group.
    """
    return MockTangoEventCallbackGroup(
        "healthState",
        "adminMode",
        "state",
        "longRunningCommandResult",
        "obsState",
        timeout=15.0,
    )


@pytest.fixture(name="station_id")
def station_id_fixture() -> int:
    """
    Return the station id of this station.

    :return: the station id of this station.
    """
    # TODO: This must match the StationId property of the station under
    # test. We should refactor the harness so that we can pull it
    # straight from the device configuration.
    return 1


@pytest.fixture(name="calibration_solutions")
def calibration_solutions_fixture(
    frequency_channel: int, outside_temperature: float, station_id: int
) -> dict[tuple[int, float, int], list[float]]:
    """
    Fixture that provides sample calibration solutions.

    :param outside_temperature: a fixture with an outside temperature
    :param frequency_channel: a fixture with a calibration frequency channel
    :param station_id: the id of the station under test.

    :return: a sample calibration solution. The keys are tuples of the channel
        and the outside temperature, and the values are lists of calibration values
    """
    return {
        (frequency_channel, outside_temperature, station_id): [float(1)]
        + [0.5 * i for i in range(2048)],
        (23, 25.0, station_id): [float(1)] + [0.5 * i for i in range(2048)],
        (45, 25.0, station_id): [float(3)] + [1.2 * (i % 2) for i in range(2048)],
        (23, 30.0, station_id): [float(5)] + [0.6 * i for i in range(2048)],
        (45, 30.0, station_id): [float(4)] + [1.4 * (i % 2) for i in range(2048)],
        (23, 35.0, station_id): [float(6)] + [0.7 * i for i in range(2048)],
        (45, 35.0, station_id): [float(2)] + [1.6 * (i % 2) for i in range(2048)],
    }


@pytest.fixture(name="outside_temperature")
def outside_temperature_fixture() -> float:
    """
    Fixture for the outside temperature.

    :return: the outside temperature
    """
    return 25.34


@pytest.fixture(name="frequency_channel")
def frequency_channel_fixture() -> int:
    """
    Fixture for the calibration frequency channel.

    :return: the frequency channel
    """
    return 23


@pytest.fixture(name="database_host")
def database_host_fixture() -> str:
    """
    Fixture that provides the database host.

    :return: the database host
    """
    return "station-calibration-postgresql"


@pytest.fixture(name="database_port")
def database_port_fixture() -> int:
    """
    Fixture that provides the database port.

    :return: the database port
    """
    return 5432


@pytest.fixture(name="database_name")
def database_name_fixture() -> str:
    """
    Fixture that provides the database name.

    :return: the database name
    """
    return "postgres"


@pytest.fixture(name="station_config_path")
def station_config_path_fixture() -> list[str]:
    """
    Fixture that provides the telmodel url and path.

    :return: the telmodel url and path
    """
    return [
        "gitlab://gitlab.com/ska-telescope/ska-low-tmdata?main#tmdata",
        "stations/s9-2.yaml",
    ]


@pytest.fixture(name="database_admin_user")
def database_admin_user_fixture() -> str:
    """
    Fixture that provides the database admin user.

    :return: the database admin user
    """
    return "postgres"


@pytest.fixture(name="database_admin_password")
def database_admin_password_fixture() -> str:
    """
    Fixture that provides the database admin password.

    :return: the database admin password
    """
    return "secretpassword"


def ensure_command_result(
    proxy: tango.DeviceProxy, timeout: int, endpoint: TaskStatus, cmd_id: str
) -> None:
    """
    Check we reach the final state in timeout period.

    :param proxy: The device proxy of interest
    :param timeout: A timeout period in seconds
    :param endpoint: The TaskStatus we want to arrive at.
    :param cmd_id: A unique id to reference progress of
        LongRunningCommand.
    """
    deadline = time.time() + timeout
    # Wait for the command to COMPLETE, FAIL otherwise.
    lrc_endpoints = [
        TaskStatus.FAILED.name,
        TaskStatus.ABORTED.name,
        TaskStatus.NOT_FOUND.name,
        TaskStatus.REJECTED.name,
        TaskStatus.COMPLETED.name,
    ]
    while time.time() < deadline:
        current_status = proxy.checklongrunningcommandstatus(cmd_id)
        if current_status in lrc_endpoints:
            if current_status != endpoint.name:
                pytest.fail(
                    "Command "
                    f"`{proxy.checklongrunningcommandstatus(cmd_id)}` "
                    "reporting "
                    f"`{json.loads(proxy.longrunningcommandresult[-1])[-1]}`. "
                )
            break
        time.sleep(1)
    else:
        pytest.fail("Command check was not acheived in {timeout} seconds")
