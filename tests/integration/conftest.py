# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains pytest-specific test harness for MCCS integration tests."""
from __future__ import annotations

import pytest


def pytest_itemcollected(item: pytest.Item) -> None:
    """
    Modify a test after it has been collected by pytest.

    This hook implementation adds the "forked" custom mark to all tests
    that use the `tango_harness` fixture, causing them to be sandboxed
    in their own process.

    :param item: the collected test for which this hook is called
    """
    if "tango_harness" in item.fixturenames:  # type: ignore[attr-defined]
        item.add_marker("forked")


@pytest.fixture(name="controller_trl")
def controller_trl_fixture() -> str:
    """
    Return a Controller TRL.

    :return: A Controller TRL.
    """
    return "low-mccs/control/control"


@pytest.fixture(name="station_trls")
def station_trls_fixture() -> list[str]:
    """
    Return a list of Station TRLs.

    :return: A list of Station TRLs.
    """
    return ["low-mccs/station/001", "low-mccs/station/002"]


@pytest.fixture(name="subarray_trls")
def subarray_trls_fixture() -> list[str]:
    """
    Return a list of Subarray TRLs.

    :return: A list of Subarray TRLs.
    """
    return ["low-mccs/subarray/01", "low-mccs/subarray/02"]


@pytest.fixture(name="subarray_beam_trls")
def subarray_beam_trls_fixture() -> list[str]:
    """
    Return A list of SubarrayBeam TRLs.

    :return: A list of SubarrayBeam TRLs.
    """
    return ["low-mccs/subarraybeam/01", "low-mccs/subarraybeam/02"]


@pytest.fixture(name="station_beam_trls")
def station_beam_trls_fixture() -> list[str]:
    """
    Return a list of StationBeam TRLs.

    :return: A list of StationBeam TRLs.
    """
    return [
        "low-mccs/beam/01",
        "low-mccs/beam/02",
        "low-mccs/beam/03",
        "low-mccs/beam/04",
    ]


@pytest.fixture(name="station_id")
def smartbox_id_fixture() -> str:
    """
    Return the ID of the antenna's Field station device.

    :return: the ID of the antenna's Field station device.
    """
    return "1"


@pytest.fixture(name="antenna_id")
def antenna_id_fixture() -> int:
    """
    Return the id of this antenna (0-256).

    :return: the port this antenna is attached.
    """
    # TODO: This must match the SmartBoxPort property of the
    # antenna device. We should refactor the harness so that we can pull
    # it straight from the device configuration.
    return 1


@pytest.fixture(name="field_station_trls")
def fndh_trls_fixture() -> list[str]:
    """
    Return a list of fieldstations TRLs.

    :return: A list of fieldstations TRLs.
    """
    return ["low-mccs/fieldstation/001", "low-mccs/fieldstation/002"]
