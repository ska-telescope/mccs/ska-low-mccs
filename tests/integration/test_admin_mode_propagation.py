# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains integration tests for AdminMode propagation."""

from __future__ import annotations

import gc
import json
import unittest.mock
from typing import Iterator

import pytest
import tango
from ska_control_model import AdminMode, HealthState, ObsState
from ska_low_mccs_common.testing.mock import MockDeviceBuilder
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_controller_trl,
    get_subarray_trl,
)

gc.disable()


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :return: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "station_adminMode",
        "controller_adminMode",
        "controller_state",
        "subarray_adminMode",
        "subarray_obsState",
        "subarray_state",
        "subarray_beam_adminMode",
        "subarray_beam_state",
        "station_beam_adminMode",
        "station_beam_state",
        "station_calibrator_adminMode",
        timeout=20.0,
        assert_no_error=False,
    )


@pytest.fixture(name="mock_device")
def mock_device_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a mock fieldStation.

    It is on so MccsStation is on.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    return builder()


@pytest.fixture(name="test_context")
def test_context_fixture(
    mock_device: unittest.mock.Mock,
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which subarray and station beam Tango devices are running.

    :param mock_device: a mock device.

    :yields: a test context.
    """
    station_name = "ci-1"

    harness = MccsTangoTestHarness()
    harness.set_controller_device(
        subarrays=[1],
        subarray_beams=[1],
        station_beams=[(station_name, 1)],
        stations=[station_name],
    )
    harness.add_subarray_device(1)
    harness.add_subarray_beam_device(1)
    harness.add_station_device(station_name, 1, [])
    harness.add_station_calibrator_device(station_name, 1)
    harness.add_station_beam_device(station_name, 1)

    harness.add_mock_calibration_store_device(station_name, mock_device)
    harness.add_mock_solver_device(station_name, mock_device)
    harness.add_mock_field_station_device(station_name, mock_device)
    harness.add_mock_sps_station_device(station_name, mock_device)

    with harness as context:
        yield context


@pytest.fixture(name="subarray_beam_device")
def subarray_beam_device_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return subarray beam device.

    :param test_context: test context.

    :return: subarray beam device.
    """
    return test_context.get_subarray_beam_device(1)


@pytest.fixture(name="station_beam_device")
def station_beam_device_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return station beam device.

    :param test_context: test context.

    :return: station beam device.
    """
    return test_context.get_station_beam_device("ci-1", 1)


@pytest.fixture(name="station_device")
def station_device_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return station device.

    :param test_context: test context.

    :return: station device.
    """
    return test_context.get_station_device("ci-1")


@pytest.fixture(name="station_calibrator_device")
def station_calibrator_device_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return station calibrator device.

    :param test_context: test context.

    :return: station calibrator device.
    """
    return test_context.get_station_calibrator_device("ci-1")


@pytest.fixture(name="controller_device")
def controller_device_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return controller device.

    :param test_context: test context.

    :return: controller device.
    """
    return test_context.get_controller_device()


@pytest.fixture(name="subarray_device")
def subarray_device_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return subarray device.

    :param test_context: test context.

    :return: subarray device.
    """
    return test_context.get_subarray_device(1)


class TestAdminModePropagation:
    """Test adminMode propagation."""

    def test_station_mode_propagation(
        self: TestAdminModePropagation,
        controller_device: tango.DeviceProxy,
        station_device: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test AdminMode propagation for station.

        :param controller_device: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param station_device: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param change_event_callbacks: group of Tango change event
            callback with asynchronous support
        """
        station_device.inheritModes = True

        assert controller_device.adminMode == AdminMode.OFFLINE
        assert station_device.adminMode == AdminMode.OFFLINE

        station_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["station_adminMode"],
        )
        change_event_callbacks["station_adminMode"].assert_change_event(
            AdminMode.OFFLINE
        )
        controller_device.adminMode = AdminMode.ONLINE
        change_event_callbacks["station_adminMode"].assert_change_event(
            AdminMode.ONLINE
        )
        assert station_device.adminMode == AdminMode.ONLINE

        controller_device.adminMode = AdminMode.OFFLINE
        change_event_callbacks.assert_change_event(
            "station_adminMode", AdminMode.OFFLINE
        )

        # Turn inheritance off by setting directly
        station_device.adminMode = AdminMode.ENGINEERING

        change_event_callbacks.assert_change_event(
            "station_adminMode", AdminMode.ENGINEERING, lookahead=2
        )

        controller_device.adminMode = AdminMode.ONLINE
        change_event_callbacks["station_adminMode"].assert_not_called()
        assert station_device.adminMode == AdminMode.ENGINEERING

        # Go back to inheriting
        station_device.inheritModes = True

        change_event_callbacks.assert_change_event(
            "station_adminMode", AdminMode.ONLINE
        )
        assert station_device.adminMode == AdminMode.ONLINE

        controller_device.adminMode = AdminMode.OFFLINE
        change_event_callbacks.assert_change_event(
            "station_adminMode", AdminMode.OFFLINE
        )
        assert station_device.adminMode == AdminMode.OFFLINE

    def test_station_calibrator_mode_propagation(
        self: TestAdminModePropagation,
        station_device: tango.DeviceProxy,
        station_calibrator_device: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test AdminMode propagation for station calibrator.

        :param station_device: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param station_calibrator_device: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param change_event_callbacks: group of Tango change event
            callback with asynchronous support
        """
        station_calibrator_device.inheritModes = True

        assert station_device.adminMode == AdminMode.OFFLINE
        assert station_calibrator_device.adminMode == AdminMode.OFFLINE

        station_calibrator_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["station_calibrator_adminMode"],
        )
        change_event_callbacks["station_calibrator_adminMode"].assert_change_event(
            AdminMode.OFFLINE
        )
        station_device.adminMode = AdminMode.ONLINE
        change_event_callbacks["station_calibrator_adminMode"].assert_change_event(
            AdminMode.ONLINE
        )
        assert station_calibrator_device.adminMode == AdminMode.ONLINE

        station_device.adminMode = AdminMode.OFFLINE
        change_event_callbacks.assert_change_event(
            "station_calibrator_adminMode", AdminMode.OFFLINE
        )

        # Turn inheritance off by setting directly
        station_calibrator_device.adminMode = AdminMode.ENGINEERING

        change_event_callbacks.assert_change_event(
            "station_calibrator_adminMode", AdminMode.ENGINEERING, lookahead=2
        )

        station_device.adminMode = AdminMode.ONLINE
        change_event_callbacks["station_calibrator_adminMode"].assert_not_called()
        assert station_calibrator_device.adminMode == AdminMode.ENGINEERING

        # Go back to inheriting
        station_calibrator_device.inheritModes = True

        change_event_callbacks.assert_change_event(
            "station_calibrator_adminMode", AdminMode.ONLINE
        )
        assert station_device.adminMode == AdminMode.ONLINE

        station_device.adminMode = AdminMode.OFFLINE
        change_event_callbacks.assert_change_event(
            "station_calibrator_adminMode", AdminMode.OFFLINE
        )
        assert station_device.adminMode == AdminMode.OFFLINE

    # pylint: disable=too-many-arguments
    def test_obs_device_mode_propagation(
        self: TestAdminModePropagation,
        controller_device: tango.DeviceProxy,
        subarray_device: tango.DeviceProxy,
        subarray_beam_device: tango.DeviceProxy,
        station_beam_device: tango.DeviceProxy,
        station_device: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test AdminMode propagation for the obs devices during allocation.

        :param controller_device: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param subarray_device: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param subarray_beam_device: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param station_beam_device: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param station_device: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param change_event_callbacks: group of Tango change event
            callback with asynchronous support
        """
        subarray_beam_device.inheritModes = True
        station_beam_device.inheritModes = True
        station_device.inheritModes = True
        assert controller_device.adminMode == AdminMode.OFFLINE
        assert subarray_device.adminMode == AdminMode.OFFLINE
        assert subarray_beam_device.adminMode == AdminMode.OFFLINE
        assert station_beam_device.adminMode == AdminMode.OFFLINE

        # Controller should initially be DISABLE as it is OFFLINE
        controller_device.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["controller_state"],
        )
        change_event_callbacks["controller_state"].assert_change_event(
            tango.DevState.DISABLE
        )

        station_beam_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["station_beam_adminMode"],
        )
        # Consuming initial event
        change_event_callbacks["station_beam_adminMode"].assert_change_event(
            AdminMode.OFFLINE
        )
        subarray_beam_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_beam_adminMode"],
        )
        # Consuming initial event
        change_event_callbacks["subarray_beam_adminMode"].assert_change_event(
            AdminMode.OFFLINE
        )
        subarray_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_adminMode"],
        )
        # consuming initial event
        change_event_callbacks["subarray_adminMode"].assert_change_event(
            AdminMode.OFFLINE
        )

        # Set controller online
        controller_device.adminMode = AdminMode.ONLINE

        # Before allocation, these devices will have the controller as their parent
        change_event_callbacks["station_beam_adminMode"].assert_change_event(
            AdminMode.ONLINE
        )
        change_event_callbacks["subarray_beam_adminMode"].assert_change_event(
            AdminMode.ONLINE
        )
        assert subarray_beam_device.adminMode == AdminMode.ONLINE
        assert station_beam_device.adminMode == AdminMode.ONLINE
        subarray_device.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_obsState"],
        )
        change_event_callbacks["subarray_obsState"].assert_change_event(ObsState.EMPTY)
        subarray_device.adminMode = AdminMode.ONLINE
        change_event_callbacks["subarray_adminMode"].assert_change_event(
            AdminMode.ONLINE
        )

        # We have mocked SpsStation and FieldStation to ON, so controller should come on
        change_event_callbacks["controller_state"].assert_change_event(
            tango.DevState.UNKNOWN
        )
        change_event_callbacks["controller_state"].assert_change_event(
            tango.DevState.ON
        )

        allocate_arg = {
            "interface": "https://schema.skao.int/ska-low-mccs-controller-allocate/3.0",
            "subarray_id": 1,
            "subarray_beams": [
                {
                    "subarray_beam_id": 1,
                    "apertures": [
                        {
                            "station_id": 1,
                            "aperture_id": "AP001.01",
                        },
                    ],
                    "number_of_channels": 8,
                },
            ],
        }
        controller_device.allocate(json.dumps(allocate_arg))

        # In allocation, subarray is first released, we go EMPTY -> RESOURCING -> EMPTY
        change_event_callbacks["subarray_obsState"].assert_change_event(
            ObsState.RESOURCING
        )
        change_event_callbacks["subarray_obsState"].assert_change_event(ObsState.EMPTY)

        # After which, we will go RESOURCING -> IDLE
        change_event_callbacks["subarray_obsState"].assert_change_event(
            ObsState.RESOURCING
        )
        change_event_callbacks["subarray_obsState"].assert_change_event(ObsState.IDLE)

        # The beam devices should now have the subarray as their parent
        subarray_device.adminmode = AdminMode.ENGINEERING

        change_event_callbacks["station_beam_adminMode"].assert_change_event(
            AdminMode.ENGINEERING
        )
        change_event_callbacks["subarray_beam_adminMode"].assert_change_event(
            AdminMode.ENGINEERING
        )
        assert subarray_beam_device.adminMode == AdminMode.ENGINEERING
        assert station_beam_device.adminMode == AdminMode.ENGINEERING

        assert subarray_beam_device.parenttrl == get_subarray_trl(1)
        assert station_beam_device.parenttrl == get_subarray_trl(1)

        # Releasing the resources assigned to the subarray should result in the beam
        # devices going back to having the controller as their parent.
        controller_device.releaseall()
        change_event_callbacks["subarray_obsState"].assert_change_event(
            ObsState.RESOURCING
        )
        change_event_callbacks["subarray_obsState"].assert_change_event(ObsState.EMPTY)

        change_event_callbacks["station_beam_adminMode"].assert_change_event(
            AdminMode.ONLINE
        )
        change_event_callbacks["subarray_beam_adminMode"].assert_change_event(
            AdminMode.ONLINE
        )
        assert subarray_beam_device.adminMode == AdminMode.ONLINE
        assert station_beam_device.adminMode == AdminMode.ONLINE

        assert subarray_beam_device.parenttrl == get_controller_trl()
        assert station_beam_device.parenttrl == get_controller_trl()
