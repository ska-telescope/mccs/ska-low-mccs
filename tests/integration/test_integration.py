# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains integration tests of MCCS device interactions."""
from __future__ import annotations

import json
import time
from typing import Iterable, Iterator, cast
from unittest.mock import Mock

import pytest
import tango
from ska_control_model import AdminMode, HealthState, ObsState, ResultCode
from ska_low_mccs_common.testing.mock import MockDeviceBuilder
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext


@pytest.fixture(name="mock_field_station")
def mock_field_station_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a mock fieldStation.

    It is on so MccsStation is on.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    return builder()


@pytest.fixture(name="mock_station_calibrator")
def mock_station_calibrator_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a mock station calibrator.

    It is on so MccsStation is on.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    return builder()


@pytest.fixture(name="mock_sps_station")
def mock_sps_station_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a mock sps station.

    It is on so MccsStation is on.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    return builder()


@pytest.fixture(name="apertures")
def apertures_fixture() -> list[dict]:
    """
    Return an example aperture for use in testing.

    :return: an example aperture for use in testing.

    """
    apertures = [
        {
            "station_id": 1,
            "aperture_id": "AP001.01",
        },
    ]

    return apertures


@pytest.fixture(name="allocate_resource_spec")
def allocate_resource_spec_fixture(
    apertures: list[dict],
) -> dict:
    """
    Return an example schema-complient allocate argument for use in testing.

    :param apertures: an example list of apertures for use in testing.

    :return: an example schema-complient allocate argument for use in testing.
    """
    resource_spec = {
        "subarray_id": 1,
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "apertures": apertures,
                "number_of_channels": 32,
            }
        ],
    }
    return resource_spec


@pytest.fixture(name="test_context")
def test_context_fixture(
    mock_field_station: Mock, mock_sps_station: Mock, mock_station_calibrator: Mock
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which subarray and station beam Tango devices are running.

    :param mock_field_station: a mocked out FieldStation device
    :param mock_sps_station: a mocked out SpsStation device.
    :param mock_station_calibrator: a mocked out StationCalibrator device.

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()

    harness.add_mock_field_station_device("ci-1", mock_field_station)
    harness.add_mock_field_station_device("ci-2", mock_field_station)

    harness.add_mock_sps_station_device("ci-1", mock_sps_station)
    harness.add_mock_sps_station_device("ci-2", mock_sps_station)

    harness.add_mock_station_calibrator_device("ci-1", mock_station_calibrator)
    harness.add_mock_station_calibrator_device("ci-2", mock_station_calibrator)

    harness.set_controller_device(
        subarrays=[1, 2],
        subarray_beams=[1, 2, 3, 4],
        stations=["ci-1", "ci-2"],
        station_beams=[("ci-1", 1), ("ci-1", 2), ("ci-2", 1), ("ci-2", 2)],
    )
    harness.add_subarray_device(1)
    harness.add_subarray_device(2)
    harness.add_station_device("ci-1", 1, [])
    harness.add_station_device("ci-2", 2, [])

    harness.add_station_beam_device("ci-1", 1)
    harness.add_station_beam_device("ci-1", 2)
    harness.add_station_beam_device("ci-2", 1)
    harness.add_station_beam_device("ci-2", 2)

    for i in range(4):
        harness.add_subarray_beam_device(i + 1)

    with harness as context:
        yield context


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :return: a callback group.
    """
    return MockTangoEventCallbackGroup(
        "healthState",
        "adminMode",
        "controller_state",
        "subarray_1_state",
        "subarray_2_state",
        "station_1_state",
        "station_2_state",
        "subarray_beam_1_state",
        "subarray_beam_2_state",
        "subarray_beam_3_state",
        "subarray_beam_4_state",
        "station_beam_1_state",
        "station_beam_2_state",
        "station_beam_3_state",
        "station_beam_4_state",
        "subarray_1_obs_state",
        "subarray_2_obs_state",
        "longRunningCommandResult",
        "obsState",
        timeout=30.0,
    )


class TestMccsIntegration:
    """Integration test cases for the Mccs device classes."""

    # pylint: disable=too-many-statements, too-many-locals
    @pytest.mark.xfail(reason="Intermittent fail in the pipeline")
    def test_controller_allocate_subarray(
        self: TestMccsIntegration,
        test_context: MccsTangoTestHarnessContext,
        change_event_callbacks: MockTangoEventCallbackGroup,
        allocate_resource_spec: dict,
    ) -> None:
        """
        Test that an MccsController can allocate resources to an MccsSubarray.

        :param test_context: the test context in which this test will be run.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.
        :param allocate_resource_spec: a schema_compliant allocate schema for use
            in testing.
        """
        controller = test_context.get_controller_device()
        subarray_1 = test_context.get_subarray_device(1)
        subarray_2 = test_context.get_subarray_device(2)
        station_1 = test_context.get_station_device("ci-1")
        station_2 = test_context.get_station_device("ci-2")

        station_beams = []
        station_beams.append(test_context.get_station_beam_device("ci-1", 1))
        station_beams.append(test_context.get_station_beam_device("ci-1", 2))
        station_beams.append(test_context.get_station_beam_device("ci-2", 1))
        station_beams.append(test_context.get_station_beam_device("ci-2", 2))

        subarray_beams = [
            test_context.get_subarray_beam_device(i + 1) for i in range(4)
        ]

        controller.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["controller_state"],
        )
        subarray_1.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_1_state"],
        )
        subarray_2.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_2_state"],
        )
        station_1.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["station_1_state"],
        )
        station_2.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["station_2_state"],
        )
        for i, subarray_beam in enumerate(subarray_beams, start=1):
            subarray_beam.subscribe_event(
                "state",
                tango.EventType.CHANGE_EVENT,
                change_event_callbacks[f"subarray_beam_{i}_state"],
            )
        for i, station_beam in enumerate(station_beams, start=1):
            station_beam.subscribe_event(
                "state",
                tango.EventType.CHANGE_EVENT,
                change_event_callbacks[f"station_beam_{i}_state"],
            )

        subarray_1.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_1_obs_state"],
        )

        controller.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(("", ""))

        change_event_callbacks["controller_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        change_event_callbacks["subarray_1_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        change_event_callbacks["subarray_2_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        change_event_callbacks["station_1_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        change_event_callbacks["station_2_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        for i in range(len(station_beams)):
            change_event_callbacks[f"station_beam_{i+1}_state"].assert_change_event(
                tango.DevState.DISABLE
            )
        for i in range(len(subarray_beams)):
            change_event_callbacks[f"subarray_beam_{i+1}_state"].assert_change_event(
                tango.DevState.DISABLE
            )

        controller.adminMode = AdminMode.ONLINE
        subarray_1.adminMode = AdminMode.ONLINE
        subarray_2.adminMode = AdminMode.ONLINE
        station_1.adminMode = AdminMode.ONLINE
        station_2.adminMode = AdminMode.ONLINE
        for station_beam in station_beams:
            station_beam.adminMode = AdminMode.ONLINE
        for subarray_beam in subarray_beams:
            subarray_beam.adminMode = AdminMode.ONLINE

        # Subracks are mocked ON. APIUs, Antennas and Tiles are mocked ON, so stations
        # will be ON too. Therefore controller will already be ON.
        change_event_callbacks["controller_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        change_event_callbacks["station_1_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        change_event_callbacks["station_2_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        change_event_callbacks["subarray_1_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        change_event_callbacks["subarray_2_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        for i in range(len(station_beams)):
            change_event_callbacks[f"station_beam_{i+1}_state"].assert_change_event(
                tango.DevState.ON, lookahead=2
            )
        for i in range(len(subarray_beams)):
            change_event_callbacks[f"subarray_beam_{i+1}_state"].assert_change_event(
                tango.DevState.ON, lookahead=2
            )

        assert subarray_1.state() == tango.DevState.ON
        assert subarray_2.state() == tango.DevState.ON
        assert station_1.state() == tango.DevState.ON
        assert station_2.state() == tango.DevState.ON
        assert controller.state() == tango.DevState.ON
        for subarray_beam in subarray_beams:
            assert subarray_beam.state() == tango.DevState.ON
        for station_beam in station_beams:
            assert station_beam.state() == tango.DevState.ON

        change_event_callbacks["subarray_1_obs_state"].assert_change_event(
            ObsState.EMPTY
        )
        assert subarray_1.obsState == ObsState.EMPTY

        # check initial state
        assert subarray_1.stationTrls == ()
        assert subarray_2.stationTrls == ()

        # Despite having asserted on the state of our devices sometimes
        # Allocate fails due to the subarray proxy not yet being ON.
        # TODO: Replace the sleep...
        time.sleep(1)

        # allocate station_1 to subarray_1
        ([result_code], [unique_id]) = controller.Allocate(
            json.dumps(allocate_resource_spec)
        )
        assert result_code == ResultCode.QUEUED

        lrc_result = (
            unique_id,
            '"The AssignResources command has completed"',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )

        change_event_callbacks["subarray_1_obs_state"].assert_change_event(
            ObsState.IDLE, lookahead=10
        )
        assert subarray_1.obsState == ObsState.IDLE

        # check that station_1 and only station_1 is allocated
        station_trls: Iterable = cast(Iterable, subarray_1.stationTrls)
        assert list(station_trls) == [station_1.dev_name()]
        assert subarray_2.stationTrls == ()

        # allocating subarray_beam_id to subarray 2 should fail, because it is already
        # allocated to subarray 1
        allocate_resource_spec["subarray_id"] = 2
        with pytest.raises(tango.DevFailed):
            ([result_code], [unique_id]) = controller.Allocate(
                json.dumps(allocate_resource_spec)
            )
            assert result_code == ResultCode.REJECTED

        # check no side-effects
        assert list(station_trls) == [station_1.dev_name()]
        assert subarray_2.stationTrls == ()

        # allocating stations 1 and 2 to subarray 1 should succeed,
        # ecause the already allocated station is allocated to the same
        # subarray, BUT we must remember that the subarray cannot reallocate
        # the same subarray_beam.

        allocate_resource_spec["subarray_id"] = 1
        extra_aperture = {
            "station_id": 2,
            "aperture_id": "AP001.02",
        }
        extra_subarray_beam = {
            "subarray_beam_id": 2,
            "apertures": [extra_aperture],
            "number_of_channels": 32,
        }
        allocate_resource_spec["subarray_beams"].append(extra_subarray_beam)
        ([result_code], [unique_id]) = controller.Allocate(
            json.dumps(allocate_resource_spec)
        )
        assert result_code == ResultCode.QUEUED
        change_event_callbacks["subarray_1_obs_state"].assert_change_event(
            ObsState.IDLE, lookahead=50
        )
        assert subarray_1.obsState == ObsState.IDLE

        station_trls = cast(Iterable, subarray_1.stationTrls)
        assert list(station_trls) == [
            station_1.dev_name(),
            station_2.dev_name(),
        ]
        assert subarray_2.stationTrls == ()

    @pytest.mark.xfail(reason="Intermittent fail in the pipeline")
    def test_controller_release_subarray(
        self: TestMccsIntegration,
        test_context: MccsTangoTestHarnessContext,
        change_event_callbacks: MockTangoEventCallbackGroup,
        allocate_resource_spec: dict,
    ) -> None:
        """
        Test that an MccsController can release the resources of an MccsSubarray.

        :param test_context: the test context in which this test will be run.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.
        :param allocate_resource_spec: a schema_compliant allocate schema for use
            in testing.
        """
        controller = test_context.get_controller_device()
        subarray_1 = test_context.get_subarray_device(1)
        subarray_2 = test_context.get_subarray_device(2)
        station_1 = test_context.get_station_device("ci-1")
        station_2 = test_context.get_station_device("ci-2")

        station_beams = []
        station_beams.append(test_context.get_station_beam_device("ci-1", 1))
        station_beams.append(test_context.get_station_beam_device("ci-1", 2))
        station_beams.append(test_context.get_station_beam_device("ci-2", 1))
        station_beams.append(test_context.get_station_beam_device("ci-2", 2))

        subarray_beams = [
            test_context.get_subarray_beam_device(i + 1) for i in range(4)
        ]

        controller.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["controller_state"],
        )
        subarray_1.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_1_state"],
        )
        subarray_2.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_2_state"],
        )
        station_1.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["station_1_state"],
        )
        station_2.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["station_2_state"],
        )
        for i, subarray_beam in enumerate(subarray_beams, start=1):
            subarray_beam.subscribe_event(
                "state",
                tango.EventType.CHANGE_EVENT,
                change_event_callbacks[f"subarray_beam_{i}_state"],
            )
        for i, station_beam in enumerate(station_beams, start=1):
            station_beam.subscribe_event(
                "state",
                tango.EventType.CHANGE_EVENT,
                change_event_callbacks[f"station_beam_{i}_state"],
            )

        subarray_1.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_1_obs_state"],
        )
        subarray_2.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["subarray_2_obs_state"],
        )

        controller.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(("", ""))

        change_event_callbacks["controller_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        change_event_callbacks["subarray_1_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        change_event_callbacks["subarray_2_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        change_event_callbacks["station_1_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        change_event_callbacks["station_2_state"].assert_change_event(
            tango.DevState.DISABLE
        )
        for i in range(len(station_beams)):
            change_event_callbacks[f"station_beam_{i+1}_state"].assert_change_event(
                tango.DevState.DISABLE
            )
        for i in range(len(subarray_beams)):
            change_event_callbacks[f"subarray_beam_{i+1}_state"].assert_change_event(
                tango.DevState.DISABLE
            )

        controller.adminMode = AdminMode.ONLINE
        subarray_1.adminMode = AdminMode.ONLINE
        subarray_2.adminMode = AdminMode.ONLINE
        station_1.adminMode = AdminMode.ONLINE
        station_2.adminMode = AdminMode.ONLINE
        for station_beam in station_beams:
            station_beam.adminMode = AdminMode.ONLINE
        for subarray_beam in subarray_beams:
            subarray_beam.adminMode = AdminMode.ONLINE

        # Subracks are mocked ON. APIUs, Antennas and Tiles are mocked ON, so stations
        # will be ON too. Therefore controller will already be ON.
        change_event_callbacks["controller_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        change_event_callbacks["station_1_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        change_event_callbacks["station_2_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        change_event_callbacks["subarray_1_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        change_event_callbacks["subarray_2_state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        for i in range(len(station_beams)):
            change_event_callbacks[f"station_beam_{i+1}_state"].assert_change_event(
                tango.DevState.ON, lookahead=2
            )
        for i in range(len(subarray_beams)):
            change_event_callbacks[f"subarray_beam_{i+1}_state"].assert_change_event(
                tango.DevState.ON, lookahead=2
            )

        assert subarray_1.state() == tango.DevState.ON
        assert subarray_2.state() == tango.DevState.ON
        assert station_1.state() == tango.DevState.ON
        assert station_2.state() == tango.DevState.ON
        assert controller.state() == tango.DevState.ON
        for subarray_beam in subarray_beams:
            assert subarray_beam.state() == tango.DevState.ON
        for station_beam in station_beams:
            assert station_beam.state() == tango.DevState.ON

        change_event_callbacks["subarray_1_obs_state"].assert_change_event(
            ObsState.EMPTY
        )
        assert subarray_1.obsState == ObsState.EMPTY

        # check initial state
        assert subarray_1.stationTrls == ()
        assert subarray_2.stationTrls == ()

        # Despite having asserted on the state of our devices sometimes
        # Allocate fails due to the subarray proxy not yet being ON.
        # TODO: Replace the sleep...
        time.sleep(1)

        # allocate station_1 to subarray_1
        ([result_code], [unique_id]) = controller.Allocate(
            json.dumps(allocate_resource_spec)
        )
        assert result_code == ResultCode.QUEUED

        lrc_result = (
            unique_id,
            '"The AssignResources command has completed"',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )
        change_event_callbacks["subarray_1_obs_state"].assert_change_event(
            ObsState.IDLE, lookahead=10
        )
        assert subarray_1.obsState == ObsState.IDLE

        allocate_resource_spec["subarray_id"] = 2
        allocate_resource_spec["subarray_beams"][0]["subarray_beam_id"] = 2
        allocate_resource_spec["subarray_beams"][0]["apertures"] = [
            {"station_id": 2, "aperture_id": "AP001.02"}
        ]
        # allocate station 2 to subarray 2
        ([result_code], [unique_id]) = controller.Allocate(
            json.dumps(allocate_resource_spec)
        )
        assert result_code == ResultCode.QUEUED

        lrc_result = (
            unique_id,
            '"The AssignResources command has completed"',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )

        change_event_callbacks["subarray_2_obs_state"].assert_change_event(
            ObsState.IDLE, lookahead=10
        )
        assert subarray_2.obsState == ObsState.IDLE

        # check initial state
        assert list(subarray_1.stationTrls) == [station_1.dev_name()]
        assert list(subarray_2.stationTrls) == [station_2.dev_name()]

        # release resources of subarray_2
        ([result_code], [_]) = controller.Release(
            json.dumps({"subarray_id": 2, "release_all": False})
        )
        assert result_code == ResultCode.OK

        # TODO: It's a bit rubbish that we can't detect when this
        # command is complete. For now, just increase the delay.
        time.sleep(0.3)

        # check
        assert list(subarray_1.stationTrls) == [station_1.dev_name()]
        assert subarray_2.stationTrls == ()

        # releasing resources of unresourced subarray_2 should succeed (as redundant)
        # i.e. OK not QUEUED because it's quick
        ([result_code], [_]) = controller.Release(
            json.dumps({"subarray_id": 2, "release_all": False})
        )
        assert result_code == ResultCode.OK

        # check no side-effect to failed release
        assert list(subarray_1.stationTrls) == [station_1.dev_name()]
        assert subarray_2.stationTrls == ()

        # release resources of subarray_1
        ([result_code], [_]) = controller.Release(
            json.dumps({"subarray_id": 1, "release_all": False})
        )
        assert result_code == ResultCode.OK

        start_time = time.time()
        timeout = 1

        while time.time() < start_time + timeout:
            try:
                assert subarray_1.stationTrls == ()
                assert subarray_2.stationTrls == ()
                break
            except AssertionError:
                time.sleep(0.1)

        assert subarray_1.stationTrls == ()
        assert subarray_2.stationTrls == ()
