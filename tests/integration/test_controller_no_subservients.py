# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains integration tests of MCCS device interactions."""
from __future__ import annotations

import time
from typing import Iterator

import pytest
import tango
from ska_control_model import AdminMode, HealthState, ResultCode
from tango import DevState

from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext


@pytest.fixture(name="test_context")
def test_context_fixture() -> Iterator[MccsTangoTestHarnessContext]:
    """
    Create a test harness providing a Tango context for devices under test.

    :yield: A tango context with devices to test.
    """

    harness = MccsTangoTestHarness()
    harness.set_controller_device(
        subarrays=[],
        subarray_beams=[],
        stations=[],
        station_beams=[],
    )
    with harness as context:
        yield context


# pylint: disable=too-few-public-methods
class TestMccsController:
    """Integration test case for the MccsController class."""

    def test_controller_no_subservient(
        self: TestMccsController,
        test_context: MccsTangoTestHarnessContext,
    ) -> None:
        """
        Test that an MccsController can allocate resources to an MccsSubarray.

        :param test_context: the test context in which this test will be run.
        """
        controller = test_context.get_controller_device()

        assert controller.adminMode == AdminMode.OFFLINE
        assert controller.State() == DevState.DISABLE
        assert controller.healthState == HealthState.UNKNOWN

        for _ in range(2):
            controller.adminMode = AdminMode.ONLINE

            assert controller.adminMode == AdminMode.ONLINE
            assert controller.State() == DevState.ON
            assert controller.healthState == HealthState.OK

            controller.adminMode = AdminMode.OFFLINE

            assert controller.adminMode == AdminMode.OFFLINE
            assert controller.State() == DevState.DISABLE
            assert controller.healthState == HealthState.UNKNOWN

        with pytest.raises(
            tango.DevFailed,
            match="Command On not allowed when the device is in DISABLE state",
        ):
            _ = controller.On()

        controller.adminMode = AdminMode.ONLINE

        ([result_code], [msg]) = controller.On()
        assert result_code == ResultCode.REJECTED
        assert msg == "Device is already in ON state."

        ([result_code], [msg]) = controller.Off()
        assert result_code == ResultCode.REJECTED
        assert msg == "No subservient devices to turn off"

        ([result_code], [msg]) = controller.StandBy()
        assert result_code == ResultCode.REJECTED
        assert msg == "No subservient devices to put into standby"

        ([result_code], [msg]) = controller.StandByFull()
        assert result_code == ResultCode.REJECTED
        assert msg == "No subservient devices to put into standby"

        ([result_code], [msg]) = controller.StandByLow()
        assert result_code == ResultCode.REJECTED
        assert msg == "No subservient devices to put into standby"

        ([result_code], [msg]) = controller.Allocate(
            '{"subarray_id": 1, "subarray_beams": []}'
        )
        assert result_code == ResultCode.QUEUED
        timeout = 5
        start_time = time.time()
        while time.time() < start_time + timeout:
            try:
                assert (
                    "Subarray 1 not present" in controller.longrunningcommandresult[1]
                )
                break
            except AssertionError:
                time.sleep(0.1)
        assert "Subarray 1 not present" in controller.longrunningcommandresult[1]
        ([result_code], [msg]) = controller.Release('{"subarray_id": 1}')
        assert result_code == ResultCode.REJECTED
        assert msg == "No subservient subarray devices to release"

        ([result_code], [msg]) = controller.RestartSubarray(1)
        assert result_code == ResultCode.REJECTED
        assert msg == "No subservient subarray devices to restart"
