# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains tests of the calibration products."""
import os
import stat
from pathlib import Path
from typing import Final

import h5py
import numpy as np
import pytest

from ska_low_mccs.calibration_solver import CalibrationSolutionProduct

__STRUCTURE_V_1_0: Final[dict[str, str]] = {
    "acquisition_time": "float64",
    "corrcoeff": "float64",
    "frequency_channel": "int64",
    "galactic_centre_elevation": "float64",
    "lst": "float64",
    "masked_antennas": "int64",
    "n_masked_final": "int64",
    "n_masked_initial": "int64",
    "residual_max": "float64",
    "residual_std": "float64",
    "solution": "float64",
    "station_id": "int64",
    "sun_adjustment_factor": "float64",
    "sun_elevation": "float64",
    "xy_phase": "float64",
}


@pytest.fixture(name="structure_version_v_1_0")
def structure_version_v_1_0_fixture() -> dict:
    """
    Return definition of version 1 dataset.

    DO NOT EDIT!, if structure has changed it is a new version.

    :return: the structure for 1.0
    """

    return __STRUCTURE_V_1_0


@pytest.fixture(name="calibration_product_v_1_0")
def calibration_product_v_1_0_fixture() -> CalibrationSolutionProduct:
    """
    Return a version 1.0 CalibrationSolutionProduct.

    :return: A CalibrationSolutionProduct for 1.0
    """
    return CalibrationSolutionProduct(
        corrcoeff=[0.95, 0.96],
        residual_max=[0.1, 0.2],
        residual_std=[0.05, 0.06],
        xy_phase=10.0,
        n_masked_initial=5,
        n_masked_final=3,
        lst=None,  # Passing None will not save that dataset.
        galactic_centre_elevation=45.0,
        sun_elevation=30.0,
        sun_adjustment_factor=1.0,
        masked_antennas=np.array([1, 2, 3], dtype=np.int64),
        solution=np.random.randn(2048),
        frequency_channel=1,
        station_id=1001,
        acquisition_time=1234567890.0,
        structure_version="1.0",
        logger=None,
    )


@pytest.fixture(name="hdf5_file_path")
def hdf5_file_path_fixture(tmpdir: str) -> str:
    """
    Fixture to create a temporary HDF5 file path.

    :param tmpdir: the pytest temporary directory fixture.

    :return: A file path as a target for tests.
    """
    return str(tmpdir.join("test_data.h5"))


def test_saved_structure_version_v_1_0(
    calibration_product_v_1_0: CalibrationSolutionProduct,
    hdf5_file_path: str,
    structure_version_v_1_0: dict,
) -> None:
    """
    Test the HDF5 structure saved is correct for version 1.0.

    :param calibration_product_v_1_0: The calibration product verson 1.0
    :param hdf5_file_path: The target file to write to.
    :param structure_version_v_1_0: The structure definition of verson 1.0
    """
    # Save a product to file.
    calibration_product_v_1_0.save_to_hdf5(hdf5_file_path)
    nof_found_datasets: int = 0
    with h5py.File(hdf5_file_path, "r") as f:
        # Check version saved is correct.
        assert f.attrs.get("structure_version", None) == "1.0"

        def __check_dataset(name: str, obj: h5py.File) -> None:
            nonlocal nof_found_datasets
            if isinstance(obj, h5py.Dataset):
                assert structure_version_v_1_0[name] == str(obj.dtype)
                nof_found_datasets += 1

        # Recursively visit all objects and check datasets.
        f.visititems(__check_dataset)


def test_save_load_hdf5(
    calibration_product_v_1_0: CalibrationSolutionProduct, hdf5_file_path: str
) -> None:
    """
    Test saving and loading from HDF5.

    :param calibration_product_v_1_0: The calibration product verson 1.0
    :param hdf5_file_path: The target file to write/read.
    """
    calibration_product_v_1_0.save_to_hdf5(hdf5_file_path)

    # Load the data back into a new object
    loaded_product = CalibrationSolutionProduct(logger=None)
    loaded_product.load_from_hdf5(hdf5_file_path)

    # Check loaded data is the same as the original
    assert (
        loaded_product.structure_version == calibration_product_v_1_0.structure_version
    )
    assert loaded_product.corrcoeff == calibration_product_v_1_0.corrcoeff
    assert loaded_product.residual_max == calibration_product_v_1_0.residual_max
    assert loaded_product.residual_std == calibration_product_v_1_0.residual_std
    assert loaded_product.xy_phase == calibration_product_v_1_0.xy_phase
    assert loaded_product.n_masked_initial == calibration_product_v_1_0.n_masked_initial
    assert loaded_product.n_masked_final == calibration_product_v_1_0.n_masked_final
    assert loaded_product.lst == calibration_product_v_1_0.lst
    assert (
        loaded_product.galactic_centre_elevation
        == calibration_product_v_1_0.galactic_centre_elevation
    )
    assert loaded_product.sun_elevation == calibration_product_v_1_0.sun_elevation
    assert (
        loaded_product.sun_adjustment_factor
        == calibration_product_v_1_0.sun_adjustment_factor
    )
    assert (
        loaded_product.masked_antennas is not None
        and calibration_product_v_1_0.masked_antennas is not None
    )
    assert np.array_equal(
        loaded_product.masked_antennas, calibration_product_v_1_0.masked_antennas
    )
    assert (
        loaded_product.solution is not None
        and calibration_product_v_1_0.solution is not None
    )
    assert np.array_equal(loaded_product.solution, calibration_product_v_1_0.solution)
    assert (
        loaded_product.frequency_channel == calibration_product_v_1_0.frequency_channel
    )
    assert loaded_product.station_id == calibration_product_v_1_0.station_id
    assert loaded_product.acquisition_time == calibration_product_v_1_0.acquisition_time


def test_invalid_structure_version() -> None:
    """Test construction with invalid version."""
    with pytest.raises(ValueError, match="version invalid_version not supported."):
        CalibrationSolutionProduct(structure_version="invalid_version")


def test_product_read_only(
    calibration_product_v_1_0: CalibrationSolutionProduct, hdf5_file_path: str
) -> None:
    """
    Test data product is read only.

    :param calibration_product_v_1_0: The calibration product verson 1.0
    :param hdf5_file_path: The target file to write/read.
    """
    calibration_product_v_1_0.save_to_hdf5(hdf5_file_path)

    file_stat = os.stat(hdf5_file_path)
    permissions = file_stat.st_mode
    symbolic_permissions = stat.filemode(permissions)
    assert symbolic_permissions == "-r--r--r--"


def test_save_when_file_exists(
    calibration_product_v_1_0: CalibrationSolutionProduct, hdf5_file_path: str
) -> None:
    """
    Test that we fail when the target file exists.

    This is to cover the case whereby another product of the exact same name
    is created by an unknown source. We do not want to wipe this data,
    incase the file permissions were incorrectly set.
    Instead we want to fail unless explicitly told to try.

    :param calibration_product_v_1_0: The calibration product verson 1.0
    :param hdf5_file_path: The target file to write/read.
    """
    # Create a file.
    with h5py.File(hdf5_file_path, "w") as f:
        f.attrs[
            "really_important_data" "i_forgot_to_set_" "correct_permissions_for"
        ] = "yes"
    # Check it was created
    my_file = Path(hdf5_file_path)
    assert my_file.is_file()

    with pytest.raises(
        PermissionError,
        match=(
            f"File {hdf5_file_path}, already exists. "
            "If you trust the file permissions on the "
            "existing file you may attempt_if_exists. "
            "This will attempt to write but will still "
            "obey the ubuntu file permissions."
        ),
    ):
        calibration_product_v_1_0.save_to_hdf5(hdf5_file_path)

    # Explicitly trust the file permission set on existing file and attempt.
    calibration_product_v_1_0.save_to_hdf5(hdf5_file_path, attempt_if_exists=True)
    file_stat = os.stat(hdf5_file_path)
    permissions = file_stat.st_mode
    symbolic_permissions = stat.filemode(permissions)
    # We trust that the ubuntu file permissions has no bugs,
    # therefore no further tests needed.
    assert symbolic_permissions == "-r--r--r--"


def test_structure_missmatch(
    calibration_product_v_1_0: CalibrationSolutionProduct, hdf5_file_path: str
) -> None:
    """
    Test we report structure missmatches.

    :param calibration_product_v_1_0: The calibration product verson 1.0
    :param hdf5_file_path: The target file to write/read.
    """
    file_structure_version = "2.0"
    # Create a file.
    with h5py.File(hdf5_file_path, "w") as f:
        f.attrs["structure_version"] = file_structure_version

    # Sanity check that the test is set up correctly.
    assert calibration_product_v_1_0.structure_version != file_structure_version

    with pytest.raises(
        ValueError,
        match=(
            "Structure version mismatch! "
            f"Expected {calibration_product_v_1_0.structure_version}, "
            f"but got {file_structure_version}."
        ),
    ):
        calibration_product_v_1_0.load_from_hdf5(hdf5_file_path)
