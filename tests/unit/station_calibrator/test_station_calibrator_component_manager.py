#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests of the station calibrator component manager."""
from __future__ import annotations

import json
import logging
import os
import time
import unittest.mock
from typing import Iterator

import numpy as np
import pytest
from ska_control_model import CommunicationStatus
from ska_low_mccs_calibration.utils import read_yaml_antennas
from ska_tango_testing.mock import MockCallableGroup
from ska_telmodel.data import TMData  # type: ignore
from tango import AttrQuality

from ska_low_mccs.calibration_solver import CalibrationSolutionProduct
from ska_low_mccs.station_calibrator import (
    NumpyEncoder,
    StationCalibratorComponentManager,
)
from tests.harness import (
    MccsTangoTestHarness,
    get_calibration_store_trl,
    get_solver_trl,
    get_station_trl,
)


@pytest.fixture(name="test_context")
def test_context_fixture(
    station_label: str,
    mock_station_device_proxy: unittest.mock.Mock,
    mock_calibration_store_device_proxy: unittest.mock.Mock,
    mock_solver_device_proxy: unittest.mock.Mock,
) -> Iterator[None]:
    """
    Yield into a context in which Tango is running, with mock devices.

    The station and calibration store devices which the station calibrator
    component manager interacts with are mocked out here

    :param station_label: name of the station
    :param mock_station_device_proxy: a mock station device proxy
        that has been configured with the required station behaviours.
    :param mock_solver_device_proxy: a mocked solver device.
    :param mock_calibration_store_device_proxy: a mock calibration store device proxy
        that has been configured with the required calibration store behaviours.

    :yields: into a context in which Tango is running
    """
    harness = MccsTangoTestHarness()
    harness.add_mock_station_device(station_label, mock_station_device_proxy)
    harness.add_mock_calibration_store_device(
        station_label, mock_calibration_store_device_proxy
    )
    harness.add_mock_solver_device(station_label, mock_solver_device_proxy)
    with harness:
        yield


@pytest.fixture(name="callbacks")
def callbacks_fixture() -> MockCallableGroup:
    """
    Return a dictionary of callables to be used as callbacks.

    :return: a dictionary of callables to be used as callbacks.
    """
    return MockCallableGroup(
        "communication_status",
        "component_state",
        "task",
        timeout=5.0,
    )


@pytest.fixture(name="station_calibrator_component_manager")
def station_calibrator_component_manager_fixture(
    test_context: None,
    logger: logging.Logger,
    callbacks: MockCallableGroup,
    station_label: str,
    station_id: int,
) -> StationCalibratorComponentManager:
    """
    Return a station calibrator component manager.

    :param test_context: a Tango test context running the required
        mock subservient devices
    :param logger: a logger to be used by the commonent manager
    :param callbacks: callback group
    :param station_label: name of the station
    :param station_id: the id of the station.

    :return: a station calibrator component manager.
    """
    return StationCalibratorComponentManager(
        logger,
        get_station_trl(station_label),
        station_id,
        get_calibration_store_trl(station_label),
        get_solver_trl(station_label),
        callbacks["communication_status"],
        callbacks["component_state"],
    )


@pytest.mark.forked
def test_communication(
    station_calibrator_component_manager: StationCalibratorComponentManager,
    callbacks: MockCallableGroup,
) -> None:
    """
    Test communication for the station calibrator component manager.

    :param station_calibrator_component_manager: the station calibrator component
        manager under test
    :param callbacks: dictionary of driver callbacks.
    """
    assert (
        station_calibrator_component_manager.communication_state
        == CommunicationStatus.DISABLED
    )

    # takes the component out of DISABLED
    station_calibrator_component_manager.start_communicating()
    callbacks["communication_status"].assert_call(CommunicationStatus.NOT_ESTABLISHED)
    callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)

    station_calibrator_component_manager.stop_communicating()

    callbacks["communication_status"].assert_call(CommunicationStatus.DISABLED)


@pytest.fixture(name="calibration_data_example")
def calibration_data_example_fixture() -> CalibrationSolutionProduct:
    """
    Return calibration data.

    :return: calibration data.
    """
    cwd = os.path.dirname(os.path.realpath(__file__))
    product = CalibrationSolutionProduct(structure_version="1.0")
    product.load_from_hdf5(
        f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.h5"
    )
    return product


def test_solution_mapping(
    station_calibrator_component_manager: StationCalibratorComponentManager,
    calibration_data_example: CalibrationSolutionProduct,
    station_config_path: tuple[str, str],
) -> None:
    """
    Test that the solution get mapped as expected.

    The calibration solution products are stored in eep
    order. The solutions gathered for station configuration
    extect this in tpm_idx. A solution is 2048 = 256 * 8
    256 elements, 4 flattened complex inverse jones numbers.

    :param station_calibrator_component_manager: the
        componentmanager under test.
    :param calibration_data_example: a CalibrationSolutionProduct
        object containing a product.
    :param station_config_path: a tuple containing
        the url and path to tmdata.
    """
    # define the element we want to test (0 <= x <= 255)s
    element_under_test = 255

    # Store the 8 values, corresponding to the element under test.
    first_eep = calibration_data_example.solution[
        element_under_test * 8 : (element_under_test * 8) + 8
    ]

    # Use a platform configuration to get a map from
    # eep (1 based) => tpm_idx(0 based).
    # For this we assume no bugs exist in external
    # moduled TMData and `read_yaml_antennas`.
    platform_config = TMData([station_config_path[0]])[
        station_config_path[1]
    ].get_dict()

    station_config = platform_config["platform"]["stations"]["s9-2"]
    antidx_yaml2data, _, _, _ = read_yaml_antennas(station_config)

    # Grab the tpm_id corresponding to element_under_test
    tpm_idx = antidx_yaml2data[element_under_test]

    # Execute the sorting function.
    sorted_result = station_calibrator_component_manager._sort_by_tpm_base(
        station_config_path, calibration_data_example.solution
    )

    # check that the results match the index of element_under_test
    assert all(np.equal(sorted_result[tpm_idx * 8 : (tpm_idx * 8) + 8], first_eep))


@pytest.fixture(name="tpm_based_solution")
def tpm_based_solution_fixture(
    station_calibrator_component_manager: StationCalibratorComponentManager,
    calibration_data_example: CalibrationSolutionProduct,
    station_config_path: tuple[str, str],
) -> np.ndarray:
    """
    Return calibration data.

    :param station_calibrator_component_manager: the
        componentmanager under test.
    :param calibration_data_example: a CalibrationSolutionProduct
        object containing a product.
    :param station_config_path: a tuple containing
        the url and path to tmdata.

    :return: calibration data.
    """
    # This method will be tested in test_sort_solution_by_tpm
    return station_calibrator_component_manager._sort_by_tpm_base(
        station_config_path, calibration_data_example.solution
    )


# pylint: disable=too-many-arguments, too-many-locals
def test_calibration(
    station_calibrator_component_manager: StationCalibratorComponentManager,
    mock_solver_device_proxy: unittest.mock.Mock,
    mock_calibration_store_device_proxy: unittest.mock.Mock,
    station_config_path: list[str],
    calibration_data_example: CalibrationSolutionProduct,
    callbacks: MockCallableGroup,
    tpm_based_solution: list[float],
) -> None:
    """
    Test the calibration process.

    The Solver device has the responsibility of calculating a solution.
    The StationCalibrator must iterate over all channels and orchestrate the
    configuration needed for calibration.

    :param station_calibrator_component_manager: the station calibrator component
        manager under test
    :param mock_solver_device_proxy: a mocked solver device.
    :param station_config_path: the url and path of the
        telmodel data.
    :param calibration_data_example: fixture with calibration data.
    :param mock_calibration_store_device_proxy: a mock calibration store device proxy
        that has been configured with the required calibration store behaviours.
    :param callbacks: dictionary of driver callbacks.
    :param tpm_based_solution: the solutions rebased to tpm_idx.
    """
    assert (
        station_calibrator_component_manager.communication_state
        == CommunicationStatus.DISABLED
    )
    # takes the component out of DISABLED
    station_calibrator_component_manager.start_communicating()
    callbacks["communication_status"].assert_call(CommunicationStatus.NOT_ESTABLISHED)
    callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)

    # Sleep because solver is treated as a service and is not wrapped up in state of
    # station calibrator. Without this sleep it is possible the solver is not yet
    # reporting ON, therefore we cannot calibrate.
    time.sleep(2)

    station_calibrator_component_manager._data_timeout = 5
    station_calibrator_component_manager._outside_temperature = 32.5
    first_channel = 50
    last_channel = 350
    station_calibrator_component_manager.start_calibration_loop(
        {
            "eep_filebase": "tmp_",
            "jones_solve": False,
            "adjust_solar_model": False,
            "masked_antennas": [],
            "station_config_path": station_config_path,
            "first_channel": first_channel,
            "last_channel": last_channel,
        }
    )
    # We are only mocking 1 happy channel, since this required a file to be created,
    # There is a file `correlation_burst_140_20240611_13120_0.npy` that has been added
    # to git to allow a happy path.
    mocked_happy_channel = 140
    cwd = os.path.dirname(os.path.realpath(__file__))
    happy_data_file_base = f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0"

    for channel_id in range(first_channel, last_channel + 1):
        # The loop will require some intervention to complete.
        # First when we call `AcquireDataForCalibration`, we expect to
        # see a callback from DAQ about the `correlation_burst_` data received for
        # a particular channel.
        if channel_id == mocked_happy_channel:
            tmp_file_name = f"{happy_data_file_base}.hdf5"
            tmp_rel_file_name = tmp_file_name
            tmp_solution_name = f"{happy_data_file_base}"
        else:
            tmp_file_name = (
                "/product/eb-mocked_id/ska-low-mccs/"
                "mocked_scan_id/mocked_dir/correlation_burst_"
                f"{channel_id}_mocked_unique_str.hdf5"
            )
            tmp_rel_file_name = (
                "eb-mocked_id/ska-low-mccs/mocked_scan_id"
                f"/mocked_dir/correlation_burst_{channel_id}_mocked_unique_str.hdf5"
            )
            tmp_solution_name = (
                "eb-mocked_id/ska-low-mccs/mocked_scan_id/"
                f"mocked_dir/correlation_burst_{channel_id}_mocked_unique_str"
            )

        station_calibrator_component_manager._station_data_received_result_changed(
            "DataRevievedResult",
            (
                "correlator",
                json.dumps(
                    {
                        "data_mode": "correlator",
                        "file_name": tmp_file_name,
                    }
                ),
            ),
            AttrQuality.ATTR_VALID,
        )
        mock_solver_device_proxy.Solve.assert_next_call(
            json.dumps(
                {
                    "eep_filebase": "tmp_",
                    "jones_solve": False,
                    "adjust_solar_model": False,
                    "masked_antennas": [],
                    "station_config_path": station_config_path,
                    "data_path": tmp_rel_file_name,
                    "solution_path": tmp_solution_name,
                }
            )
        )

        if channel_id == mocked_happy_channel:
            # Sleep to ensure StoreSolution called in thread before assert.
            time.sleep(1)
            mock_calibration_store_device_proxy.StoreSolution.assert_called_with(
                json.dumps(
                    {
                        "outside_temperature": 32.5,
                        "frequency_channel": mocked_happy_channel,
                        "solution": tpm_based_solution,
                        "corrcoeff": calibration_data_example.corrcoeff,
                        "residual_max": calibration_data_example.residual_max,
                        "residual_std": calibration_data_example.residual_std,
                        "xy_phase": calibration_data_example.xy_phase,
                        "n_masked_initial": calibration_data_example.n_masked_initial,
                        "n_masked_final": calibration_data_example.n_masked_final,
                        "lst": calibration_data_example.lst,
                        "galactic_centre_elevation": (
                            calibration_data_example.galactic_centre_elevation
                        ),
                        "sun_elevation": calibration_data_example.sun_elevation,
                        "sun_adjustment_factor": (
                            calibration_data_example.sun_adjustment_factor
                        ),
                        "masked_antennas": calibration_data_example.masked_antennas,
                        "acquisition_time": calibration_data_example.acquisition_time,
                        "station_id": calibration_data_example.station_id,
                        "calibration_path": os.path.splitext(tmp_rel_file_name)[0],
                    },
                    cls=NumpyEncoder,
                )
            )

    station_calibrator_component_manager.stop_calibration_loop()

    mock_solver_device_proxy.Solve.assert_not_called()


def test_stop_calibration(
    station_calibrator_component_manager: StationCalibratorComponentManager,
    mock_solver_device_proxy: unittest.mock.Mock,
    mock_calibration_store_device_proxy: unittest.mock.Mock,
    station_config_path: list[str],
    mock_station_device_proxy: unittest.mock.Mock,
    callbacks: MockCallableGroup,
) -> None:
    """
    Test the calibration process.

    The Solver device has the responsibility of calculating a solution.
    The StationCalibrator must iterate over all channels and orchestrate the
    configuration needed for calibration.

    :param station_calibrator_component_manager: the station calibrator component
        manager under test
    :param mock_solver_device_proxy: a mocked solver device.
    :param mock_calibration_store_device_proxy: a mock calibration store device proxy
        that has been configured with the required calibration store behaviours.
    :param station_config_path: the url and path of the
        telmodel data.
    :param mock_station_device_proxy: a mocked station device.
    :param callbacks: dictionary of driver callbacks.
    """
    assert (
        station_calibrator_component_manager.communication_state
        == CommunicationStatus.DISABLED
    )
    # takes the component out of DISABLED
    station_calibrator_component_manager.start_communicating()
    callbacks["communication_status"].assert_call(CommunicationStatus.NOT_ESTABLISHED)
    callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)

    # Sleep because solver is treated as a service and is not wrapped up in state of
    # station calibrator. Without this sleep it is possible the solver is not yet
    # reporting ON, therefore we cannot calibrate.
    time.sleep(2)

    station_calibrator_component_manager._data_timeout = 5
    station_calibrator_component_manager._outside_temperature = 32.5
    initial_channel = 50
    last_channel = 448
    station_calibrator_component_manager.start_calibration_loop(
        {
            "eep_filebase": "tmp_",
            "jones_solve": False,
            "adjust_solar_model": False,
            "masked_antennas": [],
            "station_config_path": station_config_path,
            "first_channel": initial_channel,
            "last_channel": last_channel,
        }
    )
    mock_solver_device_proxy.Solve.assert_not_called()

    station_calibrator_component_manager.start_calibration_loop(
        {
            "eep_filebase": "tmp_",
            "jones_solve": False,
            "adjust_solar_model": False,
            "masked_antennas": [],
            "station_config_path": station_config_path,
            "first_channel": initial_channel,
            "last_channel": last_channel,
        }
    )

    for channel_id in range(initial_channel, initial_channel + 6):
        if channel_id < initial_channel + 5:
            mock_station_device_proxy.AcquireDataForCalibration.assert_last_call(
                channel_id
            )
        if channel_id == initial_channel + 4:
            station_calibrator_component_manager.stop_calibration_loop()

        station_calibrator_component_manager._station_data_received_result_changed(
            "DataRevievedResult",
            (
                "correlator",
                json.dumps(
                    {
                        "data_mode": "correlator",
                        "file_name": "/product/eb-mocked_id/ska-low-mccs/"
                        "mocked_scan_id/mocked_dir/correlation_burst_"
                        f"{channel_id}_mocked_unique_str.hdf5",
                    }
                ),
            ),
            AttrQuality.ATTR_VALID,
        )

        if channel_id < initial_channel + 4:
            mock_solver_device_proxy.Solve.assert_last_call(
                json.dumps(
                    {
                        "eep_filebase": "tmp_",
                        "jones_solve": False,
                        "adjust_solar_model": False,
                        "masked_antennas": [],
                        "station_config_path": station_config_path,
                        "data_path": "eb-mocked_id/ska-low-mccs/mocked_scan_id/"
                        f"mocked_dir/correlation_burst_{channel_id}_"
                        "mocked_unique_str.hdf5",
                        "solution_path": "eb-mocked_id/ska-low-mccs/mocked_scan_id/"
                        f"mocked_dir/correlation_burst_{channel_id}"
                        "_mocked_unique_str",
                    }
                )
            )

    mock_station_device_proxy.AcquireDataForCalibration.assert_not_called()
    mock_solver_device_proxy.Solve.assert_not_called()
    station_calibrator_component_manager._calibration_loop_thread.join(timeout=10)
    assert not station_calibrator_component_manager._calibration_loop_thread.is_alive()
