# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defines a pytest harness for testing the MCCS controller module."""
from __future__ import annotations

import logging
import unittest
from typing import Callable, Iterable, Iterator, Optional

import pytest
import pytest_mock
import tango
from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    HealthState,
    PowerState,
    ResultCode,
    TaskStatus,
)
from ska_low_mccs_common import MccsDeviceProxy
from ska_low_mccs_common.testing.mock import MockDeviceBuilder, MockSubarrayBuilder
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.controller import (
    ControllerComponentManager,
    ControllerResourceManager,
    MccsController,
)
from tests.harness import (
    get_station_beam_trl,
    get_station_trl,
    get_subarray_beam_trl,
    get_subarray_trl,
)


@pytest.fixture(name="subarray_ids")
def subarray_ids_fixture() -> list[int]:
    """
    Return the IDs of subarrays managed by the controller.

    :return: the IDs of subarrays managed by the controller.
    """
    # TODO: This must match the MccsSubarrays property of the
    # controller. We should refactor the harness so that we can pull it
    # straight from the device configuration.
    return [1, 2]


@pytest.fixture(name="station_names")
def station_names_fixture() -> list[str]:
    """
    Return the names of stations managed by the controller.

    :return: the names of stations managed by the controller.
    """
    # TODO: This must match the MccsStations property of the
    # controller. We should refactor the harness so that we can pull it
    # straight from the device configuration.
    return ["ci-1", "ci-2"]


@pytest.fixture(name="station_ids")
def station_ids_fixture() -> list[int]:
    """
    Return the ids of stations managed by the controller.

    :return: the ids of stations managed by the controller.
    """
    # TODO: This must match the MccsStations property of the
    # controller. We should refactor the harness so that we can pull it
    # straight from the device configuration.
    return [1, 2]


@pytest.fixture(name="subarray_beam_ids")
def subarray_beam_ids_fixture() -> list[int]:
    """
    Return the IDs of subarray_beams managed by the controller.

    :return: the IDs of subarray_beams managed by the controller.
    """
    # TODO: This must match the MccsSubarrayBeams property of the
    # controller. We should refactor the harness so that we can pull it
    # straight from the device configuration.
    return [1, 2, 3, 4]


@pytest.fixture(name="station_beam_ids")
def station_beam_ids_fixture() -> list[tuple[str, int]]:
    """
    Return the IDs of station_beams managed by the controller.

    :return: the TRLs of station_beams managed by the controller.
    """
    # TODO: This must match the MccsStationBeams property of the
    # controller. We should refactor the harness so that we can pull it
    # straight from the device configuration.
    return [("ci-1", 1), ("ci-1", 2), ("ci-2", 1), ("ci-2", 2)]


@pytest.fixture(name="channel_blocks")
def channel_blocks_fixture() -> list[int]:
    """
    Return the channel blocks controlled by this controller.

    :return: the channel blocks controller by this controller.
    """
    return list(range(1, 49))  # TODO: Should this be "range(9, 57)"?


@pytest.fixture(name="controller_resource_manager")
def controller_resource_manager_fixture(
    subarray_ids: Iterable[int],
    subarray_beam_ids: Iterable[int],
    station_beam_ids: Iterable[tuple[str, int]],
    channel_blocks: Iterable[int],
) -> ControllerResourceManager:
    """
    Return a controller resource manager for testing.

    :param subarray_ids: IDs of all subarray devices
    :param subarray_beam_ids: IDs of all subarray beam devices
    :param station_beam_ids: IDs of all subarray beam devices
    :param channel_blocks: ordinal numbers of all channel blocks

    :return: a controller resource manager for testing
    """
    return ControllerResourceManager(
        [get_subarray_trl(i) for i in subarray_ids],
        [get_subarray_beam_trl(i) for i in subarray_beam_ids],
        [get_station_beam_trl(label, i) for label, i in station_beam_ids],
        channel_blocks,
    )


@pytest.fixture(name="mock_subarray_factory")
def mock_subarray_factory_fixture() -> MockSubarrayBuilder:
    """
    Fixture that provides a factory for mock subarrays.

    :return: a factory for mock subarray
    """
    builder = MockSubarrayBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    builder.add_result_command("AssignResources", result_code=ResultCode.QUEUED)
    builder.add_result_command("ReleaseAllResources", result_code=ResultCode.QUEUED)
    builder.add_result_command("AbortDevice", result_code=ResultCode.STARTED)
    builder.add_result_command("Restart", result_code=ResultCode.QUEUED)
    return builder


@pytest.fixture(name="mock_station_1_factory")
def mock_station_1_factory_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a factory for mock stations.

    The only special behaviour of these mocks is they return a
    (result_code, message) tuple in response on/off calls.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    builder.add_attribute("stationID", 1)
    builder.add_result_command("Off", result_code=ResultCode.QUEUED)
    builder.add_result_command("On", result_code=ResultCode.QUEUED)
    builder.add_result_command("StartAcquisition", result_code=ResultCode.QUEUED)
    return builder


@pytest.fixture(name="mock_station_2_factory")
def mock_station_2_factory_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a factory for mock stations.

    The only special behaviour of these mocks is they return a
    (result_code, message) tuple in response on/off calls.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    builder.add_attribute("stationID", 2)
    builder.add_result_command("Off", result_code=ResultCode.QUEUED)
    builder.add_result_command("On", result_code=ResultCode.QUEUED)
    builder.add_result_command("StartAcquisition", result_code=ResultCode.QUEUED)
    return builder


@pytest.fixture(name="mock_subarray_beam_factory")
def mock_subarray_beam_factory_fixture() -> Callable[[], unittest.mock.Mock]:
    """
    Return a factory that returns mock subarray beam devices for use in testing.

    :return: a factory that returns mock subarray beam devices for use in testing
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    builder.add_result_command("AbortDevice", result_code=ResultCode.STARTED)
    builder.add_result_command("Restart", result_code=ResultCode.QUEUED)
    builder.add_result_command("SetParentTrl", result_code=ResultCode.OK)
    builder.add_result_command("ResetParentTrl", result_code=ResultCode.OK)
    return builder


@pytest.fixture(name="mock_station_beam_factory")
def mock_station_beam_factory_fixture() -> Callable[[], unittest.mock.Mock]:
    """
    Return a factory that returns mock station beam devices for use in testing.

    :return: a mock station beam device for use in testing.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    builder.add_result_command("Abort", result_code=ResultCode.STARTED)
    builder.add_result_command("Restart", result_code=ResultCode.QUEUED)
    builder.add_result_command("SetParentTrl", result_code=ResultCode.OK)
    builder.add_result_command("ResetParentTrl", result_code=ResultCode.OK)
    return builder


@pytest.fixture(name="subarray_proxies")
def subarray_proxies_fixture(
    subarray_ids: Iterable[int], logger: logging.Logger
) -> list[MccsDeviceProxy]:
    """
    Return a dictioanry of proxies to subarray devices.

    :param subarray_ids: IDs of subarrays in the MCCS subsystem.
    :param logger: the logger to be used by the proxies
    :return: a list of proxies to subarray devices
    """
    return [MccsDeviceProxy(get_subarray_trl(i), logger) for i in subarray_ids]


@pytest.fixture(name="station_proxies")
def station_proxies_fixture(
    station_names: Iterable[str], logger: logging.Logger
) -> list[MccsDeviceProxy]:
    """
    Return a list of proxies to station devices.

    :param station_names: names of stations in the MCCS subsystem.
    :param logger: the logger to be used by the proxies
    :return: a list of proxies to station devices
    """
    return [MccsDeviceProxy(get_station_trl(label), logger) for label in station_names]


@pytest.fixture(name="unique_id")
def unique_id_fixture() -> str:
    """
    Return a unique ID used to test Tango layer infrastructure.

    :return: a unique ID
    """
    return "a unique id"


@pytest.fixture(name="controller_component_manager")
def controller_component_manager_fixture(  # pylint: disable=too-many-arguments
    subarray_ids: Iterable[int],
    station_names: Iterable[str],
    subarray_beam_ids: Iterable[int],
    station_beam_ids: Iterable[tuple[str, int]],
    logger: logging.Logger,
    obs_command_timeout: int,
    callbacks: MockCallableGroup,
) -> Iterator[ControllerComponentManager]:
    """
    Return a controller component manager in simulation mode.

    :param subarray_ids: IDs of all subarray devices
    :param station_names: Names of all station devices
    :param subarray_beam_ids: IDs of all subarray beam devices
    :param station_beam_ids: IDs of all station beam devices
    :param logger: the logger to be used by this object.
    :param obs_command_timeout: the default timeout for obs
        commands in seconds.
    :param callbacks: A dictionary of callbacks with async support.

    :yields: a component manager for the MCCS controller device
    """
    yield ControllerComponentManager(
        [get_subarray_trl(i) for i in subarray_ids],
        [get_station_trl(label) for label in station_names],
        [get_subarray_beam_trl(i) for i in subarray_beam_ids],
        [get_station_beam_trl(label, i) for label, i in station_beam_ids],
        logger,
        obs_command_timeout,
        callbacks["communication_state"],
        callbacks["component_state"],
    )


@pytest.fixture(name="mock_component_manager")
def mock_component_manager_fixture(
    mocker: pytest_mock.mocker,  # type: ignore[valid-type]
    unique_id: str,
) -> unittest.mock.Mock:
    """
    Return a mock component manager.

    The mock component manager is a simple mock except for one bit of
    extra functionality: when we call start_communicating() on it, it
    makes calls to callbacks signaling that communication is established
    and the component is off. A mock for get_healths(device_type) has also
    been added, this returns an example state of subdevice healths.

    :param mocker: pytest wrapper for unittest.mockunittest.mock.Mock
    :param unique_id: a unique id used to check Tango layer functionality

    :return: a mock component manager
    """
    mock = mocker.Mock()  # type: ignore[attr-defined]
    mock.is_communicating = False
    mock.on.return_value = (TaskStatus.QUEUED, unique_id)
    mock.off.return_value = (TaskStatus.QUEUED, unique_id)
    mock.reset.return_value = (TaskStatus.QUEUED, unique_id)
    mock.standby.return_value = (TaskStatus.QUEUED, unique_id)

    def _start_communicating(mock: unittest.mock.Mock) -> None:
        mock.is_communicating = True
        mock._communication_state_callback(CommunicationStatus.NOT_ESTABLISHED)
        mock._communication_state_callback(CommunicationStatus.ESTABLISHED)
        mock._component_state_callback(power=PowerState.OFF)
        mock.is_communicating = True

    mock.start_communicating.side_effect = lambda: _start_communicating(mock)
    mock.return_value = unique_id, ResultCode.QUEUED
    device_healths: dict[str, dict[str, Optional[str]]] = {
        "beam": {
            get_station_beam_trl("ci-1", 1): None,
            get_station_beam_trl("ci-1", 2): "OK",
            get_station_beam_trl("ci-2", 1): None,
            get_station_beam_trl("ci-2", 2): None,
        },
        "station": {
            get_station_trl("ci-1"): None,
            get_station_trl("ci-2"): None,
        },
        "subarraybeam": {
            get_subarray_beam_trl(1): None,
            get_subarray_beam_trl(2): None,
            get_subarray_beam_trl(3): None,
            get_subarray_beam_trl(4): None,
        },
        "subarray": {
            get_subarray_trl(1): "OK",
            get_subarray_trl(2): "OK",
        },
    }

    def get_healths(device_type: str) -> dict[str, dict[str, Optional[str]]]:
        if device_type == "all":
            return device_healths
        if device_type not in [*device_healths]:
            raise ValueError(f"{device_type} is not a valid device type.")
        return {device_type: device_healths[device_type]}

    def get_health_trl(trl: str) -> Optional[HealthState]:
        device = trl.split("/")[1]
        if device not in [*device_healths] or trl not in [*device_healths[device]]:
            raise ValueError(f"{trl} is an invalid device.")
        if device_healths[device][trl] is not None:
            return HealthState[str(device_healths[device][trl])]
        return None

    mock.get_healths.side_effect = lambda device_type="all": get_healths(device_type)
    mock.get_health_trl.side_effect = lambda trl=None: get_health_trl(trl)
    mock.max_queued_tasks = 0
    mock.max_executing_tasks = 1

    return mock


@pytest.fixture(name="patched_controller_device_class")
def patched_controller_device_class(
    mock_component_manager: ControllerComponentManager,
) -> type[MccsController]:
    """
    Return a controller device that is patched with a mock component manager.

    :param mock_component_manager: the mock component manager with
        which to patch the device
    :return: a controller device that is patched with a mock component
        manager.
    """

    class PatchedMccsController(MccsController):
        """A controller device patched with a mock component manager."""

        def create_component_manager(
            self: PatchedMccsController,
        ) -> ControllerComponentManager:
            """
            Return a mock component manager instead of the usual one.

            :return: a mock component manager
            """
            self._communication_state: Optional[CommunicationStatus] = None

            mock_component_manager._communication_state_callback = (
                self._communication_state_callback
            )

            mock_component_manager._component_state_callback = (
                self._component_state_callback
            )
            return mock_component_manager

    return PatchedMccsController


@pytest.fixture(name="apertures")
def apertures_fixture(
    station_ids: list[int],
) -> list[dict]:
    """
    Return an example aperture for use in testing.

    :param station_ids: the list of ids of stations.

    :return: an example fixture for use in testing.

    """
    apertures = (
        [
            {
                "station_id": station_ids[0],
                "aperture_id": "AP001.01",
            },
        ],
    )
    # Why does the linter force me to do this...
    return apertures[0]


@pytest.fixture(name="allocate_resource_spec")
def allocate_resource_spec_fixture(
    subarray_beam_ids: list[int],
    subarray_ids: list[int],
    apertures: list[dict],
) -> dict:
    """
    Return an example schema-complient allocate argument for use in testing.

    :param subarray_beam_ids: the list of ids of subarray beams.
    :param subarray_ids: the list of ids of subarrays.
    :param apertures: an example list of apertures for use in testing.

    :return: an example schema-complient allocate argument for use in testing.
    """
    resource_spec = {
        "subarray_id": subarray_ids[0],
        "subarray_beams": [
            {
                "subarray_beam_id": subarray_beam_ids[0],
                "apertures": apertures,
                "number_of_channels": 8 * 48,
            }
        ],
    }
    return resource_spec


@pytest.fixture(name="configure_resource_spec")
def configure_resource_spec_fixture(
    subarray_beam_ids: list[int],
    apertures: list[dict],
) -> dict:
    """
    Return an example schema-complient configure argument for use in testing.

    :param subarray_beam_ids: the list of ids of subarray beams.
    :param apertures: an example list of apertures for use in testing.

    :return: an example schema-complient configure argument for use in testing.
    """
    resource_spec = {
        "transaction_id": "0hush-1uzx6-20230101-95040539",
        "subarray_beams": [
            {
                "subarray_beam_id": subarray_beam_ids[0],
                "update_rate": 1.0,
                "logical_bands": [{"start_channel": 10, "number_of_channels": 8}],
                "apertures": apertures,
                "field": {
                    "target_name": "some star",
                    "timestamp": "2023-01-01T12:12:34+00:00",
                    "reference_frame": "ICRS",
                    "attrs": {
                        "c1": 120.51262735625372,
                        "c1_rate": 0.012002185993764836,
                        "c2": 50.18504722994737,
                        "c2_rate": 0.00918239142320329,
                    },
                },
            }
        ],
    }
    return resource_spec
