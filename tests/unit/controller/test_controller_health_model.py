# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for MccsController."""
from __future__ import annotations

from typing import Any

import pytest
from ska_control_model import HealthState, PowerState

from ska_low_mccs.controller.controller_health_model import ControllerHealthModel


class TestControllerHealth:
    """Tests fro the MCCS controller health model."""

    @pytest.fixture
    def health_model(self: TestControllerHealth) -> ControllerHealthModel:
        """
        Fixture to return the subarray health model.

        :return: Health model to be used.
        """

        def callback(*args: Any, **kwargs: Any) -> None:
            pass

        health_model = ControllerHealthModel(
            callback,
            ["station_1", "station_2"],
            ["subarray_beam_1", "subarray_beam_2"],
            ["station_beam_1", "station_beam_2"],
            {
                "stations_failed_threshold": 0.2,
                "stations_degraded_threshold": 0.05,
                "subarray_beams_failed_threshold": 0.2,
                "subarray_beams_degraded_threshold": 0.05,
                "station_beams_failed_threshold": 0.2,
                "station_beams_degraded_threshold": 0.05,
            },
        )
        health_model.update_state(communicating=True, power=PowerState.ON)

        init_states = {
            "stations": {
                "station_1": HealthState.OK,
                "station_2": HealthState.OK,
                "station_3": HealthState.OK,
                "station_4": HealthState.OK,
                "station_5": HealthState.OK,
                "station_6": HealthState.OK,
            },
            "subarray_beams": {
                "subarray_beam_1": HealthState.OK,
                "subarray_beam_2": HealthState.OK,
                "subarray_beam_3": HealthState.OK,
                "subarray_beam_4": HealthState.OK,
                "subarray_beam_5": HealthState.OK,
                "subarray_beam_6": HealthState.OK,
            },
            "station_beams": {
                "station_beam_1": HealthState.OK,
                "station_beam_2": HealthState.OK,
                "station_beam_3": HealthState.OK,
                "station_beam_4": HealthState.OK,
                "station_beam_5": HealthState.OK,
                "station_beam_6": HealthState.OK,
            },
        }

        for trl, health_state in init_states["stations"].items():
            health_model.station_health_changed(trl, health_state)

        for trl, health_state in init_states["subarray_beams"].items():
            health_model.subarray_beam_health_changed(trl, health_state)

        for trl, health_state in init_states["station_beams"].items():
            health_model.station_beam_health_changed(trl, health_state)

        return health_model

    @pytest.mark.parametrize(
        ("sub_devices", "expected_health", "expected_report"),
        [
            pytest.param(
                {
                    "stations": {
                        "station_1": HealthState.OK,
                        "station_2": HealthState.OK,
                        "station_3": HealthState.OK,
                        "station_4": HealthState.OK,
                        "station_5": HealthState.OK,
                        "station_6": HealthState.OK,
                    },
                    "subarray_beams": {
                        "subarray_beam_1": HealthState.OK,
                        "subarray_beam_2": HealthState.OK,
                        "subarray_beam_3": HealthState.OK,
                        "subarray_beam_4": HealthState.OK,
                        "subarray_beam_5": HealthState.OK,
                        "subarray_beam_6": HealthState.OK,
                    },
                    "station_beams": {
                        "station_beam_1": HealthState.OK,
                        "station_beam_2": HealthState.OK,
                        "station_beam_3": HealthState.OK,
                        "station_beam_4": HealthState.OK,
                        "station_beam_5": HealthState.OK,
                        "station_beam_6": HealthState.OK,
                    },
                },
                HealthState.OK,
                "Health is OK.",
                id="All devices healthy, expect healthy",
            ),
            pytest.param(
                {
                    "stations": {
                        "station_1": HealthState.FAILED,
                        "station_2": HealthState.DEGRADED,
                        "station_3": HealthState.FAILED,
                        "station_4": HealthState.OK,
                        "station_5": HealthState.OK,
                        "station_6": HealthState.OK,
                    },
                    "subarray_beams": {
                        "subarray_beam_1": HealthState.OK,
                        "subarray_beam_2": HealthState.OK,
                        "subarray_beam_3": HealthState.OK,
                        "subarray_beam_4": HealthState.OK,
                        "subarray_beam_5": HealthState.OK,
                        "subarray_beam_6": HealthState.OK,
                    },
                    "station_beams": {
                        "station_beam_1": HealthState.OK,
                        "station_beam_2": HealthState.OK,
                        "station_beam_3": HealthState.OK,
                        "station_beam_4": HealthState.OK,
                        "station_beam_5": HealthState.OK,
                        "station_beam_6": HealthState.OK,
                    },
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: "
                    "Station: ['station_1', 'station_2', 'station_3'], SubarrayBeam: "
                    "[], StationBeam: []"
                ),
                id="Number of sub device stations failed > 20%, expect fail",
            ),
            pytest.param(
                {
                    "stations": {
                        "station_1": HealthState.OK,
                        "station_2": HealthState.OK,
                        "station_3": HealthState.OK,
                        "station_4": HealthState.OK,
                        "station_5": HealthState.OK,
                        "station_6": HealthState.OK,
                    },
                    "subarray_beams": {
                        "subarray_beam_1": HealthState.FAILED,
                        "subarray_beam_2": HealthState.DEGRADED,
                        "subarray_beam_3": HealthState.FAILED,
                        "subarray_beam_4": HealthState.OK,
                        "subarray_beam_5": HealthState.OK,
                        "subarray_beam_6": HealthState.OK,
                    },
                    "station_beams": {
                        "station_beam_1": HealthState.OK,
                        "station_beam_2": HealthState.OK,
                        "station_beam_3": HealthState.OK,
                        "station_beam_4": HealthState.OK,
                        "station_beam_5": HealthState.OK,
                        "station_beam_6": HealthState.OK,
                    },
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: "
                    "Station: [], SubarrayBeam: "
                    "['subarray_beam_1', 'subarray_beam_2', "
                    "'subarray_beam_3'], StationBeam: []"
                ),
                id="Number of sub device subarray_beams failed > 20%, expect fail",
            ),
            pytest.param(
                {
                    "stations": {
                        "station_1": HealthState.OK,
                        "station_2": HealthState.OK,
                        "station_3": HealthState.OK,
                        "station_4": HealthState.OK,
                        "station_5": HealthState.OK,
                        "station_6": HealthState.OK,
                    },
                    "subarray_beams": {
                        "subarray_beam_1": HealthState.OK,
                        "subarray_beam_2": HealthState.OK,
                        "subarray_beam_3": HealthState.OK,
                        "subarray_beam_4": HealthState.OK,
                        "subarray_beam_5": HealthState.OK,
                        "subarray_beam_6": HealthState.OK,
                    },
                    "station_beams": {
                        "station_beam_1": HealthState.DEGRADED,
                        "station_beam_2": HealthState.FAILED,
                        "station_beam_3": HealthState.FAILED,
                        "station_beam_4": HealthState.OK,
                        "station_beam_5": HealthState.OK,
                        "station_beam_6": HealthState.OK,
                    },
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: Station: "
                    "[], SubarrayBeam: [], StationBeam: "
                    "['station_beam_1', 'station_beam_2', 'station_beam_3']"
                ),
                id="Number of sub device station_bams failed > 20%, expect fail",
            ),
            pytest.param(
                {
                    "stations": {
                        "station_1": HealthState.FAILED,
                        "station_2": HealthState.OK,
                        "station_3": HealthState.OK,
                        "station_4": HealthState.OK,
                        "station_5": HealthState.OK,
                        "station_6": HealthState.OK,
                    },
                    "subarray_beams": {
                        "subarray_beam_1": HealthState.OK,
                        "subarray_beam_2": HealthState.OK,
                        "subarray_beam_3": HealthState.OK,
                        "subarray_beam_4": HealthState.OK,
                        "subarray_beam_5": HealthState.OK,
                        "subarray_beam_6": HealthState.OK,
                    },
                    "station_beams": {
                        "station_beam_1": HealthState.OK,
                        "station_beam_2": HealthState.OK,
                        "station_beam_3": HealthState.OK,
                        "station_beam_4": HealthState.OK,
                        "station_beam_5": HealthState.OK,
                        "station_beam_6": HealthState.OK,
                    },
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: Station: "
                    "['station_1'], SubarrayBeam: [], StationBeam: []"
                ),
                id="Number of sub device stations failed > 5%, expect degraded",
            ),
        ],
    )
    def test_controller_evaluate_health(
        self: TestControllerHealth,
        health_model: ControllerHealthModel,
        sub_devices: dict,
        expected_health: HealthState,
        expected_report: str,
    ) -> None:
        """
        Test the evaluate health.

        :param health_model: Fixture to return health model.
        :param sub_devices: Child devices to be tested and their health
        :param expected_health: Expected outcome health of the controller
        :param expected_report: Expected outcome report of the controller
        """
        for trl, health_state in sub_devices["stations"].items():
            health_model.station_health_changed(trl, health_state)

        for trl, health_state in sub_devices["subarray_beams"].items():
            health_model.subarray_beam_health_changed(trl, health_state)

        for trl, health_state in sub_devices["station_beams"].items():
            health_model.station_beam_health_changed(trl, health_state)

        assert health_model.evaluate_health() == (expected_health, expected_report)

    def test_device_becomes_unhealthy(
        self: TestControllerHealth,
        health_model: ControllerHealthModel,
    ) -> None:
        """
        Test the evaluate health.

        :param health_model: Fixture to return health model.
        """
        assert health_model.evaluate_health() == (HealthState.OK, "Health is OK.")

        # With one station failed the controller is degraded
        health_model.station_health_changed("station_1", HealthState.FAILED)
        assert health_model.evaluate_health() == (
            HealthState.DEGRADED,
            (
                "Too many devices in a bad state: Station: "
                "['station_1'], SubarrayBeam: [], StationBeam: []"
            ),
        )

        # Two stations makes the controller fail however
        health_model.station_health_changed("station_2", HealthState.FAILED)
        assert health_model.evaluate_health() == (
            HealthState.FAILED,
            (
                "Too many devices in a bad state: Station: "
                "['station_1', 'station_2'], SubarrayBeam: [], StationBeam: []"
            ),
        )

    def test_can_change_thresholds(
        self: TestControllerHealth,
        health_model: ControllerHealthModel,
    ) -> None:
        """
        Test the evaluate health.

        :param health_model: Fixture to return health model.
        """
        assert health_model.evaluate_health() == (HealthState.OK, "Health is OK.")

        # With one station failed the controller is degraded
        health_model.station_health_changed("station_1", HealthState.FAILED)
        assert health_model.evaluate_health() == (
            HealthState.DEGRADED,
            (
                "Too many devices in a bad state: Station: "
                "['station_1'], SubarrayBeam: [], StationBeam: []"
            ),
        )

        # Change thresholds so that number of stations missing is ok
        health_model.health_params = {"stations_degraded_threshold": 0.2}

        assert health_model.evaluate_health() == (HealthState.OK, "Health is OK.")
