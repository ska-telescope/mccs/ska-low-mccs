# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests of the station component manager."""
from __future__ import annotations

import gc
import json
import logging
import threading
import time
import unittest
import unittest.mock
from time import sleep
from typing import Callable, Iterator
from unittest.mock import MagicMock, Mock

import numpy as np
import pytest
from astropy.time.core import Time
from ska_control_model import CommunicationStatus, PowerState, ResultCode, TaskStatus
from ska_low_mccs_common.testing.mock import MockCallable
from ska_tango_testing.mock import MockCallableGroup
from tango import DevFailed

from ska_low_mccs.station import StationComponentManager
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_antenna_trl,
    get_field_station_trl,
    get_sps_station_trl,
    get_station_calibrator_trl,
)

gc.disable()


@pytest.fixture(name="test_context")
def test_context_fixture(  # pylint: disable=too-many-arguments
    station_name: str,
    mock_field_station: unittest.mock.Mock,
    mock_station_calibrator: unittest.mock.Mock,
    mock_sps_station: unittest.mock.Mock,
    antenna_names: list[str],
    mock_antenna_factory: Callable[[], unittest.mock.Mock],
    tile_ids: list[int],
    mock_tile_factory: Callable[[], unittest.mock.Mock],
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which mock tile and field station devices are running.

    :param station_name: name of the station
    :param mock_field_station: a mock to be returned when a proxy to the
        field station is created
    :param mock_station_calibrator: a mock to be returned when a proxy to the
        station calibrator is created
    :param mock_sps_station: a mock to be returned when a proxy to the
        SPS station is created
    :param antenna_names: A list of antenna names
    :param mock_antenna_factory: a factory that returns a mock antenna
        device each time it is called
    :param tile_ids: A list of tile IDs
    :param mock_tile_factory: a factory that returns a mock tile
        device each time it is called

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()

    harness.add_mock_field_station_device(station_name, mock_field_station)
    harness.add_mock_station_calibrator_device(station_name, mock_station_calibrator)
    harness.add_mock_sps_station_device(station_name, mock_sps_station)

    for antenna_name in antenna_names:
        harness.add_mock_antenna_device(
            station_name, antenna_name, mock_antenna_factory()
        )

    for tile_id in tile_ids:
        harness.add_mock_tile_device(station_name, tile_id, mock_tile_factory())

    with harness as context:
        yield context


# pylint: disable=too-many-arguments
@pytest.fixture(name="station_component_manager")
def station_component_manager_fixture(
    station_name: str,
    station_id: int,
    antenna_names: list[str],
    antenna_positions_smartbox_order: np.ndarray,
    antenna_element_ids_smartbox_order: list[int],
    ref_latitude: float,
    ref_longitude: float,
    ref_height: float,
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> StationComponentManager:
    """
    Return a station component manager.

    :param station_name: name of the station
    :param station_id: the ID of the station
    :param antenna_names: names of the station's antennas
    :param antenna_positions_smartbox_order: array of the
        x, y, z positions of the antennas
    :param antenna_element_ids_smartbox_order: list of the element IDs of the antennas
    :param ref_latitude: Reference latitude for testing.
    :param ref_longitude: Reference longitude for testing.
    :param ref_height: Reference height for testing.

    :param logger: the logger to be used by this object.
    :param callbacks: A dictionary of callbacks with async support.

    :return: a station component manager
    """
    return StationComponentManager(
        station_id,
        ref_latitude,
        ref_longitude,
        ref_height,
        get_field_station_trl(station_name),
        [get_antenna_trl(station_name, antenna_name) for antenna_name in antenna_names],
        antenna_positions_smartbox_order,
        antenna_element_ids_smartbox_order,
        get_station_calibrator_trl(station_name),
        get_sps_station_trl(station_name),
        1.0,
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )


class TestStationComponentManager:
    """Tests of the station component manager."""

    def test_communication(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the station component manager's management of communication.

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager
            under test.
        :param callbacks: A dictionary of callbacks with async support.
        """
        assert (
            station_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )

        station_component_manager.start_communicating()

        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(
            CommunicationStatus.ESTABLISHED, lookahead=14
        )
        assert (
            station_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )
        callbacks["component_state"].assert_call(is_configured=False, lookahead=15)
        station_component_manager.stop_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.DISABLED, lookahead=10
        )
        assert (
            station_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )

    # Note: test_power_commands has been moved to
    # TestStationComponentStateChangedCallback::test_power_commands

    def test_power_events_received(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
        station_name: str,
        antenna_names: list[str],
    ) -> None:
        """
        Test the station component manager's management of power mode.

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager
            under test.
        :param callbacks: A dictionary of callbacks with async support.
        :param station_name: name of the station
        :param antenna_names: IDs of the station's antennas
        """

        # Note: since the base class 0.13 adoption most of this test has been moved into
        # TestStationComponentStateChangedCallback::test_power_events.
        # Therefore this test is very thin, and only checks that change events from the
        # antenna, FieldStation and SpsStation devices are being received.
        flat_trl_list = (
            [
                get_antenna_trl(station_name, antenna_name)
                for antenna_name in antenna_names
            ]
            + [get_field_station_trl(station_name)]
            + [get_sps_station_trl(station_name)]
        )
        station_component_manager.start_communicating()
        for device_trl in flat_trl_list:
            callbacks["component_state"].assert_call(
                power=PowerState.UNKNOWN, trl=device_trl, lookahead=10
            )

    def test_configure(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
        station_id: int,
    ) -> None:
        """
        Test tile attribute assignment.

        Specifically, test that when the station component manager
        established communication with its tiles, it write its station
        id and a unique logical tile id to each one.

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager
            under test.
        :param callbacks: A dictionary of callbacks with async support.
        :param station_id: the id of the station
        """
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        station_component_manager._number_of_channels = 256
        assert station_component_manager._pointing_delays == [[]]
        assert not station_component_manager.is_configured

        with pytest.raises(
            ValueError,
            match="Wrong station id",
        ):
            station_component_manager._configure_semi_static(
                {"StationId": station_id + 1},
                {},
                {},
                task_callback=callbacks["task"],
            )

        assert not station_component_manager.is_configured

        # result = station_component_manager._configure(station_id)
        station_component_manager._configure_semi_static(
            {"StationId": station_id},
            {},
            {},
            task_callback=callbacks["task"],
        )
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result="Configure command has completed",
            lookahead=2,
        )

    # pylint: disable=too-many-locals
    def test_apply_configure(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        nof_antenna: int,
        nof_tiles: int,
        callbacks: MockCallableGroup,
        station_calibrator_proxy: unittest.mock.Mock,
        sps_station_proxy: unittest.mock.Mock,
    ) -> None:
        """
        Test application of beamformer and calibration configuration to SpsStation.

        A unittest application of configuration. That consists of Loading a solution
        from the calibration store. Formatting this and applying to the spsstation
        correctly.

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager
            under test.
        :param nof_tiles: the number of TPMs configured.
        :param nof_antenna: the number of antenna belonging to this station.
        :param callbacks: A dictionary of callbacks with async support.
        :param station_calibrator_proxy: proxy to the station calibrator device
        :param sps_station_proxy: proxy to the sps station device
        """
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        # configure the channel table so the first channel is known
        # beam ID is 1, all other elements are unused
        channel_number = 2
        beam_idx = 0.0
        flattened_unity_jones = [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0]
        nof_channels = 384
        nof_pol = 2
        block_size = 8
        station_component_manager.configure_channels(
            [int(beam_idx), channel_number, 1, 1, 0, 1, 1, 101]
        )
        transaction_id = "txn-m001-20231109-12345"
        station_component_manager._apply_configuration(
            transaction_id, callbacks["task"]
        )
        station_calibrator_proxy.GetCalibration.assert_next_call(
            json.dumps({"frequency_channel": channel_number})
        )

        # We will LoadCalibrationCoefficients for the antennas deployed.
        # All others will get unity.
        for antenna_id in range(nof_tiles * 16):
            sps_station_proxy.LoadCalibrationCoefficients.assert_next_call(
                [antenna_id]
                + [0.5] * nof_antenna * block_size * nof_pol
                + flattened_unity_jones * (nof_channels - nof_antenna * nof_pol)
            )
        sps_station_proxy.LoadCalibrationCoefficients.assert_not_called()

        # TODO: Replace with appropriate asserts
        # sps_station_proxy.ApplyCalibration.assert_next_call("")
        # sps_station_proxy.ApplyCalibration.assert_not_called()

        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "ApplyConfiguration command has completed"),
        )

    def test_configure_fails_when_beamformertable_not_set(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        mock_sps_station: unittest.mock.Mock,
        callbacks: MockCallableGroup,
        station_calibrator_proxy: unittest.mock.Mock,
        sps_station_proxy: unittest.mock.Mock,
    ) -> None:
        """
        Test we correctly report a failure.

        SetBeamformerTable can raise an exception or it can return a ResultCode.FAILED.
        Both are to be treated as a failure.

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager
            under test.
        :param mock_sps_station: the mocked spsstation.
        :param callbacks: A dictionary of callbacks with async support.
        :param station_calibrator_proxy: proxy to the station calibrator device
        :param sps_station_proxy: proxy to the sps station device
        """
        # Create a mock Callable with a failed return.
        mocked_result: ResultCode = ResultCode.FAILED
        mocked_message: str = "mocked to fail"
        __mock_callable = MockCallable(return_value=([mocked_result], [mocked_message]))

        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        # SpsStation SetBeamformerTable returns ResultCode.FAILED
        mock_sps_station.configure_mock(SetBeamformerTable=__mock_callable)
        station_component_manager._apply_configuration(
            "txn-m001-20231109-12345", callbacks["task"]
        )
        callbacks["task"].assert_call(
            status=TaskStatus.FAILED,
            result=(
                mocked_result,
                f"ApplyConfiguration command has failed: {mocked_message}",
            ),
            lookahead=3,
        )

        # SpsStation SetBeamformerTable raises exception
        __mock_callable.set_return_value(RuntimeError("Mocked Exception"))
        with pytest.raises(RuntimeError):
            station_component_manager._apply_configuration(
                "txn-m001-20231109-12345", callbacks["task"]
            )

    def test_load_pointing_delays_alt_az(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
        sps_station_proxy: unittest.mock.Mock,
    ) -> None:
        """
        Test getting pointing delays.

        Test getting pointing delays with altitude and azimuth

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager
            under test.
        :param callbacks: A dictionary of callbacks with async support.
        :param sps_station_proxy: proxy to the sps station device
        """
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        station_component_manager._number_of_channels = 256
        assert station_component_manager._pointing_delays == [[]]
        station_component_manager.setup_pointing_helper()

        delays, _ = station_component_manager._get_pointing_delays(
            "alt_az",
            {
                "altitude": 1,
                "azimuth": 2,
            },
            Time.now(),
            10.0,
        )

        station_component_manager._load_pointing_delays(delays)

        sps_station_proxy.LoadPointingDelays.assert_next_call(delays)

    def test_load_pointing_delays_ra_dec(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
        sps_station_proxy: unittest.mock.Mock,
    ) -> None:
        """
        Test getting pointing delays.

        Test getting pointing delays with right ascension and declination

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager
            under test.
        :param callbacks: A dictionary of callbacks with async support.
        :param sps_station_proxy: proxy to the sps station device
        """
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        station_component_manager._number_of_channels = 256
        assert station_component_manager._pointing_delays == [[]]
        station_component_manager.setup_pointing_helper()

        pointing_type = "ra_dec"
        values = {
            "right_ascension": 0.0,
            "declination": -70.0,
        }

        delays, _ = station_component_manager._get_pointing_delays(
            pointing_type,
            values,
            Time.now(),
            10.0,
        )

        station_component_manager._load_pointing_delays(delays)

        sps_station_proxy.LoadPointingDelays.assert_next_call(delays)

    def test_start_acquisition(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        callbacks: MockCallableGroup,
        station_component_manager: StationComponentManager,
    ) -> None:
        """
        Test for start acquisition.

        :param test_context: the test context in which this test will be run.
        :param callbacks: A dictionary of callbacks with async support.
        :param station_component_manager: the station component manager
            under test.
        """
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        station_component_manager._start_acquisition(
            "{}", task_callback=callbacks["task"]
        )
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result="Start acquisition has completed",
        )

    def test_acquire_data_for_calibration(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        callbacks: MockCallableGroup,
        station_component_manager: StationComponentManager,
        sps_station_proxy: unittest.mock.Mock,
    ) -> None:
        """
        Test for acquire data for calibration.

        :param test_context: the test context in which this test will be run.
        :param callbacks: A dictionary of callbacks with async support.
        :param station_component_manager: the station component manager
            under test.
        :param sps_station_proxy: proxy to the sps station device
        """
        channel = 25
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        sps_station_proxy.on()

        station_component_manager._acquire_data_for_calibration(
            channel, task_callback=callbacks["task"]
        )
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result="AcquireDataForCalibration has completed.",
        )
        sps_station_proxy.AcquireDataForCalibration.assert_next_call(channel)

    def test_track_object_thread(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
        sps_station_proxy: unittest.mock.Mock,
    ) -> None:
        """
        Test track object thread.

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager
            under test.

        :param callbacks: A dictionary of callbacks with async support.

        :param sps_station_proxy: proxy to the sps station device
        """
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        sps_station_proxy.on()

        station_component_manager._number_of_channels = 256
        assert station_component_manager._pointing_delays == [[]]
        station_component_manager.setup_pointing_helper()

        pointing_type = "ra_dec"
        values = {
            "right_ascension": 0.0,
            "declination": -70.0,
        }
        station_beam_number = 1
        scan_time = 2.0
        time_step = 1.0
        tracking_id = 0

        def dummy_func() -> None:
            pass

        station_component_manager.tracking_threads[tracking_id] = threading.Thread(
            target=dummy_func
        )
        station_component_manager.stop_ids[tracking_id] = True

        assert len(station_component_manager.tracking_threads) == 1
        assert len(station_component_manager.stop_ids) == 1

        station_component_manager._track_object_thread(
            lambda: station_component_manager.stop_ids[tracking_id],
            pointing_type,
            values,
            station_beam_number,
            scan_time,
            time_step,
            "2021-05-09T23:00:00",
            tracking_id,
        )
        sps_station_proxy.LoadPointingDelays.assert_not_called()
        sps_station_proxy.ApplyPointingDelays.assert_not_called()
        assert len(station_component_manager.tracking_threads) == 0
        assert len(station_component_manager.stop_ids) == 0

        tracking_id = 1

        station_component_manager.tracking_threads[tracking_id] = threading.Thread(
            target=dummy_func
        )
        station_component_manager.stop_ids[tracking_id] = False

        assert len(station_component_manager.tracking_threads) == 1
        assert len(station_component_manager.stop_ids) == 1

        station_component_manager._track_object_thread(
            lambda: station_component_manager.stop_ids[tracking_id],
            pointing_type,
            values,
            station_beam_number,
            scan_time,
            time_step,
            "2021-05-09T23:00:00",
            tracking_id,
        )
        # Hard to compare the delay values
        # sps_station_proxy.LoadPointingDelays.assert_next_call(delays)
        sps_station_proxy.ApplyPointingDelays.assert_next_call("")
        assert len(station_component_manager.tracking_threads) == 0
        assert len(station_component_manager.stop_ids) == 0

    def test_track_object(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
        sps_station_proxy: unittest.mock.Mock,
    ) -> None:
        """
        Test track object.

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager
            under test.

        :param callbacks: A dictionary of callbacks with async support.

        :param sps_station_proxy: proxy to the sps station device
        """
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        station_component_manager._number_of_channels = 256
        assert station_component_manager._pointing_delays == [[]]
        station_component_manager.setup_pointing_helper()

        pointing_type = "ra_dec"
        values = {
            "right_ascension": 0.0,
            "declination": -70.0,
        }
        station_beam_number = 1
        scan_time = 1.0
        time_step = 1.0
        ref_time = "2021-05-09T23:00:00"

        station_component_manager._track_object(
            pointing_type,
            values,
            ref_time,
            station_beam_number,
            scan_time,
            time_step,
        )
        track_id = 0
        assert station_component_manager.tracking_threads[track_id].is_alive()

        # Assert that the tracking ends after the scan time ends
        sleep(5)

        assert track_id not in station_component_manager.tracking_threads

        scan_time = 3.0
        station_component_manager._track_object(
            pointing_type,
            values,
            ref_time,
            station_beam_number,
            scan_time,
            time_step,
        )
        track_id = 1
        assert station_component_manager.tracking_threads[track_id].is_alive()
        assert track_id in station_component_manager.tracking_threads

        # Assert that you can stop the tracking
        station_component_manager._stop_tracking(track_id)
        assert track_id not in station_component_manager.tracking_threads

        assert len(station_component_manager.tracking_threads) == 0

        # Assert you can have multiple trackings at one time
        station_component_manager._track_object(
            pointing_type,
            values,
            ref_time,
            station_beam_number,
            scan_time,
            time_step,
        )

        station_component_manager._track_object(
            pointing_type,
            values,
            ref_time,
            station_beam_number,
            scan_time,
            time_step,
        )

        station_component_manager._track_object(
            pointing_type,
            values,
            ref_time,
            station_beam_number,
            scan_time,
            time_step,
        )

        assert len(station_component_manager.tracking_threads) == 3

        for my_id in range(2, 4):
            assert station_component_manager.tracking_threads[my_id].is_alive()
            assert my_id in station_component_manager.tracking_threads

        station_component_manager._stop_tracking_all()

        assert len(station_component_manager.tracking_threads) == 0

    def test_track_object_with_bad_antenna_mapping(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
        sps_station_proxy: unittest.mock.Mock,
    ) -> None:
        """
        Test track object with a bad antenna mapping.

        :param test_context: the test context in which this test will be run.
        :param station_component_manager: the station component manager under test.
        :param callbacks: A dictionary of callbacks with async support.
        :param sps_station_proxy: proxy to the sps station device.
        """
        # Start communication
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        # Set reference position and setup pointing helper
        station_component_manager._ref_longitude = 0
        station_component_manager._ref_latitude = 0
        station_component_manager._ref_height = 0
        station_component_manager.setup_pointing_helper()

        # Initialize stop and tracking states
        station_component_manager.stop_ids[1] = False
        mock_thread = MagicMock()
        station_component_manager.tracking_threads[1] = mock_thread
        station_component_manager._sps_station_proxy = unittest.mock.Mock()
        # Simulate a bad antenna mapping by raising a DevFailed exception
        # Create the DevFailed exception
        dev_failed_exception = DevFailed()
        mock_error_object = Mock()
        mock_error_object.desc = "KeyError: 1"  # This simulates the description
        dev_failed_exception.args = [
            mock_error_object,
            "No antenna mapping detected",
            "TrackingCommand",
        ]

        # Assign the SPS station proxy to a variable
        sps_proxy = station_component_manager._sps_station_proxy

        # Assign the exception to side_effect
        sps_proxy.load_pointing_delays.side_effect = dev_failed_exception

        # Set the parameters for track object
        pointing_type = "ra_dec"
        values = {
            "right_ascension": 0.0,
            "declination": -70.0,
        }
        station_beam_number = 1
        scan_time = 3.0
        time_step = 1.0
        ref_time = "2024-01-25T23:00:00"

        # Call the tracking thread method
        station_component_manager._track_object_thread(
            lambda: False,  # Dummy stop function
            pointing_type,
            values,
            station_beam_number,
            scan_time,
            time_step,
            ref_time,
            1,  # Assuming tracking ID is 1
            task_callback=callbacks["task"],
        )
        time.sleep(0.1)

        callbacks["task"].assert_call(
            status=TaskStatus.FAILED,
            result=(
                ResultCode.FAILED,
                "No antenna mapping detected",
            ),  # Match expected result format
            lookahead=3,
        )

    def test_track_object_with_good_antenna_mapping(
        self: TestStationComponentManager,
        test_context: MccsTangoTestHarnessContext,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
        sps_station_proxy: unittest.mock.Mock,
    ) -> None:
        """
        Test tracking an object with a valid antenna mapping.

        This test simulates the successful tracking of an object using the
        station component manager. It ensures that when provided with a
        good antenna mapping, the system can load pointing delays
        correctly and track the specified object without raising any errors.
        The test also verifies that the appropriate callbacks are invoked
        and that the expected state is achieved.

        :param test_context: The test context in which this test will be run.
        :param station_component_manager: The station component manager under test.
        :param callbacks: A dictionary of callbacks with async support for
                        monitoring state changes and task results.
        :param sps_station_proxy: A mock proxy to the SPS station device
                                to simulate its behavior during the test.
        """
        # Start communication
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        # Set reference position and setup pointing helper
        station_component_manager._ref_longitude = 0
        station_component_manager._ref_latitude = 0
        station_component_manager._ref_height = 0
        station_component_manager.setup_pointing_helper()

        # Initialize stop and tracking states
        station_component_manager.stop_ids[1] = False
        # Use a mock object to simulate a thread
        mock_thread = MagicMock()
        station_component_manager.tracking_threads[1] = mock_thread
        station_component_manager._sps_station_proxy = unittest.mock.Mock()
        station_proxy = station_component_manager._sps_station_proxy
        station_proxy.load_pointing_delays.side_effect = None
        # Set the values for tracking parameters
        pointing_type = "ra_dec"
        values = {"right_ascension": 0.0, "declination": -70.0}
        station_beam_number = 1
        scan_time = 3.0
        time_step = 1.0
        reference_time = "2024-01-25T23:00:00"

        # Call the tracking thread method with good antenna mapping
        station_component_manager._track_object_thread(
            stop=lambda: False,  # Dummy stop function to keep tracking going
            pointing_type=pointing_type,
            values=values,
            station_beam_number=station_beam_number,
            scan_time=scan_time,
            time_step=time_step,
            reference_time=reference_time,
            tracking_id=1,
            task_callback=callbacks["task"],
        )
        time.sleep(4)
        # Sleep for longer than the scan time to ensure the thread finishes
        # Mock the application of delays in the proxy and ensure no errors occur
        sps_proxy = station_component_manager._sps_station_proxy
        sps_proxy.apply_pointing_delays.assert_called()
