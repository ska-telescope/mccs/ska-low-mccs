# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defined a pytest harness for testing the MCCS station beam module."""
from __future__ import annotations

import logging
import unittest.mock

import pytest
import tango
from ska_control_model import ResultCode
from ska_low_mccs_common.testing.mock import MockDeviceBuilder
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.station_beam import StationBeamComponentManager
from tests.harness import MccsTangoTestHarnessContext, get_station_trl


@pytest.fixture(name="station_on_name")
def station_on_name_fixture() -> str:
    """
    Return the station label of a station which is on.

    :return: the station label of a station which is on.
    """
    return "ci-1"


@pytest.fixture(name="station_trl")
def station_trl_fixture() -> str:
    """
    Return the station label of a station which is on.

    :return: the station label of a station which is on.
    """
    return get_station_trl("ci-1")


@pytest.fixture(name="station_off_name")
def station_off_name_fixture() -> str:
    """
    Return the station label of a station which is off.

    :return: the station label of a station which is off.
    """
    return "ci-2"


@pytest.fixture(name="beam_id")
def beam_id_fixture() -> int:
    """
    Return a beam id for the station beam under test.

    :return: a beam id for the station beam under test.
    """
    return 1


@pytest.fixture(name="mock_station_beam_component_manager")
def mock_station_beam_component_manager_fixture(
    beam_id: int,
    station_trl: str,
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> StationBeamComponentManager:
    """
    Return a subarray component manager.

    This fixture is identical to the `station_beam_component_manager`
    fixture except for the `tango_harness` which is omitted here to
    avoid a circular reference. This fixture is used to test
    station_beam_device.

    :param beam_id: a beam id for the station beam under test.
    :param station_trl: the TRL of the station to which this beam belongs.
    :param logger: the logger to be used by this object.
    :param callbacks: A dictionary of callbacks with async support.
    :return: a station beam component manager
    """
    return StationBeamComponentManager(
        beam_id,
        station_trl,
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )


@pytest.fixture(name="station_beam_component_manager")
def station_beam_component_manager_fixture(
    test_context: MccsTangoTestHarnessContext,
    beam_id: int,
    station_trl: str,
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> StationBeamComponentManager:
    """
    Return a station beam component manager.

    :param test_context: the test context in which this test will be run.
    :param beam_id: a beam id for the station beam under test.
    :param station_trl: the TRL of the station to which this beam belongs.
    :param logger: the logger to be used by this object.
    :param callbacks: A dictionary of callbacks with async support.
    :return: a station beam component manager
    """
    return StationBeamComponentManager(
        beam_id,
        station_trl,
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )


@pytest.fixture(name="mock_station_off")
def mock_station_off_fixture() -> unittest.mock.Mock:
    """
    Fixture that provides a mock MccsStation device that is in OFF state.

    :return: a mock MccsStation device that is in OFF state.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.OFF)
    return builder()


@pytest.fixture(name="mock_station_on")
def mock_station_on_fixture() -> unittest.mock.Mock:
    """
    Fixture that provides a mock MccsStation device that is in ON state.

    :return: a mock MccsStation device that is in ON state.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_result_command("ConfigureChannels", ResultCode.OK)
    builder.add_result_command("TrackObject", ResultCode.OK)
    builder.add_result_command("EndScan", ResultCode.OK)
    builder.add_result_command("StopTrackingAll", ResultCode.OK)
    return builder()


@pytest.fixture(name="callbacks")
def callbacks_fixture() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "communication_state",
        "component_state",
        "task",
        "abort_task",
        timeout=15.0,
    )
