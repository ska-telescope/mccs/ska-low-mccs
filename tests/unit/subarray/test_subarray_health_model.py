# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for MccsSubarray."""
from __future__ import annotations

from typing import Any

import pytest
from ska_control_model import HealthState

from ska_low_mccs.subarray.subarray_health_model import SubarrayHealthModel
from tests.harness import get_station_beam_trl, get_station_trl, get_subarray_beam_trl


class TestMccsSubarrayHealth:
    """Tests fro the MCCS subarray health model."""

    @pytest.fixture
    def health_model(self: TestMccsSubarrayHealth) -> SubarrayHealthModel:
        """
        Fixture to return the subarray health model.

        :return: Health model to be used.
        """

        def callback(*args: Any, **kwargs: Any) -> None:
            pass

        health_model = SubarrayHealthModel(
            callback,
            ignore_power_state=True,
            thresholds={
                "station_degraded_threshold": 0.05,
                "station_failed_threshold": 0.2,
                "subarray_beam_degraded_threshold": 0.05,
                "subarray_beam_failed_threshold": 0.2,
            },
        )
        health_model.update_state(communicating=True)

        return health_model

    @pytest.mark.parametrize(
        ("sub_devices", "expected_health", "expected_report"),
        [
            pytest.param(
                {
                    "stations": {
                        get_station_trl("ci-1"): HealthState.OK,
                        get_station_trl("ci-2"): HealthState.OK,
                    },
                    "subarray_beams": {
                        get_subarray_beam_trl(1): HealthState.OK,
                        get_subarray_beam_trl(2): HealthState.OK,
                    },
                    "station_beams": {
                        get_station_beam_trl("ci-1", 1): HealthState.OK,
                        get_station_beam_trl("ci-1", 2): HealthState.OK,
                    },
                },
                HealthState.OK,
                "Health is OK.",
                id="All devices healthy, expect healthy",
            ),
            pytest.param(
                {
                    "stations": {
                        get_station_trl("ci-1"): HealthState.FAILED,
                        get_station_trl("ci-2"): HealthState.OK,
                    },
                    "subarray_beams": {
                        get_subarray_beam_trl(1): HealthState.OK,
                        get_subarray_beam_trl(2): HealthState.OK,
                    },
                    "station_beams": {
                        get_station_beam_trl("ci-1", 1): HealthState.OK,
                        get_station_beam_trl("ci-1", 2): HealthState.OK,
                    },
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: MccsStation: "
                    f"['{get_station_trl('ci-1')}'], MccsSubarrayBeam: []"
                ),
                id="Number of sub device stations failed > 20%, expect fail",
            ),
            pytest.param(
                {
                    "stations": {
                        get_station_trl("ci-1"): HealthState.OK,
                        get_station_trl("ci-2"): HealthState.OK,
                    },
                    "subarray_beams": {
                        get_subarray_beam_trl(1): HealthState.FAILED,
                        get_subarray_beam_trl(2): HealthState.OK,
                    },
                    "station_beams": {
                        get_station_beam_trl("ci-1", 1): HealthState.OK,
                        get_station_beam_trl("ci-1", 2): HealthState.OK,
                    },
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: MccsStation: "
                    f"[], MccsSubarrayBeam: ['{get_subarray_beam_trl(1)}']"
                ),
                id="Number of sub device subarray_beams failed > 20%, expect fail",
            ),
            pytest.param(
                {
                    "stations": {
                        get_station_trl("ci-1"): HealthState.OK,
                        get_station_trl("ci-2"): HealthState.OK,
                    },
                    "subarray_beams": {
                        get_subarray_beam_trl(1): HealthState.OK,
                        get_subarray_beam_trl(2): HealthState.OK,
                    },
                    "station_beams": {
                        get_station_beam_trl("ci-1", 1): HealthState.FAILED,
                        get_station_beam_trl("ci-1", 2): HealthState.OK,
                    },
                },
                HealthState.OK,
                "Health is OK.",
                id="""Number of sub device station_bams failed > 20%,
                not dependant on subarray health, expect healthy""",
            ),
            pytest.param(
                {
                    "stations": {
                        get_station_trl("ci-1"): HealthState.DEGRADED,
                        get_station_trl("ci-2"): HealthState.OK,
                        get_station_trl("ci-3"): HealthState.OK,
                        get_station_trl("ci-4"): HealthState.OK,
                        get_station_trl("ci-5"): HealthState.OK,
                        get_station_trl("ci-6"): HealthState.OK,
                    },
                    "subarray_beams": {
                        get_subarray_beam_trl(1): HealthState.OK,
                        get_subarray_beam_trl(2): HealthState.OK,
                    },
                    "station_beams": {
                        get_station_beam_trl("ci-1", 1): HealthState.OK,
                        get_station_beam_trl("ci-1", 2): HealthState.OK,
                    },
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: MccsStation: "
                    f"['{get_station_trl('ci-1')}'], MccsSubarrayBeam: []"
                ),
                id="Number of sub device stations failed > 5%, expect degraded",
            ),
            pytest.param(
                {
                    "stations": {
                        get_station_trl("ci-1"): HealthState.OK,
                        get_station_trl("ci-2"): HealthState.OK,
                    },
                    "subarray_beams": {
                        get_subarray_beam_trl(1): HealthState.FAILED,
                        get_subarray_beam_trl(2): HealthState.OK,
                        get_subarray_beam_trl(3): HealthState.OK,
                        get_subarray_beam_trl(4): HealthState.OK,
                        get_subarray_beam_trl(5): HealthState.OK,
                        get_subarray_beam_trl(6): HealthState.OK,
                    },
                    "station_beams": {
                        get_station_beam_trl("ci-1", 1): HealthState.OK,
                        get_station_beam_trl("ci-1", 2): HealthState.OK,
                    },
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: MccsStation: "
                    f"[], MccsSubarrayBeam: ['{get_subarray_beam_trl(1)}']"
                ),
                id="Number of sub device subarray_beams failed > 5%, expect degraded",
            ),
        ],
    )
    def test_subarray_evaluate_health(
        self: TestMccsSubarrayHealth,
        health_model: SubarrayHealthModel,
        sub_devices: dict,
        expected_health: HealthState,
        expected_report: str,
    ) -> None:
        """
        Test the evaluate health.

        :param health_model: Fixture to return health model.
        :param sub_devices: Child devices to be tested and their health
        :param expected_health: Expected outcome health of the subarray
        :param expected_report: Expected outcome report of the subarray
        """
        health_model.resources_changed(
            set(sub_devices["stations"].keys()),
            set(sub_devices["subarray_beams"].keys()),
            set(sub_devices["station_beams"].keys()),
        )

        for trl, health_state in sub_devices["stations"].items():
            health_model.station_health_changed(trl, health_state)

        for trl, health_state in sub_devices["subarray_beams"].items():
            health_model.subarray_beam_health_changed(trl, health_state)

        for trl, health_state in sub_devices["station_beams"].items():
            health_model.station_beam_health_changed(trl, health_state)

        assert health_model.evaluate_health() == (expected_health, expected_report)

    @pytest.mark.parametrize(
        (
            "sub_devices",
            "health_change",
            "expected_init_health",
            "expected_init_report",
            "expected_final_health",
            "expected_final_report",
        ),
        [
            pytest.param(
                {
                    "stations": {
                        get_station_trl(f"ci-{i}"): HealthState.OK for i in range(256)
                    },
                    "subarray_beams": {
                        get_subarray_beam_trl(i): HealthState.OK for i in range(16)
                    },
                    "station_beams": {
                        get_station_beam_trl("ci-1", i): HealthState.OK
                        for i in range(16)
                    },
                },
                {
                    "stations": {
                        get_station_trl(f"ci-{i}"): HealthState.FAILED
                        for i in range(64)
                    },
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: "
                    "MccsStation: "
                    + str(sorted([get_station_trl(f"ci-{i}") for i in range(64)]))
                    + ", MccsSubarrayBeam: []"
                ),
                id="All devices healthy, expect OK, then 1/4 stations FAILED, "
                "expect FAILED",
            ),
            pytest.param(
                {
                    "stations": {
                        get_station_trl(f"ci-{i}"): HealthState.OK for i in range(256)
                    },
                    "subarray_beams": {
                        get_subarray_beam_trl(i): HealthState.OK for i in range(16)
                    },
                    "station_beams": {
                        get_station_beam_trl("ci-1", i): HealthState.OK
                        for i in range(16)
                    },
                },
                {
                    "stations": {
                        get_station_trl(f"ci-{i}"): HealthState.FAILED for i in range(2)
                    },
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.OK,
                "Health is OK.",
                id="All devices healthy, expect OK, then 2 stations FAILED, expect OK",
            ),
            pytest.param(
                {
                    "stations": {
                        get_station_trl(f"ci-{i}"): HealthState.OK for i in range(256)
                    },
                    "subarray_beams": {
                        get_subarray_beam_trl(i): (
                            HealthState.OK if i != 0 else HealthState.FAILED
                        )
                        for i in range(16)
                    },
                    "station_beams": {
                        get_station_beam_trl("ci-1", i): HealthState.OK
                        for i in range(16)
                    },
                },
                {
                    "subarray_beams": {get_subarray_beam_trl(0): HealthState.OK},
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: MccsStation: [],"
                    f" MccsSubarrayBeam: ['{get_subarray_beam_trl(0)}']"
                ),
                HealthState.OK,
                "Health is OK.",
                id="One subarray beam unhealthy, expect DEGRADED, then subarray "
                "becomes OK, expect OK",
            ),
        ],
    )
    # pylint: disable=too-many-arguments
    def test_station_evaluate_changed_health(
        self: TestMccsSubarrayHealth,
        health_model: SubarrayHealthModel,
        sub_devices: dict,
        health_change: dict[str, dict[str, HealthState]],
        expected_init_health: HealthState,
        expected_init_report: str,
        expected_final_health: HealthState,
        expected_final_report: str,
    ) -> None:
        """
        Tests of the station health model for a changed health.

        The properties of the health model are set and checked, then the health states
        of subservient devices are updated and the health is checked against the new
        expected value.

        :param health_model: The station health model to use
        :param sub_devices: the devices for which the station cares about health,
            and their healths
        :param health_change: a dictionary of the health changes, key device and value
            dictionary of trl:HealthState
        :param expected_init_health: the expected initial health
        :param expected_init_report: the expected initial report
        :param expected_final_health: the expected final health
        :param expected_final_report: the expected final report
        """
        health_model.resources_changed(
            set(sub_devices["stations"].keys()),
            set(sub_devices["subarray_beams"].keys()),
            set(sub_devices["station_beams"].keys()),
        )

        for trl, health_state in sub_devices["stations"].items():
            health_model.station_health_changed(trl, health_state)

        for trl, health_state in sub_devices["subarray_beams"].items():
            health_model.subarray_beam_health_changed(trl, health_state)

        for trl, health_state in sub_devices["station_beams"].items():
            health_model.station_beam_health_changed(trl, health_state)

        assert health_model.evaluate_health() == (
            expected_init_health,
            expected_init_report,
        )

        health_update = {
            "stations": health_model.station_health_changed,
            "subarray_beams": health_model.subarray_beam_health_changed,
            "station_beams": health_model.station_beam_health_changed,
        }

        for device in health_change:
            changes = health_change[device]
            for change in changes:
                health_update[device](change, changes[change])

        assert health_model.evaluate_health() == (
            expected_final_health,
            expected_final_report,
        )

    @pytest.mark.parametrize(
        (
            "init_thresholds",
            "expected_init_health",
            "expected_init_report",
            "final_thresholds",
            "expected_final_health",
            "expected_final_report",
        ),
        [
            pytest.param(
                {
                    "station_degraded_threshold": 0.5,
                    "station_failed_threshold": 0.6,
                    "subarray_beam_degraded_threshold": 0.5,
                    "subarray_beam_failed_threshold": 0.6,
                },
                HealthState.OK,
                "Health is OK.",
                {
                    "station_degraded_threshold": 0.05,
                    "station_failed_threshold": 0.2,
                    "subarray_beam_degraded_threshold": 0.5,
                    "subarray_beam_failed_threshold": 0.6,
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: "
                    f"MccsStation: ['{get_station_trl('ci-1')}'], "
                    f"MccsSubarrayBeam: ['{get_subarray_beam_trl(1)}',"
                    f" '{get_subarray_beam_trl(2)}']"
                ),
                id="Expect degraded",
            ),
            pytest.param(
                {
                    "station_degraded_threshold": 0.5,
                    "station_failed_threshold": 0.6,
                    "subarray_beam_degraded_threshold": 0.5,
                    "subarray_beam_failed_threshold": 0.6,
                },
                HealthState.OK,
                "Health is OK.",
                {
                    "station_degraded_threshold": 0.5,
                    "station_failed_threshold": 0.6,
                    "subarray_beam_degraded_threshold": 0.05,
                    "subarray_beam_failed_threshold": 0.2,
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: "
                    f"MccsStation: ['{get_station_trl('ci-1')}'], "
                    f"MccsSubarrayBeam: ['{get_subarray_beam_trl(1)}',"
                    f" '{get_subarray_beam_trl(2)}']"
                ),
                id="Expect failed",
            ),
            pytest.param(
                {
                    "station_degraded_threshold": 0.05,
                    "station_failed_threshold": 0.2,
                    "subarray_beam_degraded_threshold": 0.5,
                    "subarray_beam_failed_threshold": 0.6,
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: "
                    f"MccsStation: ['{get_station_trl('ci-1')}'], "
                    f"MccsSubarrayBeam: ['{get_subarray_beam_trl(1)}',"
                    f" '{get_subarray_beam_trl(2)}']"
                ),
                {
                    "station_degraded_threshold": 0.5,
                    "station_failed_threshold": 0.6,
                    "subarray_beam_degraded_threshold": 0.5,
                    "subarray_beam_failed_threshold": 0.6,
                },
                HealthState.OK,
                "Health is OK.",
                id="Expect healthy",
            ),
        ],
    )
    # pylint: disable=too-many-arguments
    def test_station_evaluate_health_changed_thresholds(
        self: TestMccsSubarrayHealth,
        health_model: SubarrayHealthModel,
        init_thresholds: dict[str, float],
        expected_init_health: HealthState,
        expected_init_report: str,
        final_thresholds: dict[str, float],
        expected_final_health: HealthState,
        expected_final_report: str,
    ) -> None:
        """
        Tests of the station health model for changed thresholds.

        The properties of the health model are set and checked, then the thresholds for
        the health rules are changed and the new health is checked against the expected
        value

        :param health_model: The station health model to use
        :param init_thresholds: the initial thresholds of the health rules
        :param expected_init_health: the expected initial health
        :param expected_init_report: the expected initial report
        :param final_thresholds: the final thresholds of the health rules
        :param expected_final_health: the expected final health
        :param expected_final_report: the expected final report
        """
        sub_devices = {
            "stations": {
                get_station_trl("ci-1"): HealthState.FAILED,
                get_station_trl("ci-2"): HealthState.OK,
                get_station_trl("ci-3"): HealthState.OK,
                get_station_trl("ci-4"): HealthState.OK,
                get_station_trl("ci-5"): HealthState.OK,
                get_station_trl("ci-6"): HealthState.OK,
            },
            "subarray_beams": {
                get_subarray_beam_trl(1): HealthState.FAILED,
                get_subarray_beam_trl(2): HealthState.FAILED,
                get_subarray_beam_trl(3): HealthState.OK,
                get_subarray_beam_trl(4): HealthState.OK,
                get_subarray_beam_trl(5): HealthState.OK,
                get_subarray_beam_trl(6): HealthState.OK,
            },
            "station_beams": {
                get_station_beam_trl("ci-1", 1): HealthState.OK,
                get_station_beam_trl("ci-1", 2): HealthState.OK,
                get_station_beam_trl("ci-1", 3): HealthState.OK,
                get_station_beam_trl("ci-1", 4): HealthState.OK,
                get_station_beam_trl("ci-1", 5): HealthState.OK,
                get_station_beam_trl("ci-1", 6): HealthState.OK,
            },
        }

        health_model.resources_changed(
            set(sub_devices["stations"].keys()),
            set(sub_devices["subarray_beams"].keys()),
            set(sub_devices["station_beams"].keys()),
        )

        for trl, health_state in sub_devices["stations"].items():
            health_model.station_health_changed(trl, health_state)

        for trl, health_state in sub_devices["subarray_beams"].items():
            health_model.subarray_beam_health_changed(trl, health_state)

        for trl, health_state in sub_devices["station_beams"].items():
            health_model.station_beam_health_changed(trl, health_state)

        health_model.health_params = init_thresholds
        assert health_model.evaluate_health() == (
            expected_init_health,
            expected_init_report,
        )

        health_model.health_params = final_thresholds
        assert health_model.evaluate_health() == (
            expected_final_health,
            expected_final_report,
        )
