# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
# pylint: disable=too-many-lines

"""This module contains the tests of the station beam component manager."""
from __future__ import annotations

import json
import threading
import time
import unittest
from typing import Iterator

import pytest
from ska_control_model import (
    CommunicationStatus,
    ObsState,
    PowerState,
    ResultCode,
    TaskStatus,
)
from ska_low_mccs_common.testing.mock import MockCallableDeque
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.subarray import SubarrayComponentManager
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_station_beam_trl,
    get_station_trl,
    get_subarray_beam_trl,
)


@pytest.fixture(name="test_context")
# pylint: disable=too-many-arguments
def test_context_fixture(
    station_on_name: str,
    station_off_name: str,
    subarray_beam_id: int,
    station_beam_id: int,
    mock_station_on: unittest.mock.Mock,
    mock_station_off: unittest.mock.Mock,
    mock_subarray_beam: unittest.mock.Mock,
    mock_station_beam: unittest.mock.Mock,
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which mock tile and field station devices are running.

    :param station_on_name: the name of a station which is on.
    :param station_off_name: the name of a station which is off.
    :param subarray_beam_id: the id of a subarray beam.
    :param station_beam_id: the id of a station beam.

    :param mock_station_on: a mock tango device for a station which is on.
    :param mock_station_off: a mock tango device for a station which is off.
    :param mock_subarray_beam: a mock tango device for a subarray beam.
    :param mock_station_beam: a mock tango device for a station beam.

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()

    harness.add_mock_station_device(station_on_name, mock_station_on)
    harness.add_mock_station_device(station_off_name, mock_station_off)
    harness.add_mock_subarray_beam_device(subarray_beam_id, mock_subarray_beam)
    harness.add_mock_station_beam_device(
        station_on_name, station_beam_id, mock_station_beam
    )

    with harness as context:
        yield context


# pylint: disable=line-too-long, useless-suppression
class TestSubarrayComponentManager:
    """Class for testing the subarray component manager."""

    # pylint: disable=too-many-arguments
    def test_communication(
        self: TestSubarrayComponentManager,
        subarray_component_manager: SubarrayComponentManager,
        callbacks: MockCallableGroup,
        assign_resource_spec: dict,
    ) -> None:
        """
        Test the component manager's communication with its assigned devices.

        :param subarray_component_manager: the subarray component
            manager under test with some methods wrapped with a
            MockCallable.
        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param callbacks: A dictionary of callbacks with async support.
        """
        subarray_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            subarray_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )

        result_code, response = subarray_component_manager.assign(
            subarray_id=assign_resource_spec["subarray_id"],
            subarray_beams=assign_resource_spec["subarray_beams"],
        )
        assert result_code == TaskStatus.QUEUED
        assert response == "Task queued"
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.DISABLED)
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            subarray_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )

        subarray_component_manager.stop_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.DISABLED, lookahead=10, consume_nonmatches=True
        )
        assert (
            subarray_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )

        subarray_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            subarray_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )

    # pylint: disable=too-many-arguments
    def test_assign_and_release(
        self: TestSubarrayComponentManager,
        subarray_component_manager: SubarrayComponentManager,
        station_on_name: str,
        station_off_name: str,
        subarray_beam_id: int,
        station_beam_id: int,
        channel_blocks: list[int],
        assign_resource_spec: dict,
        apertures: list[dict],
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's handling of configuration.

        :param subarray_component_manager: the subarray component
            manager under test.
        :param station_on_name: the label of a station that is powered
            on.
        :param station_off_name: the label of a station that is powered
            off.
        :param subarray_beam_id: the id of a subarray beam.
        :param station_beam_id: the id of a station beam.
        :param channel_blocks: a list of channel blocks.
        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param apertures: An object holding an example aperture.
        :param callbacks: A dictionary of callbacks with async support.
        """
        subarray_component_manager.start_communicating()
        callbacks["component_state"].assert_call(power=PowerState.ON)

        assert subarray_component_manager.assigned_resources_dict == {
            "stations": [],
            "subarray_beams": [],
            "station_beams": [],
            "apertures": [],
            "channels": [0],
        }

        # Assignment from empty
        result_code, response = subarray_component_manager.assign(
            callbacks["task"],
            subarray_id=assign_resource_spec["subarray_id"],
            subarray_beams=assign_resource_spec["subarray_beams"],
        )
        assert result_code == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        time.sleep(0.1)
        for trl in subarray_component_manager._device_obs_states:
            subarray_component_manager._device_obs_state_changed(trl, ObsState.IDLE)

        callbacks["component_state"].assert_call(
            resources_changed=[
                {get_station_trl(station_on_name)},
                {get_subarray_beam_trl(subarray_beam_id)},
                {get_station_beam_trl(station_on_name, station_beam_id)},
            ],
            lookahead=10,
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Assign resources completed OK."),
        )

        # subarray connects to stations, subscribes to change events on power mode,
        # doesn't consider resource assignment to be complete until it has received an
        # event from each one. So let's fake that.
        subarray_component_manager._station_power_state_changed(
            get_station_trl(station_on_name), PowerState.ON
        )

        assert subarray_component_manager.assigned_resources_dict == {
            "stations": [get_station_trl(station_on_name)],
            "subarray_beams": [get_subarray_beam_trl(subarray_beam_id)],
            "station_beams": [get_station_beam_trl(station_on_name, station_beam_id)],
            "apertures": [apertures[0]["aperture_id"]],
            "channels": [channel_block * 8 for channel_block in channel_blocks],
        }

        # Further assign
        apertures[0]["station_id"] = station_off_name
        apertures[0]["station_trl"] = get_station_trl(station_off_name)
        assign_resource_spec["apertures"] = apertures
        result_code, response = subarray_component_manager.assign(
            callbacks["task"],
            subarray_id=assign_resource_spec["subarray_id"],
            subarray_beams=assign_resource_spec["subarray_beams"],
        )
        assert result_code == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        time.sleep(1)
        for trl in subarray_component_manager._device_obs_states:
            subarray_component_manager._device_obs_state_changed(trl, ObsState.IDLE)

        callbacks["component_state"].assert_call(
            resources_changed=[
                {
                    get_station_trl(station_on_name),
                    get_station_trl(station_off_name),
                },
                {get_subarray_beam_trl(subarray_beam_id)},
                {get_station_beam_trl(station_on_name, station_beam_id)},
            ],
            lookahead=10,
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Assign resources completed OK."),
        )

        assert subarray_component_manager.assigned_resources_dict == {
            "stations": [
                get_station_trl(station_off_name),
                get_station_trl(station_on_name),
            ],
            "subarray_beams": [get_subarray_beam_trl(subarray_beam_id)],
            "station_beams": [get_station_beam_trl(station_on_name, station_beam_id)],
            "apertures": [apertures[0]["aperture_id"]],
            "channels": [channel_block * 8 for channel_block in channel_blocks],
        }

        # Release all
        result_code, response = subarray_component_manager.release_all(
            callbacks["task"]
        )
        assert result_code == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        time.sleep(0.5)
        for trl in subarray_component_manager._device_obs_states:
            subarray_component_manager._device_obs_state_changed(trl, ObsState.EMPTY)

        callbacks["component_state"].assert_call(
            resources_changed=[
                set(),
                set(),
                set(),
            ],
            lookahead=10,
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "ReleaseAllResources has completed OK."),
        )

        assert subarray_component_manager.assigned_resources_dict == {
            "stations": [],
            "subarray_beams": [],
            "station_beams": [],
            "apertures": [],
            "channels": [0],
        }

    def test_release(
        self: TestSubarrayComponentManager,
        subarray_component_manager: SubarrayComponentManager,
        station_off_name: str,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's handling of the release command.

        :param subarray_component_manager: the subarray component
            manager under test.
        :param station_off_name: the label of a station that is powered
            off.
        :param callbacks: A dictionary of callbacks with async support.
        """
        subarray_component_manager.start_communicating()
        callbacks["component_state"].assert_call(power=PowerState.ON)

        with pytest.raises(
            NotImplementedError,
            match="MCCS Subarray cannot partially release resources.",
        ):
            # Following line changed to execute ._release rather than .release
            # as .release just queues the ._release command and ._release
            # is where the exception is supposed to be raised.
            task_abort_event: threading.Event = threading.Event()
            task_callback = MockCallableDeque()
            subarray_component_manager._release(
                {"station_beams": [get_station_trl(station_off_name)]},
                task_callback,
                task_abort_event,
            )

    # pylint: disable=too-many-arguments, too-many-locals, too-many-statements
    def test_configure(
        self: TestSubarrayComponentManager,
        subarray_component_manager: SubarrayComponentManager,
        station_on_name: str,
        subarray_beam_id: int,
        station_beam_id: int,
        assign_resource_spec: dict,
        configure_resource_spec: dict,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's handling of configuration.

        :param subarray_component_manager: the subarray component
            manager under test with some callbacks wrapped with a
            `MockCallable`.

        :param station_on_name: the label of a station that is powered
            on.
        :param subarray_beam_id: the id number of a subarray beam/
        :param station_beam_id: the id of a station beam.
        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param configure_resource_spec: An object holding an example schema compliant
            configure argument.

        :param callbacks: A dictionary of callbacks with async support.
        """
        assert isinstance(
            subarray_component_manager._communication_state_callback,
            MockCallableGroup._Callable,
        )
        assert isinstance(
            subarray_component_manager._component_state_callback,
            MockCallableGroup._Callable,
        )
        subarray_component_manager.start_communicating()

        subarray_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.ESTABLISHED
        )  # noqa: E501
        task_status, response = subarray_component_manager.assign(
            callbacks["task"],
            subarray_id=assign_resource_spec["subarray_id"],
            subarray_beams=assign_resource_spec["subarray_beams"],
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        timeout = 5
        start_time = time.time()
        while len(subarray_component_manager._configuring_resources) != 2:
            time.sleep(0.1)
            if time.time() > start_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.IDLE
        )
        subarray_component_manager._device_obs_state_changed(
            get_station_beam_trl(station_on_name, station_beam_id), ObsState.IDLE
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Assign resources completed OK."),
        )
        subarray_component_manager._component_state_callback.assert_call(
            power=PowerState.ON
        )

        subarray_component_manager._component_state_callback.assert_call(
            resources_changed=[
                {get_station_trl(station_on_name)},
                {get_subarray_beam_trl(subarray_beam_id)},
                {get_station_beam_trl(station_on_name, station_beam_id)},
            ],
            lookahead=2,
        )

        # subarray connects to stations, subscribes to change events on power mode,
        # doesn't consider resource assignment to be complete until it has received an
        # event from each one. So let's fake that.
        subarray_component_manager._station_power_state_changed(
            get_station_trl(station_on_name), PowerState.ON
        )

        # About 1/2000 times the assertion fails without the sleep.
        # I'm not sure why, the above method call is not asynchronous.
        time.sleep(0.1)
        for _, station_proxy in subarray_component_manager._stations.items():
            assert station_proxy.communication_state == CommunicationStatus.ESTABLISHED
            assert station_proxy.power_state == PowerState.ON

        task_status, response = subarray_component_manager.configure(
            callbacks["task"], subarray_beams=configure_resource_spec["subarray_beams"]
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        # Wait until we get a configuring resource, then immediately pretend it has
        # finished configuring.
        timeout = 5
        start_time = time.time()

        while len(subarray_component_manager._configuring_resources) != 1:
            time.sleep(0.1)
            if time.time() > start_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.READY
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Configure has completed OK."),
        )

        subarray_component_manager._component_state_callback.assert_call(
            configured_changed=True, lookahead=20
        )

    # pylint: disable=too-many-arguments, too-many-locals, too-many-branches
    def test_scan(  # noqa: C901
        self: TestSubarrayComponentManager,
        subarray_component_manager: SubarrayComponentManager,
        station_on_name: str,
        subarray_beam_id: int,
        mock_subarray_beam: unittest.mock.Mock,
        station_beam_id: int,
        assign_resource_spec: dict,
        configure_resource_spec: dict,
        scan_id: int,
        start_time: str,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's handling of configuration.

        :param subarray_component_manager: the subarray component
            manager under test.
        :param station_on_name: the label of a station that is powered
            on.
        :param subarray_beam_id: the id number of a subarray beam that is
            powered on.

        :param mock_subarray_beam: a mock subarray beam.
        :param station_beam_id: the id number of a station beam.
        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param configure_resource_spec: An object holding an example schema compliant
            configure argument.
        :param scan_id: a scan id for use in testing
        :param start_time: a scan start time for use in testing
        :param callbacks: A dictionary of callbacks with async support.
        """
        assert isinstance(
            subarray_component_manager._communication_state_callback,
            MockCallableGroup._Callable,
        )
        assert isinstance(
            subarray_component_manager._component_state_callback,
            MockCallableGroup._Callable,
        )
        subarray_component_manager.start_communicating()
        subarray_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.ESTABLISHED
        )  # noqa: E501
        task_status, response = subarray_component_manager.assign(
            callbacks["task"],
            subarray_id=assign_resource_spec["subarray_id"],
            subarray_beams=assign_resource_spec["subarray_beams"],
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        timeout = 5
        current_time = time.time()
        while len(subarray_component_manager._configuring_resources) != 2:
            time.sleep(0.1)
            if time.time() > current_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.IDLE
        )
        subarray_component_manager._device_obs_state_changed(
            get_station_beam_trl(station_on_name, station_beam_id), ObsState.IDLE
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Assign resources completed OK."),
        )
        subarray_component_manager._component_state_callback.assert_call(
            power=PowerState.ON
        )

        subarray_component_manager._component_state_callback.assert_call(
            resources_changed=[
                {get_station_trl(station_on_name)},
                {get_subarray_beam_trl(subarray_beam_id)},
                {get_station_beam_trl(station_on_name, station_beam_id)},
            ],
            lookahead=2,
        )

        assert sorted(subarray_component_manager.assigned_resources) == sorted(
            [
                get_subarray_beam_trl(subarray_beam_id),
                get_station_trl(station_on_name),
                get_station_beam_trl(station_on_name, station_beam_id),
            ]
        )
        # subarray connects to stations, subscribes to change events on power mode,
        # doesn't consider resource assignment to be complete until it has received an
        # event from each one. So let's fake that.
        subarray_component_manager._station_power_state_changed(
            get_station_trl(station_on_name), PowerState.ON
        )

        # About 1/2000 times the assertion fails without the sleep.
        # I'm not sure why, the above method call is not asynchronous.
        time.sleep(0.1)
        for _, station_proxy in subarray_component_manager._stations.items():
            assert station_proxy.communication_state == CommunicationStatus.ESTABLISHED
            assert station_proxy.power_state == PowerState.ON

        task_status, response = subarray_component_manager.configure(
            callbacks["task"], subarray_beams=configure_resource_spec["subarray_beams"]
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        # Wait until we get a configuring resource, then immediately pretend it has
        # finished configuring.
        timeout = 5
        current_time = time.time()
        while len(subarray_component_manager._configuring_resources) != 1:
            time.sleep(0.1)
            if time.time() > current_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.READY
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Configure has completed OK."),
        )

        subarray_component_manager._component_state_callback.assert_call(
            configured_changed=True, lookahead=20
        )

        assert subarray_component_manager.scan_id is None

        task_status, response = subarray_component_manager.scan(
            callbacks["task"], scan_id=scan_id, start_time=start_time
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        # Wait until we get a configuring resource, then immediately pretend it has
        # finished configuring.
        timeout = 5
        current_time = time.time()
        while len(subarray_component_manager._configuring_resources) != 1:
            time.sleep(0.1)
            if time.time() > current_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.SCANNING
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Scan has completed OK."),
        )

        assert subarray_component_manager.scan_id == scan_id

        mock_subarray_beam.Scan.assert_next_call(
            json.dumps({"scan_id": scan_id, "duration": 0.0, "start_time": start_time})
        )
        callbacks["component_state"].assert_call(scanning_changed=True, lookahead=10)

        task_status, response = subarray_component_manager.end_scan()
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"
        callbacks["component_state"].assert_call(scanning_changed=False, lookahead=10)

    @pytest.mark.forked
    def test_local_unique_id(
        self: TestSubarrayComponentManager,
        subarray_component_manager: SubarrayComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test that we can retrieve a UID from a SKUID service.

        :param subarray_component_manager: the subarray component manager
            under test
        :param callbacks: dictionary of callbacks.
        """
        assert (
            subarray_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        subarray_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        unique_ids = set()
        unique_id_count = 10

        # Generate UIDs and assert no dupes.
        # NOTE: SkuidClient.get_local_transaction_id() *CAN* return duplicates.
        for _ in range(unique_id_count):
            unique_ids.add(subarray_component_manager._get_unique_id())
        assert len(unique_ids) == unique_id_count

    # pylint: disable=too-many-arguments, too-many-locals, too-many-statements
    def test_abort(
        self: TestSubarrayComponentManager,
        subarray_component_manager: SubarrayComponentManager,
        station_on_name: str,
        subarray_beam_id: int,
        mock_subarray_beam: unittest.mock.Mock,
        station_beam_id: int,
        assign_resource_spec: dict,
        configure_resource_spec: dict,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's handling of abort.

        :param subarray_component_manager: the subarray component
            manager under test with some callbacks wrapped with a
            `MockCallable`.

        :param station_on_name: the name of a station that is powered
            on.
        :param subarray_beam_id: the id number of a subarray beam.
        :param mock_subarray_beam: a mock tango device for a subarray beam.
        :param station_beam_id: the id of a station beam.
        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param configure_resource_spec: An object holding an example schema compliant
            configure argument.

        :param callbacks: A dictionary of callbacks with async support.
        """
        assert isinstance(
            subarray_component_manager._communication_state_callback,
            MockCallableGroup._Callable,
        )
        assert isinstance(
            subarray_component_manager._component_state_callback,
            MockCallableGroup._Callable,
        )
        subarray_component_manager.start_communicating()

        subarray_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.ESTABLISHED
        )  # noqa: E501
        task_status, response = subarray_component_manager.assign(
            callbacks["task"],
            subarray_id=assign_resource_spec["subarray_id"],
            subarray_beams=assign_resource_spec["subarray_beams"],
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        timeout = 5
        start_time = time.time()
        while len(subarray_component_manager._configuring_resources) != 2:
            time.sleep(0.1)
            if time.time() > start_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.IDLE
        )
        subarray_component_manager._device_obs_state_changed(
            get_station_beam_trl(station_on_name, station_beam_id), ObsState.IDLE
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Assign resources completed OK."),
        )
        subarray_component_manager._component_state_callback.assert_call(
            power=PowerState.ON
        )

        subarray_component_manager._component_state_callback.assert_call(
            resources_changed=[
                {get_station_trl(station_on_name)},
                {get_subarray_beam_trl(subarray_beam_id)},
                {get_station_beam_trl(station_on_name, station_beam_id)},
            ],
            lookahead=2,
        )

        # subarray connects to stations, subscribes to change events on power mode,
        # doesn't consider resource assignment to be complete until it has received an
        # event from each one. So let's fake that.
        subarray_component_manager._station_power_state_changed(
            get_station_trl(station_on_name), PowerState.ON
        )

        # About 1/2000 times the assertion fails without the sleep.
        # I'm not sure why, the above method call is not asynchronous.
        time.sleep(0.1)
        for _, station_proxy in subarray_component_manager._stations.items():
            assert station_proxy.communication_state == CommunicationStatus.ESTABLISHED
            assert station_proxy.power_state == PowerState.ON

        task_status, response = subarray_component_manager.configure(
            callbacks["task"], subarray_beams=configure_resource_spec["subarray_beams"]
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        subarray_component_manager.abort(callbacks["abort_task"])
        callbacks["abort_task"].assert_call(status=TaskStatus.IN_PROGRESS)
        # Wait until we get a configuring resource, then immediately pretend it has
        # finished aborting.
        timeout = 5
        start_time = time.time()

        while (
            len(subarray_component_manager._configuring_resources) != 1
            or subarray_component_manager._desired_state != ObsState.ABORTED
        ):
            time.sleep(0.1)
            if time.time() > start_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.ABORTED
        )
        callbacks["abort_task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Abort command has completed OK."),
        )

        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED,
            result=(ResultCode.ABORTED, "_configure aborted"),
        )
        mock_subarray_beam.Abort.assert_next_call()

        assert subarray_component_manager._is_configured is False

    # pylint: disable=too-many-arguments, too-many-locals, too-many-statements
    def test_abort_device(
        self: TestSubarrayComponentManager,
        subarray_component_manager: SubarrayComponentManager,
        station_on_name: str,
        subarray_beam_id: int,
        mock_subarray_beam: unittest.mock.Mock,
        station_beam_id: int,
        assign_resource_spec: dict,
        configure_resource_spec: dict,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's handling of abort.

        :param subarray_component_manager: the subarray component
            manager under test with some callbacks wrapped with a
            `MockCallable`.

        :param station_on_name: the name of a station that is powered
            on.
        :param subarray_beam_id: the id number of a subarray beam.
        :param mock_subarray_beam: a mock tango device for a subarray beam.
        :param station_beam_id: the id of a station beam.
        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param configure_resource_spec: An object holding an example schema compliant
            configure argument.

        :param callbacks: A dictionary of callbacks with async support.
        """
        assert isinstance(
            subarray_component_manager._communication_state_callback,
            MockCallableGroup._Callable,
        )
        assert isinstance(
            subarray_component_manager._component_state_callback,
            MockCallableGroup._Callable,
        )
        subarray_component_manager.start_communicating()

        subarray_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.ESTABLISHED
        )  # noqa: E501
        task_status, response = subarray_component_manager.assign(
            callbacks["task"],
            subarray_id=assign_resource_spec["subarray_id"],
            subarray_beams=assign_resource_spec["subarray_beams"],
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        timeout = 5
        start_time = time.time()
        while len(subarray_component_manager._configuring_resources) != 2:
            time.sleep(0.1)
            if time.time() > start_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.IDLE
        )
        subarray_component_manager._device_obs_state_changed(
            get_station_beam_trl(station_on_name, station_beam_id), ObsState.IDLE
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Assign resources completed OK."),
        )
        subarray_component_manager._component_state_callback.assert_call(
            power=PowerState.ON
        )

        subarray_component_manager._component_state_callback.assert_call(
            resources_changed=[
                {get_station_trl(station_on_name)},
                {get_subarray_beam_trl(subarray_beam_id)},
                {get_station_beam_trl(station_on_name, station_beam_id)},
            ],
            lookahead=2,
        )

        # subarray connects to stations, subscribes to change events on power mode,
        # doesn't consider resource assignment to be complete until it has received an
        # event from each one. So let's fake that.
        subarray_component_manager._station_power_state_changed(
            get_station_trl(station_on_name), PowerState.ON
        )

        # About 1/2000 times the assertion fails without the sleep.
        # I'm not sure why, the above method call is not asynchronous.
        time.sleep(0.1)
        for _, station_proxy in subarray_component_manager._stations.items():
            assert station_proxy.communication_state == CommunicationStatus.ESTABLISHED
            assert station_proxy.power_state == PowerState.ON

        task_status, response = subarray_component_manager.configure(
            callbacks["task"], subarray_beams=configure_resource_spec["subarray_beams"]
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        subarray_component_manager.abort_device(callbacks["abort_task"])
        callbacks["abort_task"].assert_call(status=TaskStatus.IN_PROGRESS)

        callbacks["abort_task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Abort command has completed OK."),
        )

        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED,
            result=(ResultCode.ABORTED, "_configure aborted"),
        )
        mock_subarray_beam.Abort.assert_not_called()

        assert subarray_component_manager._is_configured is False

    # pylint: disable=too-many-arguments, too-many-locals, too-many-statements
    def test_abort_restart(
        self: TestSubarrayComponentManager,
        subarray_component_manager: SubarrayComponentManager,
        station_on_name: str,
        subarray_beam_id: int,
        mock_subarray_beam: unittest.mock.Mock,
        station_beam_id: int,
        assign_resource_spec: dict,
        configure_resource_spec: dict,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's handling of restart.

        :param subarray_component_manager: the subarray component
            manager under test with some callbacks wrapped with a
            `MockCallable`.

        :param station_on_name: the name of a station that is powered
            on.
        :param subarray_beam_id: the id number of a subarray beam.
        :param mock_subarray_beam: a mock tango device for a subarray beam.
        :param station_beam_id: the id of a station beam.
        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param configure_resource_spec: An object holding an example schema compliant
            configure argument.

        :param callbacks: A dictionary of callbacks with async support.
        """
        assert isinstance(
            subarray_component_manager._communication_state_callback,
            MockCallableGroup._Callable,
        )
        assert isinstance(
            subarray_component_manager._component_state_callback,
            MockCallableGroup._Callable,
        )
        subarray_component_manager.start_communicating()

        subarray_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.ESTABLISHED
        )  # noqa: E501
        task_status, response = subarray_component_manager.assign(
            callbacks["task"],
            subarray_id=assign_resource_spec["subarray_id"],
            subarray_beams=assign_resource_spec["subarray_beams"],
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        timeout = 5
        start_time = time.time()
        while len(subarray_component_manager._configuring_resources) != 2:
            time.sleep(0.1)
            if time.time() > start_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.IDLE
        )
        subarray_component_manager._device_obs_state_changed(
            get_station_beam_trl(station_on_name, station_beam_id), ObsState.IDLE
        )

        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Assign resources completed OK."),
        )
        subarray_component_manager._component_state_callback.assert_call(
            power=PowerState.ON
        )

        subarray_component_manager._component_state_callback.assert_call(
            resources_changed=[
                {get_station_trl(station_on_name)},
                {get_subarray_beam_trl(subarray_beam_id)},
                {get_station_beam_trl(station_on_name, station_beam_id)},
            ],
            lookahead=2,
        )

        # subarray connects to stations, subscribes to change events on power mode,
        # doesn't consider resource assignment to be complete until it has received an
        # event from each one. So let's fake that.
        subarray_component_manager._station_power_state_changed(
            get_station_trl(station_on_name), PowerState.ON
        )

        # About 1/2000 times the assertion fails without the sleep.
        # I'm not sure why, the above method call is not asynchronous.
        time.sleep(0.1)
        for _, station_proxy in subarray_component_manager._stations.items():
            assert station_proxy.communication_state == CommunicationStatus.ESTABLISHED
            assert station_proxy.power_state == PowerState.ON

        task_status, response = subarray_component_manager.configure(
            callbacks["task"], subarray_beams=configure_resource_spec["subarray_beams"]
        )
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        subarray_component_manager.abort(callbacks["abort_task"])
        callbacks["abort_task"].assert_call(status=TaskStatus.IN_PROGRESS)
        # Wait until we get a configuring resource, then immediately pretend it has
        # finished aborting.
        timeout = 5
        start_time = time.time()

        while (
            len(subarray_component_manager._configuring_resources) != 1
            or subarray_component_manager._desired_state != ObsState.ABORTED
        ):
            time.sleep(0.1)
            if time.time() > start_time + timeout:
                pytest.fail("Didn't receive configuring devices in time")
        subarray_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(subarray_beam_id), ObsState.ABORTED
        )
        callbacks["abort_task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Abort command has completed OK."),
        )

        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED,
            result=(ResultCode.ABORTED, "_configure aborted"),
        )
        mock_subarray_beam.Abort.assert_next_call()

        assert subarray_component_manager._is_configured is False

        assert subarray_component_manager.assigned_resources_dict != {
            "stations": [],
            "subarray_beams": [],
            "station_beams": [],
            "apertures": [],
            "channels": [0],
        }

        task_status, response = subarray_component_manager.restart(callbacks["task"])
        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Restart command has completed OK."),
        )
        assert subarray_component_manager.assigned_resources_dict == {
            "stations": [],
            "subarray_beams": [],
            "station_beams": [],
            "apertures": [],
            "channels": [0],
        }
