# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for MccsSubarray."""
# pylint: disable=too-many-lines
from __future__ import annotations

import gc
import json
import sys
import time
import unittest
from copy import copy
from typing import Iterator, Type
from unittest.mock import ANY

import pytest
import tango
from _pytest.fixtures import FixtureRequest
from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    ControlMode,
    HealthState,
    ObsState,
    PowerState,
    ResultCode,
    SimulationMode,
)
from ska_low_mccs_common import MccsDeviceProxy
from ska_tango_testing.mock import MockCallable
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevState
from tango.server import command

from ska_low_mccs import MccsSubarray
from ska_low_mccs.subarray.subarray_component_manager import SubarrayComponentManager
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_station_beam_trl,
    get_station_trl,
    get_subarray_beam_trl,
)

# To prevent tests hanging during gc.
gc.disable()


@pytest.fixture(name="patched_subarray_device_class")
def patched_subarray_device_class_fixture(
    mock_subarray_component_manager: SubarrayComponentManager,
    station_on_name: str,
    subarray_beam_id: int,
) -> Type[MccsSubarray]:
    """
    Return a subarray device class, patched with extra methods for testing.

    :param mock_subarray_component_manager: A fixture that provides a
        partially mocked component manager which has access to the
        component_state_callback.

    :param station_on_name: The label of a mock station that is powered on.
    :param subarray_beam_id: The id of a mock subarray beam.
    :return: a patched subarray device class, patched with extra methods
        for testing
    """

    # pylint: disable=too-many-ancestors
    class PatchedSubarrayDevice(MccsSubarray):
        """
        MccsSubarray patched with extra commands for testing purposes.

        The extra commands allow us to mock the receipt of obs state
        change events from subservient devices, turn on DeviceProxies,
        set the initial obsState and examine the health model.
        """

        @command(dtype_in=int)
        def FakeSubservientDevicesObsState(
            self: PatchedSubarrayDevice, obs_state: ObsState
        ) -> None:
            """
            Fake ObsState and send changed events.

            :param obs_state: Starting ObsState
            """
            obs_state = ObsState(obs_state)
            for trl in self.component_manager._device_obs_states:
                self.component_manager._device_obs_state_changed(trl, obs_state)

        @command(dtype_in=None)
        def TurnOnProxies(
            self: PatchedSubarrayDevice,
        ) -> None:
            """Turn on the proxies."""
            for (
                trl,
                station_proxy,
            ) in self.component_manager._stations.items():
                if trl == get_station_trl(station_on_name):
                    station_proxy._component_state["power"] = PowerState.ON

            for (
                trl,
                subbeam_proxy,
            ) in self.component_manager._subarray_beams.items():
                if trl == get_subarray_beam_trl(subarray_beam_id):
                    subbeam_proxy._component_state["power"] = PowerState.ON

        @command(dtype_in=str)
        def set_obs_state(
            self: PatchedSubarrayDevice,
            obs_state_name: str,
        ) -> None:
            """
            Set the obsState of this device.

            A method to set the obsState for testing purposes.

            :param obs_state_name: The name of the obsState to directly transition to.
            """
            self.obs_state_model._straight_to_state(obs_state_name)

        @command(dtype_in=str, dtype_out=int)
        def examine_health_model(
            self: PatchedSubarrayDevice,
            trl: str,
        ) -> HealthState | None:
            """
            Return the health state of a subservient device.

            Returns the health state of the device at the given TRL.

            :param trl: The TRL of a device whose health state we want.
            :return: The HealthState of the device at the specified TRL.
            """
            device_type = trl.split("/")[1]
            health: HealthState | None
            if device_type == "beam":
                health = self._health_model._station_beam_healths[trl]
            if device_type == "station":
                health = self._health_model._station_healths[trl]
            if device_type == "subarraybeam":
                health = self._health_model._subarray_beam_healths[trl]
            return health

        def create_component_manager(
            self: PatchedSubarrayDevice,
        ) -> SubarrayComponentManager:
            """
            Return a partially mocked component manager instead of the usual one.

            :return: a mock component manager
            """
            wrapped_state_changed_callback = MockCallable(
                wraps=self._component_state_callback
            )
            wrapped_communication_changed_callback = MockCallable(
                wraps=self._communication_state_callback
            )

            mock_subarray_component_manager._communication_state_callback = (
                wrapped_communication_changed_callback
            )
            mock_subarray_component_manager._component_state_callback = (
                wrapped_state_changed_callback
            )

            return mock_subarray_component_manager

    return PatchedSubarrayDevice


@pytest.fixture(name="test_context")
# pylint: disable=too-many-arguments
def test_context_fixture(
    station_on_name: str,
    station_off_name: str,
    subarray_id: int,
    subarray_beam_id: int,
    station_beam_id: int,
    mock_station_on: unittest.mock.Mock,
    mock_station_off: unittest.mock.Mock,
    mock_subarray_beam: unittest.mock.Mock,
    mock_station_beam: unittest.mock.Mock,
    patched_subarray_device_class: MccsSubarray,
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which mock tile and field station devices are running.

    :param station_on_name: the name of a station which is on.
    :param station_off_name: the name of a station which is off.
    :param subarray_id: the id of a subarray.
    :param subarray_beam_id: the id of a subarray beam.
    :param station_beam_id: the id of a station beam.

    :param mock_station_on: a mock tango device for a station which is on.
    :param mock_station_off: a mock tango device for a station which is off.
    :param mock_subarray_beam: a mock tango device for a subarray beam.
    :param mock_station_beam: a mock tango device for a station beam.

    :param patched_subarray_device_class: a subarray device class, patched
        with extra methods for testing.

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()

    harness.add_mock_station_device(station_on_name, mock_station_on)
    harness.add_mock_station_device(station_off_name, mock_station_off)
    harness.add_mock_subarray_beam_device(subarray_beam_id, mock_subarray_beam)
    harness.add_mock_station_beam_device(
        station_on_name, station_beam_id, mock_station_beam
    )

    harness.add_subarray_device(subarray_id, device_class=patched_subarray_device_class)

    with harness as context:
        yield context


# pylint: disable=too-many-public-methods
class TestMccsSubarray:
    """Test class for MccsSubarray tests."""

    @pytest.fixture(name="device_under_test")
    def device_under_test_fixture(
        self: TestMccsSubarray,
        test_context: MccsTangoTestHarnessContext,
        subarray_id: int,
    ) -> MccsDeviceProxy:
        """
        Fixture that returns the device under test.

        :param test_context: the context in which the tests run
        :param subarray_id: A Subarray ID.
        :return: the device under test
        """
        return test_context.get_subarray_device(subarray_id)

    def test_InitDevice(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for Initial state.

        :todo: Test for different memorized values of adminMode.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.healthState == HealthState.OK
        assert device_under_test.controlMode == ControlMode.REMOTE
        assert device_under_test.simulationMode == SimulationMode.FALSE
        # TODO: Why is testMode stuck at TestMode.NONE?
        # assert device_under_test.testMode == TestMode.TEST

        # The following reads might not be allowed in this state once
        # properly implemented
        assert device_under_test.scanId == -1
        assert device_under_test.stationTrls == ()
        # No activationTime attr?
        # assert device_under_test.activationTime == 0

    def test_healthState(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for healthState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        device_under_test.subscribe_event(
            "healthState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.OK)
        assert device_under_test.healthState == HealthState.OK

    def test_versionId(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for versionId.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.versionId == sys.modules["ska_low_mccs"].__version__

    def test_scanId(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for scanID attribute.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.scanId == -1

    def test_stationTrls(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for stationTrls attribute.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.stationTrls == ()

    # pylint: disable=too-many-arguments
    def test_assignResources(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        station_on_name: str,
        subarray_beam_id: int,
        station_beam_id: int,
        channel_blocks: list[int],
        assign_resource_spec: dict,
        apertures: list[dict],
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for assignResources.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param station_on_name: the label of a station that is powered
            on.
        :param subarray_beam_id: The ID of a SubarrayBeam.
        :param station_beam_id: The ID of a StationBeam.

        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param apertures: An object holding an example aperture.

        :param channel_blocks: a list of channel blocks.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        device_under_test.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        device_under_test.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        device_under_test.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["obsState"],
        )

        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE
        change_event_callbacks["state"].assert_change_event(DevState.DISABLE)
        assert device_under_test.state() == DevState.DISABLE

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert device_under_test.adminMode == AdminMode.ONLINE

        change_event_callbacks["state"].assert_change_event(DevState.ON, lookahead=2)
        assert device_under_test.state() == DevState.ON
        assert device_under_test.obsState == ObsState.EMPTY

        ([result_code], [unique_id]) = device_under_test.AssignResources(
            json.dumps(assign_resource_spec)
        )
        assert result_code == ResultCode.QUEUED
        assert "AssignResources" in str(unique_id).rsplit("_", maxsplit=1)[-1].rstrip(
            "']"
        )

        assert device_under_test.state() == DevState.ON

        initial_lrc_result = ("", "")
        assert device_under_test.longRunningCommandResult == initial_lrc_result
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            initial_lrc_result
        )

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.IDLE)

        lrc_result = (
            unique_id,
            '[0, "Assign resources completed OK."]',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )

        assert device_under_test.obsState == ObsState.IDLE

        interface = "https://schema.skao.int/ska-low-mccs-assignedresources/1.0"

        assert device_under_test.assignedResources == json.dumps(
            {
                "interface": interface,
                "subarray_beam_ids": [str(subarray_beam_id)],
                "station_beam_ids": [f"{station_on_name}-{station_beam_id:02}"],
                "station_ids": [station_on_name],
                "apertures": [apertures[0]["aperture_id"]],
                "channels": [channel_block * 8 for channel_block in channel_blocks],
            }
        )

        ([result_code], [unique_id]) = device_under_test.ReleaseAllResources()
        assert result_code == ResultCode.QUEUED
        assert "ReleaseAllResources" in unique_id

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.EMPTY)

        lrc_result = (
            unique_id,
            '[0, "ReleaseAllResources has completed OK."]',
        )

        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )
        interface = "https://schema.skao.int/ska-low-mccs-assignedresources/1.0"
        assert device_under_test.assignedResources == json.dumps(
            {
                "interface": interface,
                "subarray_beam_ids": [],
                "station_beam_ids": [],
                "station_ids": [],
                "apertures": [],
                "channels": [0],
            }
        )

        change_event_callbacks["obsState"].assert_change_event(
            ObsState.RESOURCING, lookahead=10
        )
        change_event_callbacks["obsState"].assert_change_event(
            ObsState.EMPTY, lookahead=10
        )
        assert device_under_test.obsState == ObsState.EMPTY

    # pylint: disable=too-many-arguments
    def test_configure(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        assign_resource_spec: dict,
        configure_resource_spec: dict,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for configure command.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param configure_resource_spec: An object holding an example schema compliant
            configure argument.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        device_under_test.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["obsState"],
        )
        device_under_test.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert device_under_test.adminMode == AdminMode.ONLINE

        assert device_under_test.state() == DevState.ON
        change_event_callbacks["obsState"].assert_change_event(ObsState.EMPTY)
        assert device_under_test.obsState == ObsState.EMPTY

        ([result_code], [unique_id]) = device_under_test.AssignResources(
            json.dumps(assign_resource_spec)
        )

        initial_lrc_result = ("", "")
        assert device_under_test.longRunningCommandResult == initial_lrc_result
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            initial_lrc_result
        )

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.IDLE)

        lrc_result = (
            unique_id,
            '[0, "Assign resources completed OK."]',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )

        assert device_under_test.obsState == ObsState.IDLE

        # Need to force station and subarray beam proxies into PowerState.ON for
        # configure to complete.
        device_under_test.TurnOnProxies()

        ([result_code], [unique_id]) = device_under_test.Configure(
            json.dumps(configure_resource_spec)
        )

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.READY)

        lrc_result = (
            unique_id,
            '[0, "Configure has completed OK."]',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )

        assert device_under_test.obsState == ObsState.READY

    # pylint: disable=too-many-statements, too-many-locals
    @pytest.mark.parametrize(
        "resources",
        [
            "station_off_name",
            "station_on_name",
        ],
    )
    def test_assign_and_configure(
        self: TestMccsSubarray,
        mock_subarray_component_manager: SubarrayComponentManager,
        device_under_test: MccsDeviceProxy,
        station_on_id: int,
        station_on_name: str,
        station_off_id: int,
        mock_station_off: unittest.mock.Mock,
        mock_station_on: unittest.mock.Mock,
        mock_subarray_beam: unittest.mock.Mock,
        subarray_beam_id: int,
        station_beam_id: int,
        subarray_id: int,
        change_event_callbacks: MockTangoEventCallbackGroup,
        resources: str,
        assign_resource_spec: dict,
        configure_resource_spec: dict,
        apertures: list[dict],
        request: FixtureRequest,
    ) -> None:
        """
        Test the component manager's handling of configuration.

        :param mock_subarray_component_manager: A Subarray component
            manager with some callbacks wrapped with a `MockCallable`.

        :param device_under_test: A proxy to the Subarray device under test.
        :param station_on_id: the id number of a station that is
            powered on.
        :param station_off_id: the id number of a station that is
            powered off.
        :param station_on_name: the label of a station that is powered on.

        :param mock_station_off: a mock station that is powered off.
        :param mock_station_on: a mock station that is powered on.
        :param mock_subarray_beam: a mock subarray beam that is powered off.
        :param subarray_beam_id: the id number of a subarray beam.
        :param station_beam_id: the id number of a station beam.
        :param subarray_id: the id number of a subarray.

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param configure_resource_spec: An object holding an example schema compliant
            configure argument.
        :param apertures: An object holding an example aperture.

        :param resources: A list of references representing the resources to
            be allocated.
        :param request: A PyTest fixture request object.
        """
        assert isinstance(
            mock_subarray_component_manager._component_state_callback,
            MockCallable,
        )
        # Extract the parametrized fixtures from the string and populate the resource
        # spec.
        station_name: str = request.getfixturevalue(resources)
        station_trl = get_station_trl(station_name)
        if resources == "station_on_name":
            station_id = station_on_id
        else:
            station_id = station_off_id

        # Update the resources to allocate
        apertures[0]["station_id"] = station_id
        apertures[0]["station_trl"] = station_trl
        assign_resource_spec["apertures"] = apertures

        device_under_test.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["obsState"],
        )
        device_under_test.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks.assert_change_event(
            "obsState", ObsState.EMPTY, lookahead=10
        )
        assert device_under_test.obsState == ObsState.EMPTY
        mock_subarray_component_manager.start_communicating()

        assert isinstance(
            mock_subarray_component_manager._communication_state_callback, MockCallable
        )

        mock_subarray_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.ESTABLISHED
        )

        assert (
            mock_subarray_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )
        mock_subarray_component_manager._component_state_callback.assert_call(
            power=PowerState.ON
        )

        ([result_code], [unique_id]) = device_under_test.AssignResources(
            json.dumps(assign_resource_spec)
        )

        initial_lrc_result = ("", "")
        assert device_under_test.longRunningCommandResult == initial_lrc_result
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            initial_lrc_result
        )

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.IDLE)

        lrc_result = (
            unique_id,
            '[0, "Assign resources completed OK."]',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )

        assert device_under_test.obsState == ObsState.IDLE

        mock_subarray_component_manager._component_state_callback.assert_call(
            resources_changed=[
                {get_station_trl(station_name)},
                {get_subarray_beam_trl(subarray_beam_id)},
                # The allocate method needs updating to make sure we only allocate
                # stations their own station beams, at the moment it doesn't.
                {get_station_beam_trl(station_on_name, station_beam_id)},
            ],
            lookahead=1,
        )
        if station_id == station_on_id:
            # Test successful configure call.
            ([result_code], [unique_id]) = device_under_test.Configure(
                json.dumps(configure_resource_spec)
            )

            time.sleep(0.1)
            device_under_test.FakeSubservientDevicesObsState(ObsState.READY)

            lrc_result = (
                unique_id,
                '[0, "Configure has completed OK."]',
            )
            change_event_callbacks["longRunningCommandResult"].assert_change_event(
                lrc_result, lookahead=10
            )

            assert device_under_test.obsState == ObsState.READY
            # We add a timeout here to delay the test code so we don't outpace it
            # rather than using a sleep.
            mock_station_off.ApplyConfiguration.assert_not_called(timeout=1)
            # Weak assertion on Anything as it will be called with a unique ID that
            # we don't have access to.

            # TODO: modify _mock_command_inout in ska-low-mccs-common
            # to call with args and kwargs. this should be
            mock_station_on.ApplyConfiguration.assert_next_call(ANY)
            # mock_station_on.ApplyConfiguration.assert_next_call()

            expected_subarray_beam_config = copy(
                configure_resource_spec["subarray_beams"][0]
            )
            expected_subarray_beam_config["subarray_id"] = subarray_id

            mock_subarray_beam.Configure.assert_next_call(
                json.dumps(expected_subarray_beam_config)
            )
            # pylint: disable=line-too-long
            mock_subarray_component_manager._component_state_callback.assert_call(  # noqa: E501
                configured_changed=True, lookahead=10
            )
            # The obsState attribute is a mock for these devices so we can't check
            # the specific value but we can check a call comes through.
            # pylint: disable=line-too-long
            mock_subarray_component_manager._component_state_callback.assert_call(  # noqa: E501
                obsstate_changed=ANY,
                trl=get_station_beam_trl(station_name, station_beam_id),
                lookahead=10,
            )
            # pylint: disable=line-too-long
            mock_subarray_component_manager._component_state_callback.assert_call(  # noqa: E501
                obsstate_changed=ANY, trl=station_trl, lookahead=10
            )
            # pylint: disable=line-too-long
            mock_subarray_component_manager._component_state_callback.assert_call(  # noqa: E501
                obsstate_changed=ANY,
                trl=get_subarray_beam_trl(subarray_beam_id),
                lookahead=10,
            )
            # Deconfigure
            device_under_test.End()

            time.sleep(0.1)
            device_under_test.FakeSubservientDevicesObsState(ObsState.IDLE)

            mock_station_off.DeallocateSubarray.assert_not_called()
            mock_station_on.DeallocateSubarray.assert_next_call(subarray_id)
            mock_subarray_beam.End.assert_next_call()
            # pylint: disable=line-too-long
            mock_subarray_component_manager._component_state_callback.assert_call(  # noqa: E501
                configured_changed=False, lookahead=10
            )

        else:
            pytest.xfail("We cannot currently catch if the station is off.")
            # Test unsuccessful configure call.
            device_under_test.Configure(json.dumps(configure_resource_spec))

            mock_station_off.ApplyConfiguration.assert_not_called(timeout=1)
            mock_subarray_beam.Configure.assert_not_called(timeout=1)

            change_event_callbacks["obsState"].assert_change_event(
                ObsState.FAULT, lookahead=10
            )

        [result_code], [message] = device_under_test.ReleaseAllResources()
        assert result_code == ResultCode.QUEUED
        assert "ReleaseAllResources" in message

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.EMPTY)
        # Check that component_state_callback has been called with the
        # arguments expected.
        # pylint: disable=line-too-long
        mock_subarray_component_manager._component_state_callback.assert_call(  # noqa: E501
            resources_changed=[set(), set(), set()], lookahead=10
        )
        change_event_callbacks.assert_change_event(
            "obsState", ObsState.RESOURCING, lookahead=10
        )
        change_event_callbacks.assert_change_event(
            "obsState", ObsState.EMPTY, lookahead=10
        )
        assert device_under_test.obsState == ObsState.EMPTY

    # pylint: disable=too-many-arguments
    def test_assign_configure_and_scan(
        self: TestMccsSubarray,
        device_under_test: Type[MccsSubarray],
        mock_subarray_component_manager: SubarrayComponentManager,
        station_on_name: str,
        subarray_beam_id: int,
        mock_subarray_beam: unittest.mock.Mock,
        station_beam_id: int,
        scan_id: int,
        start_time: float,
        assign_resource_spec: dict,
        configure_resource_spec: dict,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test the component manager's handling of configuration.

        :param device_under_test: A proxy to the Subarray under test.
        :param mock_subarray_component_manager: A Subarray component
            manager with some callbacks wrapped with a
            `MockCallable` for testing purposes.

        :param station_on_name: the label of a station that is
            powered on.
        :param subarray_beam_id: the id number of a subarray beam.
        :param mock_subarray_beam: a mock subarray beam.
        :param station_beam_id: the id number of a station beam.
        :param scan_id: a scan id for use in testing
        :param start_time: a scan start time for use in testing
        :param assign_resource_spec: An object holding an example schema compliant
            assign argument.
        :param configure_resource_spec: An object holding an example schema compliant
            configure argument.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        assert isinstance(
            mock_subarray_component_manager._component_state_callback,
            MockCallable,
        )
        device_under_test.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["obsState"],
        )
        change_event_callbacks.assert_change_event("obsState", ObsState.EMPTY)
        device_under_test.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )

        mock_subarray_component_manager.start_communicating()
        mock_subarray_component_manager._component_state_callback.assert_call(
            power=PowerState.ON
        )
        assert mock_subarray_component_manager.power_state == PowerState.ON

        ([result_code], [unique_id]) = device_under_test.AssignResources(
            json.dumps(assign_resource_spec)
        )

        initial_lrc_result = ("", "")
        assert device_under_test.longRunningCommandResult == initial_lrc_result
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            initial_lrc_result
        )

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.IDLE)

        lrc_result = (
            unique_id,
            '[0, "Assign resources completed OK."]',
        )

        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )

        assert device_under_test.obsState == ObsState.IDLE

        mock_subarray_component_manager._component_state_callback.assert_call(
            resources_changed=[
                {get_station_trl(station_on_name)},
                {get_subarray_beam_trl(subarray_beam_id)},
                # The allocate method needs updating to make sure we only allocate
                # stations their own station beams, at the moment it doesn't.
                {get_station_beam_trl(station_on_name, station_beam_id)},
            ],
            lookahead=1,
        )

        ([result_code], [unique_id]) = device_under_test.Configure(
            json.dumps(configure_resource_spec)
        )

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.READY)

        lrc_result = (
            unique_id,
            '[0, "Configure has completed OK."]',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )

        assert device_under_test.obsState == ObsState.READY

        assert mock_subarray_component_manager.scan_id is None

        scan_resource_spec = {
            "scan_id": scan_id,
            "start_time": str(start_time),
            "duration": 1.0,
        }

        ([result_code], [unique_id]) = device_under_test.Scan(
            json.dumps(scan_resource_spec)
        )

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.SCANNING)

        lrc_result = (
            unique_id,
            '[0, "Scan has completed OK."]',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )

        assert device_under_test.obsState == ObsState.SCANNING

        assert mock_subarray_component_manager.scan_id == scan_id

        mock_subarray_beam.Scan.assert_next_call(
            json.dumps(
                {"scan_id": scan_id, "duration": 1.0, "start_time": str(start_time)}
            )
        )

        mock_subarray_component_manager._component_state_callback.assert_call(
            scanning_changed=True, lookahead=10
        )

        ([result_code], [unique_id]) = device_under_test.EndScan()

        time.sleep(0.1)
        device_under_test.FakeSubservientDevicesObsState(ObsState.READY)

        lrc_result = (
            unique_id,
            '[0, "EndScan has completed OK."]',
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            lrc_result, lookahead=10
        )
        assert device_under_test.obsState == ObsState.READY
        mock_subarray_component_manager._component_state_callback.assert_call(
            scanning_changed=False, lookahead=10
        )

    def test_sendTransientBuffer(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for sendTransientBuffer.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE

        # Seems to be a bit of an adminMode wobble here.
        # The first event to come through is another OFFLINE (possibly just a dupe
        # of the first) but is followed by ONLINE so assertion has been changed from
        # `assert_next_change_event` to `assert_last_change_event`
        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert device_under_test.adminMode == AdminMode.ONLINE

        segment_spec: list[int] = []
        [result_code], response = device_under_test.sendTransientBuffer(segment_spec)

        assert result_code == ResultCode.QUEUED
        assert "SendTransientBuffer" in str(response).rsplit("_", maxsplit=1)[
            -1
        ].rstrip("']")

    @pytest.mark.parametrize("target_power_state", list(PowerState))
    def test_component_state_callback_power_state(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,  # pylint: disable=unused-argument
        mock_subarray_component_manager: SubarrayComponentManager,
        target_power_state: PowerState,
    ) -> None:
        """
        Test `component_state_changed properly` handles power updates.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param target_power_state: The PowerState that the device should end up in.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert isinstance(
            mock_subarray_component_manager._component_state_callback,
            MockCallable,
        )
        mock_subarray_component_manager._update_component_state(
            power=target_power_state
        )
        # Check that the power state has changed.
        final_power_state = mock_subarray_component_manager.power_state
        assert final_power_state == target_power_state

    @pytest.mark.parametrize("target_health_state", list(HealthState))
    def test_component_state_callback_health_state(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
        target_health_state: HealthState,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test `component_state_changed` properly handles health updates.

        Here we only test that the change event is pushed and that we
        receive it. HealthState.OK is omitted due to it being the
        initial state.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param target_health_state: The HealthState that the device should end up in.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        device_under_test.subscribe_event(
            "healthState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.OK)

        # Initial state is OK so skip that one.
        if target_health_state != HealthState.OK:
            mock_subarray_component_manager._component_state_callback(
                health=target_health_state
            )
            change_event_callbacks["healthState"].assert_change_event(
                target_health_state
            )

    @pytest.mark.parametrize("configured_changed", [True, False])
    def test_component_state_callback_configured_changed(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
        configured_changed: bool,
    ) -> None:
        """
        Test `component_state_changed` properly handles configured_changed updates.

        Test that the obs state model is properly updated when the
        component is configured or unconfigured. Note that the two
        scenarios are completely different. True is part of the
        Configure command, and assumes configure_invoked Requires
        configure_completed to take effect False is the whole
        End/Deconfigure command Therefore configured_changed has been
        removed, basic events are used instead

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param configured_changed: Whether the component is configured.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # set initial obsState
        if configured_changed:
            # obsState change: CONFIGURING_IDLE -> READY
            initial_obs_state_name = "CONFIGURING_IDLE"
            initial_obs_state = ObsState.CONFIGURING
            final_obs_state = ObsState.READY  # This is not a great test...
        else:
            # obsState change: READY -> IDLE
            initial_obs_state_name = "READY"
            initial_obs_state = ObsState.READY
            final_obs_state = ObsState.IDLE
        device_under_test.set_obs_state(initial_obs_state_name)

        assert device_under_test.obsState == initial_obs_state

        mock_subarray_component_manager._component_state_callback(
            configured_changed=configured_changed
        )
        if configured_changed:
            # Need to fire this event explicitly to effect the
            # obsState change we're looking for.
            mock_subarray_component_manager._component_state_callback(
                task_completed="configure"
            )
        assert device_under_test.obsState == final_obs_state

    @pytest.mark.parametrize("scanning_changed", [True, False])
    def test_component_state_callback_scanning_changed(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
        scanning_changed: bool,
    ) -> None:
        """
        Test `component_state_changed` properly handles scanning_changed updates.

        Test that the obs state model is properly updated when the
        component starts or stops scanning.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param scanning_changed: Whether the subarray is scanning.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # set initial obsState
        if scanning_changed:
            # obsState change: READY -> SCANNING
            initial_obs_state_name = "READY"
            initial_obs_state = ObsState.READY
            final_obs_state = ObsState.SCANNING
        else:
            # obsState change: SCANNING -> READY
            initial_obs_state_name = "SCANNING"
            initial_obs_state = ObsState.SCANNING
            final_obs_state = ObsState.READY
        device_under_test.set_obs_state(initial_obs_state_name)

        assert device_under_test.obsState == initial_obs_state

        mock_subarray_component_manager._component_state_callback(
            scanning_changed=scanning_changed
        )
        assert device_under_test.obsState == final_obs_state

    @pytest.mark.parametrize("resourcing", [True, False])
    def test_component_state_callback_assign_completed(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
        resourcing: bool,
    ) -> None:
        """
        Test `component_state_changed` properly handles assign_completed updates.

        Test that the obs state model is properly updated when resource
        assignment completes.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param resourcing: Whether the subarray is resourcing or emptying.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # Set initial obsState.
        if resourcing:
            # obsState change: RESOURCING_IDLE -> IDLE
            initial_obs_state_name = "RESOURCING_IDLE"
            initial_obs_state = ObsState.RESOURCING
            final_obs_state = ObsState.IDLE
        else:
            # obsState change: RESOURCING_EMPTY -> EMPTY
            initial_obs_state_name = "RESOURCING_EMPTY"
            initial_obs_state = ObsState.RESOURCING
            final_obs_state = ObsState.EMPTY

        device_under_test.set_obs_state(initial_obs_state_name)
        assert device_under_test.obsState == initial_obs_state

        mock_subarray_component_manager._component_state_callback(
            task_completed="assign"
        )
        assert device_under_test.obsState == final_obs_state

    @pytest.mark.parametrize("to_empty", [True, False])
    def test_component_state_callback_release_completed(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
        to_empty: bool,
    ) -> None:
        """
        Test `component_state_changed` properly handles release_completed updates.

        Test that the obs state model is properly updated when resource
        release completes.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param to_empty: Whether the subarray is transitioning to EMPTY or not.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # Set initial obsState.
        if to_empty:
            # obsState change: RESOURCING_EMPTY -> EMPTY
            initial_obs_state_name = "RESOURCING_EMPTY"
            initial_obs_state = ObsState.RESOURCING
            final_obs_state = ObsState.EMPTY
        else:
            # obsState change: RESOURCING_IDLE -> IDLE
            initial_obs_state_name = "RESOURCING_IDLE"
            initial_obs_state = ObsState.RESOURCING
            final_obs_state = ObsState.IDLE

        device_under_test.set_obs_state(initial_obs_state_name)
        assert device_under_test.obsState == initial_obs_state

        mock_subarray_component_manager._component_state_callback(
            task_completed="release"
        )
        assert device_under_test.obsState == final_obs_state

    @pytest.mark.parametrize("to_ready", [True, False])
    def test_component_state_callback_configure_completed(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
        to_ready: bool,
    ) -> None:
        """
        Test `component_state_changed` properly handles configure_completed updates.

        Test that the obs state model is properly updated when
        configuring completes.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to thecomponent_state_callback.

        :param to_ready: Whether the subarray is transitioning to READY or not.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # Set initial obsState.
        if to_ready:
            # obsState change: CONFIGURING_READY -> READY
            initial_obs_state_name = "CONFIGURING_READY"
            initial_obs_state = ObsState.CONFIGURING
            final_obs_state = ObsState.READY
        else:
            # obsState change: CONFIGURING_IDLE -> IDLE
            initial_obs_state_name = "CONFIGURING_IDLE"
            initial_obs_state = ObsState.CONFIGURING
            final_obs_state = ObsState.IDLE

        device_under_test.set_obs_state(initial_obs_state_name)
        assert device_under_test.obsState == initial_obs_state

        mock_subarray_component_manager._component_state_callback(
            task_completed="configure"
        )
        assert device_under_test.obsState == final_obs_state

    def test_component_state_callback_abort_completed(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
    ) -> None:
        """
        Test `component_state_changed` properly handles abort_completed updates.

        Test that the obs state model is properly updated when abort
        completes.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # Set initial obsState.
        # obsState change: ABORTING -> ABORTED
        initial_obs_state_name = "ABORTING"
        initial_obs_state = ObsState.ABORTING
        final_obs_state = ObsState.ABORTED

        device_under_test.set_obs_state(initial_obs_state_name)
        assert device_under_test.obsState == initial_obs_state

        mock_subarray_component_manager._component_state_callback(
            task_completed="abort"
        )
        assert device_under_test.obsState == final_obs_state

    def test_component_state_callback_obs_reset_completed(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
    ) -> None:
        """
        Test `component_state_changed` properly handles obsreset_completed updates.

        Test that the obs state model is properly updated when obsreset
        completes.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # Set initial obsState.
        # obsState change: RESETTING -> IDLE
        initial_obs_state_name = "RESETTING"
        initial_obs_state = ObsState.RESETTING
        final_obs_state = ObsState.IDLE

        device_under_test.set_obs_state(initial_obs_state_name)
        assert device_under_test.obsState == initial_obs_state

        mock_subarray_component_manager._component_state_callback(
            task_completed="obsreset"
        )
        assert device_under_test.obsState == final_obs_state

    def test_component_state_callback_restart_completed(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
    ) -> None:
        """
        Test `component_state_changed` properly handles restart_completed updates.

        Test that the obs state model is properly updated when restart
        completes.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # Set initial obsState.
        # obsState change: RESTARTING -> EMPTY
        initial_obs_state_name = "RESTARTING"
        initial_obs_state = ObsState.RESTARTING
        final_obs_state = ObsState.EMPTY

        device_under_test.set_obs_state(initial_obs_state_name)
        assert device_under_test.obsState == initial_obs_state

        mock_subarray_component_manager._component_state_callback(
            task_completed="restart"
        )
        assert device_under_test.obsState == final_obs_state

    @pytest.mark.parametrize("initial_obs_state", list(ObsState))
    def test_component_state_callback_obsfault(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
        initial_obs_state: ObsState,
    ) -> None:
        """
        Test `component_state_changed` properly handles restart_completed updates.

        Test that the obs state model is properly updated when restart
        completes.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param initial_obs_state: The obsState that the subarray should
            start the test in.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # Set initial obsState.
        # obsState change: * -> FAULT
        initial_obs_state_name = initial_obs_state.name
        # Handle special cases of RESOURCING and CONFIGURING with their
        # transitional states.
        special_cases = ["RESOURCING", "CONFIGURING"]
        if initial_obs_state_name in special_cases:
            initial_obs_state_name = initial_obs_state_name + "_IDLE"

        final_obs_state = ObsState.FAULT

        device_under_test.set_obs_state(initial_obs_state_name)
        assert device_under_test.obsState == initial_obs_state

        mock_subarray_component_manager._component_state_callback(obsfault=True)
        assert device_under_test.obsState == final_obs_state

    @pytest.mark.parametrize("target_health_state", list(HealthState))
    @pytest.mark.parametrize(
        "device_ref",
        ["station_on_name", "subarray_beam_id", "station_beam_id"],
    )
    def test_component_state_callback_subservient_device_health_state(
        self: TestMccsSubarray,
        device_under_test: MccsDeviceProxy,
        mock_subarray_component_manager: SubarrayComponentManager,
        target_health_state: HealthState,
        device_ref: str,
        station_on_name: str,
        request: pytest.FixtureRequest,
    ) -> None:
        """
        Test `component_state_changed` properly handles health updates.

        Here we test that by calling `component_state_callback` with the
        TRL of a device and a new healthState the record of it's health
        state in the health model is properly updated.

        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.

        :param target_health_state: The HealthState that the device should end up in.
        :param device_ref: A reference for a device from which to get the TRL.
        :param station_on_name: A label for a station which is on.
        :param request: A PyTest object giving access to the requesting test.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # Get the fixture from the parametrized fixture that's not a fixture.
        # (But is really.)
        device_id = request.getfixturevalue(device_ref)

        if device_id == "station_on_name":
            trl = get_station_trl(device_id)
        elif device_id == "subarray_beam_id":
            trl = get_subarray_beam_trl(device_id)
        else:
            trl = get_station_beam_trl(station_on_name, device_id)

        mock_subarray_component_manager._component_state_callback(
            health=target_health_state, trl=trl
        )
        dev_final_health_state = device_under_test.examine_health_model(trl)
        assert dev_final_health_state == target_health_state

    @pytest.mark.parametrize(
        "transaction_id, succeed",
        [
            pytest.param("bad_id", False, id="Invalid string"),
            pytest.param("txn-m001-20231109-12345", True, id="Valid id"),
            pytest.param("sd-c231-20250910-67890", True, id="Alternate valid id"),
            pytest.param("txn-m001-2023110-12345", False, id="Too short date"),
            pytest.param("txn-m001-202311091234-12345", False, id="Too long date"),
            pytest.param("-m001-202311091234-12345", False, id="Missing type code"),
            pytest.param("txn--202311091234-12345", False, id="Missing generator id"),
            pytest.param("txn-m001-202311091234-", False, id="Missing local sequence"),
            pytest.param("", False, id="Empty string"),
        ],
    )
    def test_configure_transaction_id_regex(
        self: TestMccsSubarray,
        transaction_id: str,
        succeed: bool,
        mock_subarray_component_manager: SubarrayComponentManager,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test the regex validation of the transaction id passed to configure.

        :param transaction_id: the transaction id to validate
        :param succeed: whether the id should pass validation
        :param mock_subarray_component_manager: A fixture that provides
            a partially mocked component manager which
            has access to the component_state_callback.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        device_under_test.set_obs_state("IDLE")

        mock_subarray_component_manager.start_communicating()
        config = {"transaction_id": transaction_id, "subarray_beams": []}
        try:
            result, _ = device_under_test.Configure(json.dumps(config))
        except tango.DevFailed as e:
            assert not succeed
            assert (
                "does not match '[a-z0-9]+-[a-z0-9]+-[0-9]{8}-[0-9]+'" in e.args[0].desc
            )
        else:
            assert succeed
            assert result[0] == ResultCode.QUEUED
