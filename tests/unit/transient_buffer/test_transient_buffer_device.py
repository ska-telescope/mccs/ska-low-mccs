# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for MccsTransientBuffer."""
from __future__ import annotations

import gc
import unittest.mock
from typing import Any, Iterator

import pytest
import pytest_mock
import tango
from ska_control_model import HealthState
from ska_low_mccs_common import MccsDeviceProxy
from ska_tango_testing.mock import MockCallable
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_low_mccs import MccsTransientBuffer
from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext

# To prevent tests hanging during gc.
gc.disable()


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> MccsDeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the test context in which the tests are run.
    :return: the device under test
    """
    return test_context.get_transient_buffer_device()


class TestMccsTransientBuffer:
    """Tests of the MCCS transient buffer device."""

    @pytest.fixture()
    def mock_component_manager(
        self: TestMccsTransientBuffer,
        mocker: pytest_mock.MockerFixture,
    ) -> unittest.mock.Mock:
        """
        Return a mock to be used as a component manager for the transient buffer device.

        :param mocker: fixture that wraps the :py:mod:`unittest.mock` module
        :return: a mock to be used as a component manager for the
            transient buffer device.

        """
        return mocker.Mock()

    @pytest.fixture(name="test_context")
    def test_context_fixture(
        self: TestMccsTransientBuffer,
        patched_transient_buffer_device_class: MccsTransientBuffer,
    ) -> Iterator[MccsTangoTestHarnessContext]:
        """
        Return a test context in which the transient buffer is running.

        :param patched_transient_buffer_device_class: a transient buffer device
            class, patched with extra methods for testing.

        :yields: a test context.
        """
        harness = MccsTangoTestHarness()

        harness.add_transient_buffer_device(
            device_class=patched_transient_buffer_device_class
        )

        with harness as context:
            yield context

    @pytest.fixture(name="patched_transient_buffer_device_class")
    def patched_transient_buffer_device_class_fixture(
        self: TestMccsTransientBuffer,
        mock_component_manager: unittest.mock.Mock,
    ) -> type[MccsTransientBuffer]:
        """
        Return a transient buffer device that is patched with a mock component manager.

        :param mock_component_manager: the mock component manager with
            which to patch the device

        :return: a transient buffer device that is patched with a mock
            component manager.

        """

        class PatchedMccsTransientBuffer(MccsTransientBuffer):
            """A transient buffer device patched with a mock component manager."""

            def create_component_manager(
                self: PatchedMccsTransientBuffer,
            ) -> unittest.mock.Mock:
                """
                Return a mock component manager instead of the usual one.

                :return: a mock component manager
                """
                wrapped_communication_state_callback = MockCallable(
                    wraps=self._communication_state_callback
                )
                wrapped_component_state_callback = MockCallable(
                    wraps=self._component_state_callback
                )
                mock_component_manager._communication_state_callback = (
                    wrapped_communication_state_callback
                )
                mock_component_manager._component_state_callback = (
                    wrapped_component_state_callback
                )

                return mock_component_manager

        return PatchedMccsTransientBuffer

    def test_healthState(
        self: TestMccsTransientBuffer,
        device_under_test: MccsDeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        mock_component_manager: unittest.mock.Mock,
    ) -> None:
        """
        Test for healthState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        :param mock_component_manager: mocked component manager used to
            access the real component state changed callback

        """
        device_under_test.subscribe_event(
            "healthState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.UNKNOWN)
        assert device_under_test.healthState == HealthState.UNKNOWN

        mock_component_manager._component_state_callback(health=HealthState.OK)
        change_event_callbacks["healthState"].assert_change_event(HealthState.OK)
        assert device_under_test.healthState == HealthState.OK

    @pytest.mark.parametrize(
        ("device_attribute", "component_manager_property", "example_value"),
        [
            ("stationId", "station_id", "example_string"),
            (
                "transientBufferJobId",
                "transient_buffer_job_id",
                "example_string",
            ),
            ("transientFrequencyWindow", "transient_frequency_window", (0.0,)),
            ("resamplingBits", "resampling_bits", 0),
            ("nStations", "n_stations", 0),
            ("stationIds", "station_ids", ("example_string",)),
        ],
    )
    # pylint: disable=too-many-arguments
    def test_attributes(
        self: TestMccsTransientBuffer,
        mocker: pytest_mock.MockerFixture,
        device_under_test: MccsDeviceProxy,
        mock_component_manager: unittest.mock.Mock,
        device_attribute: str,
        component_manager_property: str,
        example_value: Any,
    ) -> None:
        """
        Test that device attributes reads result in component manager property reads.

        :param mocker: fixture that wraps the :py:mod:`unittest.mock` module
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param mock_component_manager: the mock component manager being
            used by the patched transient buffer device.

        :param device_attribute: name of the device attribute under test.
        :param component_manager_property: name of the component manager
            property that is expected to be called when the device
            attribute is called.

        :param example_value: any value of the correct type for the
            device attribute.

        """
        property_mock = mocker.PropertyMock(return_value=example_value)
        setattr(
            type(mock_component_manager),
            component_manager_property,
            property_mock,
        )
        property_mock.assert_not_called()

        _ = getattr(device_under_test, device_attribute)
        property_mock.assert_called_once_with()
