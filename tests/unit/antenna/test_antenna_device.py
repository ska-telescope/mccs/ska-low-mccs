# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
# pylint: disable=too-many-lines
"""This module contains the tests for the MccsAntenna."""
from __future__ import annotations

import gc
import json
import unittest.mock
from typing import Iterator

import pytest
import tango
from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    ControlMode,
    LoggingLevel,
    PowerState,
    ResultCode,
    SimulationMode,
)
from ska_low_mccs_common import MccsDeviceProxy
from ska_tango_testing.mock.callable import MockCallable
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango.server import command

from ska_low_mccs.antenna import AntennaComponentManager, MccsAntenna
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_field_station_trl,
)

STATION_NAME = "ci-1"
ANTENNA_NAME = "sb1-1"
ANTENNA_ID = 1
TILE_ID = 1

# To stop gc causing some tests to hang.
gc.disable()


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the context in which the tests run

    :return: the device under test
    """
    return test_context.get_antenna_device(STATION_NAME, ANTENNA_NAME)


# pylint: disable=too-many-public-methods
class TestMccsAntenna:
    """Test class for MccsAntenna tests."""

    @pytest.fixture(name="patched_antenna_device_class")
    def patched_antenna_device_class_fixture(
        self: TestMccsAntenna,
        initial_antenna_power_states: dict[str, PowerState],
    ) -> type[MccsAntenna]:
        """
        Return an antenna device class, patched with extra methods for testing.

        :param initial_antenna_power_states: whether each antenna is initially on
            in the FieldStation

        :return: a patched MccsAntenna class
        """

        class PatchedAntennaDevice(MccsAntenna):
            """
            MccsAntenna patched with extra commands for testing purposes.

            The extra commands allow us to mock the receipt of a state
            change event from its FieldStation device.

            These methods are provided here because they are quite
            implementation dependent. If an implementation change breaks
            this, we only want to fix it in this one place.
            """

            @command()
            def MockAntennaPoweredOn(self: PatchedAntennaDevice) -> None:
                """Mock the Antenna being turned on."""
                are_antennas_on = initial_antenna_power_states
                are_antennas_on[str(self.AntennaId)] = PowerState.ON

                # to break unwrappable line
                proxy = self.component_manager._field_station_proxy
                proxy._antenna_power_state_changed(
                    "antennaPowerStates",
                    json.dumps(are_antennas_on),
                    tango.AttrQuality.ATTR_VALID,
                )

            @command()
            def MockFieldStationOff(self: PatchedAntennaDevice) -> None:
                """
                Mock the FieldStation being turned off.

                Make the antenna device think it has received a state change
                event from its FieldStation indicating that the FieldStation is now
                OFF.
                """
                self.component_manager._field_station_proxy._device_state_changed(
                    "state", tango.DevState.OFF, tango.AttrQuality.ATTR_VALID
                )

            @command()
            def MockFieldStationOn(self: PatchedAntennaDevice) -> None:
                """
                Mock the FieldStation being turned on.

                Make the antenna device think it has received a state change
                event from its FieldStation indicating that the FieldStation is now
                ON.
                """
                self.component_manager._field_station_proxy._device_state_changed(
                    "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
                )

        return PatchedAntennaDevice

    @pytest.fixture(name="test_context")
    def test_context_fixture(
        self: TestMccsAntenna,
        tile_channels: tuple[int, int],
        patched_antenna_device_class: type[MccsAntenna],
        mock_field_station: unittest.mock.Mock,
        mock_tile: unittest.mock.Mock,
    ) -> Iterator[MccsTangoTestHarnessContext]:
        """
        Return a test context in which mock tile and field station devices are running.

        :param tile_channels: the tile ADC channels of
            the Y and X signals from the antenna
        :param patched_antenna_device_class: a patched MccsAntenna class
            for use in testing
        :param mock_field_station: the mock FieldStation device to be provided
            when a device proxy to the FieldStation TRL is requested.
        :param mock_tile: the mock Tile device to be provided when a device
            proxy to the Tile TRL is requested.

        :yields: a test context.
        """
        harness = MccsTangoTestHarness()
        harness.add_mock_field_station_device(STATION_NAME, mock_field_station)
        harness.add_mock_tile_device(STATION_NAME, TILE_ID, mock_tile)
        harness.add_antenna_device(
            STATION_NAME,
            ANTENNA_NAME,
            ANTENNA_ID,
            TILE_ID,
            tile_channels[0],
            tile_channels[1],
            logging_level=5,
            device_class=patched_antenna_device_class,
        )
        with harness as context:
            yield context

    def test_Reset(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for Reset.

        Expected to fail as can't reset in the Off state.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        with pytest.raises(tango.DevFailed):
            device_under_test.Reset()

    def test_antennaId(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for antennaId.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.antennaId == ANTENNA_ID

    def test_gain(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for gain.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.gain == 0.0

    def test_rms(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for rms.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.rms == 0.0

    @pytest.mark.xfail(reason="Smartbox has no monitoring point FEM voltage")
    @pytest.mark.parametrize("voltage", [19.0])
    def test_voltage(
        self: TestMccsAntenna,
        test_context: MccsTangoTestHarnessContext,
        device_under_test: tango.DeviceProxy,
        antenna_component_manager: AntennaComponentManager,
        change_event_callbacks: MockTangoEventCallbackGroup,
        voltage: float,
    ) -> None:
        """
        Test for voltage.

        :param test_context: the test context in which this test will be run.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param antenna_component_manager: An Antenna component manager
            with some callbacks wrapped with a `MockCallable`.

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        :param voltage: a voltage value to use for testing
        """
        assert isinstance(
            antenna_component_manager._component_state_callback,
            MockCallable,
        )
        mock_field_station = test_context.get_field_station_device(STATION_NAME)
        mock_field_station.get_antenna_voltage.return_value = voltage

        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE

        device_under_test.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )

        change_event_callbacks["state"].assert_change_event(tango.DevState.DISABLE)
        assert device_under_test.state() == tango.DevState.DISABLE

        with pytest.raises(
            tango.DevFailed,
            match="Communication with component is not established",
        ):
            _ = device_under_test.voltage

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(
            AdminMode.ONLINE, lookahead=2
        )
        assert device_under_test.adminMode == AdminMode.ONLINE
        change_event_callbacks["communication_state"].assert_call(
            CommunicationStatus.ESTABLISHED, lookahead=10
        )

        device_under_test.MockFieldStationBoxOn()

        # TODO How do we check that the component_manager decorators are
        # satisfied here?
        # Decorators "check_communicating" & "check_on" need to be verified in
        # this test first.
        assert device_under_test.voltage == voltage
        assert mock_field_station.get_antenna_voltage.called_once_with(1)

    # pylint: disable=too-many-arguments
    @pytest.mark.xfail()
    @pytest.mark.parametrize(("current"), [pytest.param([4.5] * 24)])
    def test_current(
        self: TestMccsAntenna,
        antenna_component_manager: AntennaComponentManager,
        test_context: MccsTangoTestHarnessContext,
        device_under_test: MccsDeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        antenna_id: int,
        current: list[float],
    ) -> None:
        """
        Test for current.

        :param antenna_component_manager: An Antenna component manager
            with some callbacks wrapped with a `MockCallable`.
        :param test_context: the test context in which this test will be run.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.
        :param antenna_id: the logical id of this antenna (0-256)
        :param current: a current value to use for testing
        """
        assert isinstance(
            antenna_component_manager._component_state_callback,
            MockCallable,
        )
        mock_field_station = test_context.get_field_station_device(STATION_NAME)
        mock_field_station.PortsCurrentDraw = current

        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE

        device_under_test.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks["state"].assert_change_event(tango.DevState.DISABLE)
        assert device_under_test.state() == tango.DevState.DISABLE

        with pytest.raises(
            tango.DevFailed,
            match="Communication with component is not established",
        ):
            _ = device_under_test.current

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)

        change_event_callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        change_event_callbacks["communication_state"].assert_call(
            CommunicationStatus.ESTABLISHED
        )

        assert (
            antenna_component_manager._field_station_communication_state
            == CommunicationStatus.ESTABLISHED
        )
        assert (
            antenna_component_manager._tile_communication_state
            == CommunicationStatus.ESTABLISHED
        )

        change_event_callbacks["state"].assert_change_event(
            tango.DevState.OFF, lookahead=2
        )
        assert device_under_test.adminMode == AdminMode.ONLINE
        assert device_under_test.state() == tango.DevState.OFF

        device_under_test.MockFieldStationBoxOn()
        antenna_component_manager._component_state_callback.assert_call(
            power=PowerState.ON,
            trl=get_field_station_trl(STATION_NAME),
            lookahead=10,
        )
        assert antenna_component_manager._field_station_power_state == PowerState.ON
        # TODO How do we check that the component_manager decorators are
        # satisfied here?
        # Decorators "check_communicating" & "check_on" need to be verified
        # in this test first.

        assert device_under_test.current == current[antenna_id - 1]
        assert mock_field_station.get_antenna_current.called_once_with(1)

    @pytest.mark.xfail(reason="Smartbox has no temperature monitoring point.")
    @pytest.mark.parametrize("temperature", [37.4])
    def test_temperature(
        self: TestMccsAntenna,
        test_context: MccsTangoTestHarnessContext,
        device_under_test: tango.DeviceProxy,
        antenna_component_manager: AntennaComponentManager,
        change_event_callbacks: MockTangoEventCallbackGroup,
        temperature: float,
    ) -> None:
        """
        Test for temperature.

        :param test_context: the test context in which this test will be run.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param antenna_component_manager: An Antenna component manager
            with some callbacks wrapped with a `MockCallable`.

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        :param temperature: a temperature value to use for testing
        """
        assert isinstance(
            antenna_component_manager._component_state_callback,
            MockCallable,
        )
        mock_field_station = test_context.get_field_station_device(STATION_NAME)
        mock_field_station.get_antenna_temperature.return_value = temperature

        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE

        device_under_test.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks["state"].assert_change_event(tango.DevState.DISABLE)
        assert device_under_test.state() == tango.DevState.DISABLE

        with pytest.raises(
            tango.DevFailed,
            match="Communication with component is not established",
        ):
            _ = device_under_test.temperature

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert device_under_test.adminMode == AdminMode.ONLINE
        change_event_callbacks["state"].assert_change_event(
            tango.DevState.OFF, lookahead=2
        )

        device_under_test.MockFieldStationBoxOn()
        antenna_component_manager._component_state_callback.assert_call(
            power=PowerState.ON,
            trl=get_field_station_trl(STATION_NAME),
            lookahead=10,
        )
        assert antenna_component_manager._field_station_power_state == PowerState.ON

        # TODO How do we check that the component_manager decorators are
        # satisfied here?
        # Decorators "check_communicating" & "check_on" need to be verified
        # in this test first.

        assert device_under_test.temperature == temperature
        assert mock_field_station.get_antenna_temperature.called_once_with(1)

    def test_xPolarisationFaulty(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for xPolarisationFaulty.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.xPolarisationFaulty is False

    def test_yPolarisationFaulty(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for yPolarisationFaulty.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.yPolarisationFaulty is False

    def test_xDisplacement(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for xDisplacement.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.xDisplacement == 0.0

    def test_yDisplacement(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for yDisplacement.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.yDisplacement == 0.0

    def test_zDisplacement(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for zDisplacement.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.zDisplacement == 0.0

    def test_timestampOfLastSpectrum(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for timestampOfLastSpectrum.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.timestampOfLastSpectrum == ""

    def test_loggingLevel(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for loggingLevel.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.loggingLevel == LoggingLevel.DEBUG

    def test_controlMode(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for controlMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.controlMode == ControlMode.REMOTE

    def test_simulationMode(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for simulationMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.simulationMode == SimulationMode.FALSE
        with pytest.raises(
            tango.DevFailed,
            match="MccsAntenna cannot be put into simulation mode.",
        ):
            device_under_test.simulationMode = SimulationMode.TRUE

    def test_logicalAntennaId(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for logicalAntennaId.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.logicalAntennaId == 0

    def test_xPolarisationScalingFactor(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for xPolarisationScalingFactor.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.xPolarisationScalingFactor) == [0]

    def test_yPolarisationScalingFactor(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for yPolarisationScalingFactor.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.yPolarisationScalingFactor) == [0]

    def test_calibrationCoefficient(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for calibrationCoefficient.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.calibrationCoefficient) == [0.0]

    def test_pointingCoefficient(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for pointingCoefficient.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.pointingCoefficient) == [0.0]

    def test_spectrumX(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for spectrumX.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.spectrumX) == [0.0]

    def test_spectrumY(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for spectrumY.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.spectrumY) == [0.0]

    def test_position(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for position.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.position) == [0.0]

    def test_loggingTargets(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for loggingTargets.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.loggingTargets == ("tango::logger",)

    def test_delays(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for delays.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.delays) == [0.0]

    def test_delayRates(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for delayRates.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.delayRates) == [0.0]

    def test_bandpassCoefficient(
        self: TestMccsAntenna,
        device_under_test: tango.DeviceProxy,
    ) -> None:
        """
        Test for bandpassCoefficient.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.bandpassCoefficient) == [0.0]

    def test_On(
        self: TestMccsAntenna,
        device_under_test: MccsDeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        mock_field_station: unittest.mock.Mock,
    ) -> None:
        """
        Test for On.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param change_event_callbacks: group of Tango change event
            callback with asynchrony support
        :param mock_field_station: a proxy to the FieldStation device.
        """
        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE

        device_under_test.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks.assert_change_event("state", tango.DevState.DISABLE)
        assert device_under_test.state() == tango.DevState.DISABLE
        with pytest.raises(
            tango.DevFailed,
            match="Command On not allowed when the device is in DISABLE state",
        ):
            _ = device_under_test.On()

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)
        assert device_under_test.adminMode == AdminMode.ONLINE
        change_event_callbacks["state"].assert_change_event(tango.DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", tango.DevState.OFF)

        device_under_test.On()
        [[result_code], [message]] = device_under_test.On()
        mock_field_station.PowerOnAntenna.assert_next_call(ANTENNA_ID)
        assert result_code == ResultCode.QUEUED
        assert message.split("_")[-1] == "On"

        # At this point the FieldStation should turn the antenna on,
        # then fire a change event.
        # so let's fake that.
        device_under_test.MockAntennaPoweredOn()
        change_event_callbacks["state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        assert device_under_test.state() == tango.DevState.ON


class TestMockedMccsAntenna:
    """Tests of MccsAntenna that mock the component manager out."""

    @pytest.fixture(name="patched_antenna_device_class")
    def patched_antenna_device_class_fixture(
        self: TestMockedMccsAntenna,
        initial_antenna_power_states: dict[str, PowerState],
        mock_component_manager: unittest.mock.Mock,
    ) -> type[MccsAntenna]:
        """
        Return an antenna device class, patched with extra methods for testing.

        :param initial_antenna_power_states: whether each antenna is initially on
            in the FieldStation
        :param mock_component_manager: A mock Antenna component manager.

        :return: an antenna device class, patched with extra methods for testing.
        """

        class PatchedAntennaDevice(MccsAntenna):
            """
            MccsAntenna patched with extra commands for testing purposes.

            The extra commands allow us to mock the receipt of a state
            change event from its FieldStation device.

            These methods are provided here because they are quite
            implementation dependent. If an implementation change breaks
            this, we only want to fix it in this one place.
            """

            def create_component_manager(
                self: PatchedAntennaDevice,
            ) -> AntennaComponentManager:
                """
                Return a partially mocked component manager instead of the usual one.

                :return: a mock component manager
                """
                return mock_component_manager

        return PatchedAntennaDevice

    @pytest.fixture(name="test_context")
    def test_context_fixture(
        self: TestMockedMccsAntenna,
        tile_channels: tuple[int, int],
        patched_antenna_device_class: type[MccsAntenna],
        mock_field_station: unittest.mock.Mock,
        mock_tile: unittest.mock.Mock,
    ) -> Iterator[MccsTangoTestHarnessContext]:
        """
        Return a test context in which mock tile and field station devices are running.

        :param tile_channels: the tile ADC channels of
            the Y and X signals from the antenna
        :param patched_antenna_device_class: a patched MccsAntenna class
            for use in testing
        :param mock_field_station: the mock FieldStation device to be provided
            when a device proxy to the FieldStation TRL is requested.
        :param mock_tile: the mock Tile device to be provided
            when a device proxy to the Tile TRL is requested.

        :yields: a test context.
        """
        harness = MccsTangoTestHarness()
        harness.add_antenna_device(
            STATION_NAME,
            ANTENNA_NAME,
            ANTENNA_ID,
            TILE_ID,
            tile_channels[0],
            tile_channels[1],
            logging_level=5,
            device_class=patched_antenna_device_class,
        )
        with harness as context:
            yield context

    def test_configure_valid_schema(
        self: TestMockedMccsAntenna,
        device_under_test: tango.DeviceProxy,
        mock_component_manager: unittest.mock.Mock,
    ) -> None:
        """
        Test for configure command.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param mock_component_manager: the mock component manage to
            patch into this antenna.

        """
        config_dict = {
            "antenna_config": {
                "antennaId": 1,
                "xDisplacement": 1,
                "yDisplacement": 1,
                "zDisplacement": 1,
            },
            "tile_config": {"fixed_delays": [1], "antenna_ids": [1]},
        }
        json_str = json.dumps(config_dict)

        device_under_test.adminMode = AdminMode.ONLINE
        device_under_test.Configure(json_str)

        mock_component_manager.configure.assert_against_call(
            call_kwargs={
                "antenna_config": {
                    "antennaId": 1,
                    "xDisplacement": 1,
                    "yDisplacement": 1,
                    "zDisplacement": 1,
                },
                "tile_config": {"fixed_delays": [1], "antenna_ids": [1]},
            },
            lookahead=2,
        )

    def test_configure_invalid_schema(
        self: TestMockedMccsAntenna,
        device_under_test: tango.DeviceProxy,
        mock_component_manager: unittest.mock.Mock,
    ) -> None:
        """
        Test for configure command.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param mock_component_manager: the mock component manage to
            patch into this antenna.

        """
        config_dict = {
            "antenna_invalid_json": {
                "antennaId": 1,
                "xDisplacement": 1,
                "yDisplacement": 1,
                "zDisplacement": 1,
            },
            "tile_invalid_json": {"fixed_delays": [1], "antenna_ids": [1]},
        }
        json_str = json.dumps(config_dict)

        with pytest.raises(tango.DevFailed) as err:
            device_under_test.Configure(json_str)

        assert "ValidationError" in str(err.value)
