# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for MccsSubarrayBeam."""
from __future__ import annotations

import gc
import json
from random import uniform
from typing import Any, Iterator, Type

import pytest
import tango
from ska_control_model import AdminMode, HealthState
from ska_low_mccs_common import MccsDeviceProxy
from ska_tango_testing.mock import MockCallable
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango.server import command

from ska_low_mccs import MccsSubarrayBeam
from ska_low_mccs.subarray_beam.subarray_beam_component_manager import (
    SubarrayBeamComponentManager,
)
from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext

# To prevent tests hanging during gc.
gc.disable()


@pytest.fixture(name="patched_subarray_beam_device_class")
def patched_subarray_beam_device_class_fixture(
    subarray_beam_component_manager: SubarrayBeamComponentManager,
) -> Type[MccsSubarrayBeam]:
    """
    Return a subarray beam device class, patched with extra methods for testing.

    :param subarray_beam_component_manager: mocked component manager
        which has access to the component_state_callback

    :return: a patched subarray beam device class, patched with extra
        methods for testing

    """

    # pylint: disable=too-many-ancestors
    class PatchedSubarrayBeamDevice(MccsSubarrayBeam):
        """MccsSubarrayBeam patched with extra commands for testing purposes."""

        @command(dtype_in=str)
        def set_obs_state(
            self: PatchedSubarrayBeamDevice,
            obs_state_name: str,
        ) -> None:
            """
            Set the obsState of this device.

            A method to set the obsState for testing purposes.

            :param obs_state_name: The name of the obsState to directly transition to.
            """
            self.obs_state_model._straight_to_state(obs_state_name)

        def create_component_manager(
            self: PatchedSubarrayBeamDevice,
        ) -> SubarrayBeamComponentManager:
            """
            Return a patched component manager instead of the usual one.

            :return: a patched component manager
            """
            wrapped_communication_state_callback = MockCallable(
                wraps=self._communication_state_callback
            )
            wrapped_component_state_callback = MockCallable(
                wraps=self._component_state_callback
            )
            subarray_beam_component_manager._communication_state_callback = (
                wrapped_communication_state_callback
            )
            subarray_beam_component_manager._component_state_callback = (
                wrapped_component_state_callback
            )
            return subarray_beam_component_manager

    return PatchedSubarrayBeamDevice


@pytest.fixture(name="test_context")
def test_context_fixture(
    subarray_beam_id: int,
    patched_subarray_beam_device_class: MccsSubarrayBeam,
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which mock tile and field station devices are running.

    :param subarray_beam_id: the id of a subarray beam.
    :param patched_subarray_beam_device_class: a subarray beam device class, patched
        with extra methods for testing.

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()

    harness.add_subarray_beam_device(
        subarray_beam_id, device_class=patched_subarray_beam_device_class
    )

    with harness as context:
        yield context


class TestMccsSubarrayBeam:
    """Test class for MccsSubarrayBeam tests."""

    @pytest.fixture()
    def device_under_test(
        self: TestMccsSubarrayBeam,
        test_context: MccsTangoTestHarnessContext,
        subarray_beam_id: int,
    ) -> MccsDeviceProxy:
        """
        Fixture that returns the device under test.

        :param test_context: the context in which the tests run
        :param subarray_beam_id: A SubarrayBeam id.
        :return: the device under test
        """
        return test_context.get_subarray_beam_device(subarray_beam_id)

    def test_healthState(
        self: TestMccsSubarrayBeam,
        device_under_test: MccsDeviceProxy,
        subarray_beam_component_manager: SubarrayBeamComponentManager,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for healthState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param subarray_beam_component_manager: mocked component manager
            which has access to the component_state_callback

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        device_under_test.subscribe_event(
            "healthState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.OK)
        assert device_under_test.healthState == HealthState.OK

        subarray_beam_component_manager._component_state_callback(
            health=HealthState.DEGRADED
        )

        change_event_callbacks["healthState"].assert_change_event(HealthState.DEGRADED)
        assert device_under_test.healthState == HealthState.DEGRADED

    @pytest.mark.parametrize(
        ("attribute", "initial_value", "write_value"),
        [
            ("subarrayId", 0, None),
            ("subarrayBeamId", 0, None),
            ("logicalBeamId", 0, None),
            ("updateRate", 0.0, None),
            ("isBeamLocked", True, None),
        ],
    )
    def test_attributes(
        self: TestMccsSubarrayBeam,
        device_under_test: MccsDeviceProxy,
        attribute: str,
        initial_value: Any,
        write_value: Any,
    ) -> None:
        """
        Test attribute values.

        This is a very weak test that simply checks that attributes take
        certain initial values, and that, when we write to a writable
        attribute, the write sticks.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param attribute: name of the attribute under test.
        :param initial_value: the expected initial value of the attribute.
        :param write_value: a value to write to check that it sticks
        """
        with pytest.raises(
            tango.DevFailed,
            match="Communication with component is not established",
        ):
            _ = getattr(device_under_test, attribute)

        device_under_test.adminMode = AdminMode.ONLINE

        assert getattr(device_under_test, attribute) == initial_value

        if write_value is not None:
            device_under_test.write_attribute(attribute, write_value)
            assert getattr(device_under_test, attribute) == write_value

    def test_stationIds(
        self: TestMccsSubarrayBeam,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test stationIds attribute.

        This is a very weak test that simply checks that the attribute
        starts as an empty list, and when we write a new value to it,
        the write sticks.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        with pytest.raises(
            tango.DevFailed,
            match="Communication with component is not established",
        ):
            _ = device_under_test.stationIds

        device_under_test.adminMode = AdminMode.ONLINE

        assert not list(device_under_test.stationIds)

        value_to_write = [3, 4, 5, 6]
        device_under_test.stationIds = value_to_write
        assert list(device_under_test.stationIds) == value_to_write

    @pytest.mark.parametrize("attribute", ["channels", "antennaWeights", "phaseCentre"])
    def test_empty_list_attributes(
        self: TestMccsSubarrayBeam,
        device_under_test: MccsDeviceProxy,
        attribute: str,
    ) -> None:
        """
        Test attribute values for attributes that return lists of floats.

        This is a very weak test that simply checks that attributes are
        initialised to the empty list.

        Due to a Tango bug with return empty lists through image
        attributes or float spectrum attributes, the attribute value
        will come through as None rather than [].

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param attribute: name of the attribute under test.
        """
        with pytest.raises(
            tango.DevFailed,
            match="Communication with component is not established",
        ):
            _ = getattr(device_under_test, attribute)
        device_under_test.adminMode = AdminMode.ONLINE
        assert not list(getattr(device_under_test, attribute))

    def test_desired_pointing(
        self: TestMccsSubarrayBeam,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test the desired pointing attribute.

        This is a weak test that simply check that the attribute's
        initial value is as expected, and that we can write a new value
        to it.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        with pytest.raises(
            tango.DevFailed,
            match="Communication with component is not established",
        ):
            _ = device_under_test.desiredPointing
        device_under_test.adminMode = AdminMode.ONLINE

        assert not list(device_under_test.desiredPointing)

        value_to_write = [1585619550.0, 192.85948, 2.0, 27.12825, 1.0]
        device_under_test.desiredPointing = value_to_write
        assert list(device_under_test.desiredPointing) == pytest.approx(value_to_write)

    def test_configure(
        self: TestMccsSubarrayBeam,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test the desired pointing attribute.

        This is a weak test that simply check that the attribute's
        initial value is as expected, and that we can write a new value
        to it.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        with pytest.raises(
            tango.DevFailed,
            match="Communication with component is not established",
        ):
            _ = device_under_test.desiredPointing
        device_under_test.adminMode = AdminMode.ONLINE

        configure_arg = {
            "subarray_beam_id": 1,
            "subarray_id": 1,
            "update_rate": 5,
            "logical_bands": [
                {
                    "start_channel": 2,
                    "number_of_channels": 8,
                },
                {
                    "start_channel": 2,
                    "number_of_channels": 8,
                },
            ],
            "sky_coordinates": {
                "target_name": "some star",
                "reference_frame": "ICRS",
                "timestamp": "2023-04-05T12:34:56.000Z",
                "c1": uniform(0, 360),
                "c1_rate": uniform(-0.016, 0.016),
                "c2": uniform(-90, 90),
                "c2_rate": uniform(-0.016, 0.016),
            },
            "apertures": [
                {
                    "station_id": 1,
                    "aperture_id": "AP001.01",
                },
            ],
        }
        device_under_test.set_obs_state("READY")
        device_under_test.Configure(json.dumps(configure_arg))
