# -*- coding: utf-8 -*
# pylint: disable=too-many-arguments
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for SubarrayBeamHealthModel."""
from __future__ import annotations

from typing import Any

import pytest
from ska_control_model import HealthState
from ska_low_mccs_common.testing.mock import MockCallable

from ska_low_mccs.subarray_beam.subarray_beam_health_model import (
    SubarrayBeamHealthModel,
)
from tests.harness import get_station_beam_trl

STATION_NAME = "ci-1"


class TestSubarrayBeamHealthModel:
    """A class for tests of the subarray beam health model."""

    @pytest.fixture
    def health_model(self: TestSubarrayBeamHealthModel) -> SubarrayBeamHealthModel:
        """
        Fixture to return the subarray beam health model.

        :return: Health model to be used.
        """
        health_model = SubarrayBeamHealthModel(
            [get_station_beam_trl(STATION_NAME, 1)],
            MockCallable(),
            ignore_power_state=True,
        )
        health_model.update_state(communicating=True)

        return health_model

    @pytest.mark.parametrize(
        (
            "health_state",
            "health_change",
            "expected_init_health",
            "expected_init_report",
            "expected_final_health",
            "expected_final_report",
        ),
        [
            pytest.param(
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): HealthState.OK
                        for i in range(1, 16 + 1)
                    },
                    "beam_locked": True,
                },
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, 1): HealthState.FAILED
                    },
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: MccsStationBeam: "
                    f"['{get_station_beam_trl(STATION_NAME,1)}']"
                ),
                id="All devices healthy, expect OK, then 1 station beam failed, "
                "expect DEGRADED",
            ),
            pytest.param(
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): HealthState.OK
                        for i in range(16)
                    },
                    "beam_locked": True,
                },
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): HealthState.FAILED
                        for i in range(8)
                    },
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: MccsStationBeam: "
                    f"{[get_station_beam_trl(STATION_NAME,i) for i in range(8)]}"
                ),
                id="All devices healthy, expect OK, then 8 station beams failed, "
                "expect FAILED",
            ),
            pytest.param(
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): HealthState.DEGRADED
                        for i in range(16)
                    },
                    "beam_locked": True,
                },
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): HealthState.OK
                        for i in range(14)
                    },
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: MccsStationBeam: "
                    f"{[get_station_beam_trl(STATION_NAME,i) for i in range(16)]}"
                ),
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: MccsStationBeam: "
                    f"{[get_station_beam_trl(STATION_NAME,i) for i in range(14, 16)]}"
                ),
                id="All devices DEGRADED, expect FAILED, then 14 station beams OK, "
                "expect DEGRADED",
            ),
            pytest.param(
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): HealthState.OK
                        for i in range(16)
                    },
                    "beam_locked": True,
                },
                {"beam_locked": False},
                HealthState.OK,
                "Health is OK.",
                HealthState.DEGRADED,
                "The beam is not locked",
                id="All devices healthy, expect OK, then beam lock lost "
                "expect DEGRADED",
            ),
            pytest.param(
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): HealthState.OK
                        for i in range(16)
                    },
                    "beam_locked": True,
                },
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): HealthState.UNKNOWN
                        for i in range(4)
                    },
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.UNKNOWN,
                (
                    "Some devices are unknown: MccsStationBeam: "
                    f"{[get_station_beam_trl(STATION_NAME,i) for i in range(4)]}"
                ),
                id="All devices healthy, expect OK, then 3 station beams UNKNOWN, "
                "expect UNKNOWN",
            ),
        ],
    )
    def test_subarray_beam_changed_health(
        self: TestSubarrayBeamHealthModel,
        health_model: SubarrayBeamHealthModel,
        health_state: dict[str, Any],
        health_change: dict[str, Any],
        expected_init_health: HealthState,
        expected_init_report: str,
        expected_final_health: HealthState,
        expected_final_report: str,
    ) -> None:
        """
        Tests of the subarray beam health model for a changed health.

        The properties of the health model are set and checked, then the health states
        of subservient devices are updated and the health is checked against the new
        expected value.

        :param health_model: The subarray beam health model to use
        :param health_state: the relevant initial health state for the subarray beam
        :param health_change: a dictionary of the health changes, key device and value
            dictionary of HealthState
        :param expected_init_health: the expected initial health
        :param expected_init_report: the expected initial report
        :param expected_final_health: the expected final health
        :param expected_final_report: the expected final report
        """
        health_model._station_beam_healths = health_state["station_beam"]
        health_model._is_beam_locked = health_state["beam_locked"]

        assert health_model.evaluate_health() == (
            expected_init_health,
            expected_init_report,
        )

        if "beam_locked" in health_change:
            health_model.is_beam_locked_changed(health_change["beam_locked"])
        if "station_beam" in health_change:
            for change in health_change["station_beam"]:
                health_model.station_beam_health_changed(
                    change, health_change["station_beam"][change]
                )

        assert health_model.evaluate_health() == (
            expected_final_health,
            expected_final_report,
        )

    @pytest.mark.parametrize(
        (
            "health_state",
            "init_thresholds",
            "expected_init_health",
            "expected_init_report",
            "final_thresholds",
            "expected_final_health",
            "expected_final_report",
        ),
        [
            pytest.param(
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): (
                            HealthState.OK if i > 1 else HealthState.FAILED
                        )
                        for i in range(16)
                    },
                    "beam_locked": True,
                },
                {
                    "station_beam_degraded_threshold": 0.05,
                    "station_beam_failed_threshold": 0.2,
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: MccsStationBeam: "
                    f"{[get_station_beam_trl(STATION_NAME, i) for i in range(2)]}"
                ),
                {
                    "station_beam_degraded_threshold": 0.15,
                    "station_beam_failed_threshold": 0.3,
                },
                HealthState.OK,
                "Health is OK.",
                id="Two station beams FAILED, expect DEGRADED, then raise DEGRADED "
                "threshold, expect OK",
            ),
            pytest.param(
                {
                    "station_beam": {
                        get_station_beam_trl(STATION_NAME, i): (
                            HealthState.OK if i > 4 else HealthState.FAILED
                        )
                        for i in range(16)
                    },
                    "beam_locked": True,
                },
                {
                    "station_beam_degraded_threshold": 0.45,
                    "station_beam_failed_threshold": 0.6,
                },
                HealthState.OK,
                "Health is OK.",
                {
                    "station_beam_degraded_threshold": 0.05,
                    "station_beam_failed_threshold": 0.2,
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: MccsStationBeam: "
                    f"{[get_station_beam_trl(STATION_NAME, i) for i in range(5)]}"
                ),
                id="Four station beams unhealthy with adjusted thresholds, expect OK, "
                "then lower thresholds, expect FAILED",
            ),
        ],
    )
    def test_subarray_beam_health_changed_thresholds(
        self: TestSubarrayBeamHealthModel,
        health_model: SubarrayBeamHealthModel,
        health_state: dict[str, Any],
        init_thresholds: dict[str, float],
        expected_init_health: HealthState,
        expected_init_report: str,
        final_thresholds: dict[str, float],
        expected_final_health: HealthState,
        expected_final_report: str,
    ) -> None:
        """
        Tests of the subarray beam health model for changed thresholds.

        The properties of the health model are set and checked, then the thresholds for
        the health rules are changed and the new health is checked against the expected
        value

        :param health_model: The subarray beam health model to use
        :param health_state: the relevant initial health state for the subarray beam
        :param init_thresholds: the initial thresholds of the health rules
        :param expected_init_health: the expected initial health
        :param expected_init_report: the expected initial report
        :param final_thresholds: the final thresholds of the health rules
        :param expected_final_health: the expected final health
        :param expected_final_report: the expected final report
        """
        health_model._station_beam_healths = health_state["station_beam"]
        health_model._is_beam_locked = health_state["beam_locked"]

        health_model.health_params = init_thresholds
        assert health_model.evaluate_health() == (
            expected_init_health,
            expected_init_report,
        )

        health_model.health_params = final_thresholds
        assert health_model.evaluate_health() == (
            expected_final_health,
            expected_final_report,
        )

    @pytest.mark.parametrize(
        (
            "initial_expected_beams",
            "new_beams",
            "expected_end_beams",
        ),
        [
            pytest.param(
                {
                    get_station_beam_trl(STATION_NAME, 1): HealthState.UNKNOWN,
                },
                (
                    get_station_beam_trl(STATION_NAME, 2),
                    get_station_beam_trl(STATION_NAME, 3),
                ),
                {
                    get_station_beam_trl(STATION_NAME, 2): HealthState.UNKNOWN,
                    get_station_beam_trl(STATION_NAME, 3): HealthState.UNKNOWN,
                },
                id="Can add new beams, expected state unknown",
            ),
            pytest.param(
                {
                    get_station_beam_trl(STATION_NAME, 1): HealthState.OK,
                },
                (
                    get_station_beam_trl(STATION_NAME, 2),
                    get_station_beam_trl(STATION_NAME, 3),
                    get_station_beam_trl(STATION_NAME, 1),
                ),
                {
                    get_station_beam_trl(STATION_NAME, 2): HealthState.UNKNOWN,
                    get_station_beam_trl(STATION_NAME, 3): HealthState.UNKNOWN,
                    get_station_beam_trl(STATION_NAME, 1): HealthState.OK,
                },
                id="assert can add new beams and keep old ones",
            ),
            pytest.param(
                {
                    get_station_beam_trl(STATION_NAME, 1): HealthState.UNKNOWN,
                    get_station_beam_trl(STATION_NAME, 2): HealthState.OK,
                    get_station_beam_trl(STATION_NAME, 3): HealthState.OK,
                },
                (
                    get_station_beam_trl(STATION_NAME, 2),
                    get_station_beam_trl(STATION_NAME, 1),
                    get_station_beam_trl(STATION_NAME, 4),
                ),
                {
                    get_station_beam_trl(STATION_NAME, 1): HealthState.UNKNOWN,
                    get_station_beam_trl(STATION_NAME, 2): HealthState.OK,
                    get_station_beam_trl(STATION_NAME, 4): HealthState.UNKNOWN,
                },
                id="assert can add new beams and remove old ones",
            ),
        ],
    )
    def test_update_station_beams(
        self: TestSubarrayBeamHealthModel,
        health_model: SubarrayBeamHealthModel,
        initial_expected_beams: dict[str, HealthState],
        new_beams: set[str],
        expected_end_beams: dict[str, HealthState],
    ) -> None:
        """
        Tests the adding and removing of station beams.

        :param health_model: The subarray beam health model to use
        :param initial_expected_beams: The initial set of beams
        :param new_beams: The new beams to be added
        :param expected_end_beams: The expected end beams
        """
        for beam in initial_expected_beams:
            health_model.station_beam_health_changed(beam, initial_expected_beams[beam])

        assert health_model._station_beam_healths == initial_expected_beams

        health_model.update_station_beams(new_beams)

        assert health_model._station_beam_healths == expected_end_beams
