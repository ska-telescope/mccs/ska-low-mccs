# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for the SpsStation tango device."""
from __future__ import annotations

import gc
import json
from typing import Any, Iterator
from unittest.mock import Mock

import numpy as np
import pytest
from psycopg import sql
from ska_control_model import AdminMode, ResultCode
from ska_tango_testing.mock.placeholders import Anything
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevFailed, DeviceProxy, DevState, EventType

from ska_low_mccs.calibration_store import MccsCalibrationStore
from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext

# TODO: Weird hang-at-garbage-collection bug
gc.disable()


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of callables to be used as Tango change event callbacks.

    :return: a dictionary of callables to be used as tango change event
        callbacks.
    """
    return MockTangoEventCallbackGroup(
        "admin_mode",
        "command_result",
        "command_status",
        "health_state",
        "state",
        timeout=2.0,
    )


# pylint: disable=too-many-arguments
@pytest.fixture(name="test_context")
def test_context_fixture(
    station_label: str,
    patched_calibration_store_device_class: type[MccsCalibrationStore],
    database_host: str,
    database_port: int,
    database_name: str,
    database_admin_user: str,
    database_admin_password: str,
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Yield into a context in which Tango is running, with mock devices.

    :param station_label: name of the station
    :param patched_calibration_store_device_class: a subclass of MccsCalibrationStore
        that has been patched to mock out the database connection
    :param database_host: the database host
    :param database_port: the database port
    :param database_name: the database name
    :param database_admin_user: the database admin user
    :param database_admin_password: the database admin password

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()
    harness.set_calibration_store_device(
        station_label=station_label,
        device_class=patched_calibration_store_device_class,
        database_host=database_host,
        database_port=database_port,
        database_name=database_name,
        database_admin_user=database_admin_user,
        database_admin_password=database_admin_password,
    )
    with harness as context:
        yield context


@pytest.fixture(name="calibration_store_device")
def calibration_store_device_fixture(
    test_context: MccsTangoTestHarnessContext,
    station_label: str,
) -> DeviceProxy:
    """
    Fixture that returns the calibration store Tango device under test.

    :param test_context: a Tango test context containing a calibration store.
    :param station_label: name of the station

    :yield: the calibration store Tango device under test.
    """
    yield test_context.get_calibration_store_device(station_label)


def test_GetSolution(
    calibration_store_device: MccsCalibrationStore,
    mock_connection: Mock,
    station_id: int,
) -> None:
    """
    Test of the GetCalibration command.

    :param calibration_store_device: the calibration store device under test
    :param mock_connection: the mock database connection
    :param station_id: the id of the station to get a solution for
    """
    solution_path = "tests/data/test_calibration_solution.npy"

    def mock_execute(*args: Any, **kwargs: Any) -> Mock:
        def get_solution_path(*args: Any, **kwargs: Any) -> dict[str, Any]:
            return {
                "solution": np.load(solution_path),
                "calibration_path": solution_path,
            }

        result_mock = Mock()
        result_mock.fetchone = get_solution_path
        return result_mock

    test_solution = np.load(solution_path)
    calibration_store_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]

    selected_frequency: int = 5
    selected_temperature: float = 24.9
    frequency_tolerance = 0
    temperature_tolerance = 0
    calibration_store_device.UpdateSelectionPolicy(
        json.dumps(
            {
                "policy_name": "closest_in_range",
                "frequency_tolerance": frequency_tolerance,
                "temperature_tolerance": temperature_tolerance,
            }
        )
    )
    mock_connection.execute.side_effect = mock_execute

    result = calibration_store_device.GetSolution(
        json.dumps(
            {
                "frequency_channel": 5,
                "outside_temperature": 24.9,
                "station_id": station_id,
            }
        )
    )
    query = (
        "SELECT frequency_channel, outside_temperature, "
        "calibration_path, creation_time, station_id, "
        "preferred, solution "
        "FROM tab_mccs_calib WHERE station_id = %s "
        "AND frequency_channel >= %s AND frequency_channel <= %s "
        "AND outside_temperature >= %s AND outside_temperature <= %s "
        "ORDER BY preferred DESC, creation_time DESC, "
        "ABS(ABS(frequency_channel - %s) + ABS(outside_temperature - %s)) DESC"
    )
    params = (
        station_id,
        selected_frequency - frequency_tolerance,
        selected_frequency + frequency_tolerance,
        selected_temperature - temperature_tolerance,
        selected_temperature + temperature_tolerance,
        selected_frequency,
        selected_temperature,
    )
    mock_connection.execute.assert_called_once_with(query, params)
    assert np.all(result == test_solution)


def test_StoreSolution(
    calibration_store_device: MccsCalibrationStore,
    mock_connection: Mock,
    sample_solution: dict[str, Any],
    invalid_sample_solution: dict[str, Any],
    expected_database_fields: list[str],
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Test of the StoreSolution command.

    :param calibration_store_device: the calibration store device under test
    :param mock_connection: the mock database connection
    :param sample_solution: sample calibration solution.
    :param invalid_sample_solution: sample calibration solution
        that is expected to raise an exception.
    :param expected_database_fields: A list of the expected fields in the
        calibration database.
    :param change_event_callbacks: A dictionary of change event
        callbacks with async support.
    """

    calibration_store_device.subscribe_event(
        "state",
        EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    change_event_callbacks["state"].assert_change_event(Anything)
    calibration_store_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
    change_event_callbacks["state"].assert_change_event(DevState.ON, lookahead=2)

    # Store an invalid calibration solution.
    with pytest.raises(DevFailed):
        [result_code], [message] = calibration_store_device.StoreSolution(
            json.dumps(invalid_sample_solution)
        )

    # Store a sample calibration solution missing an optional field.
    [result_code], [message] = calibration_store_device.StoreSolution(
        json.dumps(sample_solution)
    )

    query, values = __generate_expected_storage_sql_execution(
        expected_database_fields, sample_solution
    )

    mock_connection.execute.assert_called_once_with(query, values)
    assert result_code == ResultCode.OK
    assert message == "Solution stored successfully"


def __generate_expected_storage_sql_execution(
    database_fields: list[str], solution: dict[str, Any]
) -> tuple[sql.Composed, tuple]:
    """
    Generate the expected sql to be passed to execute.

    :param database_fields: the columns in the database that are
        filled in by placeholders.
    :param solution: a dictionary with the solution we want to
        store.

    We expect that each request to store will define all
    database columns, and that it is templated to protect against
    sql injection. As a result the execution looks like
    (query, templated_params).

    :return: a tuple containing the sql.Composed and the parameters.
    """
    # We expect all known fields to be explicitly defined.
    # Some of them are optional, in which case they should be set to
    # None.
    for column in database_fields:
        solution.setdefault(column, None)

    # The columns have been sorted in code to make testing
    # simpler.
    column_names, values = zip(*sorted(solution.items()))

    # Generate placeholders and column identifiers dynamically.
    # This is a very specific checking the sql query is as expected.
    # At the time of writing this our devcontainer does not spin up a
    # postgresql database, meaning we can only test this interface using
    # functional tests. As a result we have this highly specific test
    # checking for any changes to a query.
    placeholders = [sql.Placeholder()] * len(column_names)
    column_identifiers = [sql.Identifier(col) for col in column_names]

    query = sql.SQL(
        "INSERT INTO tab_mccs_calib "
        "(creation_time, {}) VALUES (current_timestamp, {})"
    ).format(
        sql.SQL(", ").join(column_identifiers),
        sql.SQL(", ").join(placeholders),
    )
    return query, values
