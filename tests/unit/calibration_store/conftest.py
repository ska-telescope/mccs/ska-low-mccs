#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defines a harness for unit testing the Calibration store module."""
from __future__ import annotations

import json
import logging
from typing import Any, Callable
from unittest.mock import MagicMock, Mock

import pytest
from ska_control_model import CommunicationStatus

from ska_low_mccs.calibration_store import (
    CalibrationStoreComponentManager,
    CalibrationStoreDatabaseConnection,
    MccsCalibrationStore,
)


@pytest.fixture(name="mock_connection")
def mock_connection_fixture() -> Mock:
    """
    Fixture for the mock connection for the mock connection pool to use.

    :return: a mock connection
    """
    connection = MagicMock()
    connection.__enter__ = lambda self: self
    return connection


@pytest.fixture(name="mock_connection_pool")
def mock_connection_pool_fixture(mock_connection: Mock) -> Mock:
    """
    Fixture for the mock connection pool to use.

    :param mock_connection: the mock connection to use
    :return: a mock connection pool.
    """
    connection_pool = Mock()
    connection_pool.connection = lambda _: mock_connection
    return connection_pool


@pytest.fixture(name="expected_database_fields")
def expected_database_fields_fixture() -> list[str]:
    """
    Return fields expected to be executed on a store event.

    :return: fields expected to be executed on a store event.
    """
    return [
        "calibration_path",
        "acquisition_time",
        "outside_temperature",
        "frequency_channel",
        "station_id",
        "preferred",
        "corrcoeff",
        "residual_max",
        "residual_std",
        "xy_phase",
        "n_masked_initial",
        "n_masked_final",
        "lst",
        "galactic_centre_elevation",
        "sun_elevation",
        "sun_adjustment_factor",
        "masked_antennas",
        "solution",
        "outside_temperature",
        "frequency_channel",
        "station_id",
    ]


@pytest.fixture(name="sample_solution")
def sample_solution_fixture() -> dict:
    """
    Return sample solutions.

    :return: sample solution with missing lst field.
    """
    with open(
        "tests/data/calibration_solution_data.json",
        "r",
        encoding="utf-8",
    ) as file:
        # Load data from the JSON file
        sample_solution = json.load(file)
    sample_solution.pop("lst", None)
    return dict(sample_solution)


@pytest.fixture(name="invalid_sample_solution")
def invalid_sample_solution_fixture(sample_solution: dict) -> dict[str, Any]:
    """
    Return a dictionary of callables to be used as Tango change event callbacks.

    :param sample_solution: sample solution with missing lst field.

    :return: a dictionary of callables to be used as tango change event
        callbacks.
    """
    # Pop the solution to make invalid
    invalid_solution = dict(sample_solution)
    invalid_solution.pop("solution", None)
    return invalid_solution


@pytest.fixture(name="patched_calibration_store_database_connection_class")
def patched_calibration_store_database_connection_class_fixture(
    mock_connection_pool: Mock,
) -> type[CalibrationStoreDatabaseConnection]:
    """
    Return a calibration store database connection with the connection pool mocked out.

    :param mock_connection_pool: the mocked database connection pool
    :return: a calibration store database connection with a mocked connection pool.
    """

    class PatchedCalibrationStoreDatabaseConnection(CalibrationStoreDatabaseConnection):
        """
        Patched CalibrationStoreDatabaseConnection.

        It has been patched to have a mocked out connection pool.
        """

        def _create_connection_pool(
            self: PatchedCalibrationStoreDatabaseConnection, *args: Any, **kwargs: Any
        ) -> Mock:
            """
            Create the mock connection pool for connecting to a postgres database.

            :param args: positional arguments (ignored here)
            :param kwargs: named arguments (ignored here)
            :return: the mock connection pool
            """
            return mock_connection_pool

    return PatchedCalibrationStoreDatabaseConnection


@pytest.fixture(name="patched_calibration_store_component_manager_class")
def patched_calibration_store_component_manager_class_fixture(
    patched_calibration_store_database_connection_class: type[
        CalibrationStoreDatabaseConnection
    ],
) -> type[CalibrationStoreComponentManager]:
    """
    Return a patched calibration store component manager.

    It has been patched to have the database connection mocked out.

    :param patched_calibration_store_database_connection_class: patched class for the
        database connection with a mocked connection pool

    :return: a calibration store component manager with a mocked database connection.
    """

    class PatchedCalibrationStoreComponentManager(CalibrationStoreComponentManager):
        """
        Patched CalibrationStoreComponentManager.

        It has been patched to have a mocked out database connection.
        """

        # pylint: disable=too-many-arguments
        def create_database_connection(
            self: PatchedCalibrationStoreComponentManager,
            logger: logging.Logger,
            communication_state_changed_callback: Callable[[CommunicationStatus], None],
            database_host: str,
            database_port: int,
            database_name: str,
            database_admin_user: str,
            database_admin_password: str,
        ) -> CalibrationStoreDatabaseConnection:
            return patched_calibration_store_database_connection_class(
                logger,
                communication_state_changed_callback,
                database_host,
                database_port,
                database_name,
                database_admin_user,
                database_admin_password,
                self._selection_manager,
            )

    return PatchedCalibrationStoreComponentManager


@pytest.fixture(name="patched_calibration_store_device_class")
def patched_calibration_store_device_class_fixture(
    patched_calibration_store_component_manager_class: type[
        CalibrationStoreComponentManager
    ],
) -> type[MccsCalibrationStore]:
    """
    Return a calibration store device class patched with extra methods for testing.

    :param patched_calibration_store_component_manager_class: patched class for the
        component manager with a mocked database connection

    :return: a calibration store device class patched with extra methods for testing.
    """

    class PatchedCalibrationStoreDevice(MccsCalibrationStore):
        """MccsCalibrationStore patched with mocked out database connection."""

        def create_component_manager(
            self: PatchedCalibrationStoreDevice,
        ) -> CalibrationStoreComponentManager:
            """
            Create and return a component manager for this device.

            :return: a component manager for this device.
            """
            return patched_calibration_store_component_manager_class(
                self.logger,
                self._component_communication_state_changed,
                self._component_state_changed,
                self.DatabaseHost,
                self.DatabasePort,
                self.DatabaseName,
                self.DatabaseAdminUser,
                self.DatabaseAdminPassword,
                policy_name="preferred",
                policy_frequency_tolerance=0,
                policy_temperature_tolerance=200.0,
            )

    return PatchedCalibrationStoreDevice


@pytest.fixture(name="station_label")
def station_label_fixture() -> str:
    """
    Return the station label of this station.

    :return: the station label of this station.
    """
    return "ci-1"
