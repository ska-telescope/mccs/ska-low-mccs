#  -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
This package implements SKA Low's MCCS subsystem.

The Monitoring Control and Calibration (MCCS) subsystem is responsible
for, amongst other things, monitoring and control of LFAA.
"""

__version__ = "1.0.0"
__version_info__ = (
    "ska-low-mccs",
    __version__,
    "This package implements SKA Low's MCCS subsystem.",
)

__all__ = [
    # devices
    "MccsAntenna",
    "MccsCalibrationStore",
    "MccsController",
    "MccsStation",
    "MccsStationBeam",
    "MccsSubarray",
    "MccsSubarrayBeam",
    "MccsTransientBuffer",
    "MccsStationCalibrator",
    "StationCalibrationSolverDevice",
    # device subpackages
    "antenna",
    "controller",
    "station",
    "station_beam",
    "subarray",
    "subarray_beam",
    "transient_buffer",
    "calibration_solver",
]

import tango.server

from .antenna import MccsAntenna
from .calibration_solver import StationCalibrationSolverDevice
from .calibration_store import MccsCalibrationStore
from .controller import MccsController
from .station import MccsStation
from .station_beam import MccsStationBeam
from .station_calibrator import MccsStationCalibrator
from .subarray import MccsSubarray
from .subarray_beam import MccsSubarrayBeam
from .transient_buffer import MccsTransientBuffer


def main(*args: str, **kwargs: str) -> int:  # pragma: no cover
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments

    :return: exit code
    """
    return tango.server.run(
        classes=(
            MccsAntenna,
            StationCalibrationSolverDevice,
            MccsCalibrationStore,
            MccsController,
            MccsStation,
            MccsStationBeam,
            MccsStationCalibrator,
            MccsSubarray,
            MccsSubarrayBeam,
            MccsTransientBuffer,
        ),
        args=args or None,
        **kwargs,
    )
