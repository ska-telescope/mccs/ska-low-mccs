#  -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
# pylint: disable = too-many-lines
"""This module implements component management for station beams."""
from __future__ import annotations

import functools
import json
import logging
import threading
import time
from typing import Any, Callable, Optional

from astropy.time import Time, TimeDelta
from ska_control_model import CommunicationStatus, PowerState, ResultCode, TaskStatus
from ska_low_mccs_common import EventSerialiser
from ska_low_mccs_common.component import DeviceComponentManager
from ska_tango_base.base import check_communicating, check_on
from ska_tango_base.executor import TaskExecutorComponentManager

__all__ = ["StationBeamComponentManager"]


class _StationProxy(DeviceComponentManager):
    """A station beam's proxy to its station."""

    @check_communicating
    @check_on
    def track_object(self: _StationProxy, pointing_args: str) -> ResultCode:
        """
        Start tracking at the given coordinates.

        :param pointing_args: the pointing arguments json string to be applied.
        :return: a result code.
        """
        assert self._proxy is not None
        ([result_code], _) = self._proxy.TrackObject(pointing_args)
        self.logger.debug(
            f"sending TrackObject({pointing_args}) - result {result_code}"
        )
        return result_code

    @check_communicating
    @check_on
    def stop_tracking_all(self: _StationProxy) -> ResultCode:
        """
        Stop tracking.

        :return: a result code.
        """
        assert self._proxy is not None
        ([result_code], _) = self._proxy.StopTrackingAll()
        return result_code

    @check_communicating
    @check_on
    def configure_channels(self: _StationProxy, table: list[list[int]]) -> ResultCode:
        """
        Apply the channel table to the station.

        :param table: the channel table to be applied.
        :return: a result code.
        """
        assert self._proxy is not None
        flattened_table: list[int] = []
        for block in table:
            flattened_table = flattened_table + block
        ([result_code], _) = self._proxy.ConfigureChannels(flattened_table)
        return result_code

    @check_communicating
    @check_on
    def scan(
        self: _StationProxy,
        subarray_id: int,
        scan_id: int,
        start_time: Optional[str],
        duration: float,
    ) -> ResultCode:
        """
        Start the subarray beam scanning.

        :param subarray_id: the subarray id of the station beam
        :param scan_id: the id of the scan
        :param start_time: the start time of the scan
        :param duration: Scan duration, in seconds. -1 for unlimited

        :return: A task status.
        """
        assert self._proxy is not None
        scan_arg: dict[str, int | float | str] = {
            "subarray_id": subarray_id,
            "scan_id": scan_id,
            "duration": duration,
        }
        if start_time is not None:
            scan_arg["start_time"] = start_time

        ([result_code], unique_id) = self._proxy.Scan(json.dumps(scan_arg))
        return result_code

    @check_communicating
    @check_on
    def end_scan(self: _StationProxy, subarray_id: int) -> ResultCode:
        """
        End station scan.

        :param subarray_id: The ID of the subarray whose scan is to be stopped
        :return: A task status.
        """
        assert self._proxy is not None
        (result_code, _) = self._proxy.EndScan(subarray_id)
        return result_code


# pylint: disable=too-many-instance-attributes,too-many-public-methods
class StationBeamComponentManager(TaskExecutorComponentManager):
    """A component manager for a station beam."""

    # messages for long running command task callback
    _task_message = {
        TaskStatus.COMPLETED: "completed OK.",
        TaskStatus.ABORTED: "been aborted.",
        TaskStatus.FAILED: "timed out.",
    }

    _task_to_result = {
        TaskStatus.COMPLETED: ResultCode.OK,
        TaskStatus.ABORTED: ResultCode.ABORTED,
        TaskStatus.FAILED: ResultCode.FAILED,
    }

    # pylint: disable=too-many-arguments
    def __init__(
        self: StationBeamComponentManager,
        beam_id: int,
        station_trl: str,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
        event_serialiser: Optional[EventSerialiser] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param beam_id: the beam id of this station beam
        :param station_trl: the TRL of the station to which this beam belongs.
        :param logger: the logger to be used by this object.
        :param communication_state_callback: callback to be called when
            the status of the communications channel between the
            component manager and its component changes
        :param component_state_callback: a callback to be called
            whenever the state of the station beam changes.
        :param event_serialiser: the event serialiser to be used by this object.
        """
        self._event_serialiser = event_serialiser
        self._abort_complete = threading.Event()
        self._subarray_id = 0
        self._beam_id = beam_id
        self._station_id = 0
        self._subarray_beam_id = 0
        self._hardware_beam_id = 0
        self._update_rate = 0.0
        self._scan_duration = 86400.0
        self._is_beam_locked = True
        self._aperture_id = "AP000.00"
        self._is_configured = False

        # List of allocated channel blocks. Each entry contains:
        # * channel block number
        # * first subarray logical channel
        # * first physical channel. 0 = unused
        self._channel_table: list[list[int]] = []
        self._number_of_channels = 0

        self._desired_pointing: list[float] = [0.0] * 4
        self._pointing_reference_frame = "ICRS"
        self._pointing_timestamp = ""
        self._pointing_delay: list[float] = []
        self._pointing_delay_rate: list[float] = []
        self._antenna_weights: list[float] = []
        self._phase_centre: list[float] = []

        self._station_trl: str = station_trl
        self._scan_id = 0

        self._component_state_callback: Callable[..., None]
        self._component_state_callback = component_state_callback

        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=None,  # PowerState.UNKNOWN,
            fault=None,
            beam_locked=None,
        )

        self._station_proxy = _StationProxy(
            self._station_trl,
            self.logger,
            self._device_communication_state_changed,
            functools.partial(
                self._component_state_callback,
                trl=self._station_trl,
            ),
            event_serialiser=self._event_serialiser,
        )

    def start_communicating(self: StationBeamComponentManager) -> None:
        """Establish communication with the component."""
        if self.communication_state != CommunicationStatus.ESTABLISHED:
            self._station_proxy.start_communicating()

    def stop_communicating(self: StationBeamComponentManager) -> None:
        """Cease monitoring the component, and break off all communication with it."""
        if self.communication_state != CommunicationStatus.DISABLED:
            self._station_proxy.stop_communicating()

    def _device_communication_state_changed(
        self: StationBeamComponentManager,
        communication_state: CommunicationStatus,
        trl: Optional[str] = None,
    ) -> None:
        # There is a race condition in this method.
        # When we change the station trl we will get
        # callbacks from the old station as it shuts down.
        if trl == self._station_trl or trl is None:
            self._update_communication_state(communication_state=communication_state)

    @property
    def max_executing_tasks(self) -> int:
        """
        Get the max number of tasks that can be executing at once.

        :return: max number of simultaneously executing tasks.
        """
        return 2

    @property
    def beam_id(self: StationBeamComponentManager) -> int:
        """
        Return the station beam id.

        :return: the station beam id
        """
        return self._beam_id

    @property
    def subarray_id(self: StationBeamComponentManager) -> int:
        """
        Return the subarray id.

        :return: the subarray id
        """
        return self._subarray_id

    @subarray_id.setter
    def subarray_id(self: StationBeamComponentManager, value: int) -> None:
        """
        Set the Subarray ID.

        :param value: the new subarray id
        """
        self._subarray_id = value

    @property
    def station_id(self: StationBeamComponentManager) -> int:
        """
        Return the station id.

        :return: the station ids
        """
        return self._station_id

    @station_id.setter
    def station_id(self: StationBeamComponentManager, value: int) -> None:
        """
        Set the station id.

        :param value: the new station id
        """
        self._station_id = value

    @property
    def aperture_id(self: StationBeamComponentManager) -> str:
        """
        Return the aperture id.

        :return: the aperture id
        """
        return self._aperture_id

    @property
    def logical_beam_id(self: StationBeamComponentManager) -> int:
        """
        Return the logical beam id.

        :return: the logical beam id
        """
        return self._subarray_beam_id

    @logical_beam_id.setter
    def logical_beam_id(self: StationBeamComponentManager, value: int) -> None:
        """
        Set the logical beam id.

        :param value: the new logical beam id
        """
        self._subarray_beam_id = value

    @property
    def update_rate(self: StationBeamComponentManager) -> float:
        """
        Return the update rate.

        :return: the update rate
        """
        return self._update_rate

    @property
    def is_beam_locked(self: StationBeamComponentManager) -> bool:
        """
        Return whether the beam is locked.

        :return: whether the beam is locked
        """
        return self._is_beam_locked

    @is_beam_locked.setter
    def is_beam_locked(self: StationBeamComponentManager, value: bool) -> None:
        """
        Set whether the beam is locked.

        :param value: new value for whether the beam is locked
        """
        if self._is_beam_locked != value:
            self._is_beam_locked = value
            if self._component_state_callback is not None:
                self._component_state_callback(beam_locked=value)

    @property
    def channels(self: StationBeamComponentManager) -> list[list[int]]:
        """
        Return the ids of the channels configured for this station beam.

        :return: the ids of the channels configured for this subarray
            beam.
        """
        table: list[list[int]] = []
        substation_id = int(self._aperture_id.split(".")[-1])
        for entry in self._channel_table:
            # mark as unused the unused entries
            if entry[2] == 0:
                subarray_id = 0  # means unused
            else:
                subarray_id = self._subarray_id
            table.append(
                [
                    entry[0],
                    entry[2],
                    self._hardware_beam_id,
                    subarray_id,
                    entry[1],
                    self._subarray_beam_id,
                    substation_id,
                    self._station_id * 100 + substation_id,  # aperture ID
                ]
            )
        return table  # [list(i) for i in self._channel_table]  # deep copy

    @property
    def antenna_weights(self: StationBeamComponentManager) -> list[float]:
        """
        Return the antenna weights.

        :return: the antenna weights
        """
        return list(self._antenna_weights)

    @property
    def desired_pointing(self: StationBeamComponentManager) -> list[float]:
        """
        Return the desired pointing.

        :return: the desired pointing
        """
        return self._desired_pointing

    @desired_pointing.setter
    def desired_pointing(self: StationBeamComponentManager, value: list[float]) -> None:
        """
        Set the desired pointing.

        :param value: the new desired pointing
        """
        self._desired_pointing = value

    @property
    def pointing_delay(self: StationBeamComponentManager) -> list[float]:
        """
        Return the pointing delay.

        :return: the pointing delay
        """
        return self._pointing_delay

    @pointing_delay.setter
    def pointing_delay(self: StationBeamComponentManager, value: list[float]) -> None:
        """
        Set the pointing delay.

        :param value: the new pointing delay
        """
        self._pointing_delay = value

    @property
    def pointing_delay_rate(self: StationBeamComponentManager) -> list[float]:
        """
        Return the pointing delay rate.

        :return: the pointing delay rate
        """
        return self._pointing_delay_rate

    @pointing_delay_rate.setter
    def pointing_delay_rate(
        self: StationBeamComponentManager, value: list[float]
    ) -> None:
        """
        Set the pointing delay rate.

        :param value: the new pointing delay rate
        """
        self._pointing_delay_rate = value

    @property
    def phase_centre(self: StationBeamComponentManager) -> list[float]:
        """
        Return the phase centre.

        :return: the phase centre
        """
        return self._phase_centre

    @property
    def power_state(self: StationBeamComponentManager) -> Optional[PowerState]:
        """
        Return my power state.

        :return: my power state
        """
        return self._component_state["power"]

    def _check_aborted(
        self: StationBeamComponentManager,
        task_abort_event: Optional[threading.Event],
        task_callback: Optional[Callable],
        method_name: str,
    ) -> bool:
        if task_abort_event is None or task_callback is None:
            self.logger.warning(f"Cannot check {method_name} for abort status")
            return False

        if task_abort_event.is_set():
            self.logger.info(f"{method_name} has been aborted")
            task_callback(
                status=TaskStatus.ABORTED,
                result=(ResultCode.ABORTED, f"{method_name} aborted"),
            )
            return True
        return False

    def assign_resources(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        *,
        interface: Optional[str] = None,
        subarray_id: int,
        subarray_beam_id: int,
        aperture_id: str,
        station_id: int,
        station_trl: int,
        channel_blocks: list[int],
        hardware_beam: int,
        first_subarray_channel: int,
        number_of_channels: int,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the assign_resources slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :param interface: the schema version this is running against.
        :param subarray_id: ID of the subarray to which the beam belongs
        :param subarray_beam_id: ID of the subarray beam
        :param aperture_id: ID of the aperture, of the form "APx.y"
        :param station_id: ID of the associated station
        :param station_trl: TRL of the associated station
        :param channel_blocks: List of the allocated station channel blocks
        :param hardware_beam: Allocated station hardware beam
        :param first_subarray_channel: First channel
        :param number_of_channels: Number of channels
        :return: A tuple containing a task status and a unique id string
            to identify the command
        """
        return self.submit_task(
            self._assign_resources,
            args=[
                subarray_id,
                subarray_beam_id,
                aperture_id,
                station_id,
                station_trl,
                channel_blocks,
                hardware_beam,
                first_subarray_channel,
                number_of_channels,
            ],
            task_callback=task_callback,
        )

    # pylint: disable=too-many-arguments
    def _assign_resources(
        self: StationBeamComponentManager,
        subarray_id: int,
        subarray_beam_id: int,
        aperture_id: str,
        station_id: int,
        station_trl: str,
        channel_blocks: list[int],
        hardware_beam: int,
        first_subarray_channel: int,
        number_of_channels: int,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Assign resources to device.

        :param subarray_id: ID of the subarray to which the beam belongs
        :param subarray_beam_id: ID of the subarray beam
        :param aperture_id: ID of the aperture in format "APx.y"
        :param station_id: ID of the associated station
        :param station_trl: TRL of the associated station
        :param channel_blocks: List of the allocated station channel blocks
        :param hardware_beam: Allocated station hardware beam
        :param first_subarray_channel: First channel
        :param number_of_channels: Number of channels
        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Task abort, defaults to None
        """
        if task_callback:
            task_callback(TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_assign_resources"):
            return

        self.logger.debug(
            f"Configuring station beam {subarray_id}:{subarray_beam_id}, "
            f"Aperture {aperture_id} station {station_trl}, "
            f"HW beam:{hardware_beam} channels: {number_of_channels}"
        )
        self._subarray_id = subarray_id
        self._subarray_beam_id = subarray_beam_id
        self._station_id = station_id
        self._aperture_id = aperture_id

        # Connect to station if not already
        # self.station_trl = station_trl

        if self._check_aborted(task_abort_event, task_callback, "_assign_resources"):
            return

        self._number_of_channels = number_of_channels
        self._channel_table = []
        first_channel = first_subarray_channel
        for block in channel_blocks:
            self._channel_table.append([block, first_channel, 0])
            first_channel += 8
        self._hardware_beam_id = hardware_beam
        self._component_state_callback(resources_changed=number_of_channels > 0)

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="The assign_resources command has completed",
            )

    def configure(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
        *,
        interface: Optional[str] = None,
        update_rate: Optional[float] = 1.0,
        logical_bands: list[dict],
        weighting_key_ref: Optional[str] = "uniform",
        sky_coordinates: Optional[dict[str, Any]] = None,
        field: Optional[dict[str, Any]] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the `configure` slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None

        :param interface: the schema version this is running against.
        :param update_rate: Update rate for pointing, default never
        :param logical_bands: Description of observed sky frequency bands
        :param weighting_key_ref: Antenna weights, default uniform,
        :param sky_coordinates: Pointing direction
        :param field: Pointing direction
        :return: A return code and a unique command ID.
        """
        if sky_coordinates:
            field = {
                key: val
                for key, val in sky_coordinates.items()
                if key in ["timestamp", "reference_frame", "target_name"]
            }
            field["attrs"] = {
                key: val
                for key, val in sky_coordinates.items()
                if key in ["c1", "c1_rate", "c2", "c2_rate"]
            }
            field.setdefault("target_name", "No name provided")

        return self.submit_task(
            self._configure,
            args=[
                update_rate,
                logical_bands,
                weighting_key_ref,
                field,
            ],
            task_callback=task_callback,
        )

    # pylint: disable=too-many-arguments, too-many-branches
    def _configure(
        self: StationBeamComponentManager,
        update_rate: float,
        logical_bands: list[dict],
        weighting_key_ref: str,
        field: dict[str, Any],
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Configure this station beam for scanning.

        :param update_rate: Update rate for pointing, default never
        :param logical_bands: Description of observed sky frequency bands
        :param weighting_key_ref: Antenna weights, default uniform,
        :param field: Pointing direction
        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_configure"):
            return

        current_block = 0
        for block in self._channel_table:
            block[2] = 0  # all blocks set to "unused"
        max_block = len(self._channel_table)
        for band in logical_bands:
            number_of_blocks = band["number_of_channels"] // 8
            start_channel = band["start_channel"]
            if current_block + number_of_blocks > max_block:
                self.logger.error("Too many logical blocks")
                break
            for _ in range(number_of_blocks):
                self._channel_table[current_block][2] = start_channel
                start_channel += 8
                current_block += 1

        if self._check_aborted(task_abort_event, task_callback, "_configure"):
            return

        self._update_rate = update_rate
        # pointing setup
        self.logger.debug(f"Pointing to {field['attrs']['c1']}-{field['attrs']['c2']}")
        self._pointing_reference_frame = field.get("reference_frame", "AltAz")
        self._pointing_timestamp = field.get(
            "timestamp", (Time.now() + TimeDelta(0.2, format="sec")).isot + "Z"
        )
        attributes = field["attrs"]
        self._desired_pointing = [
            attributes["c1"],
            attributes.get("c1_rate", 0.0),
            attributes["c2"],
            attributes.get("c2_rate", 0.0),
        ]

        if self._check_aborted(task_abort_event, task_callback, "_configure"):
            return
        # For now only uniform weighting is supported
        if weighting_key_ref != "uniform":
            self.logger.error("Only uniform weighting supported now")
        self._antenna_weights = [1.0] * 256
        self._phase_centre = [0.0, 0.0, 0.0]
        # TODO: forward relevant calibration and pointing related configuration
        # to participating stations
        if self._station_proxy:
            self._station_proxy.configure_channels(self.channels)

        if self._check_aborted(task_abort_event, task_callback, "_configure"):
            return

        time.sleep(0.1)

        self._apply_pointing(task_abort_event=task_abort_event)

        if self._check_aborted(task_abort_event, task_callback, "_configure"):
            return

        if not self._is_configured:
            self._component_state_callback(configured_changed=True)

        if task_callback is not None:
            task_callback(
                status=TaskStatus.COMPLETED, result="Configure has completed."
            )
        self._is_configured = True

    def apply_pointing(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the apply_pointing slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        :return: Task status and response message
        """
        return self.submit_task(
            self._apply_pointing, args=[], task_callback=task_callback
        )

    @check_communicating
    def _apply_pointing(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Apply the configured pointing to this station beam's station.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_apply_pointing"):
            return

        if self._pointing_reference_frame in ("AltAz", "topocentric"):
            coords = {
                "pointing_type": "alt_az",
                "time_step": self._update_rate,
                "scan_time": 86400,
                "reference_time": self._pointing_timestamp,
                "station_beam_number": self._hardware_beam_id,
                "values": {
                    "azimuth": self._desired_pointing[0],
                    "azimuth_rate": self._desired_pointing[1],
                    "altitude": self._desired_pointing[2],
                    "altitude_rate": self._desired_pointing[3],
                },
            }
        elif self._pointing_reference_frame == "ICRS":
            coords = {
                "pointing_type": "ra_dec",
                "time_step": self._update_rate,
                "scan_time": 86400,
                "reference_time": self._pointing_timestamp,
                "station_beam_number": self._hardware_beam_id,
                "values": {
                    "right_ascension": self._desired_pointing[0],
                    "right_ascension_rate": self._desired_pointing[1],
                    "declination": self._desired_pointing[2],
                    "declination_rate": self._desired_pointing[3],
                },
            }
        else:
            self.logger.debug(
                f"Pointing type {self._pointing_reference_frame} not supported"
            )
            if task_callback is not None:
                task_callback(TaskStatus.FAILED, result="Apply pointing has failed.")
            return

        if self._check_aborted(task_abort_event, task_callback, "_apply_pointing"):
            return

        self.logger.debug(f"Pointing coordinates {coords['values']}")

        if self._station_proxy:
            self._station_proxy.track_object(json.dumps(coords))

        if task_callback is not None:
            task_callback(TaskStatus.COMPLETED, result="Apply pointing has completed.")

        # zipped_delays_and_rates = [
        #    item
        #    for pair in zip(self.pointing_delay, self.pointing_delay_rate + [0])
        #    for item in pair
        # ]
        # station_pointing_args = [
        #    cast(float, self.logical_beam_id)
        # ] + zipped_delays_and_rates

    def _abort_callback(
        self: StationBeamComponentManager,
        *,
        status: Optional[TaskStatus] = None,
        exception: Optional[Exception] = None,
        **kwargs: Any,
    ) -> None:
        if exception is not None:
            self.logger.error(f"abort_commands raised exception: {repr(exception)}")

        if status == TaskStatus.COMPLETED:
            self.logger.debug("abort_commands has finished, setting flag")
            self._abort_complete.set()

    @check_communicating
    def abort(  # type: ignore[override]
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Abort the observation.

        :param task_callback: callback to be called when the status of
            the command changes
        :return: A task status and response message.
        """
        # We have to spin up a separate thread such that we skip the queue which
        # we would append to if we used submit_task
        threading.Thread(
            target=self._abort, kwargs={"task_callback": task_callback}
        ).start()
        return (TaskStatus.IN_PROGRESS, "Abort has started")

    @check_communicating
    def _abort(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Abort the current scan associated with the station_beam.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        task_status = TaskStatus.COMPLETED

        # This is sent in a thread, we record progress using the _abort_callback, which
        # will set an Event once the task is complete.
        self.abort_commands(task_callback=self._abort_callback)  # type:ignore

        # Wait until the timeout for the Event to be set by abort_commands
        timeout = 10.0
        if self._abort_complete.wait(timeout=timeout):
            self._abort_complete.clear()
        else:
            self.logger.error(f"Abort timed out in {timeout} seconds.")
            task_status = TaskStatus.FAILED
            # Reinitialize our event. The old one could get set at any point now.
            self._abort_complete = threading.Event()

        # TODO: Implement Abort() in MccsStation, at the moment we'll just cancel any
        # running pointing threads and stop the beamformer for this beam.
        if task_status == TaskStatus.COMPLETED:
            if self._station_proxy is not None:
                self._station_proxy.end_scan(self._subarray_id)
                self._station_proxy.stop_tracking_all()

        if task_callback:
            task_callback(
                status=task_status,
                result=(
                    self._task_to_result[task_status],
                    "Abort command " + self._task_message[task_status],
                ),
            )

    def obsreset(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the obs_reset slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        :return: Task status and response message
        """
        return self.submit_task(self._obsreset, args=[], task_callback=task_callback)

    @check_communicating
    def _obsreset(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Reset (to IDLE) the station_beam.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(TaskStatus.IN_PROGRESS)
        # # TODO MCCS-1491: Perform the relevant action on the device
        # # Do whatever is necessary
        assert self._station_proxy is not None
        self._station_proxy.stop_tracking_all()

        if task_callback is not None:
            task_callback(TaskStatus.COMPLETED, result="ObsReset has completed.")

    def restart(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the restart slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        :return: Task status and response message
        """
        return self.submit_task(self._restart, args=[], task_callback=task_callback)

    @check_communicating
    def _restart(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Restart (to EMPTY) the station_beam.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        task_status = TaskStatus.COMPLETED

        # Scanning related stuff
        self._scan_id = 0
        if self._station_proxy is not None:
            self._station_proxy.end_scan(self._subarray_id)

        # Deconfigure related stuff
        self._deconfigure_beam()
        self._is_configured = False

        # Release related stuff
        self._release_beam()

        if task_callback:
            task_callback(
                status=task_status,
                result=(
                    self._task_to_result[task_status],
                    "Restart command " + self._task_message[task_status],
                ),
            )

    def end(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the end slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :return: Task status and response message
        """
        return self.submit_task(self._end, args=[], task_callback=task_callback)

    @check_communicating
    def _end(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Deconfigure the station_beam.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(TaskStatus.IN_PROGRESS)

        self._deconfigure_beam()

        self._component_state_callback(configured_changed=False)
        if task_callback is not None:
            task_callback(TaskStatus.COMPLETED, result="Deconfigure has completed.")
        self._is_configured = False

    def _deconfigure_beam(self: StationBeamComponentManager) -> None:
        for block in self._channel_table:
            block[2] = 0  # all blocks set to "unused"
        self._update_rate = 0.0
        # for now not much
        self._pointing_reference_frame = "AltAz"
        self._pointing_timestamp = "2000-01-01T00:00:00.000Z"
        self._desired_pointing = [0.0] * 4
        if self._station_proxy is not None:
            self._station_proxy.stop_tracking_all()

    def release_all_resources(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the release__all_resources slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :return: Task status and response message
        """
        return self.submit_task(
            self._release_all_resources, args=[], task_callback=task_callback
        )

    @check_communicating
    def _release_all_resources(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Release all allocated resources from the station_beam.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(TaskStatus.IN_PROGRESS)

        self._release_beam()

        self._component_state_callback(resources_changed=False)
        if task_callback is not None:
            task_callback(
                TaskStatus.COMPLETED,
                result="ReleaseAllResources has completed.",
            )

    def _release_beam(self: StationBeamComponentManager) -> None:
        self._subarray_id = 0
        self._subarray_beam_id = 0
        self._station_id = 0
        self._aperture_id = "AP000.00"

        # Disconnect from station
        # self.station_trl = ""

        self._number_of_channels = 0
        self._channel_table = []

    def scan(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        *,
        scan_id: int,
        start_time: Optional[str] = None,
        duration: Optional[float] = 864000.0,  # one day for infinite scan
    ) -> tuple[TaskStatus, str]:
        """
        Submit the Scan slow task.

        This method returns immediately after it is submitted for
        execution.

        :param scan_id: The ID for this scan
        :param start_time: UTC time for begin of scan, None for immediate start
        :param duration: Scan duration in seconds. 0.0 or omitted means forever
        :param task_callback: Update task state, defaults to None
        :return: Task status and response message
        """
        return self.submit_task(
            self._scan,
            args=[scan_id, start_time, duration],
            task_callback=task_callback,
        )

    def _scan(
        self: StationBeamComponentManager,
        scan_id: int,
        start_time: Optional[str],
        duration: float,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Execute the Scan slow task.

        :param scan_id: The ID for this scan
        :param start_time: UTC time for begin of scan, None for immediate start
        :param duration: Scan duration in seconds. 0.0 or omitted means forever
        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)
        # TODO Fill this with meaningful actions, like commanding the
        # station beam pointing thread.
        # Note that the station.Scan comand is already issued by the subarray
        # so it must not be re-issued here.
        # assert self._station_proxy is not None
        # self._station_proxy.scan(self._subarray_id, scan_id, start_time, duration)
        self._scan_id = scan_id
        self._scan_duration = duration
        # self._apply_pointing() # This should be done in the configuration.

        self._component_state_callback(scanning_changed=True)
        if task_callback is not None:
            task_callback(
                TaskStatus.COMPLETED,
                result="Scan has completed.",
            )

    def end_scan(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the EndScan slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        :return: Task status and response message
        """
        return self.submit_task(
            self._end_scan,
            args=[],
            task_callback=task_callback,
        )

    def _end_scan(
        self: StationBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Execute the EndScan slow task.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        # TODO Fill this with meaningful actions, like commanding the
        # station beam pointing thread. Station will be started by Subarray
        # Station.EndScan is already issued by the Subarray, should not be re-issued
        # assert self._station_proxy is not None
        # self._station_proxy.end_scan(self._subarray_id)
        self._scan_id = 0

        self._component_state_callback(scanning_changed=False)
        if task_callback is not None:
            task_callback(
                TaskStatus.COMPLETED,
                result="EndScan has completed.",
            )
