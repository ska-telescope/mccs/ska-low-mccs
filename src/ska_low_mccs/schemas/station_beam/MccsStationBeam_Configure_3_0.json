{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://schema.skao.int/ska-low-mccs-stationbeam-configure/3.0",
  "title": "StationBeam Configure schema",
  "description": "Schema for StationBeam's Configure command",
  "type": "object",
  "required": [
    "logical_bands"
  ],
  "properties": {
    "interface": {
      "description": "The schema version which you expect to run against",
      "type": "string",
      "pattern": "^https?:\/\/.+\/.+\/[0-9]+.[0-9]+$"
    },
    "update_rate": {
      "type": "number",
      "minimum": 0.0
    },
    "logical_bands": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "start_channel": {
            "type": "integer",
            "minimum": 2,
            "maximum": 504,
            "multipleOf": 2
          },
          "number_of_channels": {
            "type": "integer",
            "minimum": 8,
            "maximum": 384,
            "multipleOf": 8
          }
        },
        "required": [
          "start_channel",
          "number_of_channels"
        ]
      },
      "maxItems": 48
    },
    "weighting_key_ref": {
      "description": "descriptive ID for the aperture weights in the aperture database",
      "type": "string"
    },
    "sky_coordinates": {
      "type": "object",
      "properties": {
        "timestamp": {
          "type": "string",
          "description": "UTC time for begin of drift"
        },
        "reference_frame": {
          "type": "string",
          "enum": [
            "AltAz",
            "topocentric",
            "ICRS",
            "Galactic"
          ]
        },
        "target_name": {
          "type": "string",
          "description": "The name of the target"
        },
        "c1": {
          "description": "first coordinate, in degrees",
          "type": "number",
          "minimum": 0.0,
          "maximum": 360.0
        },
        "c1_rate": {
          "description": "Drift rate for first coordinate, in degrees/second",
          "type": "number",
          "minimum": -0.016,
          "maximum": 0.016
        },
        "c2": {
          "description": "second coordinate, in degrees",
          "type": "number",
          "minimum": -90.0,
          "maximum": 90.0
        },
        "c2_rate": {
          "description": "Drift rate for second coordinate, in degrees/second",
          "type": "number",
          "minimum": -0.016,
          "maximum": 0.016
        }
      },
      "required": [
        "reference_frame",
        "c1",
        "c2"
      ]
    },
    "field": {
      "type": "object",
      "properties": {
        "timestamp": {
          "type": "string",
          "description": "UTC time for begin of drift"
        },
        "target_name": {
          "type": "string",
          "description": "The name of the target"
        },
        "reference_frame": {
          "type": "string",
          "enum": [
            "AltAz",
            "topocentric",
            "ICRS",
            "Galactic"
          ]
        },
        "attrs": {
          "description": "Coordinates and rates of scan",
          "type": "object",
          "properties": {
            "c1": {
              "description": "first coordinate, in degrees",
              "type": "number",
              "minimum": 0.0,
              "maximum": 360.0
            },
            "c1_rate": {
              "description": "Drift rate for first coordinate, in degrees/second",
              "type": "number",
              "minimum": -0.016,
              "maximum": 0.016
            },
            "c2": {
              "description": "second coordinate, in degrees",
              "type": "number",
              "minimum": -90.0,
              "maximum": 90.0
            },
            "c2_rate": {
              "description": "Drift rate for second coordinate, in degrees/second",
              "type": "number",
              "minimum": -0.016,
              "maximum": 0.016
            }
          },
          "required": [
            "c1",
            "c2"
          ]
        }
      },
      "required": [
        "reference_frame",
        "attrs",
        "target_name"
      ]
    }
  }
}