{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://schema.skao.int/ska-low-mccs-station-set_pointing_coordinates/3.0",
  "title": "Station SetPointingCoordinates schema",
  "description": "Schema for MccsStation's SetPointingCoordinates command",
  "type": "object",
  "required": [
    "subarray_id",
    "beam_id"
  ],
  "properties": {
    "interface": {
      "description": "The schema version which you expect to run against",
      "type": "string",
      "pattern": "^https?:\/\/.+\/.+\/[0-9]+.[0-9]+$"
    },
    "subarray_id": {
      "description": "The ID of the subarray which commands pointing",
      "type": "integer",
      "minimum": 1,
      "maximum": 16
    },
    "beam_id": {
      "description": "The ID of the hardware beam to point",
      "type": "integer",
      "minimum": 0,
      "maximum": 47
    },
    "update_rate": {
      "type": "number",
      "minimum": 0.0
    },
    "sky_coordinates": {
      "type": "object",
      "properties": {
        "timestamp": {
          "type": "string",
          "description": "UTC time for begin of drift"
        },
        "reference_frame": {
          "type": "string",
          "enum": [
            "AltAz",
            "topocentric",
            "ICRS",
            "Galactic"
          ]
        },
        "target_name": {
          "type": "string",
          "description": "The name of the target"
        },
        "c1": {
          "description": "first coordinate, in degrees",
          "type": "number",
          "minimum": 0.0,
          "maximum": 360.0
        },
        "c1_rate": {
          "description": "Drift rate for first coordinate, in degrees/second",
          "type": "number",
          "minimum": -0.016,
          "maximum": 0.016
        },
        "c2": {
          "description": "second coordinate, in degrees",
          "type": "number",
          "minimum": -90.0,
          "maximum": 90.0
        },
        "c2_rate": {
          "description": "Drift rate for second coordinate, in degrees/second",
          "type": "number",
          "minimum": -0.016,
          "maximum": 0.016
        }
      },
      "required": [
        "reference_frame",
        "c1",
        "c2"
      ]
    },
    "field": {
      "type": "object",
      "properties": {
        "timestamp": {
          "type": "string",
          "description": "UTC time for begin of drift"
        },
        "target_name": {
          "type": "string",
          "description": "The name of the target"
        },
        "reference_frame": {
          "type": "string",
          "enum": [
            "AltAz",
            "topocentric",
            "ICRS",
            "Galactic"
          ]
        },
        "attrs": {
          "description": "Coordinates and rates of scan",
          "type": "object",
          "properties": {
            "c1": {
              "description": "first coordinate, in degrees",
              "type": "number",
              "minimum": 0.0,
              "maximum": 360.0
            },
            "c1_rate": {
              "description": "Drift rate for first coordinate, in degrees/second",
              "type": "number",
              "minimum": -0.016,
              "maximum": 0.016
            },
            "c2": {
              "description": "second coordinate, in degrees",
              "type": "number",
              "minimum": -90.0,
              "maximum": 90.0
            },
            "c2_rate": {
              "description": "Drift rate for second coordinate, in degrees/second",
              "type": "number",
              "minimum": -0.016,
              "maximum": 0.016
            }
          },
          "required": [
            "c1",
            "c2"
          ]
        }
      },
      "required": [
        "reference_frame",
        "attrs",
        "target_name"
      ]
    }
  }
}