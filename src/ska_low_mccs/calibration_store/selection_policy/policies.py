#  -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements the policys available to the calibration."""
from __future__ import annotations

import importlib
import json
import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any, Dict, Final, Optional

from jsonschema import validate

__all__ = ["SelectPreferred", "SelectClosestInRange"]


@dataclass
class PolicyData:
    """Dataclass to contain data about the policy."""

    name: str
    query: str
    description: str
    tolerances: Optional[Dict[str, float]] = None  # Making tolerances optional

    def get_sql_policy_description(self) -> str:
        """
        Return a JSON string with a description of the policy.

        :return: A JSON string with the description.
        """
        # If no tolerances are provided, use an empty dictionary
        tolerances = self.tolerances if self.tolerances else {}

        selection_policy = {
            "policy_name": self.name,
            "query": self.query,
            "details": self.description,
            "tolerances": tolerances,
        }

        return json.dumps(selection_policy, indent=4)


class _SelectionPolicy(ABC):
    """A base selection policy."""

    # Validate against the method used to set policy in tango API.
    POLICY_VALIDATION: Final = json.loads(
        importlib.resources.read_text(
            "ska_low_mccs.schemas.calibration_store",
            "MccsCalibrationStore_UpdateSelectionPolicy.json",
        )
    )

    def __init__(
        self: _SelectionPolicy,
        logger: logging.Logger,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Initialise a SelectionPolicy.

        :param logger: the logger to use
        :param args: args
        :param kwargs: kwargs

        """
        self._policy_data: PolicyData
        self._logger = logger
        self.configure(*args, **kwargs)
        # Check that the policy is available
        self.__check_policy()

    @abstractmethod
    def configure(self: _SelectionPolicy, *args: Any, **kwargs: Any) -> None:
        """
        Configure the selectionPolicy.

        :param args: args
        :param kwargs: kwargs
        """
        # This is an abstract method and needs to be implemented in subclasses

    def __check_policy(self: _SelectionPolicy) -> None:
        if self._policy_data is None:
            raise NotImplementedError("No PolicyData implemented.")
        self._logger.info(
            f"Policy created: {self._policy_data.get_sql_policy_description()}"
        )

    @abstractmethod
    def generate_sql(
        self: _SelectionPolicy, *args: Any, **kwargs: Any
    ) -> tuple[str, tuple]:
        """
        Return an sql query.

        :param args: args
        :param kwargs: kwargs
        """
        # This is an abstract method and needs to be implemented in subclasses

    def get_configuration(self: _SelectionPolicy) -> str:
        """
        Get the configuration needed to set this policy.

        NOTE: validate against the method used in TANGO API
        to set policy.

        :return: the validated policy.
        """
        return self._get_policy(self.POLICY_VALIDATION)

    @abstractmethod
    def _get_policy(self: _SelectionPolicy, validator: dict[str, Any]) -> str:
        """
        Return the validated policy.

        :param validator: validator to check the policy
            may be used by TANGO API.
        """
        # This is an abstract method and needs to be implemented in subclasses

    def __str__(self: _SelectionPolicy) -> str:
        """
        Print information about the selection policy.

        :return: A JSON string with the description.
        """
        # This is an abstract method and needs to be implemented in subclasses
        return self._policy_data.get_sql_policy_description()


class SelectPreferred(_SelectionPolicy):
    """
    A SelectPreferred policy ``preferred``.

    The most recent solution within a defined frequency tolerance
    with the field preferred == TRUE is chosen.
    If none defined as preferred, no solution is returned.

    >>>    "SELECT frequency_channel, outside_temperature, "
    >>>    "calibration_path, creation_time, station_id, preferred, "
    >>>    "solution "
    >>>    "FROM tab_mccs_calib "
    >>>    f"WHERE station_id = {station_id} "
    >>>    f"AND frequency_channel >= {frequency_channel-self.frequency_tolerance} "
    >>>    f"AND frequency_channel <= {frequency_channel+self.frequency_tolerance} "
    >>>    "AND preferred = TRUE "
    >>>    "ORDER BY creation_time DESC"
    """

    def configure(  # pylint: disable=W0221
        self: SelectPreferred, frequency_tolerance: int = 0, **kwargs: Any
    ) -> None:
        """
        Configure the SelectPreferred policy.

        :param frequency_tolerance: the absolute frequency
            tolerance.
        :param kwargs: kwargs
        """
        self.frequency_tolerance = frequency_tolerance
        self._policy_data = PolicyData(
            name="preferred",
            query=(
                "SELECT frequency_channel, outside_temperature, "
                "calibration_path, creation_time, station_id, preferred, "
                "solution "
                "FROM tab_mccs_calib "
                "WHERE station_id = %s "
                "AND frequency_channel >= %s "
                "AND frequency_channel <= %s "
                "AND preferred = TRUE "
                "ORDER BY creation_time DESC"
            ),
            description=(
                "The query selects the most recent solution based on frequency "
                "tolerance and station ID."
                "It filters the solutions where the 'preferred' field is TRUE."
            ),
            tolerances={"frequency_tolerance [channel]": frequency_tolerance},
        )

    # pylint: disable=W0221
    def generate_sql(
        self: SelectPreferred, station_id: int, frequency_channel: int, **kwargs: Any
    ) -> tuple[str, tuple]:
        """
        Return an sql query with parameterized inputs.

        :param station_id: The id of the station we want a solution for.
        :param frequency_channel: The current frequency channel.
        :param kwargs: kwargs

        :returns: a sql query.
        """
        # Using parameterized query to safely insert values into the query
        parameters = (
            station_id,
            frequency_channel - self.frequency_tolerance,
            frequency_channel + self.frequency_tolerance,
        )

        return self._policy_data.query, parameters

    def _get_policy(self: SelectPreferred, validator: dict[str, Any]) -> str:
        """
        Return the validated policy for preferred policy.

        :param validator: validator to check the policy
            may be used by TANGO API.

        :return: the validated policy.
        """
        policy = {
            "policy_name": "preferred",
            "frequency_tolerance": self.frequency_tolerance,
        }
        validate(instance=policy, schema=validator)
        return json.dumps(policy)

    def __str__(self: SelectPreferred) -> str:
        """
        Print information about the selection policy.

        :returns: information about the policy.
        """
        return self._policy_data.get_sql_policy_description()


class SelectClosestInRange(_SelectionPolicy):
    """
    A SelectClosestInRange policy ``closest_in_range``.

    A solution is selected from a set of tolerances.
    The most recent solution within these tolerances is chosen.
    If more than one solutions tie we yield the one with the
    smallest absolute summed difference.


    >>>    "SELECT frequency_channel, outside_temperature, "
    >>>    "calibration_path, creation_time, station_id, preferred, "
    >>>    "solution "
    >>>    "FROM tab_mccs_calib "
    >>>    f"WHERE station_id = {station_id} "
    >>>    f"AND frequency_channel >= {frequency_channel-self.frequency_tolerance} "
    >>>    f"AND frequency_channel <= {frequency_channel+self.frequency_tolerance} "
    >>>    "AND outside_temperature >= "
    >>>    f"{outside_temperature-self.temperature_tolerance} "
    >>>    "AND outside_temperature <= "
    >>>    f"{outside_temperature+self.temperature_tolerance} "
    >>>    "ORDER BY preferred DESC, "
    >>>    "creation_time DESC, "
    >>>    f"ABS(ABS(frequency_channel-{frequency_channel}) "
    >>>    f"+ ABS(outside_temperature-{outside_temperature})) DESC"
    """

    def configure(  # pylint: disable=W0221
        self: SelectClosestInRange,
        temperature_tolerance: float = 0.0,
        frequency_tolerance: int = 0,
        **kwargs: Any,
    ) -> None:
        """
        Configure the SelectClosestInRange policy.

        :param temperature_tolerance: the absolute temperature
            tolerance.
        :param frequency_tolerance: the absolute frequency
            tolerance.
        :param kwargs: kwargs
        """
        self.temperature_tolerance = temperature_tolerance
        self.frequency_tolerance = frequency_tolerance

        self._policy_data = PolicyData(
            name="closest_in_range",
            query=(
                "SELECT frequency_channel, outside_temperature, "
                "calibration_path, creation_time, station_id, preferred, "
                "solution "
                "FROM tab_mccs_calib "
                "WHERE station_id = %s "
                "AND frequency_channel >= %s "
                "AND frequency_channel <= %s "
                "AND outside_temperature >= %s "
                "AND outside_temperature <= %s "
                "ORDER BY preferred DESC, "
                "creation_time DESC, "
                "ABS(ABS(frequency_channel - %s) + ABS(outside_temperature - %s)) DESC"
            ),
            description=(
                "This query selects the most recent solutions based on "
                "frequency and temperature tolerances."
                "It filters results based on station ID, "
                "frequency channel, and outside temperature."
                "It orders the results by preference and creation time, "
                "and in case of ties, by the smallest summed difference."
            ),
            tolerances={
                "frequency_tolerance [channel]": frequency_tolerance,
                "temperature_tolerance [Degrees]": temperature_tolerance,
            },
        )

    def _get_policy(self: SelectClosestInRange, validator: dict[str, Any]) -> str:
        """
        Return the validated policy for closest policy.

        :param validator: validator to check the policy
            may be used by TANGO API.

        :return: the validated policy.
        """
        policy = {
            "policy_name": "closest_in_range",
            "frequency_tolerance": self.frequency_tolerance,
            "temperature_tolerance": self.temperature_tolerance,
        }
        validate(instance=policy, schema=validator)
        return json.dumps(policy)

    # pylint: disable=W0221
    def generate_sql(
        self: SelectClosestInRange,
        outside_temperature: float,
        frequency_channel: int,
        station_id: int,
        **kwargs: Any,
    ) -> tuple[str, tuple]:
        """
        Return an sql query with parameterized inputs.

        :param outside_temperature: The current outside temperature.
        :param frequency_channel: The current frequency channel.
        :param station_id: The id of the station we want a solution for.
        :param kwargs: kwargs

        :returns: a sql query.
        """
        parameters = (
            station_id,
            frequency_channel - self.frequency_tolerance,
            frequency_channel + self.frequency_tolerance,
            outside_temperature - self.temperature_tolerance,
            outside_temperature + self.temperature_tolerance,
            frequency_channel,
            outside_temperature,
        )

        return self._policy_data.query, parameters
