#  -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements a selection manager."""
from __future__ import annotations

import logging
import os
from typing import Any, Final, Optional, Type

import numpy as np

from .policies import SelectClosestInRange, SelectPreferred

__all__ = ["SELECTION_POLICIES", "SelectionManager", "SelectionPolicyType"]


# Policy types accepted by the manager.
SelectionPolicyType = SelectPreferred | SelectClosestInRange
SELECTION_POLICIES: Final[dict[str, Type[SelectionPolicyType]]] = {
    "preferred": SelectPreferred,
    "closest_in_range": SelectClosestInRange,
}


class SelectionManager:
    """A SelectionManager."""

    def __init__(
        self: SelectionManager,
        logger: logging.Logger,
    ) -> None:
        """
        Initialise a new SelectionManager.

        :param logger: A logger for information purposes.
        """
        self._logger = logger
        self.__selection_policy: SelectionPolicyType | None = None

    @property
    def has_policy(self: SelectionManager) -> bool:
        """
        Return whether policy is defined.

        :returns: True if policy is not None.
        """
        return bool(self.__selection_policy)

    @property
    def policy(self: SelectionManager) -> str:
        """
        Return the policy.

        :returns: the selection policy.
        """
        return (
            self.__selection_policy.get_configuration()
            if self.__selection_policy is not None
            else ""
        )

    @property
    def policy_description(self: SelectionManager) -> str:
        """
        Return details about the policy description.

        :returns: information about the selection policy.
        """
        return str(self.__selection_policy) if self.has_policy else "No policy defined!"

    def update_policy(self: SelectionManager, policy: SelectionPolicyType) -> None:
        """
        Set a new policy.

        :param policy: the sql to use.
        """
        self.__selection_policy = policy

    def get_solution(
        self: SelectionManager, connection: Any, **kwargs: Any
    ) -> list[float]:
        """
        Get a solution using the loaded SelectionPolicy.

        This will return a solution from a query set by the
        loaded SelectionPolicy

        :param connection: the database connection.
        :param kwargs: kwargs to feed the selection policy

        :returns: the solution or an empty list.
        :raises ValueError: then there is no configured policy to use.
        """
        if self.__selection_policy is None:
            raise ValueError("Selection Policy not set.")

        query, parameters = self.__selection_policy.generate_sql(**kwargs)

        self._logger.debug(query)
        result = connection.execute(query, parameters)
        row: Optional[dict[str, Any]] = result.fetchone()  # type: ignore[assignment]
        if row is None:
            return []
        solution = row.get("solution", None)
        if solution is not None:
            return solution

        # This section is to allow some backward compatibility with previous
        # database schemas. Previous to 0.24.0, the calibration solution was
        # retreived from this path, now the solution is passed in. This field
        # is retained due to the importance of being able to locate the file used
        # to create the solution.
        path = row.get("calibration_path", None)
        if path is not None:
            self._logger.warning(
                f"{path=}"
                "This solution is deprecated and uses the old MCCS schema. "
                "The field `calibration_path` was previously used to read a solutions "
                "with .npy format, "
                "now we pass in the ``solution`` directly to the database. "
                "The .npy calibration product has been deprecated in favour of "
                "The new .h5 format. Please take action to convert this solution."
            )
            try:
                path_split = os.path.splitext(path)[0]
                solution_array = np.load(path_split + ".npy")
                return solution_array
            except OSError as os_error:
                self._logger.error(
                    f"Unable to load solution from artefact {repr(os_error)}"
                    f"Check path {path} exists."
                )
        self._logger.info("No solution found!")
        return []
