#  -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements the connection to the calibration database."""
from __future__ import annotations

import logging
from dataclasses import asdict, dataclass
from typing import Any, Callable

from psycopg import sql
from psycopg.rows import dict_row
from psycopg_pool import ConnectionPool, PoolClosed, PoolTimeout
from ska_control_model import CommunicationStatus, ResultCode

from .selection_policy.selection_manager import SelectionManager

DevVarLongStringArrayType = tuple[list[ResultCode], list[str]]

__all__ = ["CalibrationStoreDatabaseConnection", "DatabaseSolution"]


# pylint: disable=too-many-instance-attributes
@dataclass
class DatabaseSolution:
    """
    Class to hold a solution.

    This class holds a solution and offers some notion
    of checking types and mandatory keys before attempting to
    load into the tab_mccs_calib table.
    """

    acquisition_time: int  # NOT_NULL in database.
    frequency_channel: int  # NOT_NULL in database.
    station_id: int  # NOT_NULL in database.
    preferred: bool  # NOT_NULL in database.
    solution: list[float]  # NOT_NULL in database.
    calibration_path: str  # NOT_NULL in database.
    outside_temperature: float | None = None
    corrcoeff: list[float] | None = None
    residual_max: list[float] | None = None
    residual_std: list[float] | None = None
    xy_phase: list[float] | None = None
    n_masked_initial: int | None = None
    n_masked_final: int | None = None
    lst: float | None = None
    galactic_centre_elevation: float | None = None
    sun_elevation: float | None = None
    sun_adjustment_factor: float | None = None
    masked_antennas: list[int] | None = None

    def generate_loading_instruction(self) -> tuple[sql.Composed, tuple]:
        """
        Generate instruction needed to load into tab_mccs_calib.

        :return: Templated information to load into database.
            templating here will improve security against sql
            injection attacks.
        """
        # Sort the columns into 2 lists.
        # This is mainly done for ease of testsing.
        column_names, values = zip(*sorted(asdict(self).items()))

        # Generate placeholders and column identifiers dynamically
        placeholders = [sql.Placeholder()] * len(column_names)
        column_identifiers = [sql.Identifier(col) for col in column_names]

        query = sql.SQL(
            "INSERT INTO tab_mccs_calib "
            "(creation_time, {}) VALUES (current_timestamp, {})"
        ).format(
            sql.SQL(", ").join(column_identifiers),
            sql.SQL(", ").join(
                placeholders
            ),  # Use placeholders for parameterized queries, sql injection security.
        )
        return query, values


class CalibrationStoreDatabaseConnection:
    """A connection to a postgres database for the calibration store."""

    # pylint: disable=too-many-arguments, too-many-instance-attributes
    def __init__(
        self: CalibrationStoreDatabaseConnection,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        database_host: str,
        database_port: int,
        database_name: str,
        database_admin_user: str,
        database_admin_password: str,
        selection_manager: SelectionManager,
        timeout: float = 10,
        connection_max_tries: int = 5,
    ) -> None:
        """
        Initialise a new instance of a database connection.

        :param logger: a logger for this object to use
        :param communication_state_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param database_host: the database host
        :param database_port: the database port
        :param database_name: the database name
        :param database_admin_user: the database admin user
        :param database_admin_password: the database admin password
        :param selection_manager: The SelectionManager for device should use.
        :param timeout: the timeout for database operations
        :param connection_max_tries: the maximum number of attempts to connect to the
            database
        """
        self._selection_manager: SelectionManager = selection_manager
        self._logger = logger
        self._connect_kwargs = {"row_factory": dict_row}
        self._connection_pool = self._create_connection_pool(
            database_host,
            database_port,
            database_name,
            database_admin_user,
            database_admin_password,
        )
        self._communication_state_callback = communication_state_callback
        self._timeout = timeout
        self._connection_tries = 0
        self._connection_max_tries = connection_max_tries

    def _create_connection_pool(
        self: CalibrationStoreDatabaseConnection,
        database_host: str,
        database_port: int,
        database_name: str,
        database_admin_user: str,
        database_admin_password: str,
    ) -> ConnectionPool:
        """
        Create the connection pool for connecting to a postgres database.

        :param database_host: the database host
        :param database_port: the database port
        :param database_name: the database name
        :param database_admin_user: the database admin user
        :param database_admin_password: the database admin password

        :return: the connection pool
        """
        conninfo = (
            f"host={database_host} "
            f"port={database_port} "
            f"dbname={database_name} "
            f"user={database_admin_user} "
            f"password={database_admin_password}"
        )

        return ConnectionPool(conninfo, kwargs=self._connect_kwargs)

    def verify_database_connection(self: CalibrationStoreDatabaseConnection) -> None:
        """Verify that connection to the database can be established."""
        try:
            self._connection_pool.wait(self._timeout)
        except PoolTimeout:
            self._logger.error("Timed out forming database connection")
            self._communication_state_callback(CommunicationStatus.NOT_ESTABLISHED)
        self._logger.info("Database connection established successfully")
        self._communication_state_callback(CommunicationStatus.ESTABLISHED)

    def get_solution(
        self: CalibrationStoreDatabaseConnection,
        frequency_channel: int,
        outside_temperature: float,
        station_id: int,
    ) -> list[float]:
        """
        Get a solution for the provided frequency and outside temperature.

        This at present will return the most recently stored solution for the inputs.

        :param frequency_channel: the frequency channel of the desired solution.
        :param outside_temperature: the outside temperature of the desired solution.
        :param station_id: the id of the station to get a soluion for.

        :raises RuntimeError: if there are repeated connection issues with the database

        :return: a calibration solution from the database. Or a empty list if a solution
            could not be read from artefact.
        """
        try:
            with self._connection_pool.connection(self._timeout) as cx:
                self._connection_tries = 0
                return self._selection_manager.get_solution(
                    connection=cx,
                    outside_temperature=outside_temperature,
                    frequency_channel=frequency_channel,
                    station_id=station_id,
                )
        except PoolClosed as exc:
            self._logger.info("Pool closed already.")
            self._connection_tries += 1
            if self._connection_tries >= self._connection_max_tries:
                raise RuntimeError("Connection failed.") from exc
            self._connection_pool = ConnectionPool(
                self._connection_pool.conninfo, kwargs=self._connect_kwargs
            )
            return self.get_solution(frequency_channel, outside_temperature, station_id)

    def store_solution(
        self: CalibrationStoreDatabaseConnection,
        **kwargs: Any,
    ) -> DevVarLongStringArrayType:
        """
        Store the provided solution in the database.

        :param kwargs: fields to populate in database.

        :raises RuntimeError: if there are repeated connection issues with the database

        :return: tuple of result code and message.
        """
        prepare_solution = DatabaseSolution(**kwargs)
        query, placeholders = prepare_solution.generate_loading_instruction()
        try:
            with self._connection_pool.connection(self._timeout) as cx:
                self._connection_tries = 0
                cx.execute(query, placeholders)
        except PoolClosed as exc:
            self._logger.info("Pool closed already.")
            self._connection_tries += 1
            if self._connection_tries >= self._connection_max_tries:
                raise RuntimeError("Connection failed.") from exc
            self._connection_pool = ConnectionPool(
                self._connection_pool.conninfo, kwargs=self._connect_kwargs
            )
            return self.store_solution(
                **kwargs,
            )
        return ([ResultCode.OK], ["Solution stored successfully"])
