# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains classes for handling data products."""

from __future__ import annotations

import logging
import os
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, List

import h5py
import numpy as np

__all__ = ["CalibrationSolutionProduct", "CalibrationSolutionDataInterfaceV10"]


class CalibrationSolutionProduct:
    """
    This class is used to manage the data from the Solve command.

    Currently this supports:

    * structure_version = "1.0"

        * CalibrationSolutionDataInterfaceV10

    This class uses composition. The flag `structure_version` will  be
    used to construct a versioned interface to the data product.
    e.g:

    * 1.0 -> `CalibrationSolutionDataInterfaceV10`
    """

    def __init__(
        self,
        structure_version: str = "1.0",
        logger: logging.Logger | None = None,
        **kwargs: Any,
    ):
        """
        Initialise a new instance.

        :param logger:
            An optional logger.
        :param structure_version:
            Stores the version associated with the structure used to parse and save.
        :param kwargs: Any kwargs.
        """
        self._logger = logger

        # Compose the file interface class.
        self.__file_interface: __CalibrationSolutionDataInterface = (
            self.__create_file_interface(structure_version, **kwargs)
        )

    def __create_file_interface(
        self, structure_version: str, **kwargs: Any
    ) -> __CalibrationSolutionDataInterface:
        """
        Create the file interface class.

        :param structure_version: the structure we are using.
        :param kwargs: kwargs

        :return: the __CalibrationSolutionDataInterface.

        :raises ValueError: if object version not supported,
            or is contradicting.
        """
        match structure_version:
            case "1.0":
                interface = CalibrationSolutionDataInterfaceV10(
                    logger=self._logger, **kwargs
                )
                if interface.structure_version != structure_version:
                    raise ValueError("Version missmatch!")
                return interface
            case _:
                raise ValueError(f"version {structure_version} not supported.")

    @property
    def structure_version(self: CalibrationSolutionProduct) -> str:
        """
        Return structure version.

        :return: the structure version.
        """
        return self.__file_interface.structure_version

    def save_to_hdf5(
        self: CalibrationSolutionProduct, filename: str, attempt_if_exists: bool = False
    ) -> None:
        """
        Save the Solution to a hdf5 file.

        :param filename: the name of the file to write.
        :param attempt_if_exists: True if you want to attempt a write if
            the file exists.
            (If True you we will still obey the file permissions
            this is extra protection just in case.)
        """
        self.__file_interface.save_to_hdf5(filename, attempt_if_exists)

    def load_from_hdf5(self: CalibrationSolutionProduct, filename: str) -> None:
        """
        Load a solution from hdf5 file.

        :param filename: the name of the file to read.
        """
        self.__file_interface.load_from_hdf5(filename)

    def __getattr__(self: CalibrationSolutionProduct, name: str) -> Any:
        """
        Get attribute from the file object.

        :param name: the name of the attribute.

        :return: the attribute

        :raises AttributeError: when the attribute does not exist.
        """
        try:
            return getattr(self.__file_interface, name)
        except AttributeError as ae:
            raise AttributeError(
                f"'{type(self).__name__}' object has no attribute '{name}'"
            ) from ae


class __CalibrationSolutionDataInterface(ABC):
    """A base class representing the calibration product."""

    def __init__(
        self: __CalibrationSolutionDataInterface,
        structure_version: str,
        logger: logging.Logger | None = None,
    ):
        """
        Initialise a new calibration product.

        :param structure_version: the version.
        :param logger: the optional logger to use
        """
        self._logger = logger
        self.__structure_version = structure_version

    def save_to_hdf5(
        self: __CalibrationSolutionDataInterface,
        filename: str,
        attempt_if_exists: bool = False,
    ) -> None:
        """
        Save the Solution to a hdf5 file.

        :param filename: the name of the file to write.
        :param attempt_if_exists: True if you want to attempt a write if
            the file exists.
            (If True you we will still obey the file permissions
            this is extra protection just in case.)

        :raises PermissionError: If the file exists and attempt_if_exists
            is False.
        """
        if not attempt_if_exists:
            my_file = Path(filename)
            if my_file.is_file():
                raise PermissionError(
                    f"File {filename}, already exists. "
                    "If you trust the file permissions on the "
                    "existing file you may attempt_if_exists. "
                    "This will attempt to write but will still "
                    "obey the ubuntu file permissions."
                )

        with h5py.File(filename, "w") as f:
            # Store structure version as an attribute of the root group
            f.attrs["structure_version"] = self.__structure_version

            # Call the version-dependent method to save data
            self._save_data(f)

        # Make the file read-only after writing to it
        os.chmod(filename, 0o444)  # Read-only for all users

        if self._logger is not None:
            self._logger.info(f"File saved: {filename}")

    def load_from_hdf5(self: __CalibrationSolutionDataInterface, filename: str) -> None:
        """
        Load a solution from hdf5 file.

        :param filename: the name of the file to read.

        :raises ValueError: when attempting to load the wrong structure.
        """
        with h5py.File(filename, "r") as f:
            file_version = f.attrs.get("structure_version", None)
            if file_version != self.__structure_version:
                raise ValueError(
                    "Structure version mismatch! "
                    f"Expected {self.__structure_version}, but got {file_version}."
                )

            # Call the version-dependent method to load data
            self._load_data(f)

    @property
    def structure_version(self: __CalibrationSolutionDataInterface) -> str:
        """Return structure version.

        :return: the structure version.
        """
        return self.__structure_version

    @abstractmethod
    def _save_data(self: __CalibrationSolutionDataInterface, f: h5py.File) -> None:
        """
        Save the current object to an HDF5 file.

        :param f: The open `h5py.File`.
        """
        raise NotImplementedError("Subclasses should implement this method.")

    @abstractmethod
    def _load_data(self: __CalibrationSolutionDataInterface, f: h5py.File) -> None:
        """
        Load data from an HDF5 file into the current object.

        :param f: the open `h5py.File`.
        """
        raise NotImplementedError("Subclasses should implement this method.")


# pylint: disable=too-many-instance-attributes
class CalibrationSolutionDataInterfaceV10(__CalibrationSolutionDataInterface):
    """
    Calibration Solution structure 1.0.

    * root

      * acquisition_time
      * corrcoeff
      * frequency_channel
      * galactic_centre_elevation
      * lst
      * masked_antennas
      * n_masked_final
      * n_masked_initial
      * residual_max
      * residual_std
      * solution
      * station_id
      * sun_adjustment_factor
      * sun_elevation
      * xy_phase
    """

    _STRUCTURE_VERSION = "1.0"

    # pylint: disable=too-many-arguments, too-many-locals
    def __init__(
        self: CalibrationSolutionDataInterfaceV10,
        logger: logging.Logger | None = None,
        corrcoeff: List[float] | None = None,
        residual_max: List[float] | None = None,
        residual_std: List[float] | None = None,
        xy_phase: float | None = None,
        n_masked_initial: int | None = None,
        n_masked_final: int | None = None,
        lst: float | None = None,
        galactic_centre_elevation: float | None = None,
        sun_elevation: float | None = None,
        sun_adjustment_factor: float | None = None,
        masked_antennas: np.ndarray | None = None,
        solution: np.ndarray | None = None,
        frequency_channel: int | None = None,
        station_id: int | None = None,
        acquisition_time: float | None = None,
    ) -> None:
        """
        Initialise a new calibration product.

        :param logger:
            An optional logger.
        :param corrcoeff:
            Stores the correlation coefficients as a list of floats (`List[float]`).
            List of Pearson product-moment correlation coefficients for each
            polarization of the calibrated visibilities and the model.
        :param residual_max:
            Stores the maximum residuals as a list of floats (`List[float]`).
            List of residual visibility maximum absolute deviation.
        :param residual_std:
            Stores the standard deviation of the residuals as a list of floats
            (`List[float]`).
            List of residual visibility standard deviation per polarization (K).
        :param xy_phase:
            Stores the XY phase information as a float (`float`).
            Estimated XY-phase error (degrees).
        :param n_masked_initial:
            Stores the initial number of masked data points as an integer (`int`).
            Initial number of masked antennas.
        :param n_masked_final:
            Stores the final number of masked data points as an integer (`int`).
            Final number of masked antennas.
        :param lst:
            Stores the Local Sidereal Time (LST) as a floating-point number (`float`).
            Apparent sidereal time at the station (degrees).
        :param galactic_centre_elevation:
            Stores the elevation of the galactic center in degrees as a floating-point
            number (`float`).
            Elevation of the galactic center (degrees).
        :param sun_elevation:
            Stores the elevation angle of the Sun as a floating-point number (`float`).
            Elevation of the sun (degrees).
        :param sun_adjustment_factor:
            Stores the sun adjustment factor as a floating-point number (`float`).
            Solar adjustment factor (if *adjust_solar_model=True*, "1" otherwise).
        :param masked_antennas:
            Stores a list of integers (`np.ndarray[np.int_]`) representing the IDs of
            antennas that were masked (excluded) from the solution.
            List of antennas that have been flagged as bad and should be excluded.
            This list is indexed by eep, and has length up to nof_antenna.
        :param solution:
            Stores the calibration solution as a list of floats (`List[float]`),
            typically with a length of 2048 or another predefined size.
            Each unit is a flattened complex matrix of length 8.
            The solution stored is ordered by eep_idx (1 based)
        :param frequency_channel:
            Stores the frequency channel used for the calibration as an integer (`int`).
            This represents the specific frequency channel this solution is
            calculated for.
        :param station_id:
            Stores the station ID as an integer (`int`).
            This represents the unique identifier for the station performing the
            calibration.
        :param acquisition_time:
            Stores the acquisition time as a float (`float`) or string.
            This represents the timestamp at which the data was acquired.
        """
        super().__init__(self._STRUCTURE_VERSION, logger)

        ############################################################
        # DO NOT MODIFY THIS STRUCTURE.
        # IT IS NEEDED FOR BACKWARD COMPATIBILITY.
        # INSTEAD CREATE A NEW VERSION.
        # ----------------------------------------------------------
        self.corrcoeff = corrcoeff
        self.residual_max = residual_max
        self.residual_std = residual_std
        self.xy_phase = xy_phase
        self.n_masked_initial = n_masked_initial
        self.n_masked_final = n_masked_final
        self.lst = lst
        self.galactic_centre_elevation = galactic_centre_elevation
        self.sun_elevation = sun_elevation
        self.sun_adjustment_factor = sun_adjustment_factor
        self.masked_antennas = masked_antennas
        self.solution = solution
        self.frequency_channel = frequency_channel
        self.station_id = station_id
        self.acquisition_time = acquisition_time
        ############################################################

    # pylint: disable=too-many-branches
    def _save_data(self, f: h5py.File) -> None:
        """
        Save data for version 1.0.

        :param f: The open `h5py.File`.
        """
        # Only save attributes that are not None
        if self.corrcoeff is not None:
            f.create_dataset("corrcoeff", data=self.corrcoeff)
        if self.residual_max is not None:
            f.create_dataset("residual_max", data=self.residual_max)
        if self.residual_std is not None:
            f.create_dataset("residual_std", data=self.residual_std)
        if self.xy_phase is not None:
            f.create_dataset("xy_phase", data=self.xy_phase)
        if self.n_masked_initial is not None:
            f.create_dataset("n_masked_initial", data=self.n_masked_initial)
        if self.n_masked_final is not None:
            f.create_dataset("n_masked_final", data=self.n_masked_final)
        if self.lst is not None:
            f.create_dataset("lst", data=self.lst)
        if self.galactic_centre_elevation is not None:
            f.create_dataset(
                "galactic_centre_elevation", data=self.galactic_centre_elevation
            )
        if self.sun_elevation is not None:
            f.create_dataset("sun_elevation", data=self.sun_elevation)
        if self.sun_adjustment_factor is not None:
            f.create_dataset("sun_adjustment_factor", data=self.sun_adjustment_factor)
        if self.masked_antennas is not None:
            f.create_dataset("masked_antennas", data=self.masked_antennas)
        if self.solution is not None:
            f.create_dataset("solution", data=self.solution)
        if self.frequency_channel is not None:
            f.create_dataset("frequency_channel", data=self.frequency_channel)
        if self.station_id is not None:
            f.create_dataset("station_id", data=self.station_id)
        if self.acquisition_time is not None:
            f.create_dataset("acquisition_time", data=self.acquisition_time)

    def _load_data(self, f: h5py.File) -> None:
        """
        Load data from version 1.0.

        :param f: The open `h5py.File`.
        """
        # Load attributes if they exist in the file; if not, set them to None
        self.corrcoeff = list(f["corrcoeff"]) if "corrcoeff" in f else None
        self.residual_max = list(f["residual_max"]) if "residual_max" in f else None
        self.residual_std = list(f["residual_std"]) if "residual_std" in f else None
        self.xy_phase = f["xy_phase"][()] if "xy_phase" in f else None
        self.n_masked_initial = (
            f["n_masked_initial"][()] if "n_masked_initial" in f else None
        )
        self.n_masked_final = f["n_masked_final"][()] if "n_masked_final" in f else None
        self.lst = f["lst"][()] if "lst" in f else None
        self.galactic_centre_elevation = (
            f["galactic_centre_elevation"][()]
            if "galactic_centre_elevation" in f
            else None
        )
        self.sun_elevation = f["sun_elevation"][()] if "sun_elevation" in f else None
        self.sun_adjustment_factor = (
            f["sun_adjustment_factor"][()] if "sun_adjustment_factor" in f else None
        )
        self.masked_antennas = (
            np.array(f["masked_antennas"]) if "masked_antennas" in f else None
        )
        self.solution = np.array(f["solution"]) if "solution" in f else None
        self.frequency_channel = (
            f["frequency_channel"][()] if "frequency_channel" in f else None
        )
        self.station_id = f["station_id"][()] if "station_id" in f else None
        self.acquisition_time = (
            f["acquisition_time"][()] if "acquisition_time" in f else None
        )
