#!/usr/bin/env python3
# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains helpful functions for PI21 demo."""

import json
import time
from typing import Any

import numpy as np
import pandas as pd
import tango
from astropy.coordinates import AltAz, Angle, EarthLocation, SkyCoord, get_moon, get_sun
from astropy.time import Time, TimeDelta
from ska_control_model import ObsState


def display_adc(tiles: tango.DeviceProxy) -> pd.DataFrame:
    """
    Return a styled table of AdcPowers.

    :param tiles: the tiles to display the adcPowers

    :return: DataFrame with adcPower for each tile.
    """

    def _make_adc_pretty(
        styler: Any,
    ) -> Any:
        styler.background_gradient(axis=None, vmin=0, vmax=24, cmap="YlGnBu")
        return styler

    pd.set_option("display.max_columns", 33)
    data_table = pd.DataFrame(
        [tile.adcPower for tile in tiles],
        index=[f"Tile{i}" for i in range(1, 17)],
        columns=[f"adc {i}" for i in range(1, 33)],
    )
    return data_table.style.pipe(_make_adc_pretty)


def display_antenna_powers(fieldstation: tango.DeviceProxy) -> pd.DataFrame:
    """
    Return a styled table of antennaPowers.

    :param fieldstation: the fieldstation to display antenna
        powers for.

    :return: DataFrame with [antenna_power, masking_state] for
        each logical antenna
    """
    antennamask = json.loads(fieldstation.antennamask)
    antennamasks = []
    for antenna_id, antenna in enumerate(antennamask["antennaMask"]):
        antennamasks.append(antenna["maskingState"])
    antennapowerstates = json.loads(fieldstation.antennapowerstates)
    antenna_powers = []
    for i, v in enumerate(antennapowerstates.values()):
        antenna_powers.append(v)
    field_station_antenna_data = np.zeros((256, 2))
    field_station_antenna_data[:, 0] = antenna_powers
    field_station_antenna_data[:, 1] = antennamasks
    data_table = pd.DataFrame(
        field_station_antenna_data,
        index=[f"Antenna{i}" for i in range(1, 257)],
        columns=["PowerState", "Masked"],
    )
    pd.set_option("display.max_rows", data_table.shape[0] + 1)

    def _make_pretty(
        styler: Any,
    ) -> Any:
        styler.background_gradient(axis=None, vmin=0, vmax=5, cmap="YlGnBu")
        return styler

    return data_table.style.pipe(_make_pretty)


def display_tile_attributes(
    tiles: list[tango.DeviceProxy], *attributes: str
) -> pd.DataFrame:
    """
    Return a styled table with select tiles attributes.

    :param tiles: the tiles to display attributes for
    :param attributes: the attributes to display

    :returns: DataFrame with the selected attributes displayed for each tile.
    """

    def _tangoattr(dev: tango.DeviceProxy, k: Any) -> Any:
        try:
            v = dev[k].value
            return str(v) if isinstance(v, tango.DevState) else v
        except Exception as e:
            return repr(e)

    return pd.DataFrame(
        {attribute: _tangoattr(tile, attribute) for attribute in attributes}
        for tile in tiles
    )


def wait_for_obsstate(
    device: tango.DeviceProxy, desired_obsstate: ObsState, timeout: int = 40
) -> None:
    """
    Wait for the Obstate of a device.

    :param device: the `tango.DeviceProxy` under interest.
    :param desired_obsstate: The desired ObsState.
    :param timeout: the max time to wait

    :raises TimeoutError: is timeout period exceeded before state change.
    """
    current_time = time.time()
    while time.time() < current_time + timeout:
        try:
            assert device.obsState == desired_obsstate
            break
        except AssertionError:
            time.sleep(1)
    if device.obsState != desired_obsstate:
        raise TimeoutError(f"ObsState failed to change in {timeout} seconds")
    print(
        f"Obstate for {device.dev_name()} transitioned to "
        f"{ObsState(desired_obsstate).name}"
    )


def wait_for_lrcs_to_finish(
    devices: list[tango.DeviceProxy], timeout: int = 40
) -> None:
    """
    Wait for the LongrunningCommands to finish on devices.

    :param devices: The devices we want to check
    :param timeout: the max time to wait

    :raises TimeoutError: is timeout period exceeded before state change.
    """
    count = 0
    for device in devices:
        count = 0
        while device.longRunningCommandsInQueue != ():
            time.sleep(1)
            count += 1
            if count == timeout:
                raise TimeoutError(
                    f"LRCs still running after {timeout} seconds: "
                    f"{device.dev_name()} : {device.longRunningCommandsInQueue}"
                )


def configure_daq_for_station_beam_monitoring(daq: tango.DeviceProxy) -> None:
    """
    Configure DAQ for beam monitoring.

    :param daq: A proxy to the DAQ device.
    """
    daq_status = json.loads(daq.DaqStatus())

    if daq_status["Running Consumers"] != []:
        daq.stop()
    if daq_status["Bandpass Monitor"]:
        # Stop bandpass monitor incase pre existing one is not cofigured as desired.
        daq.StopBandpassMonitor()

    # Configure daq to collect STATION_BEAM_DATA from 16 tiles.
    daq.Configure(json.dumps({"nof_tiles": 16, "append_integrated": True}))

    daq.Start('{"modes_to_start": "STATION_BEAM_DATA"}')


def point_subarray(
    devices_with_lrc_to_watch: list[tango.DeviceProxy],
    subarray: tango.DeviceProxy,
    station_beams: list[tango.DeviceProxy],
    frame: str,
    c1: Any,
    c2: Any,
    scan_id: Any,
) -> None:
    """
    Point the subarray to the given coordinate and execute a short scan.

    :param devices_with_lrc_to_watch: devices to watch LRC on.
    :param subarray: The 'tango.DeviceProxy' for a subarray device.
    :param frame: reference frame
    :param c1: coordinate 1, in degrees
    :param c2: coordinate 2, in degrees
    :param scan_id: the logical id given to this scan
    """
    # If the subarray is configured, deconfigure it and wait for completion
    #
    if subarray.obsState == ObsState.READY:
        print("subarray already configured, deconfiguring....")
        subarray.End()
        wait_for_lrcs_to_finish(devices_with_lrc_to_watch)
        wait_for_obsstate(subarray, ObsState.IDLE)

    # configure the subarray
    # band 6.25 MHz centered at 100 MHz
    # pointing as specified
    #
    configure_args = {
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "update_rate": 10.0,
                "logical_bands": [
                    {
                        "start_channel": 124,  # 96.48 to 102.73 MHz
                        "number_of_channels": 8,
                    }
                ],
                "apertures": [
                    {
                        "aperture_id": "AP001.01",
                    }
                ],
                "field": {
                    "reference_frame": frame,
                    "timestamp": (Time.now() + TimeDelta(0.1, format="sec")).isot + "Z",
                    "attrs": {"c1": c1, "c1_rate": 0.0, "c2": c2, "c2_rate": 0.0},
                },
            }
        ]
    }
    print("Configure subarray for scan.....")
    subarray.Configure(json.dumps(configure_args))
    wait_for_lrcs_to_finish(devices_with_lrc_to_watch)
    wait_for_obsstate(subarray, ObsState.READY)
    # check that pointing is OK
    #
    print(f"Pointing scan {scan_id} at {station_beams[0].desiredpointing}")
    #
    # Perform a short scan. Starts 1s from now, lasts ~1s
    #
    print("starting scan.....")
    subarray.Scan(
        json.dumps(
            {
                "start_time": (Time.now() + TimeDelta(3.0, format="sec")).isot + "Z",
                "scan_id": scan_id,
                "duration": 86400.0,
            }
        )
    )
    wait_for_lrcs_to_finish(devices_with_lrc_to_watch)
    wait_for_obsstate(subarray, ObsState.SCANNING)

    print("Ending scan.....")
    subarray.EndScan()
    wait_for_lrcs_to_finish(devices_with_lrc_to_watch)
    wait_for_obsstate(subarray, ObsState.READY)


def route_traffic_to_daq(spsstation: tango.DeviceProxy, daq: tango.DeviceProxy) -> None:
    """
    Set the LMC download and CSP Ingest to the DAQ device.

    :param spsstation: the station we want to route.
    :param daq: the daq we want to route.
    """
    spsstation.StopDataTransmission()

    daq_status = json.loads(daq.daqstatus())

    routing_information = {
        "mode": "10G",
        "destination_ip": daq_status["Receiver IP"][0],
        "destination_port": daq_status["Receiver Ports"][0],
        "channel_payload_length": 8192,
    }
    spsstation.Setlmcdownload(json.dumps(routing_information))
    spsstation.SetCspIngest(json.dumps(routing_information))


def configure_daq_for_bandpass_monitoring(
    daq: tango.DeviceProxy, plot_directory: str
) -> None:
    """Configure DAQ for bandpass monitoring."""
    assert isinstance(plot_directory, str), "plot directory need to be a string"
    daq_status = json.loads(daq.DaqStatus())

    if daq_status["Running Consumers"] != []:
        daq.stop()
    if daq_status["Bandpass Monitor"]:
        # Stop bandpass monitor incase pre existing one is not cofigured as desired.
        daq.StopBandpassMonitor()

    # Configure daq to collect from 16 tiles.
    daq.Configure(json.dumps({"nof_tiles": 16, "append_integrated": False}))
    daq_status = json.loads(daq.daqstatus())

    # start DAQ
    daq.Start('{"modes_to_start": "INTEGRATED_CHANNEL_DATA"}')

    for i in range(6):
        if daq_status["Running Consumers"] == []:
            time.sleep(1)

    daq.StartBandpassMonitor(json.dumps({"plot_directory": plot_directory}))


def print_allocated_resources(
    controller: tango.DeviceProxy, subarray: tango.DeviceProxy
) -> None:
    """Print the allocated resources."""
    print(
        f"Controller resources allocated to subarray: {controller.GetAssignedResources(1)}"
    )
    print(f"Subarray resources\n- Stations:       {subarray.stationTRLs}")
    print(f"- Subarray beams: {subarray.subarraybeamtrls}")
    print(f"- Station beams:  {subarray.stationbeamtrls}")
    print(f"- All assigned resources: {json.loads((subarray.assignedresources))}")


def move_obsstate_to_empty(
    controller: tango.DeviceProxy, subarray: tango.DeviceProxy
) -> None:
    """Move the Controller to EMPTY."""
    match subarray.ObsState:
        case ObsState.SCANNING:
            subarray.EndScan()
            wait_for_obsstate(subarray, ObsState.READY)
            subarray.End()
            wait_for_obsstate(subarray, ObsState.IDLE)
            controller.ReleaseAll()
            wait_for_obsstate(subarray, ObsState.EMPTY)
        case ObsState.READY:
            subarray.End()
            wait_for_obsstate(subarray, ObsState.IDLE)
            controller.ReleaseAll()
            wait_for_obsstate(subarray, ObsState.EMPTY)
        case ObsState.IDLE:
            controller.ReleaseAll()
            wait_for_obsstate(subarray, ObsState.EMPTY)
        case _:
            pass
