##########################################
 MCCS Allocation and Configuration Control
##########################################

*******
Purpose
*******

MCCS must configure its components (in the end, the SPS physical devices)
to perform a specific observation. Observation is specified in terms of
independent subarrays. Each subarray is a logically virtual
instument, which can perform observations independently of other subarrays.

The telescope manager (TM) has a fixed number of available subarrays. It
assign scans to subarrays, and ask the MccsController to allocate the
necesary resources, and to assign these resources to the relevant MCCS devices.

The TM then configures the MccsSubarray (together with other subarray
elements in other subsystems) for a specific observation. The MccsSubarray
is responsible for orchestrating the configuration process, down to the
hardware devices.

The actual observation is started using a Scan command, once the subarray
has been correctly configured. Repeated Scan commands can be issued using
the same configuration, or the subarray can be reconfigured between scans
using the same allocated resources. Scans may be composed to execute more
complex observations. Examples of single scans are:

    * A single point in a mosaic,
    * Multiple points using different simultaneous beams
    * A single line of a raster scan

The coordinate system can be altaz, sidereal, or centered on a
particular solar system body (including body ephemeris). Definition of
the usable coordinate systems is in progress. A tentative, generic format
is adopted here, but it is likely to change in the future.

Allocate command
================

An observation is defined by a set of subarray beams. Resources are also
allocated to subarray beams. Beams are conceptually independent, with the
main difference between subarrays and subarray beams being that a subarray
is synchronous: subarray beams in the same subarray are allocated, deallocated,
configured and started at the same time.

Subarray beams are assigned by the TM, as they must correspond among
different subsystems. Each subarray beam is allocated

  * a number of SPS channels (a total bandwidth)

  * a set of apertures, defined by a station and a substation ID. Station
    is statically assigned, the substation identifies the possibility of
    defining a different station footprint, but does not assign actual weights.
    Apertures are identified by a string of the form "APx.y" with x the
    station ID and y a unique substation ID.

The MccsController manages all resources in the MCCS
system, especially hardware resources in the Tile Processing Modules. As
these resources are identical for all TPMs in a station, these are managed
inside a StationProxy device inside the controller.
For each aperture the MccsController allocates:

  * a StationBeam device from a common pool

  * from the relevant StationProxy (note: this does not involve the actual
    MccsStation):

    * a HardwareBeam

    * the appropriate number of channel blocks. Each channel block corresponds
      to 8 SPS channels. Blocks are rounded up as needed.

If multiple apertures are using a station, a separate set of resources
(hardware beam channel blocks) is allocated. When all resources are correctly
allocated, MccsController issues a set of AllocateResources commands to the
MccsSubarray device, and to the SubarrayBeam and StationBeam devices. The
MccsStation is not involved in the allocation process.

The sequence diagram for the Allocate command is shown below.

.. uml:: Allocate.uml

Configure command
=================

Configuration is managed by a MccsSubarray device. Configuration is defined
in terms of subarray beams, too. Each subarray beam configuration includes:

  * a set of logical bands. Each logical band is defined by a start channel
    and a number of channels. Total number of channels must fit in the
    allocated channels (channel blocks) for the subarray beam.

  * a set of apertures. The Subarray device maintains a dictionary to map
    aperture ID to station IDs. For each aperture a weighting key reference
    string is used to select a station weighting function and/or a subaperture
    from a database. By default, all antennas in the station are uniformly
    weighted

  * pointing update rate. The time interval after which the pointing
    algorithm will recalculate delays and delay rates

  * pointing direction. Format for pointing specification is TBD, this
    version includes a reference frame ID (a sting in a limited set of possibile
    coordinate systems), two coordinates, and optionally two rates and
    a reference time (start time for applying the rate).

Configuration sequence is shown here:

.. uml:: Configure.uml

Configuration is performed in 4 steps:

    * All Stations serving the Subarray are deconfigured. Information related
      to this subarray is cleared, so the MccsStation can merge together the
      information from different StationBeams

    * For each aperture, the configuration for the associated StationBeam is
      assembled. The StationBeam is identified by the ApertureId, using a
      dictionary.

    * The configuration is transmitted to the StationBeams, which translate
      it to commands for the Station. Station merges this information.

    * the Stations are commanded to apply the assembled configuration

Currently the Subarray directly forwards the configuration information to
te StationBeam devices. In the final structure, the configuration will be
transmitted to the SubarrayBeams, which will dispatch it to the StationBeams.

StationBeam unpacks the configuration structure to:

  * build a beamformer table. It computes the starting channel for each
    required channel block, and the total number of channels used by the
    configuration. The table is then forwarded to the MccsAntenna

  * manage the aperture weights. Weights are retrieved from a database which
    holds standard weighting functions for each station. Substations are
    implemented as specific weighting sets.

  * save the pointing specification in a structure used by the pointing
    task. This task wil translate the pointing to delays and delay rates for
    each individual antenna.

The MccsStation assembles the beamformer table entries from each StationBeam
into a single beamformer configuration table. StationBeams for a station may
belong to different subarrays. When the subarray beams have all completed their configuration, the MccsStation will forward the table to the SpsStation,
which will program the hardware TPMs.
