###################################
 MCCS architecture class diagrams
###################################

Subarray Structure
==================

.. uml:: subarray_class_structure.uml

Allocate command activity diagram
=================================

.. uml:: Allocate.uml

Configure command activity diagram
==================================

.. uml:: Configure.uml
