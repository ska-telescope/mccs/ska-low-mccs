#############
Data Products
#############

MCCS stores calibration solutions in the ``ArtefactRepository`` with the ``.h5`` extension and deprecated  ``.npy``.
The ``.h5`` contains all information the ``.npy`` does plus metadata.

..  tip::
    There is a class ``ska_low_mccs.calibration_solver.CalibrationSolutionProduct`` 
    to help handle the ``.h5`` files. 

+++++++++++++++++
.npy data product
+++++++++++++++++
This contains just the solution with no metadata. All information saved here is also saved in 
the `.h5` product.

.. note::
    This is deprecated in favour of the .h5 product.


++++++++++++++++
.h5 data product
++++++++++++++++
This contains the solution together with metadata. 

Each product has as ``HDF5 Attribute`` called ``structure_version`` used to 
define that dataset.


Version 1.0
###########

* root

  * acquisition_time
  * corrcoeff
  * frequency_channel
  * galactic_centre_elevation
  * lst
  * masked_antennas
  * n_masked_final
  * n_masked_initial
  * residual_max
  * residual_std
  * solution
  * station_id
  * sun_adjustment_factor
  * sun_elevation
  * xy_phase


+++++++++++++++++
Database solution
+++++++++++++++++

Each solution consists of 2048 values (4 flattened complex numbers per antenna), this 
is stored in the calibration database together with metadata.

The PostgreSQL database schema is a single table,
created using the following script:


`create_tables.sql <../../../create_tables.sql>`_
