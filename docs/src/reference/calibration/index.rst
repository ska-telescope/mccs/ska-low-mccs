MCCS Calibration
================

This document focuses on how the Tango devices in the MCCS subsystem perform calibration functions.

To manage the scope and avoid duplication, we have identified three primary interfaces involved in the calibration process. Please refer to the following links for their documentation:

1. **TMC (Telescope Monitoring and Control)**  

   `TMC Documentation <https://sah-1413.readthedocs.io/en/latest/>`_.

   The TMC interfaces with the **MccsController** and **MccsSubarray** devices.

2. **ska-low-mccs-calibration**  

   `ska-low-mccs-calibration Documentation <https://developer.skao.int/projects/ska-low-mccs-calibration>`_. 

   The **MccsSolver** device interfaces with this library to compute calibration solutions.

3. **TPM Interface (aavs-system)**  

   `aavs-system Documentation <https://developer.skao.int/projects/aavs-system>`_.

   The **MccsTile** device interfaces with this library to control the TPM boards.



.. toctree::
   :maxdepth: 1
   :caption: Architecture

   architecture/index.rst 



.. toctree::
   :maxdepth: 1
   :caption: Data Products

   products/index.rst


.. toctree::
   :maxdepth: 1
   :caption: Operations

   operation/calibration_internals.rst


.. toctree::
   :maxdepth: 1
   :caption: Reference material

   reference_material/index.rst

