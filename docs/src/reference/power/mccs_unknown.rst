MCCS in the UNKNOWN State
=========================

The diagram below indicates an MCCS which is UNKNOWN.

Since one **MccsTile** is OFF while others are ON, all **SpsStation**, 
**MccsStation**, and **MccsController** are in the UNKNOWN state.

.. uml:: mccs_unknown.puml
