Power Management in MCCS Subsystem
==================================

Hardware Power Modes
--------------------

SKA hardware may support up to three power modes:

- **ON**: The hardware is powered on and fully operational. This mode is
  supported by all SKA hardware.

- **OFF**: The hardware is powered off.

- **STANDBY**: The hardware is in a low-power standby mode. This mode is
  important in two scenarios:

  - When powering up a subsystem with many devices, limiting the inrush
    current is crucial. This is achieved by powering devices into standby
    mode, using no more than 5% of their nominal power, and then carefully
    orchestrating transitions to full power.

  - When powering up a device from off could take a long time (e.g.,
    several minutes), the device may instead be powered up into standby
    mode. In standby mode, power consumption is low, but the time to
    fully power on the hardware is short (a couple of seconds).

  Standby mode is not supported by all hardware; there may be very few
  devices that support it.

Power Mode Breakdown
--------------------

Generally, a hardware device cannot turn itself off because once it is off,
it loses the ability to turn itself back on. Instead, power to a device is
controlled by an upstream device. For example, power to a TPM is controlled
by the subrack in which that TPM is installed. Standby mode is generally
managed by an ensemble of devices (e.g., SpsStation, FieldStation) by
selectively turning off the most power-consuming devices. Thus, the
implementation of the three power modes breaks down into:

- **On()**: Tell the upstream device to supply power to the device, then tell
  the device itself to go fully operational.

- **Off()**: Tell the upstream device (e.g., subrack) to deny power to the
  device (e.g., TPM).

- **Standby()**: Selectively turn ON some sub-devices and turn OFF or leave
  OFF the TPMs (in the SPS Station) and the antennas and Front End modules
  (in the Field Station).



Power Commands
--------------

.. toctree::
   :maxdepth: 1
   :caption: Power commands

   on_command.rst
   off_command.rst
   standby_command.rst

Power Rollup
------------

.. toctree::
   :maxdepth: 1
   :caption: Power rollup

   power_rollup.rst
   mccs_on.rst
   mccs_standby.rst
   mccs_off.rst
   mccs_unknown.rst
