MCCS Power States
=================

Overview
--------

This document details the power states within the MCCS subsystem, focusing on
how these states are determined based on the power state of subservient devices
or the hardware directly controlled by a device. MCCS devices have 5 states:

1. **On**: The device or hardware is fully powered and operational.

2. **Off**: The device or hardware is powered down. This state is not always 
   implemented if power-off control is unavailable.

3. **Standby**: The device or hardware is in a low-power state, with some 
   components powered off and others remaining on.

4. **Unknown**: The device is in an error state because it cannot accurately 
   determine the power state of its hardware or its subservient devices.

5. **Fault**: The device is in an error state where the expected power state 
   conflicts with the actual power state of its hardware or subservient devices.

Power State Logic by Repository
-------------------------------

Main Repository
^^^^^^^^^^^^^^^

MccsStation
~~~~~~~~~~~

- **On**: The ``MccsStation`` is `On` if **both** the ``SpsStation`` and 
  ``FieldStation`` are `On`.
  
- **Standby**: The ``MccsStation`` is `Standby` if **both** the ``SpsStation`` 
  and ``FieldStation`` are `Standby`.
  
- **Off**: The ``MccsStation`` is `Off` if **both** the ``SpsStation`` and 
  ``FieldStation`` are `Off`.
  
- **Unknown**: The ``MccsStation`` is `Unknown` if the above conditions are 
  not met. This state is common due to mixed power states between the 
  ``SpsStation`` and ``FieldStation``.

MccsController
~~~~~~~~~~~~~~

- **On**: The ``MccsController`` is `On` if **all** controlled ``MccsStation`` 
  devices are `On`.
  
- **Standby**: The ``MccsController`` is `Standby` if **all** controlled 
  ``MccsStation`` devices are `Standby`.
  
- **Off**: The ``MccsController`` is `Off` if **all** controlled 
  ``MccsStation`` devices are `Off`.
  
- **Unknown**: The ``MccsController`` is `Unknown` if any controlled 
  ``MccsStation`` device is `Unknown`. This state is common due to mixed states 
  across stations.

SPSHW Repository
^^^^^^^^^^^^^^^^

SpsStation
~~~~~~~~~~

- **On**: The ``SpsStation`` is `On` if **all** ``MccsTile`` devices are `On` 
  and **all** ``MccsSubrack`` devices are `On`.
  
- **Standby**: The ``SpsStation`` is `Standby` if **all** ``MccsTile`` devices 
  are `Off` and **all** ``MccsSubrack`` devices are `On`.

- **Off**: The ``SpsStation`` is `Off` if **all** ``MccsTile`` devices are 
  `Off` and **all** ``MccsSubrack`` devices are `Off`. This state is currently 
  unimplemented as subracks cannot be powered off.

- **Unknown**: The ``SpsStation`` is `Unknown` if the above conditions are not 
  met. Future work may redefine the `On` state to apply if **any** tiles are 
  on, provided all subracks are on.

MccsTile
~~~~~~~~

- **On/Off**: ``MccsTile`` devices are either `On` or `Off`, depending on the 
  power supplied by the ``MccsSubrack``. They do not have a `Standby` state.
- **Fault**: As ``MccsTile`` devices rely on the ``MccsSubrack`` device to determine
  their state, if the ``MccsSubrack`` responds with a state which the ``MccsTile`` 
  doesn't think is correct, it will go to the `Fault` state.

MccsSubrack
~~~~~~~~~~~

- **On**: ``MccsSubrack`` devices are currently always `On`, but they will 
  support both `On` and `Off` states once PDU control is implemented.

PaSD Repository
^^^^^^^^^^^^^^^

FieldStation
~~~~~~~~~~~~

- **On**: The ``FieldStation`` is `On` if **all** unmasked ports of the 
  ``MccsFNDH`` and ``MccsSmartbox`` are `On`.
  
- **Standby**: The ``FieldStation`` is `Standby` if **all** unmasked ports of 
  the ``MccsFNDH`` are `On` and **all** ports of the ``MccsSmartbox`` are `Off`.
  
- **Off**: The ``FieldStation`` is `Off` if **all** ports of the ``MccsFNDH`` 
  are `Off`.

MccsSmartbox
~~~~~~~~~~~~

- **On**: A ``MccsSmartbox`` is `On` if **any** of its unmasked ports are `On`.
  
- **Standby**: A ``MccsSmartbox`` is `Standby` if **all** ports are `Off` but 
  its FNDH port is `On`.
  
- **Off**: A ``MccsSmartbox`` is `Off` if its FNDH port is `Off`.

MccsFNDH
~~~~~~~~

- **On**: ``MccsFNDH`` is always `On` as there is no control over its upstream 
  power supply.

Error Handling
--------------

Unknown State
^^^^^^^^^^^^^

A device enters the `Unknown` state when it cannot determine the power state of
its hardware or when subservient devices do not report their power states. This
state indicates uncertainty or a communication issue within the system.

Fault State
^^^^^^^^^^^

A device enters the `Fault` state when it detects a conflict between its 
expected power state and the actual power state of its hardware or subservient 
devices. For example, an ``MccsTile`` would enter the `Fault` state if it 
believes it is `On`, but the ``MccsSubrack`` it resides in reports that it is 
`Off`.

Future Considerations
---------------------

- **Revising SpsStation Logic**: There may be future work to redefine the `On`
  state of the ``SpsStation`` such that it is `On` if **any** tiles are `On`, 
  provided all subracks are `On`.

- **Mixed State Handling**: The frequent occurrence of `Unknown` states due to 
  mixed power states between devices may lead to revisions in how these states 
  are managed, particularly for the ``MccsStation`` and ``MccsController`` 
  devices.
