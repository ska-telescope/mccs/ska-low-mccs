On Command
==========

Overview
--------

This document details the On() command within the MCCS, 
covering the three primary repositories in MCCS:
SPSHW, PaSD, and the Main repository.

SPSHW Repository
----------------

Devices
~~~~~~~

- **MccsTile**: Controls the TPM (Tile Processing Module). For more
  detailed information, see `MccsTile Overview
  <https://developer.skao.int/projects/ska-low-mccs-spshw/en/latest/
  reference/tile_brief_overview.html>`_.

- **MccsSubrack**: Manages the subrack containing TPMs. Note: Power control
  for the subrack is currently unimplemented due to the absence of PDU
  devices, which are planned for future development.

- **SpsStation**: The central controller for the ``MccsTile`` and
  ``MccsSubrack`` devices.

Power Management Workflow
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **SpsStation.On() Command**:

   - When the ``On()`` command is executed, the ``SpsStation`` sequentially
     powers on each ``MccsTile`` device.
   - The process is staggered with a few seconds delay between each
     ``MccsTile``. Future enhancements may include dynamic calculation of
     this delay.

2. **MccsTile.On() Command**:

   - The ``MccsTile`` sends a request to the ``MccsSubrack`` to turn on
     power upon receiving the ``On()`` command.

3. **MccsSubrack.On() Command**:

   - Currently, the ``MccsSubrack`` lacks power control due to the absence
     of PDU devices.

Future Development
~~~~~~~~~~~~~~~~~~

- **PDU Integration**: The ``MccsSubrack`` will be updated to include power
  control once the PDU devices are developed and integrated.

PaSD Repository
---------------

Devices
~~~~~~~

- **MccsPasdBus**: A Modbus device that communicates with PaSD hardware,
  including FNDH (Field Node Distribution Hub) and Smartboxes.

- **MccsFNDH**: Manages the FNDH, which provides power to connected
  Smartboxes.

- **MccsSmartbox**: Controls a Smartbox, which provides power to
  connected antennas.

- **FieldStation**: The central controller for the ``MccsFNDH`` and
  ``MccsSmartbox`` devices.

Power Management Workflow
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **FieldStation.On() Command**:

   - The ``FieldStation`` directly commands the ``MccsFNDH`` to power on
     specific ports, enabling power to the relevant Smartboxes.
   - It then commands the ``MccsSmartbox`` devices to power on specific
     ports that have antennas attached, provided those antennas are not
     masked.
   - The ``MccsFNDH`` and ``MccsSmartbox`` devices send the necessary
     commands to the ``MccsPasdBus`` to ensure proper communication with
     the PaSD hardware.

2. **MccsSmartbox.On() Command**:

   - The ``MccsSmartbox`` currently does not have a meaningful ``On()``
     command, as the knowledge of port mappings is managed directly by
     the ``FieldStation``.

3. **MccsFNDH.On() Command**:

   - The ``MccsFNDH`` is considered to be always ON as we do not have control of upstream power supply.

Main Repository
---------------

Devices
~~~~~~~

- **MccsStation**: The top-level controller for both ``SpsStation`` and
  ``FieldStation``.

- **MccsController**: Manages multiple ``MccsStation`` devices, each
  responsible for a different station.

Power Management Workflow
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **MccsStation.On() Command**:

   - The ``MccsStation`` sends the ``On()`` command to the ``SpsStation``
     and ``FieldStation``.
   - It waits for the completion of these commands to ensure that all
     associated hardware components are powered on in the correct sequence.

2. **MccsController.On() Command**:

   - The ``MccsController`` initiates the power-on process by sending the
     ``On()`` command to each ``MccsStation``.
   - The power-on process is coordinated across multiple stations,
     ensuring a smooth startup of the entire MCCS subsystem.

Command Flow
------------

The workflow above in diagrammatic form:

.. uml:: on_command.puml