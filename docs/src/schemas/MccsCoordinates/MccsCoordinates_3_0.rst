==========================================
ska-low-mccs astronomic coordinates schema
==========================================

Schema for MccsSubarray's Allocate command

**********
Properties
**********

* **timestamp** (string): UTC time for begin of drift.

* **reference_frame** (string): Must be one of: ["AltAz", "ICRS", "Galactic"].

* **c1** (number): first coordinate, in degrees. Minimum: 0.0. Maximum: 360.0.

* **c1_rate** (number): Drift rate for first coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

* **c2** (number): second coordinate, in degrees. Minimum: -90.0. Maximum: 90.0.

* **c2_rate** (number): Drift rate for second coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

