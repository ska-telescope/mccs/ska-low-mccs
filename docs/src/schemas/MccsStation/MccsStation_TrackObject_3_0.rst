==========================
Station TrackObject schema
==========================

Schema for Station's TrackObject command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **pointing_type** (string): The type of pointing requested. Must be one of: ["alt_az", "ra_dec"].

* **time_step** (number): the time between each pointing in seconds.

* **scan_time** (number): the total scan time in seconds.

* **reference_time** (string): The reference time to start tracking, format astropy time.

* **station_beam_number** (integer): The station beam number.

* **values** (object): The values provided for either the right ascension and declination or the alitude and azimuth.

  **One of**
    * object

      * **altitude** (number, **required**): the altitude of the target, in degrees.

      * **azimuth** (number, **required**): the azimuth of the target, in degrees.

      * **altitude_rate** (number): the rate of altitude of the target.

      * **azimuth_rate** (number): the rate of  azimuth of the target.

    * object

      * **right_ascension** (number, **required**): the right_ascension of the target, in degrees.

      * **declination** (number, **required**): the declination of the target, in degrees.

      * **right_ascension_rate** (number): the rate of right_ascension of the target, in degrees/second.

      * **declination_rate** (number): the rate of declination of the target, in degrees/second.

