===================
MccsStation Schemas
===================

These schemas are for use with MccsStation commands

.. toctree::
  :caption: MccsStation Schemas
  :maxdepth: 2

  MccsStation_ConfigureSemiStatic_3_0
  MccsStation_GetPointingDelays_3_0
  MccsStation_Scan_3_0
  MccsStation_SetPointingCoordinates
  MccsStation_TrackObject_3_0
