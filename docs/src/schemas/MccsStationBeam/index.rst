=======================
MccsStationBeam Schemas
=======================

These schemas are for use with MccsStationBeam commands

.. toctree::
  :caption: MccsStationBeam Schemas
  :maxdepth: 2

  MccsStationBeam_AssignResources_3_0
  MccsStationBeam_Configure_3_0
