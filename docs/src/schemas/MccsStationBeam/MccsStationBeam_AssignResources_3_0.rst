==================================
StationBeam AssignResources schema
==================================

Schema for StationBeam's AssignResources command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **subarray_id** (integer): The ID of the subarray to which resources are allocated. Minimum: 1. Maximum: 16.

* **subarray_beam_id** (integer): The ID of the subarray beam to which resources are allocated. Minimum: 1. Maximum: 768.

* **station_id** (integer): Minimum: 1. Maximum: 512.

* **aperture_id** (string): Aperture ID, of the form APXXX.YY, XXX=station YY=substation. Must match pattern ``/^AP(?!0{3})\d{3}\.\d{2}$/``.

* **station_trl** (string): TRL of the Tango station device used by the aperture.

* **channel_blocks** (array): SPS channel blocks allocated to this station beam. Length must be at most 48.

  * **Items** (integer): Minimum: 0. Maximum: 47.

* **hardware_beam** (integer): Minimum: 0. Maximum: 47.

* **first_subarray_channel** (integer): First SPS subarray channel ID. Minimum: 0. Maximum: 376.

* **number_of_channels** (integer): The allocated number of SPS channels. Minimum: 8. Maximum: 384.

