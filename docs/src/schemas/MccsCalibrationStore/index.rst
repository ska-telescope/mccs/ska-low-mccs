============================
MccsCalibrationStore Schemas
============================

These schemas are for use with MccsCalibrationStore commands

.. toctree::
  :caption: MccsCalibrationStore Schemas
  :maxdepth: 2

  MccsCalibrationStore_GetSolution
  MccsCalibrationStore_StoreSolution
  MccsCalibrationStore_UpdateSelectionPolicy
