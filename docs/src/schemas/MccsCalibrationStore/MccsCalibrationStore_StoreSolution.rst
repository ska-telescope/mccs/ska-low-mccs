=========================================
MccsCalibrationStore StoreSolution schema
=========================================

Schema for MccsCalibrationStore's StoreSolution command

**********
Properties
**********

* **frequency_channel** (integer): Freqency channel to store a solution for. Minimum: 0. Maximum: 511.

* **outside_temperature** (number): Outside temperature to store a solution for.

* **station_id** (integer): The id of the station to store a solution for. Minimum: 1. Maximum: 512.

* **preferred** (boolean): Mark a solution as preferred.

* **solution** (array): The solution dataset. This is ordered by tpm_idx (i.e 8 values for each tpm_id * 16 + (adc_channel // 2 )).

  * **Items** (number)

* **corrcoeff** (array): List of Pearson product-moment correlation coefficients for each polarisation of the calibrated visibilities and the model.

  * **Items** (number)

* **residual_max** (array): List of residual visibility maximum absolution deviation per polarisation (K). Length must be equal to 4.

  * **Items** (number)

* **residual_std** (array): List of residual visibility standard deviation per polarisation (K). Length must be equal to 4.

  * **Items** (number)

* **xy_phase** (number): Estimated xy-phase error.

* **n_masked_initial** (integer): Initial number of masked antennas.

* **n_masked_final** (integer): Final number of masked antennas.

* **lst** (number): Apparent sidereal time at station.

* **galactic_centre_elevation** (number):  Elevation of the galactic centre.

* **sun_elevation** (number): Elevation of the sun (degrees).

* **sun_adjustment_factor** (number)

* **masked_antennas** (array): Antenna masked as bad. This is currently eep indexed. Length must be between 0 and 256 (inclusive).

  * **Items** (integer)

* **acquisition_time** (number): Time the data was acquired.

* **calibration_path** (string): The path to the correlation matrix file this solution was determined from.

