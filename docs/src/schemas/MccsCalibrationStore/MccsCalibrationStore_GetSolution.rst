=======================================
MccsCalibrationStore GetSolution schema
=======================================

Schema for MccsCalibrationStore's GetSolution command

**********
Properties
**********

* **frequency_channel** (integer): Freqency channel to calibrate for. Minimum: 0. Maximum: 511.

* **outside_temperature** (number): Outside temperature to calibrate for.

* **station_id** (number): the id of the station we want a solution for.

