=============================
MccsSubarray Configure schema
=============================

Schema for MccsSubarray's Allocate command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **transaction_id** (string): Must match pattern ``/[a-z0-9]+-[a-z0-9]+-[0-9]{8}-[0-9]+/``.

* **subarray_beams** (array): Length must be at most 48.

  * **Items** (object)

    * **subarray_beam_id** (integer): The ID of the subarray beam to which resources are allocated. Minimum: 1. Maximum: 768.

    * **update_rate** (number): Minimum: 0.0.

    * **logical_bands** (array, **required**): Length must be at most 48.

      * **Items** (object)

        * **start_channel** (integer, **required**): Minimum: 2. Maximum: 504. Must be a multiple of: 2.

        * **number_of_channels** (integer, **required**): Minimum: 8. Maximum: 384. Must be a multiple of: 8.

    * **apertures** (array): Length must be at most 512.

      * **Items** (object): Station and aperture IDs contributing to the beam.

        * **aperture_id** (string, **required**): Aperture ID, of the form APXXX.YY, XXX=station YY=substation. Must match pattern ``/^AP(?!0{3})\d{3}\.\d{2}$/``.

        * **weighting_key_ref** (string): descriptive ID for the aperture weights in the aperture database.

    * **sky_coordinates** (object): Pointed direction, including drift rate.

      * **timestamp** (string): UTC time for begin of drift.

      * **reference_frame** (string, **required**): Must be one of: ["topocentric", "AltAz", "ICRS", "Galactic"].

      * **target_name** (string): The name of the target.

      * **c1** (number, **required**): first coordinate, RA or azimuth, in degrees. Minimum: 0.0. Maximum: 360.0.

      * **c1_rate** (number): Drift rate for first coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

      * **c2** (number, **required**): second coordinate, dec or elevation, in degrees. Minimum: -90.0. Maximum: 90.0.

      * **c2_rate** (number): Drift rate for second coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

    * **field** (object): Pointed direction, including drift rate.

      * **timestamp** (string): UTC time for begin of drift.

      * **target_name** (string, **required**): The name of the target.

      * **reference_frame** (string, **required**): Must be one of: ["AltAz", "topocentric", "ICRS", "Galactic"].

      * **attrs** (object, **required**): Coordinates and rates of scan.

        * **c1** (number, **required**): first coordinate, RA or azimuth, in degrees. Minimum: 0.0. Maximum: 360.0.

        * **c1_rate** (number): Drift rate for first coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

        * **c2** (number, **required**): second coordinate, dec or elevation, in degrees. Minimum: -90.0. Maximum: 90.0.

        * **c2_rate** (number): Drift rate for second coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

