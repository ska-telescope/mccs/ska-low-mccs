============================
MccsAntenna Configure schema
============================

Used to configure the semi static data of the antennas i.e. (x, y offset)

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **antenna_config** (object): Antenna semi static data.

  * **antennaId** (integer)

  * **xDisplacement** (number)

  * **yDisplacement** (number)

  * **zDisplacement** (number)

* **tile_config** (object): Tile semi static data.

  * **fixed_delays** (array): Fixed delays for the tiles for each antenna.

    * **Items** (number)

  * **antenna_ids** (array): Antenna ids for the tile.

    * **Items** (integer)

