===========================================
MccsStationCalibrator GetCalibration schema
===========================================

Schema for MccsStationCalibrator's GetCalibration command

**********
Properties
**********

* **frequency_channel** (integer): Freqency channel to calibrate for. Minimum: 0. Maximum: 511.

