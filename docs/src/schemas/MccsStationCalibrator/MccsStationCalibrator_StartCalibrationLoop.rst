=================================================
MccsStationCalibrator StartCalibrationLoop schema
=================================================

Schema for MccsStationCalibrator's StartCalibrationLoop command

**********
Properties
**********

* **eep_filebase** (['string', 'null']): Start of EEP filenames before the frequency specification. For X/phi this gives eep_path/eep_filebase<freq>MHz_Xpol_phi.npy.

* **jones_solve** (['boolean', 'null']): Whether to call the polarised solver after initial calibration.

* **adjust_solar_model** (['boolean', 'null']): Whether to adjust the sky model after initial calibration to limit differences between galactic and solar flux scales.

* **masked_antennas** (['array', 'null']): List of antennas which should not be used for calibration.

  * **Items** (number)

* **skymodel** (['string', 'null']): Either a model visibility xarray to calibrate against or the name of a sky model type to generate. Supported sky models are `gsm` and `sun`. The `gsm` model combines components of the PyGDSM GlobalSkyModel16 Galactic sky model that are above the horizon with a point-source solar model, while sun uses only the point-source solar model.

* **min_uv** (['number', 'null']): Minimum baseline length used in calibration (in metres). The default value is zero.

* **refant** (['integer', 'null']): Antenna to use for phase referencing. Should be one of the zero-based indices used in the visibility dataset. Default is 1. Both polarisations are phase referenced against the X polarisation of refant, as a non-zero XY-phase is expected and solved for. Minimum: 0.

* **ignore_eeps** (['boolean', 'null']): If True, EEP files will not be imported and station beam patterns and rotations will be ignored when modelling visibilities. Sky model components will be added with equal amplitude in XX and YY and zeros amplitude in XY and YX. Default is False.

* **gain_threshold** (['number', 'null']): Fractional threshold used to reject antennas with low gains. Any antenna with either an X or Y gain amplitude less than the median is flagged as bad. The default values is 0.25.

* **station_config_path** (['array']): Length must be equal to 2.

  * **Items** (string): Must match pattern ``/^[^\s]+$/``.

* **nside** (['integer', 'null']): Healpix nside at which to generate sky model. Default of 32 corresponds to approximately 110 arcminutes. This is okay at low frequencies but may need to be increased at higher frequencies. Minimum: 1. Default: 32.

* **niter** (['integer', 'null']): Number of iterations for solver. Minimum: 1. Default: 200.

* **first_channel** (['integer', 'null']): The lowest channel in the loop. Minimum: 0. Maximum: 511.

* **last_channel** (['integer', 'null']): The final channel to use in loop. Minimum: 0. Maximum: 511.

* **daq_config** (['object', 'null']): Any overrides to pass to DAQ for configuration purposes. Can contain additional properties.

