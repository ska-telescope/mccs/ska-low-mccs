==============================
MccsController Allocate schema
==============================

Schema for MccsController's Allocate command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **subarray_id** (integer): The ID of the subarray to which resources are allocated. Minimum: 1. Maximum: 16.

* **subarray_beams** (array): Length must be at most 48.

  * **Items** (object)

    * **subarray_beam_id** (integer, **required**): The ID of the subarray beam to which resources are allocated. Minimum: 1. Maximum: 768.

    * **apertures** (array, **required**): Length must be at most 512.

      * **Items** (object): Station and aperture IDs contributing to the beam.

        * **station_id** (integer, **required**): Minimum: 1. Maximum: 512.

        * **aperture_id** (string, **required**): Aperture ID, of the form APXXX.YY, XXX=station YY=substation. Must match pattern ``/^AP(?!0{3})\d{3}\.\d{2}$/``.

    * **number_of_channels** (integer, **required**): The allocated number of SPS channels. Minimum: 8. Maximum: 384. Must be a multiple of: 8.

