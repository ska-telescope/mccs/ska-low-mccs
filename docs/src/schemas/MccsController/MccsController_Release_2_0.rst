=============================
MccsController Release schema
=============================

Schema for MccsController's Release command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **subarray_id** (integer): The ID of the subarray to which resources are allocated. Minimum: 1. Maximum: 16.

* **release_all** (boolean): If True, release resources for all subarrays.

