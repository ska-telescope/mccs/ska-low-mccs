======================
MccsController Schemas
======================

These schemas are for use with MccsController commands

.. toctree::
  :caption: MccsController Schemas
  :maxdepth: 2

  MccsController_Allocate_3_0
  MccsController_Allocate_lax
  MccsController_Allocate_strict
  MccsController_Release_2_0
