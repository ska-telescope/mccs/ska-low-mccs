==================================
MccsController lax Allocate schema
==================================

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **subarray_id** (integer): Minimum: 1. Maximum: 16.

* **stations** (array): Length must be between 1 and 512 (inclusive).

  * **Items** (string)

