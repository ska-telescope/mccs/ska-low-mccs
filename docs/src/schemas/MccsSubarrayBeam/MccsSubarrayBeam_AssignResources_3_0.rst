===================================
SubarrayBeam AssignResources schema
===================================

Schema for SubarrayBeam's AssignResources command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **subarray_id** (integer): The ID of the subarray to which resources are allocated. Minimum: 1. Maximum: 16.

* **subarray_beam_id** (integer): The ID of the subarray beam to which resources are allocated. Minimum: 1. Maximum: 768.

* **apertures** (array): Length must be at most 512.

  * **Items** (object): Station and aperture IDs contributing to the beam.

    * **station_id** (integer, **required**): Minimum: 1. Maximum: 512.

    * **aperture_id** (string, **required**): Aperture ID, of the form APXXX.YY, XXX=station YY=substation. Must match pattern ``/^AP(?!0{3})\d{3}\.\d{2}$/``.

    * **station_trl** (string): TRL of the Tango station device used by the aperture.

    * **station_beam_trl** (string, **required**): TRL of the Tango station beam device allocated to the aperture.

    * **channel_blocks** (array, **required**): Length must be at most 48.

      * **Items** (integer): Minimum: 0. Maximum: 47.

    * **hardware_beam** (integer, **required**): Minimum: 0. Maximum: 47.

* **first_subarray_channel** (integer): First SPS subarray channel ID. Minimum: 0. Maximum: 376. Must be a multiple of: 8.

* **number_of_channels** (integer): The allocated number of SPS channels. Minimum: 8. Maximum: 384. Must be a multiple of: 8.

