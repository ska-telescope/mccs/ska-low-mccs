========================
MccsSubarrayBeam Schemas
========================

These schemas are for use with MccsSubarrayBeam commands

.. toctree::
  :caption: MccsSubarrayBeam Schemas
  :maxdepth: 2

  MccsSubarrayBeam_AssignResources_3_0
  MccsSubarrayBeam_Configure_3_0
