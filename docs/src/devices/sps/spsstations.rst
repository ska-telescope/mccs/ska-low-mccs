Spsstations
===========

Below is an example spsstations configuration.

.. code-block:: yaml

  ci-1:
    low-mccs/spsstation/ci-1:
        antenna_config_uri: []
        daq_trl: low-mccs/calibration-daq/ci-1
        logging_level_default: 5
        sdn_first_interface: 10.0.0.128/16
        station_id: 1
        subracks:
        - low-mccs/subrack/ci-1-sr1
        tpms:
        - low-mccs/tile/ci-1-tpm01
