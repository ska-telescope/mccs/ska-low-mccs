Simulators
==========

Below is an example simulators configuration.

.. code-block:: yaml

  subracks:
    ci-1-sr1:
        srmb_host: subrack-simulator-ci-1-sr1
        srmb_port: 8081
