Configservers
=============

Below is an example configServers configuration.

.. code-block:: yaml

  array:
    host: pasd-configuration-service
    port: 8081
