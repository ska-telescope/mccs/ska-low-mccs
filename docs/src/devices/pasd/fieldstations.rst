Fieldstations
=============

Below is an example fieldstations configuration.

.. code-block:: yaml

  ci-1:
    low-mccs/fieldstation/ci-1:
        configuration_host: pasd-configuration-service
        configuration_port: 8081
        fndh_name: low-mccs/fndh/ci-1
        logging_level_default: 5
        smartbox_names:
        - low-mccs/smartbox/ci-1-sb01
