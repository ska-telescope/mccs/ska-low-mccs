Simulators
==========

Below is an example simulators configuration.

.. code-block:: yaml

  pasdbuses:
    ci-1:
        host: pasd-simulator-ci-1
        port: 9502
