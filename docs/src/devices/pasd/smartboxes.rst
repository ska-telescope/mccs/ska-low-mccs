Smartboxes
==========

Below is an example smartboxes configuration.

.. code-block:: yaml

  ci-1-sb01:
    low-mccs/smartbox/ci-1-sb01:
        field_station_name: low-mccs/fieldstation/ci-1
        fndh_name: low-mccs/fndh/ci-1
        fndh_port: 1
        logging_level_default: 5
        pasdbus_name: low-mccs/pasdbus/ci-1
        smartbox_number: 1
