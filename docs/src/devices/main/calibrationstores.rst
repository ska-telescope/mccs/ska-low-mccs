Calibrationstores
=================

Below is an example calibrationstores configuration.

.. code-block:: yaml

  ci-1:
    low-mccs/calibrationstore/ci-1:
        logging_level_default: 5
