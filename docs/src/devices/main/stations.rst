Stations
========

Below is an example stations configuration.

.. code-block:: yaml

  station-ci-1:
    low-mccs/station/ci-1:
        antenna_ids:
        - 244
        - 189
        antenna_trls:
        - low-mccs/antenna/ci-1-sb01-01
        - low-mccs/antenna/ci-1-sb01-02
        antenna_xs:
        - -5.422
        - -3.975
        antenna_ys:
        - 17.79
        - 15.948
        antenna_zs:
        - 0.008
        - 0.016
        field_station_trl: low-mccs/fieldstation/ci-1
        logging_level_default: 5
        ref_height: 1
        ref_latitude: -1
        ref_longitude: 1
        sps_station_trl: low-mccs/spsstation/ci-1
        station_calibrator_trl: low-mccs/stationcalibrator/ci-1
        station_id: 1
