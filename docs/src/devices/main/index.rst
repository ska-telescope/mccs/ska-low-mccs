Example Configuration of MCCS devices
=====================================
.. toctree::
  :caption: MCCS device configuration
  :maxdepth: 1

  antennas
  calibrationstores
  controllers
  solvers
  stationbeams
  stationcalibrators
  stations
  subarraybeams
  subarrays