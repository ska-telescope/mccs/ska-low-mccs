Antennas
========

Below is an example antennas configuration.

.. code-block:: yaml

  antennas-ci-1:
    low-mccs/antenna/ci-1-sb01-01:
        antenna_id: 244
        field_station_trl: low-mccs/fieldstation/ci-1
        logging_level_default: 5
        tile_trl: low-mccs/tile/ci-1-tpm01
        tile_x_channel: 31
        tile_y_channel: 30
        x_displacement: -5.422
        y_displacement: 17.79
        z_displacement: 0.008
