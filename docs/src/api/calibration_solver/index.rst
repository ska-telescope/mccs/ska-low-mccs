==================
Calibration solver
==================

The CalibrationSolver will ingest .hdf5 files and solve to produce data products 
that may be stored.


.. toctree::
    :maxdepth: 2

      Calibration solver component manager<solver_component_manager>
      Calibration solver device<solver_device>
      Calibration solution product<calibration_solution_product>