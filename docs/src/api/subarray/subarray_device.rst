
===============
Subarray Device
===============

.. uml:: subarray_device_class_diagram.uml

.. automodule:: ska_low_mccs.subarray.subarray_device
   :members:
