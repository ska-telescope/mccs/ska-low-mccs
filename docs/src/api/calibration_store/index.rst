============================
Calibration store subpackage
============================

The CalibrationStore is the interface from the tango controls
to the calibration data products.


.. toctree::
    :maxdepth: 2

      Calibration store component manager<calibration_store_component_manager>
      Calibration store device<calibration_store_device>
      Calibration store health model<calibration_store_health_model>
      Calibration store database connection<calibration_store_database_connection>
      Selection Policy<selection_policy/index>