
====================
Subarray Beam Device
====================

.. uml:: subarray_beam_device_class_diagram.uml

.. automodule:: ska_low_mccs.subarray_beam.subarray_beam_device
   :members:
