=====================
Controller subpackage
=====================

.. automodule:: ska_low_mccs.controller


.. toctree::

  Controller CLI<controller_cli>
  Controller component manager<controller_component_manager>
  Controller device<controller_device>
  Controller health model<controller_health_model>
  Controller health rules<controller_health_rules>
  Controller resource manager<controller_resource_manager>
