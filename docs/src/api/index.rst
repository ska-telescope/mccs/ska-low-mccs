===
API
===

See this confluence page to see the schemas and examples for all TANGO commands for MCCS
https://confluence.skatelescope.org/display/SE/MCCS+1703%3A+Updated+TM+-%3E+MCCS+schemas

.. toctree::
  :caption: Device subpackages
  :maxdepth: 2

  Antenna<antenna/index>
  Controller<controller/index>
  Station<station/index>
  Station beam<station_beam/index>
  Subarray<subarray/index>
  Subarray beam<subarray_beam/index>
  Transient buffer<transient_buffer/index>
  Station calibrator<station_calibrator/index>
  Calibration store<calibration_store/index>
  Calibration solver<calibration_solver/index>
