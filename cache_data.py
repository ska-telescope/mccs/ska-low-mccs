# The following code downloads data which needs to be cached before we deploy, else
# we get hit by CORBA errors, or, more annoyingly, our devices take over a minute to
# actually be ready after Kubernetes thinks they are, as they're downloading 0.5Gb of
# data.

from astropy.time.core import Time

# This will eventually have a dependency on pygdsm. But let's not pin ourslves to a
# specific import, and instead import the component manager, which will get all the
# dependencies for us.
from ska_low_mccs.calibration_solver import StationCalibrationSolverComponentManager

Time.now().ut1
