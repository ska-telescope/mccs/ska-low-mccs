#!/bin/bash

# Set variables
HELMFILE_ENV="stfc-ci"
BUILD_DIR="helmfile.d/build"

# Step 1: Run helmfile write-values
helmfile -e "$HELMFILE_ENV" write-values --skip-deps --output-file-template="$BUILD_DIR/{{ .Release.Name}}.yaml"

# Check if helmfile was successful
if [ $? -ne 0 ]; then
    echo "Helmfile command failed. Exiting."
    exit 1
fi

# Step 2: Run the Python script
python docs/scripts/document_devices.py

# Check if Python script was successful
if [ $? -ne 0 ]; then
    echo "Python script failed. Exiting."
    exit 1
fi

# Step 3: Delete files in the .helmfile/build directory
if [ -d "$BUILD_DIR" ]; then
    rm -f "$BUILD_DIR"/*.yaml
    echo "Deleted files in $BUILD_DIR"
else
    echo "Directory $BUILD_DIR does not exist."
fi

echo "Script completed successfully."
