import time

import tango

db = tango.Database()

all_devices_strings = db.get_device_exported("low-mccs/*")

for device_str in all_devices_strings:
    device = tango.DeviceProxy(device_str)
    try:
        if device.adminMode != 0:
            device.adminMode = 0
    except Exception as e:
        # If a device is unreachable/not ready.
        print(f"{device} - Caught exception: {e}")
        continue
